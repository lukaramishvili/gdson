
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
	var context = this, args = arguments;
	var later = function() {
	    timeout = null;
	    if (!immediate) func.apply(context, args);
	};
	var callNow = immediate && !timeout;
	clearTimeout(timeout);
	timeout = setTimeout(later, wait);
	if (callNow) func.apply(context, args);
    };
};

function setLeavePageNotice(on, message){
    message = message || "Are you sure you want to leave this page?";
    if(on){
	$(window).unbind('beforeunload');//avoid multiple binds
	$(window).on('beforeunload', function(){
	    return message;
	});
    } else {
	$(window).unbind('beforeunload');
    }
};

function isBrowser(browser){
    return navigator.userAgent.toLowerCase().indexOf(browser) > -1;
};

function isFirefox(){
    return isBrowser('firefox');
};

function flashSupported(){
    return swfobject.getFlashPlayerVersion().major > 0;
};

function isiOS(){
    return (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPod/i));
};

function showNotificationMoreButton(data){
    $("#notification-more-div a.notification-link").attr("href", data.url);
    $("#notification-more-div .popup-content").css({
	"background" : "transparent url(" + data.image + ") center center no-repeat",
	"background-size" : "100% auto"
    });
    $("#notification-more-div").show();
    $("html,body").css("overflow", "hidden");
};

function videoScrollAutoload() {
    $(window).scroll(function() {
        var postVideoContainers    = $('.post-video-container')
            , scrolledWindowHeight = $(window).scrollTop()
            , windowHeight         = $(window).height();

        postVideoContainers.each(function(index, value) {
            var $value          = $(value)
                , heightFromTop = $value.offset().top
                , height        = $value.height()
                , id            = $value.find('a').attr('id');

            if (!$value.hasClass('autoplayed') && height > 0 && scrolledWindowHeight + windowHeight / 2 > heightFromTop && scrolledWindowHeight + windowHeight / 2 < heightFromTop + height) {
                $value.find('.slide-play').click();
                $value.addClass('autoplayed');
            } else if ($value.hasClass('autoplayed') && !$value.hasClass('autoplay-canceled') && (scrolledWindowHeight + windowHeight / 2 < heightFromTop || scrolledWindowHeight + windowHeight / 2 > heightFromTop + height)) {
		//GDSON-821 if the scrolling was too fast, then players wouldnt have enough time to load and would display buffering and resume icons together, so don't try to pause players until they're playing
		var waitForPlayingInterval = setInterval(function(){
		    var currentPlayerState = $f($value.find('.slide-video').attr('id')).getState();
		    if(currentPlayerState == 3){
			clearInterval(waitForPlayingInterval);
			$value.addClass('autoplay-canceled');
			$f(id).pause();
		    }
		}, 200);
            }
        });
    });
}

if (window.location.hash && window.location.hash == '#_=_') {
    //fb callback url via API doesn't work (we get this hash instead), so do the 
    //same we're trying to do in app/Http/Middleware/RedirectIfAuthenticated.php
    //change url in both files if we should redirect somewhere else after fb login
    if(window.location.pathname == "/oauth-canceled"){
	window.location.hash = '';
	window.location = '/';
    } else {
	//window.location.hash = '';
	//window.location = '/';
	history.pushState({}, document.title, document.location.href.split("#")[0]);
    }
}

jQuery(document).ready(function($){

    if(!$("body").hasClass("mobile")){//if(!isiOS()) {
	videoScrollAutoload();
    }

    $(".popup .popup-close").click(function(){
	$(this).parents(".popup").hide();
    });
    $(".popup .popup-shadow").click(function(e){
	if($(e.target).hasClass('popup-shadow')){
	    $(this).parents(".popup").hide();
	}
    });


    /* before opening the menu begins */
    $(".navbar-collapse").on("show.bs.collapse", function(){
	$(".header-main").addClass("menu-expanded");
    });
    /* after closing the menu finishes */
    $(".navbar-collapse").on("hidden.bs.collapse", function(){
	$(".header-main").removeClass("menu-expanded");
    });
    /* set active class on current page menu item */
    $(".header-main nav.nav-main ul.nav li a").each(function(){
	//skip "home" link
	if($(this).is(".header-main nav.nav-main ul.nav > li > a")){ return; }
	//
	var href = $(this).attr("href");
	if(href == document.location.pathname || href == document.location.href){
	    $(this).addClass('active');
	}
    });
    /* opening/closing submenus on responsive */
    $(".header-main nav.nav-main ul.nav > li > a").click(function(e){
	/* on mobile, show responsive submenus; on tablets, show desktop submenus */
	/* but some androids have bigger resolution than 1024, so cover that */
	if($("body").hasClass("mobile") || $(window).width() < 1025){//if($(window).width() < 1025){
	    var $submenu = $(this).parent().find("> ul");
	    if($submenu.length > 0){
		//only on desktop menu, allow only one open submenu at a time
		if($(window).width() > 767){
		    $(".header-main nav.nav-main ul.nav li > ul").not($submenu).hide();
		}
		if($submenu.is(":visible")){
		    $submenu.hide();console.log('hiding');
		} else {
		    $submenu.show();console.log('showing');
		}
		//e.preventDefault();
	    }
	}
	//prevent clicking on parent items other than "home"
	if( ! $(this).parent("li").is(":first-child")){
	    e.preventDefault();
	}
    });


    $(".slider .slide .slide-social-btn").click(function(){
	$(this).parents(".slide-buttons")
	    .find(".slide-playlist-btn-list")
	    .hide();
    });

    $(".slider .slide .slide-playlist-btn").click(function(){
	if($("body").hasClass("authed")){
	    $(this).parents(".slide-buttons").find(".slide-social-btn .social")
		.removeClass('active');
	    $(this).parents(".slide-buttons")
		.find(".slide-playlist-btn-list")
		.toggle(0);
	} else {
	    $("#login-div").show();
	}
    });/* $(".slider .slide .slide-playlist-btn").click() */


    $(".slider .slide .slide-playlist-btn-list-item").click(function(){
	var playlist_id = $(this).attr('data-playlist-id');
	var video_id = $(this).attr('data-video-id');
	var on = $(this).hasClass('selected') ? 0 : 1;
	$(this).toggleClass('selected');
	$.ajax({
	    url: "/user/setvideoinplaylist/"+playlist_id+"/"+video_id+"/"+on,
	    type: "POST",
	    dataType: "json",
	    success: function(data){
	    }
	});

    });/* $(".slider .slide .slide-playlist-btn-list-item").click() */




    function toggleFpHeadingBigItems(){
	if($(".fp-big-items-container .fp-big-items").hasClass("hidden")){
	    $(".fp-heading-big-items button").addClass("open");
	    $(".fp-big-items-container .fp-big-items").removeClass("hidden");
	} else {
	    $(".fp-heading-big-items button").removeClass("open");
	    $(".fp-big-items-container .fp-big-items").addClass("hidden");
	}
    }
    $(".fp-heading-big-items button").each(function(i, el){
	//uncomment to leave accordion functionality only on mobiles
	//in that case, then also uncomment button/img styles in _media.css, at the
	//|->end of body:not(.mobile) {} AND also the if block after this each()
	//if(!$("body").hasClass("mobile")){ return; }
	//var $target = $(this).parents(".fp-big-items-container").find(".fp-big-items");
	$(el).click(function(){
	    if($(window).width() < 768){
		toggleFpHeadingBigItems();
	    }
	});
    });
    //if($("body").hasClass("mobile")){
    if($(window).width() < 768){
	toggleFpHeadingBigItems();
    }
    $(window).resize(function(){
	if($("body").hasClass("desktop") && $(window).width() < 768 && $(".fp-heading-big-items .toggle-fp-big-items").hasClass("open")){
	    toggleFpHeadingBigItems();
	}
	if($("body").hasClass("desktop") && $(window).width() >= 768 && !$(".fp-heading-big-items .toggle-fp-big-items").hasClass("open")){
	    toggleFpHeadingBigItems();
	}
    });


    $('.fp-big-items').owlCarousel({
	loop: false,
	dots: true,
	nav: false,
	margin: 15,
	items: 3,
	responsive : {
	    // breakpoint from 0 up
	    0 : {
		items : 1,
		margin: 0,
		//we're disabling owl on <767 with css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 768 up
	    768 : {
		items : 2,
	    },
	    // breakpoint from 992 up
	    992 : {
	    }
	}
    });/*$('.fp-big-items').owlCarousel() */

    $('.fp-list-items:not(.fp-last-added-carousel-items)').owlCarousel({
	loop: false,
	dots: true,
	nav: false,
	margin: 16,
	items: 3,
	responsive : {
	    // breakpoint from 0 up
	    0 : {
		items : 2,
		//we're disabling owl on <767 (and <992/1024) using css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 768 up
	    768 : {
		items : 3,
		//we're disabling owl on <767 (and <992/1024) using css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 992 up
	    992 : {
	    }
	}
    });/*$('.fp-list-items:not(.fp-last-added-carousel-items)').owlCarousel() */

    $('.fp-last-added-carousel-items').owlCarousel({
	loop: false,
	dots: true,
	nav: false,
	margin: 16,
	items: 3,
	responsive : {
	    // breakpoint from 0 up
	    0 : {
		items : 1,
		//we're disabling owl on <767 (and <992/1024) using css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 768 up
	    768 : {
		items : 3,
		//we're disabling owl on <767 (and <992/1024) using css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 992 up
	    992 : {
	    }
	}
    });/*$('.fp-last-added-carousel-items').owlCarousel() */


    $('.episode-items').owlCarousel({
	loop: false,
	dots: true,
	nav: false,
	margin: 15,
	items: 6,
	slideBy: 6,
	responsive : {
	    // breakpoint from 0 up
	    0 : {
		items : 2,
	    },
	    // breakpoint from 768 up
	    768 : {
		items : 4,
	    },
	    // breakpoint from 992 up
	    992 : {
	    }
	}
    });/*$('.episode-items').owlCarousel() */


    $('.profile-just-added-items').owlCarousel({
	loop: false,
	dots: true,
	nav: false,
	margin: 15,
	items: 3,
	responsive : {
	    // breakpoint from 0 up
	    0 : {
		items : 1,
		margin: 0,
		//we're disabling owl on <767 with css (0:false doesn't work),
		//so disable mouse drag to avoid user dragging items out of view
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false
	    },
	    // breakpoint from 768 up
	    768 : {
		items : 2,
	    },
	    // breakpoint from 992 up
	    992 : {
	    }
	}
    });/*$('.profile-just-added-items').owlCarousel() */


    window.initProfilePlaylistCarousel = function(el){
	$(el).owlCarousel({
	    loop: false,
	    dots: true,
	    nav: false,
	    margin: 15,
	    items: 3,
	    responsive : {
		// breakpoint from 0 up
		0 : {
		    items : 1,
		    margin: 0,
		    //to disable owl on <767 with css (0:false doesn't work), disable
		    //|->mouse drag to avoid user dragging items out of view. also
		    //|->set margin to 0 (if not disabling,set margin 15 and others
		    //|->to true)
		    dots: false,
		    nav: false,
		    touchDrag: false,
		    mouseDrag: false
		},
		// breakpoint from 768 up
		768 : {
		    items : 2,
		},
		// breakpoint from 992 up
		992 : {
		}
	    }
	});/*$('.profile-playlist-items').owlCarousel() */
    };
    //reinitializes profile playlist owl carousel (e.g. when an item is removed, etc)
    //call with an element of class ".profile-playlist-items" as an argument
    window.reinitProfilePlaylistCarousel = function(items_el){
	var $items_el = $(items_el);
	//var $container = $items_el.parents(".profile-playlist");
	var $holder = $("<div></div");
	$items_el.find(".profile-playlist-item").each(function(i, it){
	    $(it).appendTo($holder);
	});
	$items_el.removeClass("owl-loaded owl-drag").removeData("owl.carousel");
	$items_el.find("*").remove();
	$holder.find(".profile-playlist-item").each(function(i, it){
	    $(it).appendTo($items_el);
	});
	initProfilePlaylistCarousel(items_el);
    };

    $('.profile-playlist-items').each(function(i,el){
	initProfilePlaylistCarousel(el);
    });/*$('.profile-playlist-items').each() */


    $("#fp-expand-last-added-btn").click(function(){
	var $div = $(".fp-expanding-last-added-items");
	var expanded = parseInt($div.data('expanded'));
	expanded += 6;
	$div.data('expanded', expanded);
	$div.find(".videos-list-item-container").each(function(i, el){
	    if(i < expanded){ $(el).show(); }
	});
	if(expanded >= $div.find(".videos-list-item-container").length){
	    $(this).hide();
	}
    }).click();

    
    $(".profile-add-playlist-btn").click(function(e){
	var $div = $(this).parents(".profile-add-playlist-div");
	$div.find(".profile-add-playlist-form").toggle();
    });
    $(".profile-add-playlist-submit").click(function(e){
	var $div = $(this).parents(".profile-add-playlist-div");
	var playlistName = $div.find(".profile-add-playlist-input").val();
	if(new RegExp("^\\s*$", "gi").test(playlistName)){
	    alert("გთხოვთ შეიყვანოთ სიის სახელი!");
	    return;
	}
	$.ajax({
	    url: "/user/addplaylist",
	    type: "POST",
	    dataType: "json",
	    data: {
		name: playlistName
	    },
	    error: function(){
		alert("მოხდა შეცდომა.");
	    },
	    success: function(data){
		alert(data.result);
		document.location.reload();
	    }
	});
    });


    $(window).scroll(function(){
	/* start fixed header code - commented because it's always fixed now - GDSON-578 */
	/*
	var fResponsive = $(window).width() > 768;//responsive or not
	var hHeader = $(".header-main").height() + parseInt($(".header-main").css('padding-top'));//header height
	var hMenu = $('nav.nav-main').height();
	var jumpPoint = fResponsive ? hHeader : hHeader + hMenu;
	if($(window).scrollTop() > jumpPoint){
	    $(".header-main").addClass('fixed-header');
	} else {
	    $(".header-main").removeClass('fixed-header');
	}
	*/
	/* end fixed header code */
    });

    $(window).resize(function(){
	$('.post').each(function(i, el){
	    $post = $(el);
	    post_text_inner = $post.find(".post-text-inner")[0];
	    if(post_text_inner.scrollHeight > post_text_inner.offsetHeight){
		$post.find(".post-more-btn").show();
	    } else { $post.find(".post-more-btn").hide(); }
	});
    }).trigger('resize');
    $('.post .post-text .post-more-btn').click(function(){
	$(this).parents('.post').toggleClass('expanded');
    });

    //make all HTML5 videos fullscreen (we only have HTML5 videos on mobile devices)
    $("video").on("playing", function(x){
	if(this.webkitEnterFullscreen){
	    this.webkitEnterFullscreen();
	}
    });

    
});/* document.ready */
