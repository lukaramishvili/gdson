<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html itemscope itemtype="http://schema.org/Article" lang="ka"> <!--<![endif]-->
  <head>
    <title>GDSON</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- add a post var _token containing this token when doing AJAX calls -->
    <meta name="csrf-token" content="{{{ Session::token() }}}">
    <link rel="shortcut icon" href="{{ url('/favicon.ico') }}">

    <meta property="fb:app_id" content="{{ Config::get('services.facebook.client_id') }}" />


    <meta property="og:site_name" content="GDSON" />
    @section('social-meta-tags')
    <meta property="og:title" content="GDSON" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="პირველი სრულად ქართული ონლაინ კინოთეატრი" />
    <meta property="og:image" content="{{ asset('img/share-cover.jpg') }}" />

    <meta itemprop="name" content="GDSON">
    <meta itemprop="description" content="პირველი სრულად ქართული ონლაინ კინოთეატრი">
    <meta itemprop="image" content="{{ asset('img/share_cover.jpg') }}">
    @show


  <!-- Global CSS -->
  <!-- Plugins CSS -->
  <link rel="stylesheet" href="{{ asset('/css/vendor.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}?v={{ filemtime(public_path().'/css/app.css') }}">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<!-- google analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '{{ Config::get('services.ganalytics.account_id') }}', 'auto');
  ga('send', 'pageview');
</script>

  <style type="text/css">

      @section('style-head')
      @show

  </style>

      @section('head-extras')
      @show

      <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class=" before-aspect-ratio @if(App\Helper::isMobile()) mobile @else desktop @endif @if(Auth::check()) authed @endif lang-{{ 'geo' }} @if(isset($currentpage)) {{ $currentpage }}-page @endif ">
  <div id="fb-root"></div>
  <script>
    window.fbAppId = '{{ Config::get('services.facebook.client_id') }}';
    window.fbAsyncInit = function() {
	FB.init({
	    appId      : window.fbAppId,
	    xfbml      : true,
	    version    : 'v2.4'
	});

	FB.Canvas.setAutoGrow();
    };

    (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>




  {{-- GDSON-578 fixed header --}}
<header class="header-main fixed-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-1 col-md-2 col-sm-3 col-xs-4 logo">
	<a href="{{ url('/') }}">
	  <img src="{{ asset('img/logo.svg') }}" />
	</a>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 pull-right lang-switcher">
	<ul>
	  <li><a href="{{ url('/ru') }}">RUS</a></li>
	  <li><a href="{{ url('/ge') }}">GEO</a></li>
	  <li><a href="{{ url('/en') }}" class="active">EN</a></li>
	</ul>
      </div>
      <div class="col-md-4 col-sm-5 col-xs-5 pull-right toolbox">
	<a href="{{ url('/user/profile') }}" class="btn-account">
	  @if(Auth::check())
	  <span class="btn-settings-icon"></span>
          <span class="btn-account-avatar">
	    <img src="{{ Auth::user()->avatar }}" />
	  </span>
	  <span class="btn-account-username">
	    {{ Auth::user()->firstname.' '.Auth::user()->lastname }}
          </span>
	  @else
	  <span class="btn-account-icon"></span>
	  @endif
	</a>
	<a href="{{ url('/search') }}" class="btn-search"></a>
      </div>
    </div>
  </div>
  <nav class="nav-main">
    <div class="container">
      <div class="row">
	<div class="col-md-12">



	  <div class="navbar-header">
	    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
	      <span class="sr-only">Toggle Navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	  </div>
	  <div class="navbar-collapse collapse" id="navbar-collapse">
	    <ul class="nav navbar-nav">
	      @foreach(App\Menu::rootLevelItems() as $mm_item)
	      <li>
            <a href="{{ $mm_item->getUrl() }}" class=" ">{{ $mm_item->getTitle("geo") }}</a>
            @if($mm_item->children()->count() > 0)
              <ul>
              @foreach($mm_item->children() as $mm_subitem)
                <li>
                  <a href="{{ $mm_subitem->getUrl() }}" class=" ">{{ $mm_subitem->getTitle("geo") }}</a>
                </li>
              @endforeach
              </ul>
            @endif
            </li>
	      @endforeach

	      {{--
	      <li><a href="{{ url('/') }}">მთავარი</a></li>
	      @foreach(App\Genre::all() as $mm_genre)
	      <li>
            <a href="{{ $mm_genre->getUrl() }}" class=" @if(isset($activemenu) && $activemenu=='genre-'.$mm_genre->id) active @endif ">{{ $mm_genre->getTitle("geo") }}</a>
            @if($mm_genre->shows->count() > 0)
              <ul>
              @foreach($mm_genre->shows as $mm_show)
                <li>
                  <a href="{{ $mm_show->getUrl() }}" class=" @if(isset($activemenu) && $activemenu=='show-'.$mm_show->id) active @endif ">{{ $mm_show->getTitle("geo") }}</a>
                </li>
              @endforeach
              </ul>
            @endif
            </li>
	      @endforeach
	      --}}
	      
	      {{--
	      <li><a href="{{ url('/about') }}" class=" @if(isset($activemenu) && $activemenu=='about') active @endif ">GDSON-ის შესახებ</a></li>
	      <li><a href="{{ url('/faq') }}" class=" @if(isset($activemenu) && $activemenu=='faq') active @endif ">კითხვა-პასუხი</a></li>
	      <li><a href="{{ url('/contact') }}" class=" @if(isset($activemenu) && $activemenu=='contact') active @endif ">კონტაქტი</a></li>
	      --}}
	    </ul>
	  </div>



	</div>
      </div>
    </div>
  </nav>
</header>


<section id="main">
  @section('content')
  default content here
  @show
</section>

<footer class="footer-main">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-9 col-xs-12">
	<nav class="nav-footer">
	  <ul class="nav">
	    <li><a href="{{ asset('/') }}">მთავარი</a></li>
	    <li><a href="{{ asset('/about') }}">GDSON-ის შესახებ</a></li>
	    <li><a href="{{ asset('/faq') }}">კითხვა-პასუხი</a></li>
	    <li><a href="{{ asset('/contact') }}">კონტაქტი</a></li>
	  </ul>
	</nav>
      </div>
      <div class="col-md-4 col-sm-3 col-xs-12 pull-right footer-social">
	<a href="https://www.facebook.com/gdsonofficial" target="_blank" class="footer-facebook"></a>
	<a href="https://twitter.com" target="_blank" class="footer-twitter"></a>
      </div>
    </div>
  </div>
</footer>



@include('user.login')
@include('user.settings')
@include('fbnotification.more-popup')





  <link rel="stylesheet" href="//releases.flowplayer.org/6.0.3/skin/functional.css">

  <!-- Javascript -->
  <script src="{{ asset('/js/vendor.js') }}"></script>
  <!-- integrated in vendor.js -->
  <!--<script src="//releases.flowplayer.org/6.0.3/flowplayer.min.js"></script>-->
  <script src="{{ asset('/js/main.js') }}?v={{ filemtime(public_path().'/js/main.js') }}"></script>

  <!-- integrated in vendor.js -->
  <!--<script src="{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer-3.2.13.min.js"></script>-->

  <script type="text/javascript">



    $(".btn-account").click(function(e){
        @if(Auth::check())
	    //if the settings icon has been clicked or we're already on profile page
            if($(e.target).hasClass("btn-settings-icon") || $(".profile-div").length > 0){
                $("#settings-div").show();
                return false;
	    } else {
		//continue to profile page
	    }
	/*
            {{-- this var is set in App\Http\Controllers\UserController::getProfile, can be set from elsewhere too --}}
            @if(isset($btn_account_opens_settings) && $btn_account_opens_settings == true)
                //if on profile page, do nothing when clicking on avatar
                if(($(e.target).hasClass(".btn-account-avatar") || $(e.target).is(".btn-account-avatar img")) && $(".profile-div").length > 0){
                return false;
            } else {
                $("#settings-div").show();
                return false;
            }
            @else
            //go to user profile page (don't return false and a href="/user/profile")
            //but if the settings icon was clicked inside it, open the settings div
            if($(e.target).hasClass("btn-settings-icon") && $(".profile-div").length > 0){
                $("#settings-div").show();
                return false;
            }
            @endif
	*/
        @else
            $("#login-div").show();
            return false;
        @endif
    });

    @if(isset($popup))
        @if($popup=="login")
            $("#login-div").show();
        @elseif($popup=="settings")
            $("#settings-div").show();
        @endif
    @endif


    window.playerClasses = ["playing", "paused", "stopped", "loading", "buffering"];

    window.onlyOneSliderPlayerClass = function(sliderSelector, activeClass){
	for(var iClass in playerClasses){
	    var theclass = playerClasses[iClass];
	    if(typeof(activeClass) != 'undefined' && activeClass == theclass){
		//do nothing
	    } else {
		$(sliderSelector).removeClass(theclass);
	    }
	}
	if(activeClass){
	    $(sliderSelector).addClass(activeClass);
	}
    };

    window.sliderPlayerBeingUsed = function(){
	return $(".slider").is(playerClasses.map(function(c){
	    return "." + c;
	}).join(","));
    }

    window.onSlideVideoBuffering = function(slider){
	onlyOneSliderPlayerClass(slider, 'buffering');
    };
    window.onSlideVideoPlay = function(slider){
	onlyOneSliderPlayerClass(slider, 'playing');
    };
    window.onSlideVideoPause = function(slider){
	onlyOneSliderPlayerClass(slider, 'paused');
    };
    window.onSlideVideoStop = function(slider){
	onlyOneSliderPlayerClass(slider, 'stopped');
    };


    window.resumeCurrentSliderPlayer = function(){
	//flowplayer code on custom play icon works perfectly, so comment this
	if(typeof window.currentSlide != "undefined"){
            //$f($(".slider-main .slide:nth-child("+window.currentSlide+") .slide-video").attr("id")).resume();
	} else {
            //$f($(".slider.paused .slide .slide-video").attr("id")).resume();
	}
    };

    window.addVideoWatch = function(video_id){
	if(null == localStorage.getItem("video_watch_" + video_id)){
	    $.ajax({
		url: "{{ url('/video/addwatch') }}" + "/" + video_id + "/geo",
		type: "POST",
		dataType: "json",
		success: function(data){
		    localStorage.setItem("video_watch_" + video_id, 1);
		}
	    });
	}
    };


    /* in the case of .show-slide-video, we have only one of .show-slide-video right now but still support multiple items; maybe we'll use ajax in the future and will have multiple .show-slide-video elements */
//    $(".slide-video").each(function(i, el){
      $(".slide-play").click(function(){
	  //we don't have .slide-play while the video is playing anymore,
	  //|->because it wouldn't show up when going fullscreen flash;
	  //|->so now the play icon only shows up before playing and after playing,
	  //|->so we don't need to check if the video is currently playing anymore
          if(false && sliderPlayerBeingUsed()){
              //clicking play when the player is paused
              //no need to resume manually here, as flowplayer thinks it was clicked
          } else {

        var el = $(this).parents(".slide").find(".slide-video")[0];
	var $slider = $(el).parents(".slider");
        var HTTPstreaming = true;
//
	var $id = $(el).attr('id');
	var trailer_href = $(el).attr('data-target-href');
	var video_id = $(el).attr('data-video-id');
	var video_cdn_dir = $(el).attr('data-video-cdn-dir');
	var sliderLocation = 'none';
	if($slider.hasClass('slider-main')){sliderLocation='main';}
	if($slider.hasClass('slider-show')){sliderLocation='show';}
	var slideRtmpUrl = '{{ Config::get('services.cloudfront.rtmp-distribution-url') }}/cfx/st/';
	var rtmpServerType = 'fms';

	// Returns a random integer between min (included) and max (excluded)
	// Using Math.round() will give you a non-uniform distribution!
	var getRandomInt = function(min, max) {
	    return Math.floor(Math.random() * (max - min)) + min;
	};
        var getVideoQualityUrl = function(quality){
	    //quality should be "1920x1080", "1280x720" or "858x480"
	    if(HTTPstreaming){
		//return "http:{{ Config::get('services.cloudfront.web-distribution-url') }}" + "/" + video_cdn_dir + quality + ".mp4";
		return "http://vcdn" + getRandomInt(1,8)  + ".gdson.net" + "/" + video_cdn_dir + quality + ".mp4";
	    } else {
		return "mp4:" + video_cdn_dir + quality;
	    }
	};
	/*if(sliderLocation == 'main'){
	    slideRtmpUrl = '{{ Config::get('services.red5.rtmp-url-path') }}';
	    rtmpServerType = 'red5';
	}*/
	//slideRtmpUrl = 'rtmp://vcdn' + getRandomInt(1,8)+ '.gdson.net:1935/oflaDemo';
	//rtmpServerType = 'red5';

	var addWatchCuepoint = 15000;
	//autobuffer only on show page; on firstpage it would be too much
	//removed; don't waste bandwidth if the user doesn't watch the video
	//var autobuffering_flag = sliderLocation == 'show';
	var autobuffering_flag = sliderLocation == 'show';
	var contextMenu = [];
	if(trailer_href && trailer_href != ""){
	    contextMenu.push({'უყურე სერიებს' : function() {
                   location.href = trailer_href;
               }});
            // menu separator
	    contextMenu.push('-');
	}
	contextMenu.push({'GDSON-ის შესახებ' : function() {
            location.href = "{{ url('/about') }}";
        }});


    @if(App\Helper::isMobile())

/*
	$("#" + $id).flowplayer({
	    // configuration common to all players in
	    // containers with class="slide-video" goes here
	});
*/

	$("#" + $id).find("video").on("play", function(){
	    onSlideVideoPlay($slider);
	});
	$("#" + $id).find("video").on("pause", function(){
	    onSlideVideoPause($slider);
	});
	$("#" + $id).find("video").on("ended", function(){
	    onSlideVideoStop($slider);
	});


	/* end if isMobile */
    @else


	$f($id, 
	   //"{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.commercial-3.2.18.swf",
"{{ asset('/cloudfront/flowplayer.commercial-3.2.18.swf') }}?v={{ filemtime(public_path().'/cloudfront/flowplayer.commercial-3.2.18.swf') }}",
	   {

               key: '#$3eb3d3098bb0ffbc3e7',

               contextMenu: contextMenu,
	       
               // wmode: "direct",
               wmode: isFirefox() ? "opaque" : "window",

	       //hide/show the default flowplayer buffering icon
	       //if true, also uncomment onlyOneSliderPlayerClass in onLoad
	       //|->and revert to disp:none in .slider&.buffering .slide-buffering-icon
	       buffering: true,

	       /* this is only fired on the first error of clip,
		  so hide the buffering icon manually
		*/
	       onLoad: function () {
		   //if buffering is set to true, uncomment this
		   onlyOneSliderPlayerClass($slider, 'playing');
		   //compensate for remembered mute behavior after autoplayed videos
		   this.setVolume(65);
		   //
		   setTimeout(function() {
                       if ($('#' + $id).parents('.post-video-container').hasClass('autoplay-canceled')) {
			   $f($id).pause();
                       }
		   }, 100);
               },

	       onError: function(errorCode, errorMessage){
		   onSlideVideoStop($slider);
		   $("#" + $id).parents('.slide')
		       .find('.slide-buffering-icon').hide();
		   //console.log(arguments);
	       },

	       onBeforeKeyPress: function(keyCode){
		   if(keyCode == 37){/* left arrow */
		       this.seek(this.getTime() - 5);
		       return false;
		   }
		   if(keyCode == 39){/* right arrow */
		       this.seek(this.getTime() + 5);
		       return false;   
		   }
	       },

	       onVolume: function(level){
		   if(level < 1.5){
		       $f($id).setVolume(0);
		       $f($id).mute();
		   } else {
		       window.lastPlayerVolume = level;
		       $f($id).unmute();
		   }
	       },

	       onBeforeMute: function(){
		   if(!($f($id).getVolume() < 1.5)){
		       window.lastPlayerVolume = $f($id).getVolume();
		       $f($id).setVolume(0);
		   }
	       },

	       onUnmute: function(){
		   //if($f($id).getVolume() == 0){
		       if(window.lastPlayerVolume){
			   $f($id).setVolume(window.lastPlayerVolume);
		       }
		   //}
	       },


	       canvas: {
		   background: '#000000',
		   backgroundGradient: 'none',
	       },

	       // play: null,
	       play: {
		   url: '/swf/play_hover.swf',
		   //flowplayer subtracts some opacity for hover effect,
		   //so provide excess opacity to compensate and add hover effect in swf
		   //1.5 cancelled even the opacity in swf itself, so reduced
		   opacity: 1.1,
		   label: null,
		   replayLabel: null
	       },


	       plugins: {

		   /*
		   cluster: HTTPstreaming ? {
		       // http cluster
		       // url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.cluster-3.2.10.swf",
		       url: "{{ asset('/cloudfront/flowplayer.cluster-3.2.10.swf') }}",
		       hosts: [
			   'http://vcdn2.gdson.net/' + video_cdn_dir,
			   'http://vcdn3.gdson.net/' + video_cdn_dir,
			   'http://vcdn4.gdson.net/' + video_cdn_dir,
			   'http://vcdn5.gdson.net/' + video_cdn_dir,
			   'http://vcdn6.gdson.net/' + video_cdn_dir,
			   'http://vcdn7.gdson.net/' + video_cdn_dir
		       ],		       
		       // that are load balanced (accessed randomly)
		       loadBalance: true,
		       // callback method that updates our info box
		       onConnect: function(host, index) {
			   console.log("connecting to: ", host, " @ ", index);
		       }
		   } : {
		       //rtmp cluster
		       url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.cluster-3.2.10.swf",
		       netConnectionUrl: slideRtmpUrl,
		       
		       // Our hosts array contains the netConnectionUrls to the servers
		       hosts: [
			   <?php $vcdn_comma = ""; ?>
			   @foreach(Config::get('services.red5.cdn-servers') as $vcdn)
			   {{$vcdn_comma }}
			   { host: '{{ $vcdn['rtmp-url-path'] }}' }
			   <?php $vcdn_comma = ","; ?>
			   @endforeach
		       ]
		   },
		   */

		   pseudo: {
		       //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.pseudostreaming-3.2.13.swf"
		       url: "{{ asset('/cloudfront/flowplayer.pseudostreaming-3.2.13.swf') }}"
		   },

		   // bandwidth check plugin
		   bwcheck: {
		       url: "{{ asset('/cloudfront/flowplayer.bwcheck-3.2.13.swf') }}",
		       // CloudFront uses Adobe FMS servers
		       serverType: HTTPstreaming ? 'http' : rtmpServerType,
// we use dynamic switching, the appropriate bitrate is switched on the fly
		       dynamic: true,
		       //checkOnStart requires bwchecker application on the server; not supported on wowza
		       checkOnStart: false,
		       dynamicBuffer: true,

		       //the resource on which to test connection speed
		       netConnectionUrl: HTTPstreaming ? getVideoQualityUrl("352x240") /*"{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.bwcheck-3.2.13.swf"*/ : slideRtmpUrl
		       
		   },

		   // example of bwcheck and brselect together
		   // http://flowplayer.blacktrash.org/test/bwcheck-brselect.html

		   // bitrate selector plugin
		   bitrateselect: {
		       //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.bitrateselect-3.2.14.swf",
		       url: "{{ asset('/cloudfront/flowplayer.bitrateselect-3.2.14.swf') }}",

		       menu: true,

		       //don't use the HD toggle button or the HD notification splash
		       hdButton: {
			   place: 'none',
			   splash: false
		       },

		       onStreamSwitchBegin: function(newItem, currentItem) {
			   //calling play() causes the center play icon to disappear,
			   //but because we're switching quality, playback will still
			   //be paused after quality switching ends
			   //probably caused bugs GDSON-462 and double play icons after switching
			   //$f($id).play();
			   //temporarily disable buffering animation when the player is paused
			   if(!$f($id).isPaused()){
			       onlyOneSliderPlayerClass($slider, 'buffering');
			   }
			   /*
			   console.log('raw switch event');
			   $slider.addClass("switching-quality");
			   var curPlayTime = parseInt($f($id).getTime());
			   if(typeof window.switchTime=='undefined' && curPlayTime>0){
				   if(window.forceSwitchingTime){
				       window.switchTime = window.forceSwitchingTime;
				       delete window.forceSwitchingTime;
				   } else {
				       window.switchTime = curPlayTime;
				   }
				   if(typeof window.lastVolume == 'undefined'){
				       window.lastVolume = $f($id).getVolume();
				       $f($id).setVolume(0);
				   }
				   console.log('starting switch, pausing', switchTime);
			   } else { window.secondSwitchTriggered = true; }
			   */
		       },
		       onStreamSwitch: function(newItem) {
			   onlyOneSliderPlayerClass($slider, 'playing');
			   /*
			   if(typeof window.switchTime != 'undefined' && window.switchTime > 0){
			       var _swTime = window.switchTime;
			       delete window.switchTime;
			       console.log('finished switch', $id);
//sometimes two switches are called, sometimes only one, so wait a maximum of 3 seconds
			       window.finishSwitchInterval = setInterval(function(){
				   if(
(typeof window.secondSwitchTriggered != 'undefined' && window.secondSwitchTriggered)
|| (typeof window.finishSwitchWaited !='undefined' && window.finishSwitchWaited>3000)){
				       clearInterval(window.finishSwitchInterval);
				       delete window.secondSwitchTriggered;
				       console.log(_swTime);
				       $f($id).seek(_swTime);
//sometimes player goes to unstarted mode (1), rewinds to 0, and starts switching again.
//then onStreamSwitchBegin will be called, which will think we're at 0
//in this case we tell the upcoming onStreamSwitchBegin where we should really rewind
				       if($f($id).getState() == 1){
					   window.forceSwitchingTime = _swTime;
					   console.log('restarting process');
					   $f($id).play();
				       } else {
					   $slider.removeClass("switching-quality");
					   onlyOneSliderPlayerClass($slider, 'playing');
					   console.log('finished process');
					   $f($id).setVolume(window.lastVolume);
					   delete window.lastVolume;
				       }
				   } else {
				      if(typeof window.finishSwitchWaited=='undefined'){
					  window.finishSwitchWaited = 0;
				      }
				       window.finishSwitchWaited += 500;
				   }
			       }, 500);
			   }
			   */
		       }

		   },


		   //bitrate selector menu
		   menu: {
		       //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.menu-3.2.13.swf",
		       url: "{{ asset('/cloudfront/flowplayer.menu-3.2.13.swf') }}?v={{ filemtime(public_path().'/cloudfront/flowplayer.menu-3.2.13.swf') }}",
		       items: [
			   {label: "ხარისხი:", enabled: false}
		       ],
		       right: 10,/*10*/
		       width: 110,

		       //the button in the dock which opens the menu
		       button: {
			   
		       },
		       menuItem: {
			   height: 26,
			   //menu item background color
			   color: 'rgba(26, 26, 26, 0.9)',
			   //disabled item (quality label) background color:
			   //disabledFontColor: '#d9d9d9',
			   //menu item text color
			   fontColor: '#d9d9d9',
			   //hovered menu item background color
			   overColor: 'rgba(26, 26, 26, 0.9)',
			   //GDSON custom build: hovered menu item text color
			   overFontColor: '#db2729',
			   //GDSON custom build: selected menu item text color
			   selectedFontColor: '#db2729'
		       }
		   },


		   // Specify the location of the RTMP plugin.
		   rtmp: {
		       //url: '{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.rtmp-3.2.13.swf',
		       url: "{{ asset('/cloudfront/flowplayer.rtmp-3.2.13.swf') }}",
		       //CloudFront RTMP distribution,e.g. s5c39gqb8ow64r.cloudfront.net
		       //this will get overridden with cluster plugin
		       netConnectionUrl: slideRtmpUrl
		   },

		   //gatracker caused buggy play/seek functionality
		   /*gatracker: {
		       url: "{{ asset('/cloudfront/flowplayer.analytics-3.2.9.swf') }}",
		       events: {
			   all: true
		       },
		       debug: true,
		       accountId: '{{ Config::get('services.ganalytics.account_id') }}'
		   },*/

		   //style control bar
		   controls: {
                       //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.controls-3.2.16.swf",
		       url: "{{ asset('/cloudfront/flowplayer.controls-gdson-3.2.16.swf') }}?v={{ filemtime(public_path().'/cloudfront/flowplayer.controls-gdson-3.2.16.swf') }}",

		       height: 32,
		       // customize the appearance to make the control bar transparent
		       // colors can be in rgba() too
		       //whole controlbar background
		       backgroundColor: "rgba(26, 26, 26, 0.9)",
		       backgroundGradient: "none",
		       //border radius
		       borderRadius: '0',
		       //whole seek bar background
		       sliderColor: '#404040',
		       sliderBorder: '0px',
		       //buffered line background
		       bufferColor: '#bcbec0',
		       //progress line background
		       progressColor: '#db2729',
		       progressGradient: 'none',
		       //all buttons foreground color
                       buttonColor: '#b2b2b2',
		       buttonOverColor: '#db2729',

		       //volume full line background color
		       volumeSliderColor: '#404040',
		       volumeBorder: 'none',

		       //current time
		       timeColor: '#db2729',
		       timeSeparator: ' / ',
		       //clip duration
		       durationColor: '#666666',
		       timeBorder: '0',
		       timeBgColor: 'transparent',
		       //scrubber bar
		       scrubberBorderRadius: '0',
		       //the slider control's height will be (scrubberHeightRatio*controlbar's height). take note that the actual progress/buffer lines will be further reduced by scrubberBarHeightRatio
		       //scrubberHeightRatio: '1',
		       //the scrubber bar's line's height (e.g. progress, buffer indicator) will be (scrubberBarHeightRatio*slider's height)
		       scrubberBarHeightRatio: '0.3',
		       //volume bar
		       volumeBorderRadius: '0',
		       //same logic as scrubber[Bar]HeightRatio
		       //volumeSliderHeightRatio: '1',
		       volumeBarHeightRatio: '0.25',

		       tooltipColor: 'rgba(255, 255, 255, 0.7)',
		       tooltipTextColor: '#000000'

		   },

		   /*
		   // a content box so that we can see the selected bitrate.
		   content: {
		       url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.content-3.2.9.swf",
		       top: 0,
		       left: 0,
		       width: 400,
		       height: 150,
		       backgroundColor: 'transparent',
		       backgroundGradient: 'none',
		       border: 0,
		       textDecoration: 'outline',
		       style: {
			   body: {
			       fontSize: 14,
			       fontFamily: 'Arial',
			       textAlign: 'center',
			       color: '#ffffff'
			   }
		       }
		   }
		   */


	       },

	       // Configure Flowplayer to use the RTMP plugin for streaming.
	       clip: {
		   urlResolvers: HTTPstreaming
		       ? ['bwcheck', 'bitrateselect']    //, 'cluster'
		       : ['bwcheck', 'bitrateselect']   //, 'cluster'
		   ,
		   provider: HTTPstreaming ? 'pseudo'/*http*/ : 'rtmp',
		   autoPlay: false,
		   scaling: 'fit',

		   
		   //connectionProvider: HTTPstreaming ? null : 'cluster',


		   //flowplayer doesn't play until the buffer is loaded fully
		   //so increasing it would delay video start
		   bufferLength: 3,

		   autoBuffering: autobuffering_flag,
		   //wmode: 'direct' should be specified at the top of this config
		   // accelerated: true,

// available bitrates and the corresponding files. We specify also the video width
// here, so that the player does not use a too large file. It switches to a
// file/stream with larger dims when going fullscreen if the available b/w permits.
		   bitrates: [

               @if(Auth::check() && Auth::user()->hasRole('Admin'))

//{ url: getVideoQualityUrl("352x240"), width: 352, bitrate: 780, label: "240p" }, /* 350kbps/0.8/0.8/0.7  */
//{ url: getVideoQualityUrl("480x360"), width: 480, bitrate: 1350, label: "360p" }, /* 600kbps/0.8/0.8/0.7  */
{ url: getVideoQualityUrl("858x480"), width: 854/*858*/, bitrate: 200/* vbr 0.5-2*/, label: "480p"
}, /* 1000kbps/0.8/0.8/0.7 */
{ url: getVideoQualityUrl("1280x720"), width: 1280/*1280*/, bitrate: 3000/* vbr 1.5-3 */, label: "720p",
    // this is the default bitrate, the playback kicks off with this and after that
    // Quality Of Service monitoring adjusts to the most appropriate bitrate
    isDefault: true
 }, /* 2200kbps/0.8/0.8/0.7 */
{ url: getVideoQualityUrl("1920x1080"), width: 1280/*1920*/, bitrate: 4000/* vbr 2.25-4 */, label: "1080p" } /* 4500kbps/0.8/0.8/0.7 */

               @else
//{ url: getVideoQualityUrl("352x240"), width: 352, bitrate: 780, label: "240p" }, /* 350kbps/0.8/0.8/0.7  */
//{ url: getVideoQualityUrl("480x360"), width: 480, bitrate: 1350, label: "360p" }, /* 600kbps/0.8/0.8/0.7  */
{ url: getVideoQualityUrl("858x480"), width: 858, bitrate: 2230, label: "480p",
    // this is the default bitrate, the playback kicks off with this and after that
    // Quality Of Service monitoring adjusts to the most appropriate bitrate
    isDefault: true
}, /* 1000kbps/0.8/0.8/0.7 */
{ url: getVideoQualityUrl("1280x720"), width: 1280, bitrate: 4900, label: "720p" }, /* 2200kbps/0.8/0.8/0.7 */
{ url: getVideoQualityUrl("1920x1080"), width: 1920, bitrate: 10000, label: "1080p" } /* 4500kbps/0.8/0.8/0.7 */
               @endif

		   ],/* bitrates */

		   onCuepoint: [
		       // each integer represents milliseconds in the timeline
		       [addWatchCuepoint],

		       // this function is triggered when a cuepoint is reached
		       function(clip, cuepoint) {
			   if(cuepoint == addWatchCuepoint){
			       addVideoWatch(video_id);
			   }
		       }
		   ],

		   onStart: function(){
		       //resume autohiding the controls when paused
		       $f($id).getControls().setAutoHide({enabled:true,hideDelay:1000});
		       //
		       onSlideVideoPlay($slider);
               if(sliderLocation=='show'){
                   var episode_id = $("#" + $id).attr('data-episode-id');
                   var saved_last_pos = localStorage.getItem("lastwatch_episode_" + episode_id);
		   var curEpisodePlayer = $f($id);
		   var maxTimeToSave = parseInt(curEpisodePlayer.getClip().duration) - 180;
                   if(episode_id && parseInt(saved_last_pos) > 0 && saved_last_pos < maxTimeToSave){
                       setTimeout(function(){
                           $f($id).seek(parseInt(saved_last_pos));
                       }, 100);
                   } else { console.log(JSON.stringify(saved_last_pos)); }
               }
               setTimeout(function() {
                   if ($('#' + $id).parents('.post-video-container').hasClass('autoplayed')) {
                       $f($id).mute(true);
                   }
               }, 100);
		   },
		   onPause: function(){
		       onSlideVideoPause($slider);
		       //don't autohide the controls when paused
		       $f($id).getControls().setAutoHide({enabled:false});
		   },
		   onResume: function(){
		       //resume autohiding the controls when paused
		       $f($id).getControls().setAutoHide({enabled:true,hideDelay:1000});
		       //
		       onSlideVideoPlay($slider);
		   },
		   onFinish: function(){
		       onSlideVideoStop($slider);
		   },
		   onSeek: function(){
		       addVideoWatch(video_id);
		   },
		   onBufferEmpty: function(){
		       //onSlideVideoBuffering($slider);
		   }

	       }


	   });//$f() - flowplayer flash


	/* end if isMobile else  */
    @endif


      }//end else {} of if(sliderPlayerBeingUsed())

    });/* end $(".slide-play").click(), previously $(".slide-video").each()  */




    $(".slider .slide .slide-play").click(function(){

	var el = $(this).parents(".slide").find(".slide-video")[0];
	var $id = $(el).attr('id');
	var $slide = $(this).parents(".slide");
        //$(this).hide();
	//$slide.find(".slide-video").show();
        var $slider = $(this).parents(".slider");
	//this class is also added by $f onStart, but not if stream not found
	onlyOneSliderPlayerClass($slider, 'loading');

	var $video = $slide.find(".slide-video");

	@if(App\Helper::isMobile())
	    $video.find("video")[0].play();
	@else
	    $f($video.attr('id')).play();
	@endif
    });/* $(".slider .slide .slide-play").click() */







    $("#settings-div .subscribe-dropdown-title").click(function(){
	$(".subscribe-items").toggle();
    });
    $("#settings-div .subscribe-item").click(function(){
	var genreId = $(this).attr("data-genre-id");
	var on = $(this).find(".subscribe-checkbox").hasClass("selected") ? 0 : 1;
	$.ajax({
	    url: "/user/subscribe/" + genreId + "/" + on,
	    type: "POST", cache: false
	});
	$(this).find(".subscribe-checkbox").toggleClass("selected");
    });
    $("#settings-div .btn-settings-delete").click(function(){
	$("#settings-div .btn-settings-delete-confirm").show();
    });
    $("#settings-div .btn-settings-delete-confirm-no").click(function(){
	$("#settings-div .btn-settings-delete-confirm").hide();
    });
    $("#settings-div .btn-settings-delete-confirm-yes").click(function(){
	//if(confirm("თქვენი ექაუნთი და ყველა მონაცემი სამუდამოდ წაიშლება!")){
	    $.ajax({
		url: "/user/removemyself",
		type: "POST",
		dataType: "json",
		success: function(data){
		    alert(data.result);
		    if(data.success){
			if(document.location.pathname != "/"){
			    document.location.href = "/";
			} else {
			    document.location.reload(true);
			}
		    }
		}
	    });
	//}
    });

    @if(isset($notificationId))
	<?php     $notif = App\FbNotification::find($notificationId);    ?>
        @if($notif->whose)
          showNotificationMoreButton({ url: "{{ $notif->whose->getUrl() }}", image: "{{ url($notif->whose->getImageUrl()) }}" });
        @endif
    @endif




    @section('script-bottom')
    @show

  </script>

<div id="images-cache">
  <img src="{{ asset('/img/icons/search-red.svg') }}" data-no-retina />
  <!-- <img src="{{ asset('/img/icons/search-red.png') }}" data-no-retina /> -->
  <img src="{{ asset('/img/icons/user-red.svg') }}" data-no-retina />
  <!-- <img src="{{ asset('/img/icons/user-red.png') }}" data-no-retina /> -->
  <img src="{{ asset('/img/icons/settings-red.svg') }}" data-no-retina />
  <!-- <img src="{{ asset('/img/icons/settings-red.png') }}" data-no-retina /> -->
  <img src="{{ asset('/img/icons/play.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/play-red.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/prev-white.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/next-white.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/add-white.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/social-white.svg') }}" data-no-retina />
  <img src="{{ asset('/img/icons/collapse.svg') }}" data-no-retina />
</div>

</body>
</html>
