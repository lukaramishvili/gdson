@extends('layouts.admin')

@section('content')

        <!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12" style="background-color: grey; color: white; padding: 10px 0; border-bottom: 2px solid green;">
                <div class="col-md-3">სათაური</div>
                <div class="col-md-3">მშობელი</div>
                <div class="col-md-3">ლინკი</div>
                <div class="col-md-1 no-padding">order</div>
                <div class="col-md-2">Delete</div>
            </div>
            @foreach($menu as $key => $menuItem)
            <div class="col-md-12" style="border-bottom: 2px solid green;
                @if($menuItem->parent_id) padding: 20px 0 20px 50px; background-color: whitesmoke; @else padding: 20px 0 20px 0; background-color: antiquewhite; @endif">
                <div class="col-md-3">
                    <a href="javascript:;" id="menu-item-title-geo" data-pk="{{ $menuItem->id }}" data-type="text" data-name="geo" data-field="title" data-original-title="მენიუს სათაური" class="editable editable-click" style="display: inline;">
                        {{ $menuItem->getTitle('geo') }}
                    </a>
                </div>

                <div class="col-md-3">
                    <select name="menu-parent" class="menu-parent" style="width: 50%;">
                        <option value="" menu-id="{{ $menuItem->id }}" parent-id=""> - </option>
                        @for($index = 1; $index <= count($menu); $index ++)
                            @if($index - 1 != $key && !$menu[$index - 1]->parent_id)
                                <option
                                        value="{{ $index }}"
                                        parent-id="{{ $menu[$index - 1]->id }}"
                                        menu-id="{{ $menuItem->id }}"
                                        @if($menuItem->parent_id == $menu[$index - 1]->id) selected @endif
                                >
                                    {{ $menu[$index - 1]->getTitle('geo') }}
                                </option>
                            @endif
                        @endfor
                    </select>
                </div>

                <div class="col-md-3">
                    <a href="javascript:;" id="menu-item-link-geo" data-pk="{{ $menuItem->id }}" data-type="text" data-name="geo" data-field="link" data-original-title="მენიუს ლინკი" class="editable editable-click" style="display: inline;">
                        {{ $menuItem->link }}
                    </a>
                </div>

                <div class="col-md-1 no-padding">
                    <select name="menu-order" class="menu-order" style="width: 60%;">
                        <option value="" menu-id="{{ $menuItem->id }}"> - </option>
                        @for($i = 1; $i <= count($menu); $i ++)
                            <option
                                    value="{{ $i }}"
                                    menu-id="{{ $menuItem->id }}"
                                    @if($i == $menuItem->order) selected @endif
                            >
                                {{ $i }}
                            </option>
                        @endfor
                    </select>
                </div>

                <div class="col-md-2">
                    <button class="btn btn-danger btn-circle delete-menu-item" menu-item-id="{{ $menuItem->id }}">
                        Delete ?
                    </button>
                </div>
            </div>
            @endforeach

            <div class="col-md-12" style="margin-top: 30px;">
                <a href="{{ url('/admin/addmenuitem') }}">
                    <button class="btn btn-danger btn-circle">
                        დამატება
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection
