<div class="profile-add-playlist-div">
  <button type="button" class="profile-add-playlist-btn">სიის დამატება</button>
  <div class="profile-add-playlist-form">
    <input type="text" class="profile-add-playlist-input" />
    <button type="button" class="profile-add-playlist-submit">&#10003;</button>
  </div>
</div>
