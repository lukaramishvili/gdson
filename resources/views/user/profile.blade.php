@extends('layouts.master')

    @section('content')


<div class="profile-div">
  <div class="profile-just-added">
    <div class="container">
      <div class="row">
	<h3 class="col-md-12 profile-just-added-heading">ახალი დამატებული</h3>
	<div class="col-md-12 videos-list-big-items profile-just-added-items">
	  @foreach(App\Episode::orderBy('created_at', 'DESC')->get() as $just_added_ep)
	  <?php
	     if(!$just_added_ep->isPublished()){ continue; }
	  if(!isset($just_added_iter)){ $just_added_iter = 0; }
	  $just_added_iter++;
	  if($just_added_iter > 12){ break; }
	  $just_added_ep_show = $just_added_ep->season->show;
	  ?>
	  <div class="videos-list-item profile-just-added-item"><!--col-md-4 col-sm-6 col-xs-12-->
	    <a href="{{ $just_added_ep->getUrl() }}">
	      <img src="{{ asset($just_added_ep->getImageUrl()) }}" />
	      <div class="videos-list-item-title profile-just-added-item-title">
		@if($just_added_ep_show && $just_added_ep_show->mainGenre())
		<div class="videos-list-item-genre profile-just-added-item-genre">{{ $just_added_ep_show->mainGenre()->getTitle("geo") }}</div>
		@endif
		{{ $just_added_ep->getFullTitleNbsp("geo") }}
	      </div>
	    </a>
	  </div>
	  @endforeach
	</div>
      </div>
    </div>
  </div>
  <div class="profile-playlists">
    <div class="container">
      <!-- start add playlist btn -->
      <div class="row">
	<div class="col-md-12">
	  @include('user.add-playlist-btn')
	</div>
      </div>
      <!-- end add playlist btn -->
      @foreach(Auth::user()->playlists as $playlist)
      <div class="row profile-playlist">
	  
	  <h3 class="col-md-12 profile-playlist-title">
	    {{ $playlist->getTitle("geo") }}
	  </h3>

	  <div class="col-md-12 profile-playlist-items">

	  @foreach($playlist->videos as $p_video)
	  <!-- <div class="col-md-2 col-sm-4 col-xs-6"> -->
	  <div class="videos-list-item profile-playlist-item" data-playlist-id="{{ $playlist->id }}" data-video-id="{{ $p_video->id }}">
	    <div class="btn-remove-from-playlist"></div>
	    @if(App\Post::byVideoId($p_video->id))
	    <?php
	       $p_post = App\Post::byVideoId($p_video->id);
	       $p_show = $p_post->getTargetShow();
	    ?>
	    <a class="videos-list-item" href="{{ $p_post->getTarget()->getUrl() }}">
	      <img src="{{ asset($p_post->getImageUrl()) }}" />
	      <div class="videos-list-item-title">
		@if($p_show)
		<div class="videos-list-item-genre profile-playlist-item-genre">{{ $p_show->mainGenre()->getTitle("geo") }}</div>
		{{ $p_post->getTitle("geo") }}
	      </div>
	        @endif
	    </a>
	    @elseif($p_video->episode)
	    <?php
	       $p_episode = $p_video->episode;
	       $p_show = $p_video->episode->season->show;
	    ?>
	    <a class="videos-list-item" href="{{ $p_episode->getUrl() }}">
	      <img src="{{ asset($p_episode->getImageUrl()) }}" />
	      <div class="videos-list-item-title">
		<div class="videos-list-item-genre profile-playlist-item-genre">{{ $p_episode->mainGenre()->getTitle("geo") }}</div>
		{{ $p_episode->getFullTitleNbsp("geo") }}
	      </div>
	    </a>
	    @elseif($p_video->trailer)
	    <a class="videos-list-item" href="{{ $p_video->trailer->whose->getUrl() }}">
	      <img src="{{ asset($p_video->trailer->whose->getImageUrl()) }}" />
	      <div class="videos-list-item-title">
		<div class="videos-list-item-genre profile-playlist-item-genre">{{ $p_video->trailer->whose->mainGenre()->getTitle("geo") }}</div>
		{{ $p_video->trailer->whose->getFullTitle("geo")." - ".$p_video->trailer->whose->getTitle("geo") }}
	      </div>
	    </a>
	    @endif
	  </div>
	  @endforeach
	  </div><!-- .profile-playlist-items -->

      </div>
      @endforeach
      <!-- start add playlist btn -->
      <div class="row">
	<div class="col-md-12">
	  @include('user.add-playlist-btn')
	</div>
      </div>
      <!-- end add playlist btn -->
    </div>
  </div>
</div>


    @endsection

@section('script-bottom')

    $(".profile-playlist-item .btn-remove-from-playlist").click(function(){
        //don't ask anymore, delete directly
        if(true || confirm("ნამდვილად გსურთ ვიდეოს ფლეილისტიდან წაშლა?")){
            var $item = $(this).parents(".profile-playlist-item");
	    var playlist_id = $item.attr('data-playlist-id');
	    var video_id = $item.attr('data-video-id');
            var on = 0;
	    $.ajax({
		url: "/user/setvideoinplaylist/"+playlist_id+"/"+video_id+"/"+on,
		type: "POST",
		dataType: "json",
		success: function(data){
		    var $items_container = $item.parents(".profile-playlist-items");
		    $item.parents(".owl-item").remove();
		    reinitProfilePlaylistCarousel($items_container);
		}
	    });
	}
    });/* $(".profile-playlist-item .btn-remove-from-playlist").click() */

@endsection
