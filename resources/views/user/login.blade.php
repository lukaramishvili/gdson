<div class="popup" id="login-div">
  <div class="popup-shadow">
    <div class="popup-inner">
      <div class="popup-title">
     <!--LOGIN &bull; REGISTRATION-->
         შესვლა &bull; რეგისტრაცია
	<div class="popup-close"></div>
      </div>
      <div class="popup-content">
        <div class="login-btn-container">
                           <!--Login to GDSON with-->
                           შესვლა
	  <br>
	  <a href="{{ url('/login/facebook') }}" class="btn-login-popup-login">
	    <img src="{{ asset('/img/icons/btn-fb-login.png') }}" />
	  </a>
	</div>
      </div>
    </div>
  </div>
</div>
