@if(Auth::check())
<div class="popup" id="settings-div">
  <div class="popup-shadow">
    <div class="popup-inner">
      <div class="popup-title">
	პროფილის მართვა
	<div class="popup-close"></div>
      </div>
      <div class="popup-content">

	<div class="row">
	  <div class="col-md-4 col-sm-4 col-xs-4 settings-avatar">
	    <img src="{{ Auth::user()->avatar }}" />
	  </div>
	  <div class="col-md-8 col-sm-8 col-xs-8 settings-name">
	    {{ Auth::user()->firstname.' '.Auth::user()->lastname }}
	  </div>
	  {{--
	  <div class="col-md-8 col-sm-8 col-xs-8 settings-subscribe-title">
	    გამოწერა
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-6 settings-genres">
	    <div class="settings-subscribe-dropdown">
	      <div class="subscribe-dropdown-title">ჟანრები</div>
	      <div class="subscribe-items">
		@foreach(App\Genre::all() as $genre)
		<div class="subscribe-item" data-genre-id="{{ $genre->id }}">
		  <div class="subscribe-checkbox @if(Auth::user()->genres->contains($genre->id)) selected @endif"></div>
		  {{ $genre->getTitle("geo") }}
	        </div>
	        @endforeach
	      </div>
            </div>
          </div>
	  --}}
        </div>

	<div class="popup-toolbox">
	  <div class="btn-settings-delete">პროფილის წაშლა</div>
	  <div class="btn-settings-delete-confirm">
	    <div class="btn-settings-delete-confirm-label">
	      დარწმუნებული ხართ, რომ გსურთ პროფილის წაშლა?
	    </div>
	    <div class="btn-settings-delete-confirm-no">არა
	    </div>
	    <div class="btn-settings-delete-confirm-yes">კი, წაშალე
	    </div>
	  </div>
	  <a href="{{ url('/auth/logout') }}" class="btn-subscription-logout">
	    გამოსვლა
	  </a>
	</div>
      </div>
    </div>
  </div>
</div>
@endif
