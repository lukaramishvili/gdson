@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="highlight-shows">
                        <h2>სერიალები</h2>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>სერიალის სათაური</th>
                                <th>ვიდეოს სათაური</th>
                                <th>სლაიდერი</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shows as $show)
                                <tr>
                                    <td>
                                        {{ $show['show']->getTitle('geo') }}
                                    </td>
                                    <td>
                                        {{ $show['video_title_geo'] }}
                                    </td>
                                    <td>
                                        <input type="checkbox" class="make-switch trailer-highlight" data-on-text="Yes" data-off-text="No"
                                               data-video-id="{{ $show['video_id'] }}" @if($show['highlight']) checked @endif>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="highlight-seasons">
                        <h2>სეზონები</h2>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>სერიალის სათაური</th>
                                <th>სეზონის ნომერი</th>
                                <th>ვიდეოს სათაური</th>
                                <th>სლაიდერი</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($seasons as $season)
                                <tr>
                                    <td>
                                        {{ $season['show']->getTitle('geo') }}
                                    </td>
                                    <td>
                                        {{ $season['season']->season_number }}
                                    </td>
                                    <td>
                                        {{ $season['video_title_geo'] }}
                                    </td>
                                    <td>
                                        <input type="checkbox" class="make-switch trailer-highlight" data-on-text="Yes" data-off-text="No"
                                               data-video-id="{{ $season['video_id'] }}" @if($season['highlight']) checked @endif>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="highlight-seasons">
                        <h2>სერიები</h2>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>სერიალის სათაური</th>
                                <th>სეზონის ნომერი</th>
                                <th>სერიის სათაური</th>
                                <th>ვიდეოს სათაური</th>
                                <th>სლაიდერი</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($episodes as $episode)
                                <tr>
                                    <td>
                                        {{ $episode['show']->getTitle('geo') }}
                                    </td>
                                    <td>
                                        {{ $episode['season']->season_number }}
                                    </td>
                                    <td>
                                        {{ $episode['episode']->getTitle('geo') }}
                                    </td>
                                    <td>
                                        {{ $episode['video_title_geo'] }}
                                    </td>
                                    <td>
                                        <input type="checkbox" class="make-switch trailer-highlight" data-on-text="Yes" data-off-text="No"
                                               data-video-id="{{ $episode['video_id'] }}" @if($episode['highlight']) checked @endif>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="highlights-order">
                        <a href="{{ url('/highlight/order') }}">
                            <button class="btn btn-primary btn-circle">
                                Order
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection