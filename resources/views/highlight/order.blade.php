@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>video</th>
                                <th>order</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($trailers as $trailer)
                            <tr>
                                <td>{{ $trailer['video']->getTitle('geo') }}</td>
                                <td>
                                    <select name="order" class="order" data-highlight-id="{{ $trailer['highlight_id'] }}">
                                        <option value="100">order</option>
                                        @for($i = 1; $i <= sizeof($trailers); $i++)
                                            <option value="{{ $i }}" @if($trailer['order'] == $i) selected @endif>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="highlight-go-back">
                        <a href="{{ URL::previous() }}">
                            <button class="btn btn-primary btn-circle">
                                უკან
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection