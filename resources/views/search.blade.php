@extends('layouts.master')



@section('content')



<div class="search-container">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12 col-lg-offset-1">
	<div class="search-heading">
	  <div class="search-input-div">
	    <input type="text" name="keyword" class="search-input" id="search-input" placeholder="ძებნა" value="{{ $name }}"/>
	    <div class="btn-show-search-results"></div>
	  </div>
	</div>
	<div class="search-content">
	  <div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	      <div class="search-results">
	      </div>
	    </div>
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>











  @endsection








  @section('script-bottom')

    var lastSearchKeyword = "";
    var search            = function() {
        var curSearchKeyword = $("#search-input").val();
        if(curSearchKeyword.length < 3) {
            return;
        }

        lastSearchKeyword = curSearchKeyword;
        $.ajax({
            url: "/search",
            type: "POST",
            cache: false,
            data: { keyword: curSearchKeyword },
            'dataType': "json",
            success: function(data) {
                if(curSearchKeyword != lastSearchKeyword) {
                    console.log(curSearchKeyword + " canceled");
                    return;
                }

                //data.results = [ /* { title:, desc:, image:, href: } , ... */ ];
                $(".search-results .search-result").remove();

                if (Object.keys(data.results).length) {
                    for(var iresult in data.results) {
                        var result  = data.results[iresult];
                        var $result = $(
                        "<div class='search-result'>"
                            + "<img src='" + result.image + "' />"
                            + "<span class='result-title'>" + result.title + "</span>"
                            + "<a href='" + result.href + "' class='btn-search-result-more'></a>"
                        );
                        $(".search-results").append($result);
                    }
                } else {
                    var $result = $(
                    "<div class='search-result'>"
                        + "<div class='alert alert-danger'>არ მოიძებნა</div>"
                    );
                    $(".search-results").append($result);
                }
            }
        });
    };
    $("#search-input").keyup(search);

    if ($("#search-input").val()) {
        search();
    }


  @endsection
