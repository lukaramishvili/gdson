
<div class="col-md-4 col-sm-6 col-xs-12">
  <a class="videos-list-item" href="{{ $href }}">
    <img src="{{ asset($image) }}" />
    <div class="videos-list-item-title">
    @if(isset($genre))
    <div class="videos-list-item-genre">{{ $genre }}</div>
    @endif
    {{ $title }}
    </div>
  </a>
</div>
