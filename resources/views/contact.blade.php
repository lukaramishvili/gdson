@extends('layouts.master')



@section('content')



<div class="contact-container">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12 col-lg-offset-1">
	<div class="contact-heading">
	  <h1>კონტაქტი</h1>
	  <p class="subtitle">GDSON-ის საკონტაქტო ინფორმაცია</p>
	</div>
	<div class="contact-content">
	  <div class="row">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          @if($contact_message)
              <div class="col-md-12">
                  <div class="alert alert-info">
                      {{ $contact_message }}
                  </div>
              </div>
          @endif
	    <div class="col-md-6 col-sm-6 col-xs-12">
	      <div class="contact-form-div">
		<form method="post" action="{{ url('/contact') }}">
		  <input type="text" name="name" id="input-name" placeholder="სახელი" />
		  <input type="text" name="email" id="input-email" placeholder="ელ-ფოსტა" />
		  <textarea name="message" id="textarea-message" placeholder="მესიჯი"></textarea>
          <div class="g-recaptcha" data-sitekey="6Ldc2hATAAAAAH1Si7951Zb535727jCHEci7b5h7"></div>
		  <button type="submit">გაგზავნა</button>
		</form>
	      </div>
	    </div>
	    <div class="col-md-6 col-sm-6 col-xs-12">
	      <div class="address">
		<h4 class="address-title">მისამართი</h4>
		<div class="address-text">
		  <div class="address-location">
		    კოჯრის გზატკეცილი, #6 0105.<br>
		    თბილისი, საქართველო<br>
		  </div>
		  <br>
		  <div class="address-phone-text">
		    <div class="address-phone-icon"></div>
		    <a href="tel://+995322557070">+995 [32] 255 70 70</a>
		  </div>
		  <div class="address-email-text">
		    <div class="address-email-icon"></div>
		    <a href="mailto:info@gdson.net">info@gdson.net</a>
		  </div>
		  <br>
		  <div class="address-website"><a href="{{ url('/') }}">www.gdson.net</a></div>
		</div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="contact-map-div">
	  <div id="contact-map"></div>
	</div>
      </div>
    </div>
  </div>
</div>




  @endsection








  @section('head-extras')

    <script src="https://maps.googleapis.com/maps/api/js"></script>

  @endsection








  @section('script-bottom')

function initMap() {

    // Specify features and elements to define styles.
    var styleArray = [
	{
	    featureType: "all",
	    stylers: [
		{ saturation: -80 }
	    ]
	},{
	    featureType: "road.arterial",
	    elementType: "geometry",
	    stylers: [
		{ hue: "#00ffee" },
		{ saturation: 50 }
	    ]
	},{
	    featureType: "poi.business",
	    elementType: "labels",
	    stylers: [
		{ visibility: "off" }
	    ]
	}
    ];

    var hq_coords = { lat: 41.684569, lng: 44.7920243 };

    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('contact-map'), {
	center: hq_coords,
	scrollwheel: false,
	// Apply the map style array to the map.
	styles: styleArray,
	zoom: 13
    });

    var marker = new google.maps.Marker({
	position: hq_coords,
	map: map,
	title: 'GDSON',
	icon: {
            size: new google.maps.Size(26, 41),
            scaledSize: new google.maps.Size(26, 41),
            url: '{{ asset('img/icons/lokalization.png') }}'
	}
    });
};

google.maps.event.addDomListener(window, 'load', initMap);


  @endsection
