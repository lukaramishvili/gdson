@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართული</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Ruski</a></li>
                            <li role="presentation"><a href="#image" aria-controls="image" role="tab" data-toggle="tab">სურათი</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                                <div class="person-first-name">
                                    <div class="add-person-label">
                                        სახელი
                                    </div>
                                    <a href="javascript:;" id="person-first-name-geo" data-type="text" data-pk="{{ $person->id}}" data-name="geo" data-original-title="სახელი" class="editable editable-click" style="display: inline;">
                                        {{ $person['first_name']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="person-last-name">
                                    <div class="add-person-label">
                                        სახელი
                                    </div>
                                    <a href="javascript:;" id="person-last-name-geo" data-type="text" data-pk="{{ $person->id}}" data-name="geo" data-original-title="გვარი" class="editable editable-click" style="display: inline;">
                                        {{ $person['last_name']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="person-description">
                                    <div class="add-person-label">
                                        აღწერა
                                    </div>
                                    <a href="javascript:;" id="person-description-geo" data-type="textarea" data-pk="{{ $person->id}}" data-name="geo" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $person['desc_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="person-profession">
                                    <div class="add-person-label">
                                        პროფესია
                                    </div>
                                    <div class="col-md-2 no-padding">
                                        {!! Form::select('professions[]', $professions, $person_professions, ['class' => 'form-control person-professions', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="person-back">
                                    <div class="add-person-label">
                                        <a href="{{ url('/person/deleteperson/' . $person->id) }}">
                                            <button class="btn btn-primary btn-circle person-cancel">
                                                @if($type == 'add')
                                                    გაუქმება
                                                @else
                                                    წაშლა
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="person-first-name">
                                    <div class="add-person-label">
                                        First name
                                    </div>
                                    <a href="javascript:;" id="person-first-name-eng" data-type="text" data-pk="{{ $person->id}}" data-name="eng" data-original-title="სახელი" class="editable editable-click" style="display: inline;">
                                        {{ $person['first_name']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="person-last-name">
                                    <div class="add-person-label">
                                        Last name
                                    </div>
                                    <a href="javascript:;" id="person-last-name-eng" data-type="text" data-pk="{{ $person->id}}" data-name="eng" data-original-title="გვარი" class="editable editable-click" style="display: inline;">
                                        {{ $person['last_name']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="person-description">
                                    <div class="add-person-label">
                                        description
                                    </div>
                                    <a href="javascript:;" id="person-description-eng" data-type="textarea" data-pk="{{ $person->id}}" data-name="eng" data-original-title="description" class="editable editable-click" style="display: inline;">
                                        {{ $person['desc_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="person-back">
                                    <div class="add-person-label">
                                        <a href="{{ url('/person/deleteperson/' . $person->id) }}">
                                            <button class="btn btn-primary btn-circle person-cancel">
                                                @if($type == 'add')
                                                    Discard
                                                @else
                                                    Delete
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="person-first-name">
                                    <div class="add-person-label">
                                        First name
                                    </div>
                                    <a href="javascript:;" id="person-first-name-rus" data-type="text" data-pk="{{ $person->id}}" data-name="rus" data-original-title="სახელი" class="editable editable-click" style="display: inline;">
                                        {{ $person['first_name']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="person-last-name">
                                    <div class="add-person-label">
                                        Last name
                                    </div>
                                    <a href="javascript:;" id="person-last-name-rus" data-type="text" data-pk="{{ $person->id}}" data-name="rus" data-original-title="გვარი" class="editable editable-click" style="display: inline;">
                                        {{ $person['last_name']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="person-description">
                                    <div class="add-person-label">
                                        description (russian)
                                    </div>
                                    <a href="javascript:;" id="person-description-rus" data-type="textarea" data-pk="{{ $person->id}}" data-name="rus" data-original-title="description" class="editable editable-click" style="display: inline;">
                                        {{ $person['desc_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="person-back">
                                    <div class="add-person-label">
                                        <a href="{{ url('/person/deleteperson/' . $person->id) }}">
                                            <button class="btn btn-primary btn-circle person-cancel">
                                                @if($type == 'add')
                                                    Discard
                                                @else
                                                    Delete
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="image">
                                <div class="person-image-upload">
                                    <div class="add-episode-label">
                                        სურათი
                                    </div>
                                    {!! Form::open(['url' => '/person/savepersonimage', 'id' => 'person-image-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_person_image']) !!}
                                        {!! Form::hidden('person-id', $person->id) !!}
                                        {!! Form::file('person-image', ['class' => '', 'id' => 'person-image']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_person_image" name="hidden_person_image" style="display: none;"></iframe>
                                </div>
                                {{--<div class="person-image-div">
                                    <div class="add-episode-label">
                                        <img id="person-img" src="{{ $person['image'] }}" class="col-md-4 no-padding">
                                            <span class="col-md-1 glyphicon glyphicon-remove remove-person-img"
                                                  data-person-id="{{ $person->id }}"></span>
                                    </div>
                                </div>--}}
                                <div class="person-image-url">
                                    <div class="add-episode-label">
                                        სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}person/{{ $person->id }}/">
                                        {{ '/person/' . $person->id . '/person.jpg' }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="person-id" value="{{ $person->id }}">
    </div>
    <!-- END CONTENT -->

@endsection