@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>ქართული</th>
                                <th>English</th>
                                <th>Russian</th>
                                <th>Keyword</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($professions as $profession)
                            <tr>
                                <td>
                                    <a href="javascript:;" id="profession-geo" data-type="text" data-pk="{{ $profession->id }}" data-name="geo" data-original-title="პროფესია" class="editable editable-click profession-geo" style="display: inline;">
                                        {{ $profession->getTitle('geo') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:;" id="profession-eng" data-type="text" data-pk="{{ $profession->id }}" data-name="eng" data-original-title="პროფესია" class="editable editable-click profession-eng" style="display: inline;">
                                        {{ $profession->getTitle('eng') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:;" id="profession-rus" data-type="text" data-pk="{{ $profession->id }}" data-name="rus" data-original-title="პროფესია" class="editable editable-click profession-rus" style="display: inline;">
                                        {{ $profession->getTitle('rus') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:;" id="profession-key" data-type="text" data-pk="{{ $profession->id }}" data-name="" data-original-title="keyword" class="editable editable-click profession-key" style="display: inline;">
                                        {{ $profession->keyword }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ url('person/deleteprofession/' . $profession->id) }}" class="delete-profession">
                                        <button class="btn btn-danger btn-circle">
                                            <i class="fa fa-remove"></i> წაშლა
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        <form action="/person/addprofession" method="post">
                            <tr>
                                <td class="form-group">
                                    <input type="text" name="profession-geo" class="form-control">
                                </td>
                                <td>
                                    <input type="text" name="profession-eng" class="form-control">
                                </td>
                                <td>
                                    <input type="text" name="profession-rus" class="form-control">
                                </td>
                                <td>
                                    <input type="text" name="profession-key" class="form-control">
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-success btn-circle" value="დამატება">
                                </td>
                            </tr>
                        </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection