@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        სახელი
                                    </th>
                                    <th>
                                        გვარი
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($persons as $person)
                                <tr>
                                    <td>
                                        {{ $person->id }}
                                    </td>
                                    <td>
                                        {{ $person->getFirstname->value_geo }}
                                    </td>
                                    <td>
                                        {{ $person->getLastname->value_geo }}
                                    </td>
                                    <td>
                                        <div class="actions">
                                            <a href="{{ url('/person/editperson/' . $person->id) }}" class="btn btn-circle btn-default">
                                                <i class="fa fa-pencil"></i> Edit </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="add-person-div">
                        <a href="{{ url('/person/addperson') }}">
                            <button class="btn btn-primary btn-circle">
                                Add person
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection