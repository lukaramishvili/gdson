@extends('layouts.master')







@section('style-head')

@endsection









@section('content')


@include('slider-main')


<div class="fp-big-items-container">
  <div class="container">
    <div class="row">
      <h3 class="col-md-12 fp-heading-big-items">
<!--if you remove the "open" class,then add "hidden" class to div.fp-big-items below-->
	<button type="button" class="toggle-fp-big-items open">
	  ვიდეოთეკა
	  <img src="{{ asset('img/icons/toggle-arrow.svg') }}" />
	</button>
      </h3>
      <div class="col-md-12 big-items fp-big-items owl-carousel">
	@foreach(App\Promo::orderBy('order')->get() as $promo)
    <?php $promoted = $promo->whose; ?>
    @if (!$promoted)
        <?php continue; ?>
    @endif
	<!--<div class="col-md-4 col-sm-6 col-xs-12">-->
    <?php $promotedUrl = $promo->whose_type == "show" ? $promoted->getPriorityUrl() : $promoted->getUrl() ?>
	<a class="fp-big-item" href="{{ $promotedUrl }}">
	  <img src="{{ asset($promoted->getImageUrl()) }}" />
	  <div class="fp-big-item-title">
	    @if($promo->whose_type == "show")
	      @if($promoted->mainGenre())
<div class="fp-big-item-genre">{{ $promoted->mainGenre()->getTitle("geo") }}</div>
	      @endif
	    @elseif($promo->whose_type == "season")
	      @if($promoted->show->mainGenre())
<div class="fp-big-item-genre">{{ $promoted->show->mainGenre()->getTitle("geo") }}</div>
	      @endif
	    @elseif($promo->whose_type == "episode")
	      @if($promoted->season->show->mainGenre())
<div class="fp-big-item-genre">{{ $promoted->season->show->mainGenre()->getTitle("geo") }}</div>
	      @endif
	    @endif
	    {{-- $promoted->getFullTitle("geo") --}}
	    @if($promo->whose_type == "show")
                {{ $promoted->getFullTitle("geo") }}
	    @elseif($promo->whose_type == "season")
                {{ $promoted->show->getFullTitle("geo") }}
	    @elseif($promo->whose_type == "episode")
	        {{ $promoted->season->show->getFullTitle("geo") }}
            @endif
	  </div>
	</a>
	<!--</div>-->
	@endforeach
      </div>
    </div>
  </div>
</div>
<div class="fp-genre-list-container">
  <div class="container">
<?php /*
    <!-- begin newly added list (new design) -->
    <div class="row fp-expanding-last-added-items-row">
        <h3 class="col-md-12 fp-heading-expanding-last-added">
            <a href="{{ url('/lastAdded') }}">ახალი დამატებულები</a>
        </h3>
        <div class="col-md-12 fp-expanding-last-added-items" data-expanded="0">
            @foreach(App\Episode::lastAdded(27) as $last_added)
                <div class="col-md-4 col-sm-4 col-xs-6 videos-list-item-container">
                <a class="videos-list-item" href="{{ $last_added->getUrl() }}">
                    <img src="{{ asset($last_added->getImageUrl()) }}" />
                    <div class="videos-list-item-title">{{ $last_added->getTitle("geo") }}</div>
                </a>
                </div>
            @endforeach
	    <button type="button" id="fp-expand-last-added-btn">მეტის ნახვა</button>
        </div>
    </div>
    <!-- end newly added list (new design) -->
    */ ?>
<?php /*
    <!-- begin newly added list (owl-carousel slide) -->
    <div class="row fp-last-added-items-carousel-row">
        <h3 class="col-md-12 fp-heading-genre">
          <!-- <a href="{{ url('/lastAdded') }}"> -->
            ახალი დამატებულები
          <!-- </a> -->
        </h3>
        <div class="col-md-12 fp-list-items fp-last-added-carousel-items owl-carousel">
	  <?php $fp_last_added = App\Episode::lastAdded(18); ?>
            @for($fp_ladd_i=0;$fp_ladd_i<count($fp_last_added);$fp_ladd_i+=2)
		<?php 
		   $ladd_1 = $fp_last_added[$fp_ladd_i];
		   if($fp_ladd_i + 1 < count($fp_last_added)){
		       $ladd_2 = $fp_last_added[$fp_ladd_i + 1];
		   } else {
		       unset($ladd_2);
		   }
		   ?>
                <!--<div class="col-md-2 col-sm-4 col-xs-6">-->
               <div class="two-video-container">
                <a class="videos-list-item" href="{{ $ladd_1->getUrl() }}">
                    <img src="{{ asset($ladd_1->getImageUrl()) }}" />
                    <div class="videos-list-item-title">
		      @if($ladd_1->season && $ladd_1->season->show && $ladd_1->season->show->mainGenre())
	                  <div class="videos-list-item-genre">{{ $ladd_1->season->show->mainGenre()->getTitle("geo") }}</div>
		      @endif
		      {{ $ladd_1->getFullTitleNbsp("geo") }}
	            </div>
                </a>
@if(isset($ladd_2))
                <a class="videos-list-item" href="{{ $ladd_2->getUrl() }}">
                    <img src="{{ asset($ladd_2->getImageUrl()) }}" />
                    <div class="videos-list-item-title">
		      @if($ladd_2->season && $ladd_2->season->show && $ladd_2->season->show->mainGenre())
	                  <div class="videos-list-item-genre">{{ $ladd_2->season->show->mainGenre()->getTitle("geo") }}</div>
		      @endif
		      {{ $ladd_2->getFullTitleNbsp("geo") }}
		    </div>
                </a>
                @endif
                </div>
                <!--</div>-->
            @endfor
        </div>
    </div>
    <!-- end newly added list (owl-carousel slide) -->
    */ ?>
<!-- begin newly added list (one-row owl-carousel slide) -->
    <div class="row fp-last-added-items-carousel-one-row">
        <h3 class="col-md-12 fp-heading-genre">
          <!-- <a href="{{ url('/lastAdded') }}"> -->
            ახალი დამატებულები
          <!-- </a> -->
        </h3>
        <div class="col-md-12 fp-list-items fp-last-added-carousel-items owl-carousel">
	  <?php $fp_last_added = App\Episode::lastAdded(12); ?>
            @for($fp_ladd_i=0;$fp_ladd_i<count($fp_last_added);$fp_ladd_i++)
		<?php 
		   $ladd_1 = $fp_last_added[$fp_ladd_i];
		   ?>
                <!--<div class="col-md-2 col-sm-4 col-xs-6">-->
                <a class="videos-list-item" href="{{ $ladd_1->getUrl() }}">
                    <img src="{{ asset($ladd_1->getImageUrl()) }}" />
                    <div class="videos-list-item-title">
		      @if($ladd_1->season && $ladd_1->season->show && $ladd_1->season->show->mainGenre())
	                  <div class="videos-list-item-genre">{{ $ladd_1->season->show->mainGenre()->getTitle("geo") }}</div>
		      @endif
		      {{ $ladd_1->getFullTitleNbsp("geo") }}
	            </div>
                </a>
                <!--</div>-->
            @endfor
        </div>
    </div>
    <!-- end newly added list (one-row owl-carousel slide) -->




<?php /*
  <!-- begin popular list -->
  <div class="row">
    <h3 class="col-md-12 fp-heading-genre">
      <a href="{{ url('/popular') }}">კვირის პოპულარული ეპიზოდები</a>
    </h3>
    <div class="col-md-12 fp-list-items owl-carousel">
      @foreach(App\Episode::getWeeklyPopularEpisodes() as $popular)
      <!--<div class="col-md-2 col-sm-4 col-xs-6">-->
      <a class="videos-list-item" href="{{ $popular->getUrl() }}">
	<img src="{{ asset($popular->getImageUrl()) }}" />
	<div class="videos-list-item-title">{{ $popular->getTitle("geo") }}</div>
      </a>
      <!--</div>-->
      @endforeach
    </div>
  </div>
  <!-- end popular list -->
      */ ?>
<?php /*
  <!-- firstpage category list items template -->
  @foreach(App\Genre::orderBy('order')->get() as $genre)
    @if($genre->shows()->count() > 0)
    <div class="row">
      <h3 class="col-md-12 fp-heading-genre">
	<a href="{{ $genre->getUrl() }}">{{ $genre->getTitle("geo") }}</a>
      </h3>
      <div class="col-md-12 fp-list-items owl-carousel">
	@foreach($genre->shows as $show)
      <?php
      $imageUrl = $show->getImageUrl();
      $season   = $show->seasons
                       ->filter(function ($season) { return $season->isPublished(); })
                       ->last();

      if (isset($season))
      {
          $imageUrl = $season->getImageUrl();

          $episode  = $season->episodes
                             ->filter(function ($episode) { return $episode->isPublished(); })
                             ->last();

          if (isset($episode))
          {
              $imageUrl = $episode->getImageUrl();
          }
      }

      ?>
	<!--<div class="col-md-2 col-sm-4 col-xs-6">-->
	<a class="videos-list-item" href="{{ $show->getPriorityUrl() }}">
	  <img src="{{ asset($imageUrl) }}" />
	  <div class="videos-list-item-title">{{ $show->getTitle("geo") }}</div>
	</a>
	<!--</div>-->
	@endforeach
      </div>
    </div>
    @endif
  @endforeach
  <!-- end firstpage category list items template -->
      */ ?>
  </div><!-- .fp-genre-list-container .container -->
</div><!-- .fp-genre-list-container -->

@endsection








@section('script-bottom')

//don't write code here; it will be overwritten by included slider-main template

@endsection

