

<?php /*parameters: pass $title and either array of Video-s ($videos), array of Episode-s ($episodes), or Show-s ($shows)*/ ?>

<?php /*this subtemplate will be included by others, e.g. genre.view or playlist.playlist*/ ?>


<div class="list-page-container">
  <div class="container">
    <div class="row">
      <div class="videos-list-items @if(isset($big_items) && $big_items) videos-list-big-items @endif">
        <h3 class="col-md-12 list-page-title">{{ $title }}</h3>
	@if(isset($videos))
	@foreach($videos as $video)
	<?php
        $video_rel_episode = $video->episode;
        $video_rel_season = null;
        $video_rel_show = null;
        if(isset($video_rel_episode)){
            $video_rel_season = $video->episode->season;
        }
        if(isset($video_rel_season)){
            $video_rel_show = $video->episode->season->show;
        }
	?>
    @if(isset($video_rel_show))
	  @include('playlist.item', array(
	  /* find out video's owner episode and link to it */
      'title' => $video_rel_show->getTitle("geo"),
      'image' => $video_rel_show->getImageUrl(),
      'genre' => $video_rel_show->mainGenre()->getTitle("geo"),
      'href' => $video_rel_episode->getUrl(),
      ))
    @endif
	@endforeach
	@endif

	@if(isset($shows))
	@foreach($shows as $show)
	    @include('playlist.item', array(
	        'title' => $show->getTitle("geo"),
	        'image' => $show->getImageUrl(),
            'genre' => $show->mainGenre()->getTitle("geo"),
            'href' => $show->getUrl(),
	))
	@endforeach
	@endif

	@if(isset($episodes))
	@foreach($episodes as $episode)
	    @include('playlist.item', array(
	        'title' => $episode->getFullTitle("geo"),
	        'image' => $episode->getImageUrl(),
            'genre' => $episode->season->show->mainGenre()->getTitle("geo"),
            'href' => $episode->getUrl(),
	))
	@endforeach
	@endif
      </div>
    </div>

  </div>
</div>

