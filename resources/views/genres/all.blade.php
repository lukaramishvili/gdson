@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-striped genres-table">
                        <thead>
                            <tr>
                                <td>ქართულად</td>
                                <td>English</td>
                                <td>Russian</td>
                                <td>ალიასი</td>
                                <td>Parent</td>
                                <td>Delete</td>
                                <td>Order</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($genres as $genre)
                            <tr>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk="{{ $genre['genre_id'] }}" data-name="geo" data-original-title="ჟანრი" class="editable editable-click genre-geo" style="display: inline;">
                                        {{ $genre['translations']['value_geo'] }}
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk="{{ $genre['genre_id'] }}" data-name="eng" data-original-title="ჟანრი" class="editable editable-click genre-eng" style="display: inline;">
                                        {{ $genre['translations']['value_eng'] }}
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk="{{ $genre['genre_id'] }}" data-name="rus" data-original-title="ჟანრი" class="editable editable-click genre-rus" style="display: inline;">
                                        {{ $genre['translations']['value_rus']  }}
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk="{{ $genre['genre_id'] }}" data-name="alias" data-original-title="ჟანრი" class="editable editable-click genre-alias" style="display: inline;">
                                        {{ $genre['alias']}}
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    {!! Form::select('parent-genre', $parent_genres, $genre['default_genre'], ['id' => 'parent-genre', 'class' => 'form-control', 'data-genre' => $genre['genre_id']]) !!}
                                </td>
                                <td class="col-md-2">
                                    <a href="{{ url('/genres/delete/' . $genre['genre_id']) }}" class="btn btn-circle btn-default delete-genre">
                                        <i class="fa fa-remove"></i> Delete </a>
                                </td>
                                <td>
                                    <select name="order" class="genre-order" data-genre-id="{{ $genre['genre_id'] }}">
                                        <option value="100">order</option>
                                        @for($i = 1; $i <= sizeof($genres); $i++)
                                            <option value="{{ $i }}" @if($genre['order'] == $i) selected @endif>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk=0 data-name="geo" class="editable editable-click add-genre-geo" style="display: inline;">
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk=0 data-name="eng" class="editable editable-click add-genre-eng" style="display: inline;">
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk=0 data-name="rus" class="editable editable-click add-genre-rus" style="display: inline;">
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    <a href="javascript:;" data-type="text" data-pk=0 data-name="alias" class="editable editable-click add-alias" style="display: inline;">
                                    </a>
                                </td>
                                <td class="col-md-2 seasons-episode-title">
                                    &nbsp;
                                </td>
                                <td class="col-md-2">
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection
