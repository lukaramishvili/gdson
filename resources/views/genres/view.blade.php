@extends('layouts.master')


@section('content')
    

    @include('slider-main')



    @include('video.list-page', array(
        'title' => $genre->getTitle("geo"),
        'shows' => $genre->shows,
        'big_items' => true
    ))


@endsection





@section('script-bottom')

//don't write code here; it will be overwritten by included slider-main template

@endsection
