@extends('layouts.master')


@section('content')


    @include('slider-main')

    @include('video.list-page', array(
        'title' => "ახალი დამატებულები",
        'episodes' => App\Episode::lastAdded(10),
        'big_items' => true
    ))


@endsection





@section('script-bottom')

    //don't write code here; it will be overwritten by included slider-main template

@endsection
