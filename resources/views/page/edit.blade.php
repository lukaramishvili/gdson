@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართული</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Russian</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                                <div class="page-title-div">
                                    <div class="add-page-label">
                                        სათაური
                                    </div>
                                    <a href="javascript:;" id="page-title-geo" data-type="text" data-pk="{{ $page->id }}" data-name="geo" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $page['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="page-description">
                                    <div class="add-page-label">
                                        აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="page-description-geo" data-pk="{{ $page->id }}" data-name="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $page['desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right page-description-save" data-textarea="page-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="page-alias">
                                    <div class="add-page-label">
                                        ალიასი
                                    </div>
                                    <a href="javascript:;" id="page-alias" data-type="text" data-pk="{{ $page->id }}" data-original-title="ალიასი" class="editable editable-click" style="display: inline;">
                                        {{ $page->alias }}
                                    </a>
                                </div>
                                <div class="page-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                დამატება
                                            @else
                                                უკან დაბრუნება
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="page-delete-btn-div">
                                    <a href="{{ url('/page/delete/' . $page->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="page-title-div">
                                    <div class="add-page-label">
                                        Title
                                    </div>
                                    <a href="javascript:;" id="page-title-eng" data-type="text" data-pk="{{ $page->id }}" data-name="eng" data-original-title="title" class="editable editable-click" style="display: inline;">
                                        {{ $page['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="page-description">
                                    <div class="add-page-label">
                                        Description
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="page-description-eng" data-pk="{{ $page->id }}" data-name="eng" rows="6" style="visibility: hidden; display: none;">
                                            {{ $page['desc_tr']['value_eng'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right page-description-save" data-textarea="page-description-eng">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="page-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Add
                                            @else
                                                GO back
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="page-delete-btn-div">
                                    <a href="{{ url('/page/delete/' . $page->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="page-title-div">
                                    <div class="add-page-label">
                                        Title (Russian)
                                    </div>
                                    <a href="javascript:;" id="page-title-rus" data-type="text" data-pk="{{ $page->id }}" data-name="rus" data-original-title="title" class="editable editable-click" style="display: inline;">
                                        {{ $page['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="page-description">
                                    <div class="add-page-label">
                                        Description (Russian)
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="page-description-rus" data-pk="{{ $page->id }}" data-name="rus" rows="6" style="visibility: hidden; display: none;">
                                            {{ $page['desc_tr']['value_rus'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right page-description-save" data-textarea="page-description-rus">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="page-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Add
                                            @else
                                                GO back
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="page-delete-btn-div">
                                    <a href="{{ url('/page/delete/' . $page->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection
