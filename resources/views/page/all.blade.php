@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    @foreach($pages as $page)
                        <div class="page">
                            <h2>{{ $page->getTitle('geo') }}</h2>
                            <p>{{ $page->getDescription('geo') }}</p>
                            <a href="{{ url('/page/edit/' . $page->id) }}">
                                <button class="btn btn-primary btn-circle">
                                    რედაქტირება
                                </button>
                            </a>
                            <hr/>
                        </div>
                    @endforeach
                    <div class="add-new-page-btn">
                        <a href="{{ url('/page/add') }}">
                            <button class="btn btn-primary btn-circle">
                                დამატება
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection