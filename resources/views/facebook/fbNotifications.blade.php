@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <h2>Facebook Notifications</h2>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>სახელი</th>
                                <th>სათაური</th>
                                <th>აღწერა</th>
                                <th>დრო</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifications as $notification)
                                @if($notification->already_sent != 1)
                                    <tr>
                                        <th>
                                            {{ $notification->whose->getTitle('geo') }}
                                        </th>
                                        <th>
                                            <a href="javascript:;" id="fb-notification-title-geo" data-type="text" data-pk="{{ $notification->id }}" data-name="geo" data-original-title="ნოთიფიქეიშენის სათაური" class="editable editable-click" style="display: inline;">
                                                {{ $notification->getTitle('geo') }}
                                            </a>
                                        </th>
                                        <th>
                                            <a href="javascript:;" id="fb-notification-desc-geo" data-type="text" data-pk="{{ $notification->id }}" data-name="geo" data-original-title="ნოთიფიქეიშენის აღწერა" class="editable editable-click" style="display: inline;">
                                                {{ $notification->getDescription('geo') }}
                                            </a>
                                        </th>
                                        <th>
                                            <input id="fb-notification-publish-date" name="publish-date" data-notification-id="{{ $notification->id }}"
                                                   data-datetime="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notification->publish_date)->format('m/d/Y H:i') }}">
                                        </th>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection