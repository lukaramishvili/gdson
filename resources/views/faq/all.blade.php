@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    @foreach($faqs as $faq)
                        <div class="faq">
                            <h2>{{ $faq->getTitle('geo') }}</h2>
                            <p>{{ $faq->getDescription('geo') }}</p>
                            <a href="{{ url('/faq/edit/' . $faq->id) }}">
                                <button class="btn btn-primary btn-circle">
                                    რედაქტირება
                                </button>
                            </a>
                            <hr/>
                        </div>
                    @endforeach
                    <div class="add-new-faq-btn">
                        <a href="{{ url('/faq/add') }}">
                            <button class="btn btn-primary btn-circle">
                                დამატება
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection