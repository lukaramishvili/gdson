@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართული</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Russian</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                                <div class="faq-title">
                                    <div class="add-faq-label">
                                        სათაური
                                    </div>
                                    <a href="javascript:;" id="faq-title-geo" data-type="text" data-pk="{{ $faq->id }}" data-name="geo" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $faq['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="faq-description">
                                    <div class="add-faq-label">
                                        აღწერა
                                    </div>
                                    <a href="javascript:;" id="faq-desc-geo" data-type="textarea" data-pk="{{ $faq->id }}" data-name="geo" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $faq['desc_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="faq-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                დამატება
                                            @else
                                                უკან დაბრუნება
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="faq-delete-btn-div">
                                    <a href="{{ url('/faq/delete/' . $faq->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="faq-title">
                                    <div class="add-faq-label">
                                        Title
                                    </div>
                                    <a href="javascript:;" id="faq-title-eng" data-type="text" data-pk="{{ $faq->id }}" data-name="eng" data-original-title="title" class="editable editable-click" style="display: inline;">
                                        {{ $faq['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="faq-description">
                                    <div class="add-faq-label">
                                        Description
                                    </div>
                                    <a href="javascript:;" id="faq-desc-eng" data-type="textarea" data-pk="{{ $faq->id }}" data-name="eng" data-original-title="description" class="editable editable-click" style="display: inline;">
                                        {{ $faq['desc_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="faq-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Add
                                            @else
                                                GO back
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="faq-delete-btn-div">
                                    <a href="{{ url('/faq/delete/' . $faq->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="faq-title">
                                    <div class="add-faq-label">
                                        Title (Russian)
                                    </div>
                                    <a href="javascript:;" id="faq-title-rus" data-type="text" data-pk="{{ $faq->id }}" data-name="rus" data-original-title="title" class="editable editable-click" style="display: inline;">
                                        {{ $faq['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="faq-description">
                                    <div class="add-faq-label">
                                        Description (Russian)
                                    </div>
                                    <a href="javascript:;" id="faq-desc-rus" data-type="textarea" data-pk="{{ $faq->id }}" data-name="rus" data-original-title="description" class="editable editable-click" style="display: inline;">
                                        {{ $faq['desc_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="faq-go-back">
                                    <a href="{{ URL::previous() }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Add
                                            @else
                                                GO back
                                            @endif
                                        </button>
                                    </a>
                                </div>
                                <div class="faq-delete-btn-div">
                                    <a href="{{ url('/faq/delete/' . $faq->id) }}">
                                        <button class="btn btn-primary btn-circle">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection
