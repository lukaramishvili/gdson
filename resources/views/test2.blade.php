<html>
<head>
    <title>Laravel and Jcrop</title>
    <meta charset="utf-8">
    <link href="/admin/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
    <script src="/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/admin/js/jquery.Jcrop.min.js" type="text/javascript"></script>
</head>
<body>
<h2>Image Cropping with Laravel and Jcrop</h2>
<div>
    <img src="<?php echo '/uploads/test.png' ?>" id="cropimage">
</div>

<?= Form::open(['url' => '/admin/test']) ?>
<?= Form::hidden('image', public_path() . '/uploads/test.png') ?>
<?= Form::hidden('x', '', array('id' => 'x')) ?>
<?= Form::hidden('y', '', array('id' => 'y')) ?>
<?= Form::hidden('w', '', array('id' => 'w')) ?>
<?= Form::hidden('h', '', array('id' => 'h')) ?>
<?= Form::submit('Crop it!') ?>
<?= Form::close() ?>

<script type="text/javascript">

    var api;
    var cropWidth = 1920;
    var cropHeight = 720;

    // set default options
    var opt = {};

    // if height and width must be exact, dont allow resizing
    opt.allowResize = false;
    opt.allowSelect = false;

    // initialize jcrop
    api = $.Jcrop('#cropimage', opt);

    // set the selection area [left, top, width, height]
    api.animateTo([0,0,cropWidth,cropHeight]);

    // you can also set selection area without the fancy animation
    api.setSelect([0,0,cropWidth,cropHeight]);

    $(function() {
        $('#cropimage').Jcrop({
            onSelect: updateCoords
        });
    });

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }
</script>
</body>
</html>