@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="main-page-blocks">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding">
                        <h1>სერიალები</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="shows-page-blocks">
                    @foreach($shows as $key => $show)
                        <div class="col-lg-3 col-md-3 @if($key % 3 != 0) col-md-offset-1 @endif col-sm-6 col-xs-12 no-padding">
                            <div class="dashboard-stat" style="background-image: url('{{ $show['cover'] }}')">
                                <a href="{{ url('/show/seasons/' . $show['alias']) }}">
                                    <div class="overflow-hidden show-block-text">
                                        <div class="visual">
                                            <i class="fa"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                {{ $show['title']['value_geo'] }}
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="more" href="{{ url('/show/editshow/' . $show['alias']) }}">
                                    რედაქტირება <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-lg-3 col-md-3 @if(sizeof($shows) % 3 != 0) col-md-offset-1 @endif col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat purple-plum">
                            <a href="{{ url('/show/addshow') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            დამატება
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link" href="{{ url('/show/addshow') }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row" style="margin-top: 50px;">
                <div class="main-page-blocks">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding">
                        <h1>სამართავი პანელი</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="main-page-blocks">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat blue-madison">
                            <a href="{{ url('/show/shows') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            სერიალები
                                        </div>
                                        <div class="desc">
                                            სერიალების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat red-intense">
                            <a href="{{ url('/user/users') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            მომხმარებლები
                                        </div>
                                        <div class="desc">
                                            მომხმარებლების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
<!--
                    <div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat green-haze">
                            <a href="{{ url('/person/actors') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            მსახიობები
                                        </div>
                                        <div class="desc">
                                            მსახიობების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat blue-madison">
                            <a href="{{ url('/person/producers') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            პროდიუსერები
                                        </div>
                                        <div class="desc">
                                            პროდიუსერების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat red-intense">
                            <a href="{{ url('/person/directors') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            დირექტორები
                                        </div>
                                        <div class="desc">
                                            დირექტორების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat green-haze">
                            <a href="{{ url('/person/writers') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            მწერლები
                                        </div>
                                        <div class="desc">
                                            მწერლების დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
-->
@foreach(App\Profession::all() as $profession)
<div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-6 col-xs-12 no-padding">
  <div class="dashboard-stat green-haze">
    <!-- <a href="{{ url('/person/'.$profession->keyword.'s') }}"> -->
    <a href="{{ url('/person/byprofession/'.$profession->keyword) }}">
      <div class="overflow-hidden">
        <div class="visual">
          <i class="fa fa-comments"></i>
        </div>
        <div class="details">
          <div class="number">
            {{ preg_replace("/ი$/","ები",$profession->getTitle("geo")) }}
          </div>
          <div class="desc">
            {{ preg_replace("/ი$/","ები",$profession->getTitle("geo")) }}ს დესქრიფშენი
          </div>
        </div>
      </div>
    </a>
    <a class="more add-show-bottom-link"></a>
  </div>
</div>
@endforeach
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat blue-madison">
                            <a href="{{ url('/person/all') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            ყველა
                                        </div>
                                        <div class="desc">
                                            ყველას დესქრიფშენი
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
