<div class="slider-container slider-container-main">
  <div class="slider slider-main">
    @foreach(App\Highlight::orderBy('order')->get() as $highlight)
    @if($highlight->trailer->whose && $highlight->trailer->whose->isPublished())
    <?php
       if(!isset($slide_iter)){ $slide_iter = 0; }
       $slide_iter++;
       $h_trailer = $highlight->trailer;
       $h_video = $h_trailer->video;
       $h_title = $h_video->getTitle("geo");
       $h_desc = $h_video->getDescription("geo");
       $h_cover = asset($h_trailer->getImageUrl());
       $h_video_host = Config::get('services.video-cdn.servers')[rand(0, count(Config::get('services.video-cdn.servers')) - 1)];
       $h_video_cdn_dir = "videos/".$h_video->id."/";
       $h_video_url = "videos/".$h_video->id."/1920x1080";
       //for the link of "watch later"
       $whose_type = $h_trailer->whose_type;
       $h_href = $h_trailer->whose ? url($whose_type == "show" ? $h_trailer->whose->getPriorityUrl() : $h_trailer->whose->getUrl()) : "#";
       ?>
    <div class="slide" style="background: black url({{ $h_cover }}) center top no-repeat; background-size: auto 100%;">
      <div class="slide-video-container">
	@if(App\Helper::isMobile())
	<div class="slide-video" id="home-slide-video-{{ $h_video->id }}">
	  <video preload="none" controls="controls">
	    <!-- <source type="video/webm" src="//mydomain.com/video.webm"> -->
	    <!-- <source type="video/mp4" src="{{ Config::get('services.cloudfront.web-distribution-url') }}/videos/{{ $h_video->id }}/1280x720.mp4"> -->
	      <source type="video/mp4" src="{{ $h_video_host }}/videos/{{ $h_video->id }}/1280x720.mp4">
	  </video>
	</div>
	@else
	<a class="slide-video home-slide-video" id="home-slide-video-{{ $h_video->id }}" href="mp4:{{ $h_video_url }}" data-video-id="{{ $h_video->id }}" data-video-cdn-dir="{{ $h_video_cdn_dir }}" data-target-href="{{ $h_href }}"></a>
	@endif
      </div>
      <div class="slide-play"></div>
      <div class="slide-buffering-icon"></div>
      <div class="slide-toolbox-container">
	<div class="container">
	  <div class="row">
	    <div class="col-md-5 col-sm-8 col-xs-8">
	      <div class="slide-text-container">
		<div class="slide-text">
		  <a href="{{ $h_href }}" class="slide-title">{{ $h_title }}</a>
		  <div class="slide-desc">
		    {!! $h_desc !!}
		  </div>
          @if($h_trailer->whose_type=="episode"
          || ($h_trailer->whose_type=="season" && $h_trailer->whose && $h_trailer->whose->episodes()->count() > 0)
          || ($h_trailer->whose_type=="show" && $h_trailer->whose && $h_trailer->whose->getAllEpisodesOfAllSeasons()->count() > 0))
            <a href="{{ $h_href }}" class="slide-watch-more @if(!$highlight->trailer->whose->isPublished()) hidden-watch-more @endif">
              უყურე სერიებს
            </a>
          @endif
		  {{-- viewcount --}}
		  @if(isset($h_video))
		  <div class="slide-viewcount">
		    <img src="{{ asset('/img/icons/viewcount.svg') }}" />
		    {!! number_format($h_video->getViewcount(), 0, ".", " ") !!}
		  </div>
		  @endif
		</div>
	      </div>
	    </div>
	    <div class="col-md-2 col-sm-4 col-xs-4 col-md-offset-5 slide-buttons-container">
	      <div class="slide-buttons">
		<div class="slide-social-btn" id="slide-social-btn-{{ $slide_iter }}" data-title="{!! htmlspecialchars($h_title) !!}" data-desc="{!! htmlspecialchars($h_desc) !!}" data-image="https:{!! htmlspecialchars($h_cover) !!}" data-href="{!! htmlspecialchars($h_href) !!}">
		  <div class="slide-social-btn-inner"></div>
		</div>
		<div class="slide-playlist-btn"></div>
		@if(Auth::check())
		<div class="slide-playlist-btn-list">
		  <div class="slide-playlist-btn-list-inner">
		    @if(Auth::check())
		    @foreach(Auth::user()->playlists as $playlist)
		    <div class="slide-playlist-btn-list-item @if($playlist->videos->contains($h_video->id)) selected @endif " data-playlist-id="{{ $playlist->id }}" data-video-id="{{ $h_video->id }}">
		      {{ $playlist->getTitle("geo") }}
		    </div>
		    @endforeach
		    @endif
		  </div>
		</div>
		@endif
	      </div>
	    </div>
	  </div>
	</div>
      </div>
    </div>
    @endif
    @endforeach
    <div class="slider-nav">
      <div class="container">
	<div class="row">
	  <div class="col-md-2 col-sm-2 col-xs-2">
	    <div class="slider-arrow-prev">
	    </div>
	  </div>
	  <div class="col-md-8 col-sm-8 col-xs-7 slider-bullets-container">
	    <div class="slider-bullets"></div>
	  </div>
	  <div class="col-md-2 col-sm-2 col-xs-3 pull-right">
	    <div class="slider-arrow-next">
	    </div>
	  </div>
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>


@section('script-bottom')

window.currentSlide = 1;
function syncSliderContainer(sliderNumber){
    if(typeof(sliderNumber) == 'undefined'){
	sliderNumber = window.currentSlide;
    }
    $(".slider-main").css({
	height : $(".slide:nth-child("+sliderNumber+")").height()
    });
};
function slideTo(number){
    if($(".slider").hasClass('sliding')){
	return;
    }
    $(".slider").addClass('sliding');
    window.currentSlide = number;
    window.syncSliderContainer(number);
    onlyOneSliderPlayerClass(".slider-main");
    $(".slider-main .slider-bullets .slider-bullet").removeClass('active');
    $(".slider-main .slider-bullets .slider-bullet:nth-child("+number+")")
	.addClass('active');
    $(".slider-main .slide:not(:nth-child("+number+"))")
	.animate({ opacity: 0 }, 800)
	.removeClass('active');
    $(".slider-main .slide:nth-child("+number+")")
	.animate({ opacity: 1 }, {
	    complete: function(){
		$(".slider").removeClass('sliding');
	    }
	})
	.addClass('active', 800);
};
function slidePrev(){
    var prev = window.currentSlide - 1;
    if(prev < 1){
	prev = $(".slide").length;
    }
    slideTo(prev);
};
function slideNext(){
    var next = window.currentSlide + 1;
    if(next > $(".slide").length){
	next = 1;
    }
    slideTo(next);
};
$(document).ready(function(){

    $(window).resize(function(){
	window.syncSliderContainer();
    });

    $(".slider-main .slide").each(function(i, el){
	var $slide = $(el);
	$("<div class='slider-bullet'></div>")
	    .appendTo(".slider-main .slider-bullets")
	    .click(function(){
		slideTo(i + 1);
	    });
    });
    $(".slider-main .slider-bullets .slider-bullet:first-child").click();

    $(".slider-arrow-prev").click(function(){
	slidePrev();
    });
    $(".slider-arrow-next").click(function(){
	slideNext();
    });

    setInterval(function(){
      if(!sliderPlayerBeingUsed()){
          slideNext();
      }
    }, 8000);



    $(".slider-main .slide-social-btn").each(function(i, el){
	var $id = $(el).attr('id');
	var title = $(el).attr('data-title');
	var desc = $(el).attr('data-desc');
	if(desc.length == 0){
	    /* space required; otherwise fb inherits site desc */
	    desc = " ";
	}
	var image = $(el).attr('data-image');
	var href = $(el).attr('data-href');
	var slide_social_btn = new Share("#" + $id + " .slide-social-btn-inner", {
	    title: title,
	    url: href,
	    description: desc,
	    image: image,
	    ui: {
		    flyout: "top right"
	    },
	    networks: {
		facebook: {
		    app_id: window.fbAppId
            /*  ,caption: title  */
		},
                twitter: {
		    description: desc.substring(0, 100) + (desc.length > 100 ? "..." : "")
		},
		google_plus: {
		    title: title,
		    url: href,
		    description: desc,
		    image: image
		},
		pinterest: {
		    enabled: false
		},
		email: {
		    title: title,
		    description: title + " - " + href,
		}
	    }
	});/* slide button share init */
    });/* foreach .slide-social-btn */


});/* /document.ready */

@endsection
