<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Migrate Videos to Eagle Platform</title>
  </head>
  <body>

    <div class="migrate-all-videos-btn-container">
      <button type="button" class="migrate-all-videos-btn">ყველას გადატანა</button>
    </div>

    <br><br>

    <div class="video-items">
      
      @foreach($videos as $video)
      <?php
      $video_target_title = $video->getTitle("geo");
      if(App\Episode::where('video_id', $video->id)->count()>0){
          $video_target_title = App\Episode::where('video_id', $video->id)->get()->first()->getTitle("geo");
      }
      if(App\Trailer::where('video_id', $video->id)->count()>0 && !is_null(App\Trailer::where('video_id', $video->id)->get()->first()->whose)){
          $video_target_title = App\Trailer::where('video_id', $video->id)->get()->first()->whose->getTitle("geo");
      }
      if(App\Post::byVideoId($video->id)){
          $video_target_title = App\Post::byVideoId($video->id)->getTitle("geo");
      }
      ?>
      <div class="video-item" data-video_id="{{ $video->id }}" data-video_title="{{ $video_target_title }}">
      Video #{{ $video->id }} - {{ $video_target_title }}
	<button type="button" class="migrate-btn">ატვირთვა</button>
      </div>
      @endforeach
      
    </div>
          

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <script type="text/javascript">
window.$ = jQuery;
$(document).ready(function() {

    $(".migrate-all-videos-btn").click(function(){
	$(".video-item .migrate-btn").click();
    });
    
    $(".video-item .migrate-btn").click(function(){
	var $item = $(this).parents(".video-item");
	var video_id = $item.data("video_id");
	var video_title = $item.data("video_title");
	// create record
	$.ajax({
	    url: 'http://api.eagleplatform.com/media/records.json?account=gdson&auth_token=JET7NXoh1JcMhLyDsDGP',
            type: 'POST',
            data: {
                record : {
                    name : video_title
                },
                source : {
                    type         : 'http',
                    parameters    : {
                        url : 'http://vcdn1.gdson.net/videos/' + video_id + '/1920x1080.mp4'
                    }
                }
            },
            timeout: 13000,
            success : function(response){
		var success = response.errors.length === 0;
		if(success){
                    //console.log("update ", video_id, "set record_id = ", response.data.record.id);
		    var record_id = response.data.record.id;
		    $.ajax({
			url: "/admin/migratevideo",
			type: 'POST',
			data: {
			    'video_id'  : video_id,
			    'record_id' : record_id
			},
			success: function(data){
			    //data = { success: boolean, message: string }
			    console.log(data);
			    if(!data.success){ console.log(data.message); }
			}
		    });
		} else {
		    console.log("errors were encountered");
		    console.log(response.errors);
		}
            }
        });
	
    });/* $(".video-item .migrate-btn").click() */
});/* document.ready */

</script>

  </body>
</html>
