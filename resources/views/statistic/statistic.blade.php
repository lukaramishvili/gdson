@extends('layouts.admin')

@section('content')

        <!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['url' => '/admin/statistic', 'class' => 'form form-horizontal']) !!}
        <div class="form-group">
            {!! Form::label('statistic-show', 'სერიალი', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <select name="statistic-show" id="statistic-show">
                    <option value=0>ყველა</option>
                    @foreach($shows as $show)
                        <option value="{{ $show->id }}">{{ $show->getTitle('geo') }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('statistic-season', 'სეზონი', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <select name="statistic-season" id="statistic-season">
                    <option value=0>არ არის არჩეული</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('statistic-episode', 'ეპიზოდი', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <select name="statistic-episode" id="statistic-episode">
                    <option value=0>არ არის არჩეული</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('statistic-from-time', 'დასაწყისი', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <input id="statistic-from-time" class="statistic-from-time" name="statistic-from-time">
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('statistic-to-time', 'დასასრული', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <input id="statistic-to-time" class="statistic-to-time" name="statistic-to-time">
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('statistic-quality', 'ხარისხი', ['class' => 'col-md-2']) !!}
            <div class="col-md-4">
                <select name="statistic-quality" class="statistic-quality" id="statistic-quality">
                    <option value=0>ყველა</option>
                    <option value="858x480.mp4" {{ isset($quality) && $quality == '858x480.mp4' ? 'selected' : null }}>858x480.mp4</option>
                    <option value="1280x720.mp4" {{ isset($quality) && $quality == '1280x720.mp4' ? 'selected' : null }}>1280x720.mp4</option>
                    <option value="1920x1080.mp4" {{ isset($quality) && $quality == '1920x1080.mp4' ? 'selected' : null }}>1920x1080.mp4</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('სტატისტიკის ნახვა', ['class' => 'btn btn-danger btn-circle']) !!}
        </div>

        {!! Form::close() !!}

        @if(isset($allWatches) && isset($watches))
            <div id="chart_div"></div>
            <input type="hidden" class="all-watches" value="{{ $allWatches }}">
            <input type="hidden" class="watches" value="{{ $watches }}">
        @endif

        @if(isset($statisticFromTime) && isset($statisticToTime))
            <input type="hidden" class="statisticFromTime" value="{{ $statisticFromTime }}">
            <input type="hidden" class="statisticToTime" value="{{ $statisticToTime }}">
        @endif
    </div>
</div>
<!-- END CONTENT -->

@endsection