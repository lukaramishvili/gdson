<div class="show-container">
    <div class="slider-container slider-container-show">
        <div class="slider slider-show">
            <div class="slide" style="background: black url({{ $trailer->getImageUrl() }}) center top no-repeat; background-size: auto 100%;">
                <div class="slide-video-container" style="margin-bottom: 20px">
                    <?php
                        $video = $trailer->video;
                        $h_video_cdn_dir = "videos/" . $video->id . "/";
                        $h_video_url = "videos/" . $video->id . "/1920x1080";
                    ?>
                    @if(App\Helper::isMobile())
                        <div class="slide-video" id="home-slide-video-{{ $video->id }}">
                            <video preload="none" controls="controls">
                                <!-- <source type="video/webm" src="//mydomain.com/video.webm"> -->
                                <source type="video/mp4" src="{{ Config::get('services.cloudfront.web-distribution-url') }}/videos/{{ $video->id }}/1280x720.mp4">
                            </video>
                        </div>
                    @else
                        <a class="slide-video show-slide-video" id="show-slide-video-{{ $video->id }}" href="mp4:videos/{{ $video->id }}/1920x1080" data-video-id="{{ $video->id }}" data-video-cdn-dir="{{ $h_video_cdn_dir }}"></a>
                    @endif
                </div>
                <div class="slide-play"></div>
                <div class="slide-buffering-icon"></div>
            </div>
        </div>
    </div>
</div>
