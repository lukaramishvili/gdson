@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="shows-page-blocks">
                    @foreach($shows as $key => $show)
                    <div class="col-lg-3 col-md-3 @if($key % 3 != 0) col-md-offset-1 @endif col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat" style="background-image: url('{{ $show['cover'] }}') ">
                            <a href="{{ url('/show/seasons/' . $show['alias']) }}">
                                <div class="overflow-hidden show-block-text">
                                    <div class="visual">
                                        <i class="fa"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            {{ $show['title']['value_geo'] }}
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more" href="{{ url('/show/editshow/' . $show['alias']) }}">
                                რედაქტირება <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-lg-3 col-md-3 @if(sizeof($shows) % 3 != 0) col-md-offset-1 @endif col-sm-6 col-xs-12 no-padding">
                        <div class="dashboard-stat purple-plum">
                            <a href="{{ url('/show/addshow') }}">
                                <div class="overflow-hidden">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            დამატება
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="more add-show-bottom-link" href="{{ url('/show/addshow') }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
