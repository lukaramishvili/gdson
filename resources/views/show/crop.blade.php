<div>
    <img src="@if($type == 'cover') {{ $video->getCoverToCrop() }} @else {{ $video->getImageToCrop() }} @endif" id="cropimage">
</div>

<div>
    {!! Form::open(['url' => '/show/cropimage/' . $video->id, 'style' => 'display: inline-block; margin-right: 100px']) !!}
        {!! Form::hidden('image', $type == 'cover' ? $video->getCoverToCropPath() : $video->getImageToCropPath()) !!}
        {!! Form::hidden('x', '', array('id' => 'x')) !!}
        {!! Form::hidden('y', '', array('id' => 'y')) !!}
        {!! Form::hidden('w', $type == 'cover' ? 1920 : 370, array('id' => 'w')) !!}
        {!! Form::hidden('h', $type == 'cover' ? 720 : 260, array('id' => 'h')) !!}
        {!! Form::hidden('type', $type) !!}
        {!! Form::hidden('redirect-path', $redirect_path) !!}
        {!! Form::submit('Crop it!', array('style' => 'background-color: #5cb85c; font-size: 20px; padding: 5px; margin: 30px 0; border-radius: 5px;')) !!}
    {!! Form::close() !!}

    {!! Form::open(['url' => '/show/resizeimage/' . $video->id, 'style' => 'display: inline-block;']) !!}
        {!! Form::hidden('image', $type == 'cover' ? $video->getCoverToCropPath() : $video->getImageToCropPath()) !!}
        {!! Form::hidden('redirect-path', $redirect_path) !!}
        {!! Form::number('width', '', ['style' => 'height: 35px; border-radius: 5px; font-size: 20px;']) !!}
        {!! Form::submit('Resize', array('style' => 'background-color: #5cb85c; font-size: 20px; padding: 5px; margin: 30px 0; border-radius: 5px;')) !!}
    {!! Form::close() !!}
</div>


<link href="/admin/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>

<script src="/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/admin/js/jquery.Jcrop.min.js" type="text/javascript"></script>

<script type="text/javascript">

    var api
        , cropWidth  = $('#w').val()
        , cropHeight = $('#h').val();

    // set default options
    var opt = {};

    // if height and width must be exact, dont allow resizing
    opt.allowResize = false;
    opt.allowSelect = false;

    // initialize jcrop
    api = $.Jcrop('#cropimage', opt);

    // set the selection area [left, top, width, height]
    api.animateTo([0,0,cropWidth,cropHeight]);

    // you can also set selection area without the fancy animation
    api.setSelect([0,0,cropWidth,cropHeight]);

    $(function() {
        $('#cropimage').Jcrop({
            onSelect: updateCoords
        });
    });

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }
</script>
