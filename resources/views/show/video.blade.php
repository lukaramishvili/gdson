<div class="show-container">
    <div class="slider-container slider-container-show">
        <div class="slider slider-show">
            <div class="slide">
                <div class="slide-video-container" style="width: 100%; margin-bottom: 20px">
                    <?php
                    $h_video_cdn_dir = "videos/" . $video->id . "/";
                    $h_video_url = "videos/" . $video->id . "/1920x1080";
                    ?>
                    @if(App\Helper::isMobile())
                        <div class="slide-video" id="home-slide-video-{{ $video->id }}">
                            <video preload="none" controls="controls">
                                <!-- <source type="video/webm" src="//mydomain.com/video.webm"> -->
                                <source type="video/mp4" src="{{ Config::get('services.cloudfront.web-distribution-url') }}/videos/{{ $video->id }}/1280x720.mp4">
                            </video>
                        </div>
                    @else
                        <a class="slide-video show-slide-video" id="show-slide-video-{{ $video->id }}" href="mp4:videos/{{ $video->id }}/1920x1080" data-video-id="{{ $video->id }}" data-video-cdn-dir="{{ $h_video_cdn_dir }}"></a>
                    @endif
                </div>
                <div class="slide-play"></div>
                <div class="slide-buffering-icon"></div>
            </div>
        </div>
    </div>
</div>

<script src="/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>
    window.playerClasses = ["playing", "paused", "stopped", "loading", "buffering"];

    window.onlyOneSliderPlayerClass = function(sliderSelector, activeClass){
        for(var iClass in playerClasses){
            var theclass = playerClasses[iClass];
            if(typeof(activeClass) != 'undefined' && activeClass == theclass){
                //do nothing
            } else {
                $(sliderSelector).removeClass(theclass);
            }
        }
        if(activeClass){
            $(sliderSelector).addClass(activeClass);
        }
    };

    window.sliderPlayerBeingUsed = function(){
        return $(".slider").is(playerClasses.map(function(c){
            return "." + c;
        }).join(","));
    };

    window.onSlideVideoBuffering = function(slider){
        onlyOneSliderPlayerClass(slider, 'buffering');
    };
    window.onSlideVideoPlay = function(slider){
        onlyOneSliderPlayerClass(slider, 'playing');
    };
    window.onSlideVideoPause = function(slider){
        onlyOneSliderPlayerClass(slider, 'paused');
    };
    window.onSlideVideoStop = function(slider){
        onlyOneSliderPlayerClass(slider, 'stopped');
    };



    /* in the case of .show-slide-video, we have only one of .show-slide-video right now but still support multiple items; maybe we'll use ajax in the future and will have multiple .show-slide-video elements */
    //    $(".slide-video").each(function(i, el){
    $(".slide-play").click(function(){
        if(false && sliderPlayerBeingUsed()){
            //clicking play when the player is paused
            //no need to resume manually here, as flowplayer thinks it was clicked
        } else {

            var el = $(this).parents(".slide").find(".slide-video")[0];
            var $slider = $(el).parents(".slider");
            var HTTPstreaming = true;
//
            var $id = $(el).attr('id');
            var trailer_href = $(el).attr('data-target-href');
            var video_id = $(el).attr('data-video-id');
            var video_cdn_dir = $(el).attr('data-video-cdn-dir');
            var sliderLocation = 'none';
            if($slider.hasClass('slider-main')){sliderLocation='main';}
            if($slider.hasClass('slider-show')){sliderLocation='show';}
            var slideRtmpUrl = '{{ Config::get('services.cloudfront.rtmp-distribution-url') }}/cfx/st/';
            var rtmpServerType = 'fms';

            // Returns a random integer between min (included) and max (excluded)
            // Using Math.round() will give you a non-uniform distribution!
            var getRandomInt = function(min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            };
            var getVideoQualityUrl = function(quality){
                //quality should be "1920x1080", "1280x720" or "858x480"
                if(HTTPstreaming){
                    //return "http:{{ Config::get('services.cloudfront.web-distribution-url') }}" + "/" + video_cdn_dir + quality + ".mp4";
                    return "http://vcdn" + getRandomInt(1,15)  + ".gdson.net" + "/" + video_cdn_dir + quality + ".mp4";
                } else {
                    return "mp4:" + video_cdn_dir + quality;
                }
            };

            var addWatchCuepoint = 15000;
            //autobuffer only on show page; on firstpage it would be too much
            //removed; don't waste bandwidth if the user doesn't watch the video
            //var autobuffering_flag = sliderLocation == 'show';
            var autobuffering_flag = sliderLocation == 'show';
            var contextMenu = [];
            if(trailer_href && trailer_href != ""){
                contextMenu.push({'უყურე სერიებს' : function() {
                    location.href = trailer_href;
                }});
                // menu separator
                contextMenu.push('-');
            }
            contextMenu.push({'GDSON-ის შესახებ' : function() {
                location.href = "{{ url('/about') }}";
            }});


            @if(App\Helper::isMobile())

            $("#" + $id).find("video").on("play", function(){
                onSlideVideoPlay($slider);
            });
            $("#" + $id).find("video").on("pause", function(){
                onSlideVideoPause($slider);
            });
            $("#" + $id).find("video").on("ended", function(){
                onSlideVideoStop($slider);
            });


            /* end if isMobile */
            @else


            $f($id,
                    //"{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.commercial-3.2.18.swf",
                    "{{ asset('/cloudfront/flowplayer.commercial-3.2.18.swf') }}",
                    {

                        key: '#$3eb3d3098bb0ffbc3e7',

                        contextMenu: contextMenu,

                        wmode: isFirefox() ? "opaque" : "window",

                        //hide the default flowplayer buffering icon
                        buffering: false,

                        /* this is only fired on the first error of clip,
                         so hide the buffering icon manually
                         */
                        onError: function(errorCode, errorMessage){
                            onSlideVideoStop($slider);
                            $("#" + $id).parents('.slide')
                                    .find('.slide-buffering-icon').hide();
                        },

                        onBeforeKeyPress: function(keyCode){
                            if(keyCode == 37){/* left arrow */
                                this.seek(this.getTime() - 5);
                                return false;
                            }
                            if(keyCode == 39){/* right arrow */
                                this.seek(this.getTime() + 5);
                                return false;
                            }
                        },


                        canvas: {
                            background: '#000000',
                            backgroundGradient: 'none',
                        },

                        // play: null,
                        play: {
                            url: '/swf/play_hover.swf',
                            //flowplayer subtracts some opacity for hover effect,
                            //so provide excess opacity to compensate and add hover effect in swf
                            //1.5 cancelled even the opacity in swf itself, so reduced
                            opacity: 0,
                            label: null,
                            replayLabel: null
                        },


                        plugins: {

                            pseudo: {
                                //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.pseudostreaming-3.2.13.swf"
                                url: "{{ asset('/cloudfront/flowplayer.pseudostreaming-3.2.13.swf') }}"
                            },

                            // bandwidth check plugin
                            bwcheck: {
                                url: "{{ asset('/cloudfront/flowplayer.bwcheck-3.2.13.swf') }}",
                                // CloudFront uses Adobe FMS servers
                                serverType: HTTPstreaming ? 'http' : rtmpServerType,
                                // we use dynamic switching, the appropriate bitrate is switched on the fly
                                dynamic: true,
                                //checkOnStart requires bwchecker application on the server; not supported on wowza
                                checkOnStart: false,
                                dynamicBuffer: true,

                                //the resource on which to test connection speed
                                netConnectionUrl: HTTPstreaming ? getVideoQualityUrl("352x240") /*"{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.bwcheck-3.2.13.swf"*/ : slideRtmpUrl

                            },

                            // bitrate selector plugin
                            bitrateselect: {
                                //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.bitrateselect-3.2.14.swf",
                                url: "{{ asset('/cloudfront/flowplayer.bitrateselect-3.2.14.swf') }}",

                                menu: true,

                                //don't use the HD toggle button or the HD notification splash
                                hdButton: {
                                    place: 'none',
                                    splash: false
                                },

                                onStreamSwitchBegin: function(newItem, currentItem) {
                                    onlyOneSliderPlayerClass($slider, 'buffering');
                                },
                                onStreamSwitch: function(newItem) {
                                    onlyOneSliderPlayerClass($slider, 'playing');
                                }

                            },


                            //bitrate selector menu
                            menu: {
                                //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.menu-3.2.13.swf",
                                url: "{{ asset('/cloudfront/flowplayer.menu-3.2.13.swf') }}",
                                items: [
                                    {label: "ხარისხი:", enabled: false}
                                ],
                                right: 10,/*10*/
                                width: 110,

                                //the button in the dock which opens the menu
                                button: {

                                },
                                menuItem: {
                                    height: 26,
                                    //menu item background color
                                    color: 'rgba(26, 26, 26, 0.9)',
                                    //menu item text color
                                    fontColor: '#d9d9d9',
                                    //hovered menu item background color
                                    overColor: 'rgba(26, 26, 26, 0.9)',
                                    //hovered menu item text color
                                    selectedFontColor: '#db2729'
                                }
                            },


                            // Specify the location of the RTMP plugin.
                            rtmp: {
                                //url: '{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.rtmp-3.2.13.swf',
                                url: "{{ asset('/cloudfront/flowplayer.rtmp-3.2.13.swf') }}",
                                //CloudFront RTMP distribution,e.g. s5c39gqb8ow64r.cloudfront.net
                                //this will get overridden with cluster plugin
                                netConnectionUrl: slideRtmpUrl
                            },

                            //gatracker caused buggy play/seek functionality
                            /*gatracker: {
                             url: "{{ asset('/cloudfront/flowplayer.analytics-3.2.9.swf') }}",
                             events: {
                             all: true
                             },
                             debug: true,
                             accountId: '{{ Config::get('services.ganalytics.account_id') }}'
                             },*/

                            //style control bar
                            controls: {
                                //url: "{{ Config::get('services.cloudfront.web-distribution-url') }}/flowplayer.controls-3.2.16.swf",
                                url: "{{ asset('/cloudfront/flowplayer.controls-gdson-3.2.16.swf') }}",

                                // customize the appearance to make the control bar transparent
                                // colors can be in rgba() too
                                //whole controlbar background
                                backgroundColor: "rgba(26, 26, 26, 0.9)",
                                backgroundGradient: "none",
                                //border radius
                                borderRadius: '0',
                                //whole seek bar background
                                sliderColor: '#404040',
                                sliderBorder: '0px',
                                //buffered line background
                                bufferColor: '#bcbec0',
                                //progress line background
                                progressColor: '#db2729',
                                progressGradient: 'none',
                                //all buttons foreground color
                                buttonColor: '#b2b2b2',
                                buttonOverColor: '#db2729',

                                //volume full line background color
                                volumeSliderColor: '#404040',
                                volumeBorder: 'none',

                                //current time
                                timeColor: '#db2729',
                                timeSeparator: ' / ',
                                //clip duration
                                durationColor: '#666666',
                                timeBorder: '0',
                                timeBgColor: 'transparent',

                                tooltipColor: 'rgba(255, 255, 255, 0.7)',
                                tooltipTextColor: '#000000'

                            },
                        },

                        // Configure Flowplayer to use the RTMP plugin for streaming.
                        clip: {
                            urlResolvers: HTTPstreaming
                                    ? ['bwcheck', 'bitrateselect']    //, 'cluster'
                                    : ['bwcheck', 'bitrateselect']   //, 'cluster'
                            ,
                            provider: HTTPstreaming ? 'pseudo'/*http*/ : 'rtmp',
                            autoPlay: false,
                            scaling: 'fit',


                            //connectionProvider: HTTPstreaming ? null : 'cluster',


                            //flowplayer doesn't play until the buffer is loaded fully
                            //so increasing it would delay video start
                            bufferLength: 3,

                            autoBuffering: autobuffering_flag,
                            //wmode: 'direct' should be specified at the top of this config
                            // accelerated: true,

                            // available bitrates and the corresponding files. We specify also the video width
                            // here, so that the player does not use a too large file. It switches to a
                            // file/stream with larger dims when going fullscreen if the available b/w permits.
                            bitrates: [

                                @if(Auth::check() && Auth::user()->hasRole('Admin'))

                                //{ url: getVideoQualityUrl("352x240"), width: 352, bitrate: 780, label: "240p" }, /* 350kbps/0.8/0.8/0.7  */
                                //{ url: getVideoQualityUrl("480x360"), width: 480, bitrate: 1350, label: "360p" }, /* 600kbps/0.8/0.8/0.7  */
                                { url: getVideoQualityUrl("858x480"), width: 854/*858*/, bitrate: 200/* vbr 0.5-2*/, label: "480p"
                                }, /* 1000kbps/0.8/0.8/0.7 */
                                { url: getVideoQualityUrl("1280x720"), width: 1280/*1280*/, bitrate: 3000/* vbr 1.5-3 */, label: "720p",
                                    // this is the default bitrate, the playback kicks off with this and after that
                                    // Quality Of Service monitoring adjusts to the most appropriate bitrate
                                    isDefault: true
                                }, /* 2200kbps/0.8/0.8/0.7 */
                                { url: getVideoQualityUrl("1920x1080"), width: 1280/*1920*/, bitrate: 4000/* vbr 2.25-4 */, label: "1080p" } /* 4500kbps/0.8/0.8/0.7 */

                                @else
                                //{ url: getVideoQualityUrl("352x240"), width: 352, bitrate: 780, label: "240p" }, /* 350kbps/0.8/0.8/0.7  */
                                //{ url: getVideoQualityUrl("480x360"), width: 480, bitrate: 1350, label: "360p" }, /* 600kbps/0.8/0.8/0.7  */
                                { url: getVideoQualityUrl("858x480"), width: 858, bitrate: 2230, label: "480p",
                                    // this is the default bitrate, the playback kicks off with this and after that
                                    // Quality Of Service monitoring adjusts to the most appropriate bitrate
                                    isDefault: true
                                }, /* 1000kbps/0.8/0.8/0.7 */
                                { url: getVideoQualityUrl("1280x720"), width: 1280, bitrate: 4900, label: "720p" }, /* 2200kbps/0.8/0.8/0.7 */
                                { url: getVideoQualityUrl("1920x1080"), width: 1920, bitrate: 10000, label: "1080p" } /* 4500kbps/0.8/0.8/0.7 */
                                @endif

                            ],/* bitrates */

                            onCuepoint: [
                                // each integer represents milliseconds in the timeline
                                [addWatchCuepoint],

                                // this function is triggered when a cuepoint is reached
                                function(clip, cuepoint) {
                                    if(cuepoint == addWatchCuepoint){
                                        $.ajax({
                                            url: "{{ url('/video/addwatch') }}" + "/" + video_id + "/geo",
                                            type: "POST",
                                            dataType: "json",
                                            success: function(data){
                                            }
                                        });
                                    }
                                }
                            ],

                            onStart: function(){
                                //resume autohiding the controls when paused
                                $f($id).getControls().setAutoHide({enabled:true,hideDelay:1000});
                                //
                                onSlideVideoPlay($slider);
                                if(sliderLocation=='show'){
                                    var episode_id = $("#" + $id).attr('data-episode-id');
                                    var saved_last_pos = localStorage.getItem("lastwatch_episode_" + episode_id);
                                    var curEpisodePlayer = $f($id);
                                    var maxTimeToSave = parseInt(curEpisodePlayer.getClip().duration) - 180;
                                    if(episode_id && parseInt(saved_last_pos) > 0 && saved_last_pos < maxTimeToSave){
                                        setTimeout(function(){
                                            $f($id).seek(parseInt(saved_last_pos));
                                        }, 100);
                                    } else { console.log(JSON.stringify(saved_last_pos)); }
                                }
                            },
                            onPause: function(){
                                onSlideVideoPause($slider);
                                //don't autohide the controls when paused
                                $f($id).getControls().setAutoHide({enabled:false});
                            },
                            onResume: function(){
                                //resume autohiding the controls when paused
                                $f($id).getControls().setAutoHide({enabled:true,hideDelay:1000});
                                //
                                onSlideVideoPlay($slider);
                            },
                            onFinish: function(){
                                onSlideVideoStop($slider);
                            },
                            onBufferEmpty: function(){
                                //onSlideVideoBuffering($slider);
                            }

                        }


                    });//$f() - flowplayer flash


            /* end if isMobile else  */
            @endif


              }//end else {} of if(sliderPlayerBeingUsed())

    });/* end $(".slide-play").click(), previously $(".slide-video").each()  */




    $(".slider .slide .slide-play").click(function(){

        var el = $(this).parents(".slide").find(".slide-video")[0];
        var $id = $(el).attr('id');
        var $slide = $(this).parents(".slide");
        //$(this).hide();
        //$slide.find(".slide-video").show();
        var $slider = $(this).parents(".slider");
        //this class is also added by $f onStart, but not if stream not found
        onlyOneSliderPlayerClass($slider, 'loading');

        var $video = $slide.find(".slide-video");

        @if(App\Helper::isMobile())
        $video.find("video")[0].play();
        @else
            $f($video.attr('id')).play();
        @endif
        });/* $(".slider .slide .slide-play").click() */
</script>