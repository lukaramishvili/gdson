@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit-header">
                        სერიალი - {{ $show->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სეზონი - {{ $season->getTitle('geo') }}
                    </div>
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართულად</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Ruski</a></li>
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">ვიდეო</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                            <div class="col-md-8 no-padding">
                                <div class="post-title">
                                    <div class="add-post-label">
                                        პოსტის სათაური
                                    </div>
                                    <a href="javascript:;" id="post-title-geo" data-type="text" data-pk="{{ $post->id }}" data-name="geo" data-original-title="პოსტის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $post['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="post-description">
                                    <div class="add-post-label">
                                        პოსტის აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="post-description-geo" data-pk="{{ $post->id }}" data-name="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $post['desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right post-description-save" data-textarea="post-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="short-description">
                                    <div class="add-post-label">
                                        პოსტის მოკლე აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="short-description-geo" data-whose="post" data-whose-id="{{ $post->id }}" data-lang="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $post['short_desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right short-description-save" data-textarea="short-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="post-visibility">
                                    <div class="add-show-label">
                                        გამოქვეყნება
                                    </div>
                                    <input type="checkbox" id="post-visibility" class="make-switch" data-on-text="Yes" data-off-text="No"
                                    @if($post->visibility) checked @endif>
                                </div>

                                @foreach($professions as $key => $profession)
                                    <div class="post-actors">
                                        <div class="add-post-label">
                                            {{ $profession }}
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            {!! Form::select('post-persons[]', $persons, isset($default_persons[$profession]) ? $default_persons[$profession] : null, ['data-profession-id' => $key, 'class' => 'form-control post-persons', 'multiple' => 'multiple']) !!}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endforeach

                                <div class="post-video">
                                    <div class="add-episode-label">
                                        პოსტის ვიდეო
                                    </div>
                                    {!! Form::open(['url' => '/show/savepostvideo', 'id' => 'video-form-geo', 'enctype' => 'multipart/form-data', 'target' => 'hidden_video_geo']) !!}
                                        {!! Form::hidden('post_id', $post->id) !!}
                                        {!! Form::hidden('lang', 'geo') !!}
                                        {!! Form::file('video', ['class' => '', 'id' => 'video-geo']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_video_geo" name="hidden_video_geo" style="display: none;"></iframe>
                                </div>
                                <div class="post-video-url">
                                    <div class="add-post-label">
                                        პოსტის ვიდეოს მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}videos/{{ $post->video_id }}/">
                                        {{ '/video/' . $post->video_id . '/' }}
                                    </a>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <a href="{{ url('/show/editseason/' . $show->alias . '/' . $season->season_number) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    დამატება
                                                @else
                                                    უკან დაბრუნება
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <button class="btn btn-primary btn-circle season-post-cancel">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="post-cover">
                                    <div class="add-post-label">
                                        პოსტის ქავერი
                                    </div>
                                    {!! Form::open(['url' => '/show/savecover', 'id' => 'cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload']) !!}
                                        {!! Form::hidden('target_id', $post->id) !!}
                                        {!! Form::hidden('target', 'post') !!}
                                        {!! Form::file('cover', ['class' => '', 'id' => 'cover']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload" name="hidden_upload" style="display:none"></iframe>
                                </div>
                                {{--<div class="post-cover-image">
                                    <div class="add-post-label">
                                        <img id="cover-image" src="{{ $post['cover'] }}" class="col-md-10 no-padding">
                                        <span class="col-md-1 glyphicon glyphicon-remove remove-cover-img"
                                              data-target="post" data-target-id="{{ $post->id }}"></span>
                                        <input type="hidden" id="cover-target" value="post">
                                        <input type="hidden" id="cover-target-id" value="{{ $post->id }}">
                                    </div>
                                </div>--}}
                                <div class="post-image-url">
                                    <div class="add-post-label">
                                        სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}post/{{ $post->id }}/">
                                        {{ '/post/' . $post->id . '/cover.jpg' }}
                                    </a>
                                </div>
                                @if(isset($post['cover_image']))
                                    <div class="episode-cover-mage">
                                        <div class="add-episode-label">
                                            <img id="episode-cover-image" src="{{ $post['cover_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endif
                                <div class="post-slider">
                                    <div class="add-post-label">
                                        პოსტის სლაიდერი
                                    </div>
                                    {!! Form::open(['url' => '/show/saveslider', 'id' => 'slider-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_slider_upload']) !!}
                                        {!! Form::hidden('post_id', $post->id) !!}
                                        {!! Form::file('slider', ['class' => '', 'id' => 'slider']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_slider_upload" name="hidden_slider_upload" style="display: none;"></iframe>
                                </div>
                                {{--<div class="post-slider-image">
                                    <div class="add-post-label slider-images">
                                        @foreach($sliders as $key => $slider)
                                            <div class="row slider-row">
                                                <img class="slider-image col-md-4" src="{{ $slider }}">
                                                <span class="col-md-1 no-padding glyphicon glyphicon-remove remove-slider-img"
                                                      data-slider-img="{{ $slider }}"></span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>--}}
                                <div class="post-slider-image-url">
                                    <div class="add-episode-label">
                                        სლაიდერის სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}post/{{ $post->id }}/">
                                        {{ '/post/' . $post->id . '/imageName.jpg' }}
                                    </a>
                                </div>
                            </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="post-title">
                                    <div class="add-post-label">
                                        Post title
                                    </div>
                                    <a href="javascript:;" id="post-title-eng" data-type="text" data-pk="{{ $post->id }}" data-name="eng" data-original-title="პოსტის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $post['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="post-description">
                                    <div class="add-post-label">
                                        Post description
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="post-description-eng" data-pk="{{ $post->id }}" data-name="eng" rows="6" style="visibility: hidden; display: none;">
                                            {{ $post['desc_tr']['value_eng'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right post-description-save" data-textarea="post-description-eng">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="post-video">
                                    <div class="add-post-label">
                                        Post video
                                    </div>
                                    {!! Form::open(['url' => '/show/savepostvideo', 'id' => 'video-form-eng', 'enctype' => 'multipart/form-data', 'target' => 'hidden_video_eng']) !!}
                                        {!! Form::hidden('post_id', $post->id) !!}
                                        {!! Form::hidden('lang', 'eng') !!}
                                        {!! Form::file('video', ['class' => '', 'id' => 'video-eng']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_video_eng" name="hidden_video_eng" style="display: none;"></iframe>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <a href="{{ url('/show/editseason/' . $show->alias . '/' . $season->season_number) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <button class="btn btn-primary btn-circle season-post-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="post-title">
                                    <div class="add-post-label">
                                        Post title(Russian)
                                    </div>
                                    <a href="javascript:;" id="post-title-rus" data-type="text" data-pk="{{ $post->id }}" data-name="rus" data-original-title="პოსტის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $post['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="post-description">
                                    <div class="add-post-label">
                                        Post Description (Russian)
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="post-description-rus" data-pk="{{ $post->id }}" data-name="rus" rows="6" style="visibility: hidden; display: none;">
                                            {{ $post['desc_tr']['value_rus'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right post-description-save" data-textarea="post-description-rus">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="post-video">
                                    <div class="add-post-label">
                                        Post video(Russian)
                                    </div>
                                    {!! Form::open(['url' => '/show/savepostvideo', 'id' => 'video-form-rus', 'enctype' => 'multipart/form-data', 'target' => 'hidden_video_rus']) !!}
                                        {!! Form::hidden('post_id', $post->id) !!}
                                        {!! Form::hidden('lang', 'rus') !!}
                                        {!! Form::file('video', ['class' => '', 'id' => 'video-rus']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_video_rus" name="hidden_video_rus" style="display: none;"></iframe>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <a href="{{ url('/show/editseason/' . $show->alias . '/' . $season->season_number) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="post-back">
                                    <div class="add-post-label">
                                        <button class="btn btn-primary btn-circle season-post-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="video">
                                <?php $video = \App\Video::find($post->video_id); ?>
                                <div class="col-md-6">
                                    @include('show.video', ['video' => $video])
                                </div>
                                <div class="col-md-6">
                                    <div class="crop-cover">
                                        <div class="add-post-label">
                                            სეზონის პოსტის სურათი
                                        </div>
                                        {!! Form::open(['url' => '/show/savecrop', 'id' => 'trailer-video-crop-image-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_crop_image_trailer']) !!}
                                            {!! Form::hidden('video_id', $video->id) !!}
                                            {!! Form::hidden('type', 'image') !!}
                                            {!! Form::file('crop-cover', ['class' => '', 'id' => 'crop-image']) !!}
                                        {!! Form::close() !!}
                                        <iframe id="hidden_crop_image_trailer" name="hidden_crop_image_trailer" style="display: none;"></iframe>
                                    </div>
                                    <div class="crop-image-div">
                                        <div class="add-post-label">
                                            <img id="crop-trailer-img" src="@if(is_file($video->getCroppedImagePath()))
                                                                                {{ $video->getCroppedImage() }}
                                                                            @else
                                                                                {{ $video->getImageToCrop() }}
                                                                            @endif" class="col-md-4 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a href="{{ url('/show/crop/' . $video->id . '/image/editseasonpost') }}">
                                        <button class="btn btn-circle btn-success" style="margin: 10px 20px 10px 0;"    >
                                            Crop image
                                        </button>
                                    </a>
                                    {!! Form::open(['url' => '/show/uploadcroppedpost', 'style' => 'display: inline-block;']) !!}
                                        {!! Form::hidden('target', 'post') !!}
                                        {!! Form::hidden('target_id', $post->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::submit('Upload', ['class' => 'btn btn-circle btn-success']) !!}
                                    {!! Form::close() !!}
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="show-id" value="{{ $show->id }}">
        <input type="hidden" class="show-alias" value="{{ $show->alias }}">
        <input type="hidden" class="season-id" value="{{ $season->id }}">
        <input type="hidden" class="post-id" value="{{ $post->id }}">
    </div>
    <!-- END CONTENT -->

@endsection