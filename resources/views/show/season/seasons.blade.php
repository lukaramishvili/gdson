@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#seasons" aria-controls="seasons" role="tab" data-toggle="tab">სეზონები</a></li>
                            <li role="presentation"><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">პოსტები</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seasons">
                                @foreach($seasons as $key => $season)
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet light">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-speech"></i>
                                                <span class="caption-subject bold uppercase">სეზონი {{ $season->season_number }}</span>
                                            </div>
                                            <div class="actions">
                                                <a href="{{ url('/show/editseason/' . $show->alias . '/' . $season->season_number) }}" class="btn btn-circle btn-default">
                                                    <i class="fa fa-pencil"></i> Edit </a>
                                                <a href="{{ url('/show/addepisode/' . $show->alias . '/' . $season->season_number) }}" class="btn btn-circle btn-default">
                                                    <i class="fa fa-plus"></i> Add episode </a>
                                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="scroller" style="max-height: 300px; overflow-y: auto;" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                                                <div class="row">
                                                    <div class="col-md-4 blog-img blog-tag-data">
                                                        @if ($key != 0)
                                                            <a href="{{ url('/show/changeseasonorder/' . $show->id . '/' . $season->season_number . '/up') }}">
                                                                <span class="glyphicon glyphicon-chevron-up seasons-number-change-btn"></span>
                                                            </a>
                                                        @endif
                                                        <img src="{{ $season['cover'] }}" alt="" class="img-responsive">
                                                        @if ($key != count($seasons) - 1)
                                                                <a href="{{ url('/show/changeseasonorder/' . $show->id . '/' . $season->season_number . '/down') }}">
                                                                <span class="glyphicon glyphicon-chevron-down seasons-number-change-btn"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-8 blog-article">
                                                        <table class="table table-responsive table-striped">
                                                            <tbody>
                                                            @foreach($season['episodes'] as $episode)
                                                                <tr>
                                                                    <td class="col-md-8 seasons-episode-title">
                                                                        {{ $episode['title_tr']['value_geo'] }}
                                                                    </td>
                                                                    <td class="col-md-4">
                                                                        <a href="{{ url('/show/editepisode/' . $show->alias . '/' . $season->season_number . '/' . $episode['episode_number']) }}" class="btn btn-circle btn-default">
                                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                        <a href="{{ url('/show/deleteepisode/' . $episode->id) }}" class="btn btn-circle btn-default delete-episode">
                                                                            <i class="fa fa-remove"></i> Delete </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Portlet PORTLET-->
                                @endforeach
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ url('/show/addseason/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                სეზონის დამატება
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="posts">
                                <div class="show-posts">
                                    @foreach($posts as $post)
                                        <div class="show-post">
                                            <h2>{{ $post['title_tr']['value_geo'] }}</h2>
                                            <p>{{ str_limit($post['desc_tr']['value_geo'], 200) }}</p>
                                            <a href="{{ url('/show/editshowpost/' . $post->id) }}">
                                                <button class="btn btn-primary btn-circle">
                                                    რედაქტირება
                                                </button>
                                            </a>
                                            <hr/>
                                        </div>
                                    @endforeach
                                </div>
                                <a href="{{ url('/show/addshowpost/' . $show->alias) }}">
                                    <button class="btn btn-primary btn-circle">
                                        პოსტის დამატება
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
