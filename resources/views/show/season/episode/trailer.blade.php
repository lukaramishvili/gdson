@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit-header">
                        სერიალი - {{ $show->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სეზონი - {{ $season->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სერია - {{ $episode->getTitle('geo') }}
                    </div>
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართული</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">ვიდეო</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                                <div class="trailer-title">
                                    <div class="add-show-label">
                                        ვიდეოს სათაური
                                    </div>
                                    <a href="javascript:;" id="trailer-title-geo" data-type="text" data-pk="{{ $video->id }}" data-name="geo" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $video['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="trailer-desc">
                                    <div class="add-show-label">
                                        ვიდეოს აღწერა
                                    </div>
                                    <a href="javascript:;" id="trailer-desc-geo" data-type="textarea" data-pk="{{ $video->id }}" data-name="geo" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['desc_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="trailer-subtitle">
                                    <div class="add-show-label">
                                        ვიდეოს სუბტიტრები
                                    </div>
                                    <a href="javascript:;" id="trailer-subt-geo" data-type="textarea" data-pk="{{ $video->id }}" data-name="geo" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['subtitle_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="show-title">
                                    <div class="add-show-label">
                                        ვიდეოს მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix=videos/{{ $video->id }}/">
                                        {{ '/videos/' . $video->id . '/1920x1080.mp4' }}
                                    </a>
                                </div>
                                <div class="show-trailer">
                                    <div class="add-show-label">
                                        ეპიზოდის ტრეილერის სურათი (cover.jpg)
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-geo', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_geo']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'geo') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-geo']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_geo" name="hidden_trailer_geo" style="display: none;"></iframe>
                                </div>
                                <div class="show-image-url">
                                    <div class="add-episode-label">
                                        ტრეილერის სურათის მისამართი (cover.jpg)
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}episode/{{ $episode->id }}/video/{{ $video->id }}/geo/">
                                        {{ '/episode/' . $episode->id . '/video/' . $video->id . '/geo/cover.jpg' }}
                                    </a>
                                </div>
                                @if(isset($video['cover_image']))
                                    <div class="episode-cover-mage">
                                        <div class="add-episode-label">
                                            <img id="episode-cover-image" src="{{ $video['cover_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endif
                                <div class="clearfix"></div>

                                <div class="episode-trailer-image">
                                    <div class="add-episode-label">
                                        სერიალის ტრეილერის სურათი (image.jpg)
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailerimage', 'id' => 'trailer-image-form-geo', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_image_geo']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'geo') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-image-geo']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_image_geo" name="hidden_trailer_image_geo" style="display: none;"></iframe>
                                </div>
                                <div class="show-image-url">
                                    <div class="add-episode-label">
                                        ტრეილერის სურათის მისამართი (image.jpg)
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}episode/{{ $episode->id }}/video/{{ $video->id }}/geo/">
                                        {{ '/episode/' . $episode->id . '/video/' . $video->id . '/geo/iamge.jpg' }}
                                    </a>
                                </div>
                                @if(isset($video['post_image']))
                                    <div class="episode-cover-mage">
                                        <div class="add-episode-label">
                                            <img id="episode-cover-image" src="{{ $video['post_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endif
                                <div class="clearfix"></div>

                                <div class="trailer-visibility">
                                    <div class="add-episode-label">
                                        ჰაილაიტი
                                    </div>
                                    <input type="checkbox" class="make-switch trailer-highlight" data-on-text="Yes" data-off-text="No"
                                           data-video-id="{{ $video->id }}" @if($highlight) checked @endif>
                                </div>
                                <div class="trailer-back">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/editepisode/' . $show->alias . '/' . $season->season_number . '/' . $episode->episode_number) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    დამატება
                                                @else
                                                    უკან დაბრუნება
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/cancelepisodevideo/' . $video->id . '/' . $show->alias . '/' . $season->season_number . '/' . $episode->episode_number) }}">
                                            <button class="btn btn-primary btn-circle video-cancel">
                                                @if($type == 'add')
                                                    გაუქმება
                                                @else
                                                    წაშლა
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="trailer-title">
                                    <div class="add-show-label">
                                        Video title
                                    </div>
                                    <a href="javascript:;" id="trailer-title-eng" data-type="text" data-pk="{{ $video->id }}" data-name="eng" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $video['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="trailer-desc">
                                    <div class="add-show-label">
                                        Video description
                                    </div>
                                    <a href="javascript:;" id="trailer-desc-eng" data-type="textarea" data-pk="{{ $video->id }}" data-name="eng" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['desc_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="trailer-subtitle">
                                    <div class="add-show-label">
                                        Video subtitles
                                    </div>
                                    <a href="javascript:;" id="trailer-subt-eng" data-type="textarea" data-pk="{{ $video->id }}" data-name="eng" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['subtitle_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="show-trailer">
                                    <div class="add-show-label">
                                        Show trailer
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-eng', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_eng']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'eng') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-eng']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_eng" name="hidden_trailer_eng" style="display: none;"></iframe>
                                </div>
                                <div class="show-trailer-image">
                                    <div class="add-episode-label">
                                        <img id="trailer-image-eng" src="{{ $image['eng'] }}" class="col-md-4 no-padding">
                                        <span class="col-md-1 glyphicon glyphicon-remove remove-video-img"
                                              data-target="episode" data-target-id="{{ $episode->id }}" data-lang="eng"></span>
                                        <input type="hidden" id="video-target" value="episode">
                                        <input type="hidden" id="video-target-id" value="{{ $episode->id }}">
                                        <input type="hidden" id="video-image-id" value="{{ $video->id }}">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="trailer-title">
                                    <div class="add-show-label">
                                        Video title (Russian)
                                    </div>
                                    <a href="javascript:;" id="trailer-title-rus" data-type="text" data-pk="{{ $video->id }}" data-name="rus" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $video['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="trailer-title">
                                    <div class="add-show-label">
                                        Video description (Russian)
                                    </div>
                                    <a href="javascript:;" id="trailer-desc-rus" data-type="textarea" data-pk="{{ $video->id }}" data-name="rus" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['desc_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="trailer-title">
                                    <div class="add-show-label">
                                        Video subtitles (Russian)
                                    </div>
                                    <a href="javascript:;" id="trailer-subt-rus" data-type="textarea" data-pk="{{ $video->id }}" data-name="rus" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                        {{ $video['subtitle_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="show-trailer">
                                    <div class="add-show-label">
                                        Show trailer (Russian)
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-rus', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_rus']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'rus') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-rus']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_rus" name="hidden_trailer_rus" style="display: none;"></iframe>
                                </div>
                                <div class="show-trailer-image">
                                    <div class="add-episode-label">
                                        <img id="trailer-image-rus" src="{{ $image['rus'] }}" class="col-md-4 no-padding">
                                        <span class="col-md-1 glyphicon glyphicon-remove remove-video-img"
                                              data-target="episode" data-target-id="{{ $episode->id }}" data-lang="rus"></span>
                                        <input type="hidden" id="video-target" value="episode">
                                        <input type="hidden" id="video-target-id" value="{{ $episode->id }}">
                                        <input type="hidden" id="video-image-id" value="{{ $video->id }}">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="col-md-6">
                                    @include('show.video', ['video' => $video])
                                </div>
                                <div class="col-md-6">
                                    <div class="crop-cover">
                                        <div class="add-episode-label">
                                            სერიის ტრეილერის სურათი (cover.jpg)
                                        </div>
                                        {!! Form::open(['url' => '/show/savecrop', 'id' => 'trailer-video-crop-cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_crop_cover_trailer']) !!}
                                            {!! Form::hidden('video_id', $video->id) !!}
                                            {!! Form::hidden('type', 'cover') !!}
                                            {!! Form::file('crop-cover', ['class' => '', 'id' => 'crop-cover']) !!}
                                        {!! Form::close() !!}
                                        <iframe id="hidden_crop_cover_trailer" name="hidden_crop_cover_trailer" style="display: none;"></iframe>
                                    </div>
                                    <div class="crop-image-div">
                                        <div class="add-season-label">
                                            <img id="crop-trailer-img" src="@if(is_file($video->getCroppedCoverPath()))
                                                                                {{ $video->getCroppedCover() }}
                                                                            @else
                                                                                {{ $video->getCoverToCrop() }}
                                                                            @endif" class="col-md-4 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a href="{{ url('/show/crop/' . $video->id . '/cover/editepisodetrailer') }}">
                                        <button class="btn btn-circle btn-success" style="margin: 10px 20px 10px 0;">
                                            Crop image
                                        </button>
                                    </a>
                                    {!! Form::open(['url' => '/show/uploadcropped', 'style' => 'display: inline-block;']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'geo') !!}
                                        {!! Form::hidden('type', 'cover') !!}
                                        {!! Form::submit('Upload', ['class' => 'btn btn-circle btn-success']) !!}
                                    {!! Form::close() !!}

                                    <div class="clearfix"></div>

                                    <div class="crop-image">
                                        <div class="add-episode-label">
                                            სერიის ტრეილერის სურათი (image.jpg)
                                        </div>
                                        {!! Form::open(['url' => '/show/savecrop', 'id' => 'trailer-video-crop-image-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_crop_image_trailer']) !!}
                                            {!! Form::hidden('video_id', $video->id) !!}
                                            {!! Form::hidden('type', 'image') !!}
                                            {!! Form::file('crop-cover', ['class' => '', 'id' => 'crop-image']) !!}
                                        {!! Form::close() !!}
                                        <iframe id="hidden_crop_image_trailer" name="hidden_crop_image_trailer" style="display: none;"></iframe>
                                    </div>
                                    <div class="crop-image-div">
                                        <div class="add-episode-label">
                                            <img id="crop-trailer-img" src="@if(is_file($video->getCroppedImagePath()))
                                                                                {{ $video->getCroppedImage() }}
                                                                            @else
                                                                                {{ $video->getImageToCrop() }}
                                                                            @endif" class="col-md-4 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a href="{{ url('/show/crop/' . $video->id . '/image/editepisodetrailer') }}">
                                        <button class="btn btn-circle btn-success" style="margin: 10px 20px 10px 0;">
                                            Crop image
                                        </button>
                                    </a>
                                    {!! Form::open(['url' => '/show/uploadcropped', 'style' => 'display: inline-block;']) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::hidden('lang', 'geo') !!}
                                        {!! Form::hidden('type', 'image') !!}
                                        {!! Form::submit('Upload', ['class' => 'btn btn-circle btn-success']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection