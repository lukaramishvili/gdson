@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit-header">
                        სერიალი - {{ $show->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სეზონი - {{ $season->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სერია - {{ $episode->getTitle('geo') }}
                    </div>
                    <div>
                        <!-- Nav tabs -->
                        <?php $episodeNotification = $episode->getFbNotification(); ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართულად</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Russian</a></li>
                            @if(isset($posts))<li role="presentation"><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">პოსტები</a></li>@endif
                            <li role="presentation"><a href="#episode-video" aria-controls="episode-video" role="tab" data-toggle="tab">სერიის ვიდეო</a></li>
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">თრეილერის ვიდეო</a></li>
                            <li role="presentation"><a href="#video-image" aria-controls="video-image" role="tab" data-toggle="tab">ვიდეოს სურათი</a></li>
                            @if($type == 'edit' && isset($episodeNotification) && !empty($episodeNotification))<li role="presentation"><a href="#fb" aria-controls="fb" role="tab" data-toggle="tab">Facebok</a></li>@endif
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                            <div class="col-md-8 no-padding">
                                <div class="episode-number">
                                    <h2>
                                        სეზონი {{ $season->season_number }} სერია {{ $episode->episode_number }}
                                    </h2>
                                </div>
                                <div class="episode-title">
                                    <div class="add-episode-label">
                                        სერიის სათაური
                                    </div>
                                    <a href="javascript:;" id="episode-title-geo" data-type="text" data-pk="{{ $episode->id }}" data-name="geo" data-original-title="სერიის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $episode['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="episode-description">
                                    <div class="add-episode-label">
                                        სერიის აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="episode-description-geo" data-pk="{{ $episode->id }}" data-name="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $episode['desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right episode-description-save" data-textarea="episode-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="short-description">
                                    <div class="add-episode-label">
                                        სერიის მოკლე აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="short-description-geo" data-whose="episode" data-whose-id="{{ $episode->id }}" data-lang="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $episode['short_desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right short-description-save" data-textarea="short-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="episode-publish-date">
                                    <div class="add-episode-label">
                                        გამოქვეყნების თარიღი
                                    </div>
                                    <input id="publish-date" class="datetime" name="publish-date" data-target="episode" data-target-id="{{ $episode->id }}"
                                           data-field="publish_date" data-datetime="{{ $episode['publish_datetime'] }}">
                                </div>
                                <div class="episode-release-date">
                                    <div class="add-episode-label">
                                        სერიის გამოსვლის თარიღი
                                    </div>
                                    <input id="release-date" class="datetime" name="release-date" data-target="episode" data-target-id="{{ $episode->id }}"
                                           data-field="release_date" data-datetime="{{ $episode['release_datetime'] }}">
                                </div>
                                <div class="episode-visibility">
                                    <div class="add-episode-label">
                                        გამოქვეყნება
                                    </div>
                                    <input type="checkbox" id="episode-visibility" class="make-switch" data-on-text="Yes" data-off-text="No"
                                    @if($episode->visibility) checked @endif>
                                </div>
                                <div class="episode-notification">
                                    <div class="add-episode-label">
                                        facebook notification
                                    </div>
                                    <input type="checkbox" id="fb-notification" class="make-switch" data-on-text="Yes" data-off-text="No"
                                           data-whose-id="{{ $episode->id }}" data-whose-type="episode" @if($episode->fbNotification()) checked @endif>
                                </div>
                                <div class="episode-number">
                                    <div class="add-episode-label">
                                        ეპიზოდის ნომერი
                                    </div>
                                    <div class="col-md-1 no-padding">
                                        {!! Form::select('episode-number', $episode['series_count'], $episode->episode_number, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                @foreach($professions as $key => $profession)
                                    <div class="episode-actors">
                                        <div class="add-episode-label">
                                            {{ $profession }}
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            {!! Form::select('crew-persons[]', $persons, isset($default_persons[$profession]) ? $default_persons[$profession] : null,
                                                array(
                                                    'data-target'        => 'episode',
                                                    'data-target-id'     => $episode->id,
                                                    'data-profession-id' => $key,
                                                    'class'              => 'form-control crew-persons',
                                                    'multiple'           => 'multiple'
                                                )
                                            ) !!}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endforeach

                                <div class="actings">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/editactings/episode/' . $episode->id) }}">
                                            <button class="btn btn-primary btn-circle">
                                                პერსონაჟების რედაქტირება
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="episode-alias">
                                    <div class="add-episode-label">
                                        სერიის ალიასი
                                    </div>
                                    <a href="javascript:;" id="episode-alias" data-type="text" data-pk="{{ $episode->id }}" data-placeholder="სერიის ალიასი" data-original-title="Enter comments">
                                        {{ $episode->alias }}
                                    </a>
                                </div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    დამატება
                                                @else
                                                    უკან დაბრუნება
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <button class="btn btn-primary btn-circle episode-cancel">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="episode-cover">
                                    <div class="add-episode-label">
                                        სერიის სურათი
                                    </div>
                                    {!! Form::open(['url' => '/show/savecover', 'id' => 'cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload']) !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::file('cover', ['class' => '', 'id' => 'cover']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload" name="hidden_upload" style="display:none"></iframe>
                                </div>
                                <div class="episode-image-url">
                                    <div class="add-episode-label">
                                        სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}episode/{{ $episode->id }}/">
                                        {{ '/episode/' . $episode->id . '/cover.jpg' }}
                                    </a>
                                </div>
                                @if(isset($episode['cover_image']))
                                    <div class="episode-cover-mage">
                                        <div class="add-episode-label">
                                            <img id="episode-cover-image" src="{{ $episode['cover_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif

                                <div class="episode-cover-second">
                                    <div class="add-episode-label">
                                        სერიის სურათი (2x)
                                    </div>
                                    {!! Form::open(['url' => '/show/savecoversecond', 'id' => 'cover-form-second', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload_second']) !!}
                                        {!! Form::hidden('target_id', $episode->id) !!}
                                        {!! Form::hidden('target', 'episode') !!}
                                        {!! Form::file('cover-second', ['class' => '', 'id' => 'cover-second']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload_second" name="hidden_upload_second" style="display: none;"></iframe>
                                </div>
                                <div class="episode-image-url-second">
                                    <div class="add-episode-label">
                                        სურათის მისამართი (2x)
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}episode/{{ $episode->id }}/">
                                        {{ '/episode/' . $episode->id . '/cover@2x.jpg' }}
                                    </a>
                                </div>
                                @if(isset($episode['cover_image_second']))
                                    <div class="episode-cover-image-second">
                                        <div class="add-episode-label">
                                            <img id="episode-cover-image-second" src="{{ $episode['cover_image_second'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif
                            </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="episode-number">
                                    <h2>
                                        season {{ $season->season_number }} episode {{ $episode->episode_number }}
                                    </h2>
                                </div>
                                <div class="episode-title">
                                    <div class="add-episode-label">
                                        Episode title
                                    </div>
                                    <a href="javascript:;" id="episode-title-eng" data-type="text" data-pk="{{ $episode->id }}" data-name="eng" data-original-title="სერიის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $episode['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="episode-description">
                                    <div class="add-episode-label">
                                        Episode description
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="episode-description-eng" data-pk="{{ $episode->id }}" data-name="eng" rows="6" style="visibility: hidden; display: none;">
                                            {{ $episode['desc_tr']['value_eng'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right episode-description-save" data-textarea="episode-description-eng">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <button class="btn btn-primary btn-circle episode-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="episode-number">
                                    <h2>
                                        season {{ $season->season_number }} episode {{ $episode->episode_number }}
                                    </h2>
                                </div>
                                <div class="episode-title">
                                    <div class="add-episode-label">
                                        Episode title(Russian)
                                    </div>
                                    <a href="javascript:;" id="episode-title-rus" data-type="text" data-pk="{{ $episode->id }}" data-name="rus" data-original-title="სერიის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $episode['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="episode-description">
                                    <div class="add-episode-label">
                                        Episode description (russian)
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="episode-description-rus" data-pk="{{ $episode->id }}" data-name="rus" rows="6" style="visibility: hidden; display: none;">
                                            {{ $episode['desc_tr']['value_rus'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right episode-description-save" data-textarea="episode-description-rus">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="episode-back">
                                    <div class="add-episode-label">
                                        <button class="btn btn-primary btn-circle episode-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if(isset($posts))
                                <div role="tabpanel" class="tab-pane" id="posts">
                                    <div class="episode-posts">
                                        @foreach($posts as $post)
                                            <div class="episode-post">
                                                <h2>{{ $post['title_tr']['value_geo'] }}</h2>
                                                <p>{{ str_limit($post['desc_tr']['value_geo'], 200) }}</p>
                                                <a href="{{ url('/show/editepisodepost/' . $post->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                                <hr/>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a href="{{ url('/show/addepisodepost/' . $show->alias . '/' . $season->season_number . '/' . $episode->episode_number) }}">
                                        <button class="btn btn-primary btn-circle">
                                            პოსტის დამატება
                                        </button>
                                    </a>
                                </div>
                            @endif

                            <div role="tabpanel" class="tab-pane" id="episode-video">
                                <div class="col-md-3">
                                    <div class="episode-video-title">
                                        <div class="add-show-label">
                                            ვიდეოს სათაური
                                        </div>
                                        <a href="javascript:;" id="episode-video-title-geo" data-type="text" data-pk="{{ $episode['video']->id }}" data-name="geo" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getTitle('geo') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-desc">
                                        <div class="add-show-label">
                                            ვიდეოს აღწერა
                                        </div>
                                        <a href="javascript:;" id="episode-video-desc-geo" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="geo" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getDescription('geo') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-subtitles">
                                        <div class="add-show-label">
                                            ვიდეოს სუბტიტრები
                                        </div>
                                        <a href="javascript:;" id="episode-video-subt-geo" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="geo" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getSubtitle('geo') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="episode-video-title">
                                        <div class="add-show-label">
                                            Video title
                                        </div>
                                        <a href="javascript:;" id="episode-video-title-eng" data-type="text" data-pk="{{ $episode['video']->id }}" data-name="eng" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getTitle('eng') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-desc">
                                        <div class="add-show-label">
                                            Video description
                                        </div>
                                        <a href="javascript:;" id="episode-video-desc-eng" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="eng" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getDescription('eng') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-subtitles">
                                        <div class="add-show-label">
                                            Video subtitles
                                        </div>
                                        <a href="javascript:;" id="episode-video-subt-eng" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="eng" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getSubtitle('eng') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="episode-video-title">
                                        <div class="add-show-label">
                                            Video title (Russian)
                                        </div>
                                        <a href="javascript:;" id="episode-video-title-rus" data-type="text" data-pk="{{ $episode['video']->id }}" data-name="rus" data-original-title="სათაური" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getTitle('rus') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-desc">
                                        <div class="add-show-label">
                                            Video description (Russian)
                                        </div>
                                        <a href="javascript:;" id="episode-video-desc-rus" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="rus" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getDescription('rus') }}
                                        </a>
                                    </div>
                                    <div class="episode-video-subtitles">
                                        <div class="add-show-label">
                                            Video subtitles (Russian)
                                        </div>
                                        <a href="javascript:;" id="episode-video-subt-rus" data-type="textarea" data-pk="{{ $episode['video']->id }}" data-name="rus" data-original-title="აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episode['video']->getSubtitle('rus') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="episode-video-url">
                                        <div class="add-episode-label">
                                            ვიდეოს მისამართი
                                        </div>
                                        <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix=videos/{{ $episode['video']->id }}/">
                                            {{ '/videos/' . $episode['video']->id . '/1920x1080.mp4' }}
                                        </a>
                                    </div>
                                    <div class="episode-video-image-upload">
                                        <div class="add-episode-label">
                                            სერიის ვიდეოს სურათი
                                        </div>
                                        {!! Form::open(['url' => '/show/saveepisodevideoimage', 'id' => 'episode-video-image-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_episode_video_image']) !!}
                                            {!! Form::hidden('episode-id', $episode->id) !!}
                                            {!! Form::hidden('video-id', $episode['video']->id) !!}
                                            {!! Form::file('video-image', ['class' => '', 'id' => 'episode-video-image']) !!}
                                        {!! Form::close() !!}
                                        <iframe id="hidden_episode_video_image" name="hidden_episode_video_image" style="display: none;"></iframe>
                                    </div>
                                    {{--<div class="episode-video-image">
                                        <div class="add-episode-label">
                                            <img id="episode-video-img" src="{{ $episode['video_img'] }}" class="col-md-11 no-padding">
                                            <span class="col-md-1 glyphicon glyphicon-remove remove-episode-video-img"
                                                  data-target-id="{{ $episode->id }}" data-video-id="{{ $episode['video']->id }}"></span>
                                            <input type="hidden" id="episode-video-id" value="{{ $episode['video']->id }}">
                                        </div>
                                    </div>--}}
                                    <div class="episode-video-image-url">
                                        <div class="add-episode-label">
                                            სერიის ვიდეოს სურათის მისამართი
                                        </div>
                                        <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}episode/{{ $episode->id }}/video/{{ $episode['video']->id }}/">
                                            {{ '/episode/' . $episode->id . '/video/' . $episode['video']->id . '/video.jpg' }}
                                        </a>
                                    </div>
                                    @if(isset($episode['video_image']))
                                        <div class="episode-cover-mage">
                                            <div class="add-episode-label">
                                                <img id="episode-cover-image" src="{{ $episode['video_image'] }} " class="col-md-10 no-padding">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="trailers">
                                    @if (isset($trailers) && !empty($trailers))
                                        @foreach($trailers as $trailer)
                                            @if (isset($trailer['video']) && !empty($trailer['video']))
                                                <div class="trailer">
                                                    <div class="add-show-label">
                                                        {{ $trailer['video']['title_tr']['value_geo'] }}
                                                    </div>
                                                    {{ $trailer['video']['desc_tr']['value_geo'] }}
                                                </div>
                                                <a href="{{ url('/show/editepisodetrailer/' . $trailer['video']->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <a href="{{ url('/show/addepisodetrailer/' . $episode->id) }}">
                                    <button class="btn btn-primary btn-circle">
                                        თრეილერის დამატება
                                    </button>
                                </a>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="video-image">
                                <?php $video = \App\Video::find($episode['video']->id); ?>
                                <div class="col-md-6">
                                    @include('show.video', ['video' => $video])
                                </div>
                                <div class="col-md-6">
                                    <div class="crop-cover">
                                        <div class="add-post-label">
                                            სერიის ვიდეოს სურათი
                                        </div>
                                        {!! Form::open(['url' => '/show/savecrop', 'id' => 'trailer-video-crop-cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_crop_cover_trailer']) !!}
                                            {!! Form::hidden('video_id', $video->id) !!}
                                            {!! Form::hidden('type', 'cover') !!}
                                            {!! Form::file('crop-cover', ['class' => '', 'id' => 'crop-cover']) !!}
                                        {!! Form::close() !!}
                                        <iframe id="hidden_crop_cover_trailer" name="hidden_crop_cover_trailer" style="display: none;"></iframe>
                                    </div>
                                    <div class="crop-image-div">
                                        <div class="add-post-label">
                                            <img id="crop-trailer-img" src="@if(is_file($video->getCroppedCoverPath()))
                                                                                {{ $video->getCroppedCover() }}
                                                                            @else
                                                                                {{ $video->getCoverToCrop() }}
                                                                            @endif" class="col-md-4 no-padding">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a href="{{ url('/show/crop/' . $video->id . '/cover/editepisode') }}">
                                        <button class="btn btn-circle btn-success" style="margin: 10px 20px 10px 0;">
                                            Crop image
                                        </button>
                                    </a>
                                    {!! Form::open(['url' => '/show/uploadcroppedepisode', 'style' => 'display: inline-block;']) !!}
                                        {!! Form::hidden('episode-id', $episode->id) !!}
                                        {!! Form::hidden('video_id', $video->id) !!}
                                        {!! Form::submit('Upload', ['class' => 'btn btn-circle btn-success']) !!}
                                    {!! Form::close() !!}
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            @if($type == 'edit' && isset($episodeNotification) && !empty($episodeNotification))
                                <div role="tabpanel" class="tab-pane" id="fb">
                                    <div class="episode-notification">
                                        <div class="add-episode-label">
                                            სათაური
                                        </div>
                                        <a href="javascript:;" id="fb-notification-title-geo" data-type="text" data-pk="{{ $episodeNotification->id }}" data-name="geo" data-original-title="ნოთიფიქეიშენის სათაური" class="editable editable-click" style="display: inline;">
                                            {{ $episodeNotification->getTitle('geo') }}
                                        </a>
                                    </div>
                                    <div class="episode-notification">
                                        <div class="add-episode-label">
                                            აღწერა
                                        </div>
                                        <a href="javascript:;" id="fb-notification-desc-geo" data-type="text" data-pk="{{ $episodeNotification->id }}" data-name="geo" data-original-title="ნოთიფიქეიშენის აღწერა" class="editable editable-click" style="display: inline;">
                                            {{ $episodeNotification->getDescription('geo') }}
                                        </a>
                                    </div>
                                    <div class="fbnotification-publish-date">
                                        <div class="add-episode-label">
                                            ნოთიფიკაციის გაგზავნის თარიღი
                                        </div>
                                        <input id="fb-notification-publish-date" name="publish-date" data-notification-id="{{ $episodeNotification->id }}"
                                               data-datetime="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $episodeNotification->publish_date)->format('m/d/Y H:i') }}">
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="show-id" value="{{ $show->id }}">
        <input type="hidden" class="show-alias" value="{{ $show->alias }}">
        <input type="hidden" class="season-id" value="{{ $season->id }}">
        <input type="hidden" class="episode-id" value="{{ $episode->id }}">
        <input type="hidden" class="fb-url" value="{{ $episode->getUrl() }}">
    </div>
    <!-- END CONTENT -->

@endsection
