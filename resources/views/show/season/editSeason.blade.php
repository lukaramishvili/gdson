@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit-header">
                        სერიალი - {{ $show->getTitle('geo') }}
                    </div>
                    <div class="edit-header">
                        სეზონი - {{ $season->getTitle('geo') }}
                    </div>
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართულად</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Russian</a></li>
                            @if(isset($posts))
                                <li role="presentation"><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">პოსტები</a></li>
                            @endif
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">თრეილერის ვიდეო</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                            <div class="col-md-8 no-padding">
                                <div class="season-title">
                                    <div class="add-season-label">
                                        სეზონის სათაური
                                    </div>
                                    <a href="javascript:;" id="season-title-geo" data-type="text" data-pk="{{ $season->id }}" data-name="geo" data-original-title="სეზონის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $season['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="season-description">
                                    <div class="add-season-label">
                                        სეზონის აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="season-description-geo" data-pk="{{ $season->id }}" data-name="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $season['desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right season-description-save" data-textarea="season-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="short-description">
                                    <div class="add-season-label">
                                        სეზონის მოკლე აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="short-description-geo" data-whose="season" data-whose-id="{{ $season->id }}" data-lang="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $season['short_desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right short-description-save" data-textarea="short-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="season-publish-date">
                                    <div class="add-season-label">
                                        გამოქვეყნების თარიღი
                                    </div>
                                    <input id="publish-date" class="datetime" name="publish-date" data-target="season" data-target-id="{{ $season->id }}"
                                           data-field="publish_date" data-datetime="{{ $season['publish_datetime'] }}">
                                </div>
                                <div class="season-release-date">
                                    <div class="add-season-label">
                                        სერიის გამოსვლის თარიღი
                                    </div>
                                    <input id="release-date" class="datetime" name="release-date" data-target="season" data-target-id="{{ $season->id }}"
                                           data-field="release_date" data-datetime="{{ $season['release_datetime'] }}">
                                </div>
                                <div class="season-visibility">
                                    <div class="add-show-label">
                                        გამოქვეყნება
                                    </div>
                                    <input type="checkbox" id="season-visibility" class="make-switch" data-on-text="Yes" data-off-text="No"
                                    @if($season->visibility) checked @endif>
                                </div>

                                @foreach($professions as $key => $profession)
                                    <div class="season-actors">
                                        <div class="add-season-label">
                                            {{ $profession }}
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            {!! Form::select('crew-persons[]', $persons, isset($default_persons[$profession]) ? $default_persons[$profession] : null,
                                                array(
                                                    'data-target'        => 'season',
                                                    'data-target-id'     => $season->id,
                                                    'data-profession-id' => $key,
                                                    'class'              => 'form-control crew-persons',
                                                    'multiple'           => 'multiple'
                                                )
                                            ) !!}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endforeach

                                <div class="actings">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/editactings/season/' . $season->id) }}">
                                            <button class="btn btn-primary btn-circle">
                                                პერსონაჟების რედაქტირება
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="season-priority">
                                    <div class="add-season-label">
                                        Switch show url to this season
                                    </div>
                                    <input type="checkbox" id="season-priority" class="make-switch" data-on-text="Yes" data-off-text="No"
                                            @if($show->priority_type == 'season' && $show->priority_id == $season->id) checked @endif>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    დამატება
                                                @else
                                                    უკან დაბრუნება
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <button class="btn btn-primary btn-circle season-cancel">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="season-cover">
                                    <div class="add-season-label">
                                        სეზონის სურათი
                                    </div>
                                    {!! Form::open(['url' => '/show/savecover', 'id' => 'cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload']) !!}
                                        {!! Form::hidden('target_id', $season->id) !!}
                                        {!! Form::hidden('target', 'season') !!}
                                        {!! Form::file('cover', ['class' => '', 'id' => 'cover']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload" name="hidden_upload" style="display: none;"></iframe>
                                </div>
                                <div class="season-image-url">
                                    <div class="add-season-label">
                                        სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}season/{{ $season->id }}/">
                                        {{ '/season/' . $season->id . '/cover.jpg' }}
                                    </a>
                                </div>
                                @if(isset($season['cover_image']))
                                    <div class="season-cover-mage">
                                        <div class="add-season-label">
                                            <img id="season-cover-image" src="{{ $season['cover_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif

                                <div class="season-cover-second">
                                    <div class="add-season-label">
                                        სეზონის სურათი (2x)
                                    </div>
                                    {!! Form::open(['url' => '/show/savecoversecond', 'id' => 'cover-form-second', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload_second']) !!}
                                        {!! Form::hidden('target_id', $season->id) !!}
                                        {!! Form::hidden('target', 'season') !!}
                                        {!! Form::file('cover-second', ['class' => '', 'id' => 'cover-second']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload_second" name="hidden_upload_second" style="display: none;"></iframe>
                                </div>
                                <div class="season-image-url-second">
                                    <div class="add-season-label">
                                        სურათის მისამართი (2x)
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}season/{{ $season->id }}/">
                                        {{ '/season/' . $season->id . '/cover@2x.jpg' }}
                                    </a>
                                </div>
                                @if(isset($season['cover_image_second']))
                                    <div class="season-cover-mage-second">
                                        <div class="add-season-label">
                                            <img id="season-cover-image-second" src="{{ $season['cover_image_second'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row col-md-12 season-episodes">
                                <div class="add-season-label">
                                    სერიები
                                </div>
                                <table class="table table-responsive table-striped">
                                    <tbody>
                                    @foreach($season['episodes'] as $episode)
                                        <tr>
                                            <td class="col-md-9 seasons-episode-title">
                                                {{ $episode['title_tr']['value_geo'] }}
                                            </td>
                                            <td class="col-md-3">
                                                <a href="{{ url('/show/editepisode/' . $show->alias . '/' . $season->season_number . '/' . $episode['episode_number']) }}" class="btn btn-circle btn-default">
                                                    <i class="fa fa-pencil"></i> Edit </a>
                                                <a href="{{ url('/show/deleteepisode/' . $episode->id) }}" class="btn btn-circle btn-default delete-episode">
                                                    <i class="fa fa-remove"></i> Delete </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="season-title">
                                    <div class="add-season-label">
                                        season title
                                    </div>
                                    <a href="javascript:;" id="season-title-eng" data-type="text" data-pk="{{ $season->id }}" data-name="eng" data-original-title="season title" class="editable editable-click" style="display: inline;">
                                        {{ $season['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="season-description">
                                    <div class="add-season-label">
                                        season description
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="season-description-eng" data-pk="{{ $season->id }}" data-name="eng" rows="6" style="visibility: hidden; display: none;">
                                            {{ $season['desc_tr']['value_eng'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right season-description-save" data-textarea="season-description-eng">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="season-trailer">
                                    <div class="add-season-label">
                                        Season trailer
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-eng', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_eng']) !!}
                                        {!! Form::hidden('target_id', $season->id) !!}
                                        {!! Form::hidden('target', 'season') !!}
                                        {!! Form::hidden('lang', 'eng') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-eng']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_eng" name="hidden_trailer_eng" style="display: none;"></iframe>
                                </div>
                                <div class="season-episodes">
                                    <div class="add-season-label">
                                        episodes
                                    </div>
                                    <table class="table table-responsive table-striped">
                                        <tbody>
                                        @foreach($season['episodes'] as $episode)
                                            <tr>
                                                <td class="col-md-9 seasons-episode-title">
                                                    {{ $episode['title_tr']['value_eng'] }}
                                                </td>
                                                <td class="col-md-3">
                                                    <a href="{{ url('/show/editepisode/' . $show->alias . '/' . $season->season_number . '/' . $episode['episode_number']) }}" class="btn btn-circle btn-default">
                                                        <i class="fa fa-pencil"></i> Edit </a>
                                                    <a href="{{ url('/show/deleteepisode/' . $episode->id) }}" class="btn btn-circle btn-default delete-episode">
                                                        <i class="fa fa-remove"></i> Delete </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <button class="btn btn-primary btn-circle season-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="season-title">
                                    <div class="add-season-label">
                                        season title (russian)
                                    </div>
                                    <a href="javascript:;" id="season-title-rus" data-type="text" data-pk="{{ $season->id }}" data-name="rus" data-original-title="season title" class="editable editable-click" style="display: inline;">
                                        {{ $season['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="season-description">
                                    <div class="add-season-label">
                                        season description (russian)
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="season-description-rus" data-pk="{{ $season->id }}" data-name="rus" rows="6" style="visibility: hidden; display: none;">
                                            {{ $season['desc_tr']['value_rus'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right season-description-save" data-textarea="season-description-rus">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="season-trailer">
                                    <div class="add-season-label">
                                        Season trailer(rus)
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-rus', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_rus']) !!}
                                        {!! Form::hidden('target_id', $season->id) !!}
                                        {!! Form::hidden('target', 'season') !!}
                                        {!! Form::hidden('lang', 'rus') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-rus']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_rus" name="hidden_trailer_rus" style="display: none;"></iframe>
                                </div>
                                <div class="season-episodes">
                                    <div class="add-season-label">
                                        episodes
                                    </div>
                                    <table class="table table-responsive table-striped">
                                        <tbody>
                                        @foreach($season['episodes'] as $episode)
                                            <tr>
                                                <td class="col-md-9 seasons-episode-title">
                                                    {{ $episode['title_tr']['value_rus'] }}
                                                </td>
                                                <td class="col-md-3">
                                                    <a href="{{ url('/show/editepisode/' . $show->alias . '/' . $season->season_number . '/' . $episode['episode_number']) }}" class="btn btn-circle btn-default">
                                                        <i class="fa fa-pencil"></i> Edit </a>
                                                    <a href="{{ url('/show/deleteepisode/' . $episode->id) }}" class="btn btn-circle btn-default delete-episode">
                                                        <i class="fa fa-remove"></i> Delete </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <a href="{{ url('/show/seasons/' . $show->alias) }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="season-back">
                                    <div class="add-season-label">
                                        <button class="btn btn-primary btn-circle season-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if(isset($posts))
                                <div role="tabpanel" class="tab-pane" id="posts">
                                    <div class="season-posts">
                                        @foreach($posts as $post)
                                            <div class="season-post">
                                                <h2>{{ $post['title_tr']['value_geo'] }}</h2>
                                                <p>{{ str_limit($post['desc_tr']['value_geo'], 200) }}</p>
                                                <a href="{{ url('/show/editseasonpost/' . $post->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                                <hr/>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a href="{{ url('/show/addseasonpost/' . $show->alias . '/' .  $season->season_number) }}">
                                        <button class="btn btn-primary btn-circle">
                                            პოსტის დამატება
                                        </button>
                                    </a>
                                </div>
                            @endif

                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="trailers">
                                    @if (isset($trailers) && !empty($trailers))
                                        @foreach($trailers as $trailer)
                                            @if (isset($trailer['video']) && !empty($trailer['video']))
                                                <div class="trailer">
                                                    <div class="add-show-label">
                                                        {{ $trailer['video']['title_tr']['value_geo'] }}
                                                    </div>
                                                    {{ $trailer['video']['desc_tr']['value_geo'] }}
                                                </div>
                                                <a href="{{ url('/show/editseasontrailer/' . $trailer['video']->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <a href="{{ url('/show/addseasontrailer/' . $season->id) }}">
                                    <button class="btn btn-primary btn-circle">
                                        თრეილერის დამატება
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="show-id" value="{{ $show->id }}">
        <input type="hidden" class="show-alias" value="{{ $show->alias }}">
        <input type="hidden" class="season-id" value="{{ $season->id }}">
    </div>
    <!-- END CONTENT -->

@endsection