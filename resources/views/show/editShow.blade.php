@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit-header">
                        სერიალი - {{ $show['title_tr']['value_geo'] }}
                    </div>
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#geo" aria-controls="geo" role="tab" data-toggle="tab">ქართული</a></li>
                            <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" aria-controls="rus" role="tab" data-toggle="tab">Ruski</a></li>
                            <li role="presentation"><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">პოსტები</a></li>
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">თრეილერის ვიდეო</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="geo">
                            <div class="col-md-8 no-padding">
                                <div class="show-title">
                                    <div class="add-show-label">
                                        სერიალის სათაური
                                    </div>
                                    <a href="javascript:;" id="show-title-geo" data-type="text" data-pk="{{ $show['id'] }}" data-name="geo" data-original-title="სერიალის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $show['title_tr']['value_geo'] }}
                                    </a>
                                </div>
                                <div class="show-description">
                                    <div class="add-show-label">
                                        სერიალის აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="show-description-geo" data-pk="{{ $show['id'] }}" data-name="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $show['desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right show-description-save" data-textarea="show-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="short-description">
                                    <div class="add-show-label">
                                        სერიალის მოკლე აღწერა
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="short-description-geo" data-whose="show" data-whose-id="{{ $show['id'] }}" data-lang="geo" rows="6" style="visibility: hidden; display: none;">
                                            {{ $show['short_desc_tr']['value_geo'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right short-description-save" data-textarea="short-description-geo">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="show-publish-date">
                                    <div class="add-show-label">
                                        გამოქვეყნების თარიღი
                                    </div>
                                    <input id="publish-date" class="datetime" name="publish-date" data-target="show" data-target-id="{{ $show['id'] }}"
                                           data-field="publish_date" data-datetime="{{ $show['publish_datetime'] }}">
                                </div>
                                <div class="show-release-date">
                                    <div class="add-show-label">
                                        სერიალის გამოსვლის თარიღი
                                    </div>
                                    <input id="release-date" class="datetime" name="release-date" data-target="show" data-target-id="{{ $show['id'] }}"
                                           data-field="release_date" data-datetime="{{ $show['release_datetime'] }}">
                                </div>
                                <div class="show-visibility">
                                    <div class="add-show-label">
                                        გამოქვეყნება
                                    </div>
                                    <input type="checkbox" id="show-visibility" class="make-switch" data-on-text="Yes" data-off-text="No"
                                    @if($show['visibility']) checked @endif>
                                </div>
                                <div class="show-tags">
                                    <div class="add-show-label">
                                        სერიალის ჟანრები
                                    </div>
                                    <?php
                                    if (!isset($default_genres))
                                        $default_genres = null;
                                    ?>
                                    <div class="col-md-6 no-padding">
                                        {!! Form::select('genres[]', $all_genres, $default_genres, ['class' => 'form-control show-genres', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                @foreach($professions as $key => $profession)
                                    <div class="show-actors">
                                        <div class="add-show-label">
                                            {{ $profession }}
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            {!! Form::select('crew-persons[]', $persons, isset($default_persons[$profession]) ? $default_persons[$profession] : null,
                                                array(
                                                    'data-target'        => 'show',
                                                    'data-target-id'     => $show['id'],
                                                    'data-profession-id' => $key,
                                                    'class'              => 'form-control crew-persons',
                                                    'multiple'           => 'multiple'
                                                )
                                            ) !!}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @endforeach

                                <div class="actings">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/editactings/show/' . $show['id']) }}">
                                            <button class="btn btn-primary btn-circle">
                                                პერსონაჟების რედაქტირება
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="show-alias">
                                    <div class="add-show-label">
                                        სერიალის ალიასი
                                    </div>
                                    <a href="javascript:;" id="show-alias" data-type="text" data-pk="{{ $show['id'] }}" data-placeholder="სერიალის ალიასი" data-original-title="Enter comments">
                                        {{ $show['alias'] }}
                                    </a>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/shows') }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    დამატება
                                                @else
                                                    უკან დაბრუნება
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <button class="btn btn-primary btn-circle show-cancel">
                                            @if($type == 'add')
                                                გაუქმება
                                            @else
                                                წაშლა
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="show-cover">
                                    <div class="add-show-label">
                                        სერიალის სურათი
                                    </div>
                                    {!! Form::open(['url' => '/show/savecover', 'id' => 'cover-form', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload']) !!}
                                        {!! Form::hidden('target_id', $show['id']) !!}
                                        {!! Form::hidden('target', 'show') !!}
                                        {!! Form::file('cover', ['class' => '', 'id' => 'cover']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload" name="hidden_upload" style="display: none;">
                                    </iframe>
                                </div>
                                <div class="show-image-url">
                                    <div class="add-show-label">
                                        სურათის მისამართი
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}show/{{ $show['id'] }}/">
                                        {{ '/show/' . $show['id'] . '/cover.jpg' }}
                                    </a>
                                </div>
                                @if(isset($show['cover_image']))
                                    <div class="show-cover-mage">
                                        <div class="add-show-label">
                                            <img id="show-cover-image" src="{{ $show['cover_image'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif

                                <div class="show-cover2">
                                    <div class="add-show-label">
                                        სერიალის სურათი (2x)
                                    </div>
                                    {!! Form::open(['url' => '/show/savecoversecond', 'id' => 'cover-form-second', 'enctype' => 'multipart/form-data', 'target' => 'hidden_upload_second']) !!}
                                        {!! Form::hidden('target_id', $show['id']) !!}
                                        {!! Form::hidden('target', 'show') !!}
                                        {!! Form::file('cover-second', ['class' => '', 'id' => 'cover-second']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_upload_second" name="hidden_upload_second" style="display: none;">
                                    </iframe>
                                </div>
                                <div class="show-image-url">
                                    <div class="add-show-label">
                                        სურათის მისამართი (2x)
                                    </div>
                                    <a target="_blank" href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=gdsonvideos1&prefix={{ Config::get('services.cloudfront.bucket-prefix') }}show/{{ $show['id'] }}/">
                                        {{ '/show/' . $show['id'] . '/cover@2x.jpg' }}
                                    </a>
                                </div>
                                @if(isset($show['cover_image_second']))
                                    <div class="show-cover-image-second">
                                        <div class="add-show-label">
                                            <img id="show-cover-image-second" src="{{ $show['cover_image_second'] }} " class="col-md-10 no-padding">
                                        </div>
                                    </div>
                                @endif
                            </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="eng">
                                <div class="show-title">
                                    <div class="add-show-label">
                                        Show title
                                    </div>
                                    <a href="javascript:;" id="show-title-eng" data-type="text" data-pk="{{ $show['id'] }}" data-name="eng" data-original-title="სერიალის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $show['title_tr']['value_eng'] }}
                                    </a>
                                </div>
                                <div class="show-description">
                                    <div class="add-show-label">
                                        Show description
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="show-description-eng" data-pk="{{ $show['id'] }}" data-name="eng" rows="6" style="visibility: hidden; display: none;">
                                            {{ $show['desc_tr']['value_eng'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right show-description-save" data-textarea="show-description-eng">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="show-trailer">
                                    <div class="add-show-label">
                                        show trailer
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-eng', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_eng']) !!}
                                        {!! Form::hidden('target_id', $show['id']) !!}
                                        {!! Form::hidden('target', 'show') !!}
                                        {!! Form::hidden('lang', 'eng') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-eng']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_eng" name="hidden_trailer_eng" style="display: none;"></iframe>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/shows') }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <button class="btn btn-primary btn-circle show-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="rus">
                                <div class="show-title">
                                    <div class="add-show-label">
                                        Show title(Russian)
                                    </div>
                                    <a href="javascript:;" id="show-title-rus" data-type="text" data-pk="{{ $show['id'] }}" data-name="rus" data-original-title="სერიალის სათაური" class="editable editable-click" style="display: inline;">
                                        {{ $show['title_tr']['value_rus'] }}
                                    </a>
                                </div>
                                <div class="show-description">
                                    <div class="add-show-label">
                                        Show description(Russian)
                                    </div>
                                    <div class="col-md-10 no-padding">
                                        <textarea class="ckeditor" name="show-description-rus" data-pk="{{ $show['id'] }}" data-name="rus" rows="6" style="visibility: hidden; display: none;">
                                            {{ $show['desc_tr']['value_rus'] }}
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 no-padding">
                                        <button class="btn btn-success btn-circle pull-right show-description-save" data-textarea="show-description-rus">
                                            submit
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="show-trailer">
                                    <div class="add-show-label">
                                        Show trailer(rus)
                                    </div>
                                    {!! Form::open(['url' => '/show/savetrailer', 'id' => 'trailer-form-rus', 'enctype' => 'multipart/form-data', 'target' => 'hidden_trailer_rus']) !!}
                                        {!! Form::hidden('target_id', $show['id']) !!}
                                        {!! Form::hidden('target', 'show') !!}
                                        {!! Form::hidden('lang', 'rus') !!}
                                        {!! Form::file('trailer', ['class' => '', 'id' => 'trailer-rus']) !!}
                                    {!! Form::close() !!}
                                    <iframe id="hidden_trailer_rus" name="hidden_trailer_rus" style="display: none;"></iframe>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <a href="{{ url('/show/shows') }}">
                                            <button class="btn btn-primary btn-circle">
                                                @if($type == 'add')
                                                    Add
                                                @else
                                                    Go back
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="show-back">
                                    <div class="add-show-label">
                                        <button class="btn btn-primary btn-circle show-cancel">
                                            @if($type == 'add')
                                                Discard
                                            @else
                                                Delete
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if(isset($posts))
                                <div role="tabpanel" class="tab-pane" id="posts">
                                    <div class="show-posts">
                                        @foreach($posts as $post)
                                            <div class="show-post">
                                                <h2>{{ $post['title_tr']['value_geo'] }}</h2>
                                                <p>{{ str_limit($post['desc_tr']['value_geo'], 200) }}</p>
                                                <a href="{{ url('/show/editshowpost/' . $post->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                                <hr/>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a href="{{ url('/show/addshowpost/' . $show['alias']) }}">
                                        <button class="btn btn-primary btn-circle">
                                            პოსტის დამატება
                                        </button>
                                    </a>
                                </div>
                            @endif

                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="trailers">
                                    @if (isset($trailers) && !empty($trailers))
                                        @foreach($trailers as $trailer)
                                            @if (isset($trailer['video']) && !empty($trailer['video']))
                                                <div class="trailer">
                                                    <div class="add-show-label">
                                                        {{ $trailer['video']['title_tr']['value_geo'] }}
                                                    </div>
                                                    {{ $trailer['video']['desc_tr']['value_geo'] }}
                                                </div>
                                                <a href="{{ url('/show/editshowtrailer/' . $trailer['video']->id) }}">
                                                    <button class="btn btn-primary btn-circle">
                                                        რედაქტირება
                                                    </button>
                                                </a>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <a href="{{ url('/show/addshowtrailer/' . $show['id']) }}">
                                    <button class="btn btn-primary btn-circle">
                                        თრეილერის დამატება
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="show-id" value="{{ $show['id'] }}">
    </div>
    <!-- END CONTENT -->

@endsection