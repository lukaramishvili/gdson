@extends('layouts.master')



@section('social-meta-tags')

    @if(isset($episode) && $episode && $episode->isPublished())

    <meta property="og:title" content="{!! htmlspecialchars(strip_tags($episode->getTitleForShare("geo"))) !!}" />
    <meta property="og:url" content="{!! $episode->getUrl() !!}" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:description" content="&nbsp;" />
    <meta property="og:image" content="https:{!! $episode->getImageUrlSecond() !!}" />

    <meta itemprop="name" content="{!! htmlspecialchars(strip_tags($episode->getTitleForShare("geo"))) !!}">
    {{--<meta itemprop="description" content="{!! htmlspecialchars(strip_tags($episode->getDescription("geo"))) !!}">--}}
    <meta itemprop="image" content="https:{!! $episode->getImageUrlSecond() !!}">

    @elseif(isset($season) && $season && $season->isPublished())

    <meta property="og:title" content="{!! htmlspecialchars(strip_tags($season->getTitleForShare("geo"))) !!}" />
    <meta property="og:url" content="{!! $season->getUrl() !!}" />
    <meta property="og:type" content="video.movie" />
    {{--<meta property="og:description" content="{!! htmlspecialchars(strip_tags($season->getDescription("geo"))) !!}" />--}}
    <meta property="og:description" content="&nbsp;" />
    <meta property="og:image" content="https:{!! $season->getImageUrlSecond() !!}" />

    <meta itemprop="name" content="{!! htmlspecialchars(strip_tags($season->getTitleForShare("geo"))) !!}">
    {{--<meta itemprop="description" content="{!! htmlspecialchars(strip_tags($season->getDescription("geo"))) !!}">--}}
    <meta itemprop="image" content="https:{!! $season->getImageUrlSecond() !!}">

    @else

    <meta property="og:title" content="{!! htmlspecialchars(strip_tags($show->getTitleForShare("geo"))) !!}" />
    <meta property="og:url" content="{!! $show->getUrl() !!}" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:description" content="{!! htmlspecialchars(strip_tags($show->getDescription("geo"))) !!}" />
    <meta property="og:description" content="&nbsp;" />
    <meta property="og:image" content="https:{!! $show->getImageUrlSecond() !!}" />

    <meta itemprop="name" content="{!! htmlspecialchars(strip_tags($show->getTitleForShare("geo"))) !!}">
    {{--<meta itemprop="description" content="{!! htmlspecialchars(strip_tags($show->getDescription("geo"))) !!}">--}}
    <meta itemprop="image" content="https:{!! $show->getImageUrlSecond() !!}">

    @endif

@endsection




@section('style-head')






@endsection









@section('content')


<!-- $show, $season, $episode -->

<div class="show-container @if($show->hasMoviesGenre()) movie-container @endif">

<?php
    if(isset($episode) && $episode && $episode->isPublished()){
        if($episode->video){
            $slide_cover = $episode->getVideoImageUrl();
        } else {
            $slide_cover = $episode->getImageUrl();
        }
    } elseif(isset($season) && $season && $season->isPublished()){
        $slide_cover = $season->getImageUrl();
    } else {
        $slide_cover = $show->getImageUrl();
    }
?>
<div class="slider-container slider-container-show">
  <div class="slider slider-show">
    <div class="slide" style="background: black url({{ $slide_cover }}) center top no-repeat; background-size: auto 100%;">
      <div class="slide-video-container">
	@if(isset($episode) && $episode && $episode->isPublished())
	<?php
	   $h_video_host = Config::get('services.video-cdn.servers')[rand(0, count(Config::get('services.video-cdn.servers')) - 1)];
	   $h_video_cdn_dir = "videos/".$episode->video->id."/";
	   $h_video_url = "videos/".$episode->video->id."/1920x1080";
	?>
	  @if(App\Helper::isMobile())
	  <div class="slide-video" id="show-slide-video-{{ $episode->video->id }}">
	    <video preload="none" controls="controls">
	      <!-- <source type="video/webm" src="//mydomain.com/video.webm"> -->
	      <!-- <source type="video/mp4" src="{{ Config::get('services.cloudfront.web-distribution-url') }}/videos/{{ $episode->video_id }}/1280x720.mp4"> -->
	      <source type="video/mp4" src="{{ $h_video_host }}/videos/{{ $episode->video_id }}/1280x720.mp4">
	    </video>
	  </div>
	  @else
	  <a class="slide-video show-slide-video" id="show-slide-video-{{ $episode->video_id }}" href="mp4:videos/{{ $episode->video_id }}/1920x1080" data-video-id="{{ $episode->video_id }}" data-video-cdn-dir="{{ $h_video_cdn_dir }}" data-episode-id="{{ $episode->id }}" ></a>
	  @endif
        @endif
      </div>
      @if(isset($episode) && $episode && $episode->isPublished())
      <div class="slide-play"></div>
      <div class="slide-buffering-icon"></div>
      @endif
      <div class="slide-toolbox-container">
	<div class="container">
	  <div class="row">
	    <div class="col-md-5 col-sm-6 col-xs-8">
	      <div class="slide-text-container">
		<div class="slide-text">
		  <h1 class="slide-title">
		    @if(isset($episode) && $episode && $episode->isPublished())
		    {{ $episode->getTitle("geo") }}
		    @else
		    <!--{{ "No episodes added yet" }}-->
		    {{ "სერიები არ არის დამატებული" }}
		    @endif
		  </h1>
		  <div class="slide-desc">
		    @if(isset($episode) && $episode && $episode->isPublished())
		      {!! $episode->getDescription("geo") !!}
		    @endif
		  </div>
		  @if(isset($episode->video))
		  <div class="slide-viewcount">
		    <img src="{{ asset('/img/icons/viewcount.svg') }}" />
		    {!! number_format($episode->video->getViewcount(), 0, ".", " ") !!}
		  </div>
		  @endif
		</div>
	      </div>
	    </div>
	    <div class="col-md-2 col-sm-4 col-xs-4 pull-right slide-buttons-container">
	      <div class="slide-buttons">
                <?php
                if(isset($episode) && $episode && $episode->isPublished()){
		    $h_title = $episode->getTitleForShare("geo");
		    $h_desc = $episode->getDescription("geo");
		    $h_cover = $episode->getImageUrlSecond();
		    $h_href = $episode->getUrl();
		} elseif(isset($season) && $season && $season->isPublished()){
		    $h_title = $season->getTitleForShare("geo");
		    $h_desc = $season->getDescription("geo");
		    $h_cover = $season->getImageUrlSecond();
		    $h_href = $season->getUrl();
		} else {
		    $h_title = $show->getTitleForShare("geo");
		    $h_desc = $show->getDescription("geo");
		    $h_cover = $show->getImageUrlSecond();
		    $h_href = $show->getUrl();
		}
                ?>
		<div class="slide-social-btn" id="slide-social-btn-1" data-title="{!! htmlspecialchars(strip_tags($h_title)) !!}" data-desc="{!! htmlspecialchars(strip_tags($h_desc)) !!}" data-image="https:{!! htmlspecialchars(strip_tags($h_cover)) !!}" data-href="{!! htmlspecialchars(strip_tags($h_href)) !!}">
		  <div class="slide-social-btn-inner"></div>
                </div>
		<div class="slide-playlist-btn"></div>
		@if(Auth::check())
		<div class="slide-playlist-btn-list">
		  <div class="slide-playlist-btn-list-inner">
		    @if(isset($episode) && $episode && $episode->isPublished())
		    @if(Auth::check())
		    @foreach(Auth::user()->playlists as $playlist)
		    <div class="slide-playlist-btn-list-item @if($playlist->videos->contains($episode->video_id)) selected @endif " data-playlist-id="{{ $playlist->id }}" data-video-id="{{ $episode->video_id }}">
		      {{ $playlist->getTitle("geo") }}
		    </div>
		    @endforeach
		    @endif
		    @endif
		  </div>
		</div>
		@endif
	      </div>
	    </div>
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>

<div class="season-items-container">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 col-xs-5">
	<div class="season-choose">
	  @if($show->seasons()->count() > 0)
	  აირჩიე სეზონი:
	  @else
	  სეზონები არ მოიძებნა
	  @endif
	</div>
      </div>
      <div class="col-md-10 col-sm-8 col-xs-7">
	<div class="season-items">
	  @foreach($show->orderedSeasons as $cur_season)
	  @if($cur_season->isPublished())
	  <a class="season-item 
 @if(isset($season) && $season && $cur_season->season_number == $season->season_number) active @endif 
 @if($cur_season->episodes->count() > 0) @else disabled @endif 
 " href="{{ $cur_season->getUrl() }}">
	    {{ $cur_season->season_number }}
	  </a>
	  @endif
	  @endforeach
	</div>
      </div>
    </div>
  </div>
</div>


<div class="episode-items-container">
  <div class="container">
    <div class="row">
      <div class="episode-items big-items owl-carousel">
	@if(isset($season) && $season && $season->isPublished())
      {{-- use the following to reverse episode ordering --}}
      {{-- episodes()->orderBy('episode_number', 'DESC')->get() --}}
	@foreach($season->episodes as $cur_episode)
	@if($cur_episode->isPublished() && $cur_episode->episode_number != 0)
	<!-- <div class="col-md-2 col-sm-4 col-xs-12"> -->
	  <a class="episode-item @if(isset($episode) && $episode && $cur_episode->episode_number == $episode->episode_number) active @endif " href="{{ $cur_episode->getUrl() }}">
	    <img src="{{ asset($cur_episode->getImageUrl()) }}" />
	    <div class="episode-item-title">
	      {{ $cur_episode->getTitle("geo") }}
	    </div>
	  </a>
	<!-- </div> -->
	@endif
	@endforeach
	@endif
      </div>
    </div>
  </div>
</div>


<div class="posts-container">

@if(isset($season) && $season && $season->isPublished())
  @if($season->trailers()->count() > 0)
    @include('post.generic-template',
        array(
            'dir' => 'left',
            'post_has_video' => true,
            'video_id' => $season->trailers()->first()->video_id,
            'video_cdn_dir' => "videos/".$season->trailers()->first()->video_id."/",
	    /*start parameters needed for sharing button*/
	    'video_title' => $season->getTitle("geo"),
	    'video_desc' => $season->getDescription("geo"),
	    'video_cover' => $season->getImageUrl(),
	    'video_href' => $season->getUrl(),
	    /*end parameters needed for sharing button*/
            'post_image' => $season->getImageUrl(),
            'post_title' => $season->getTitle("geo"),
            'post_desc' => $season->getDescription("geo"),
            'post_prof_to_tags' => $season->getCrewmemberTags(),
        )
    )
  @else
    @include('post.generic-template',
        array(
            'dir' => 'left',
            'post_image' => $season->getImageUrl(),
            'post_title' => $season->getTitle("geo"),
            'post_desc' => $season->getDescription("geo"),
            'post_prof_to_tags' => $season->getCrewmemberTags(),
        )
    )
  @endif
@endif
<?php
            /* also ability to include red tags in the season info as post -
               include the following in the above array:
               'post_prof_to_tags' => array(
               "$profession_id" => array($tag, $tag, ..)),
               "$profession_id" => array($tag, $tag, ..)),
            */
?>

<?php $post_dir = 'right'; ?>
@foreach($posts as $post)
  @if($post->isPublished())
        @include('post.item', array('post' => $post, 'dir' => $post_dir))
        <?php $post_dir = $post_dir == 'left' ? 'right' : 'left'; ?>
  @endif
@endforeach


</div>




</div><!-- .show-container -->


@endsection








@section('script-bottom')

$(document).ready(function(){


    @if(isset($episode) && $episode && $episode->isPublished())
        //regularily save last watch position (in seconds)
	setInterval(function(){
	    var $slideVideo = $(".slide-video[data-episode-id='{{ $episode->id}}']");
            var curEpisodePlayer = $f($slideVideo.attr('id'));
            if(curEpisodePlayer && ($slideVideo.parents(".slider").hasClass('playing')
				    || $slideVideo.parents(".slider").hasClass('paused'))){
		var curTime = parseInt(curEpisodePlayer.getTime());
		var maxTimeToSave = parseInt(curEpisodePlayer.getClip().duration) - 180;
		if(!isNaN(curTime) && curTime < maxTimeToSave){
		    localStorage.setItem("lastwatch_episode_{{ $episode->id}}",
					 curTime.toString());
		} else { /* console.log(curTime); */ }
	    }
	}, 2000);
        //
        //this code jumps to episode_number-th item
        $(".episode-items").trigger('to.owl.carousel', [{{ $episode->episode_number - 1 }}, 0, true])
        //this code jumps to episode_number-th item, counted from end (episode order
        //has been reversed)
        //$(".episode-items").trigger('to.owl.carousel', [ $(".episode-item").length - 1 - {{ $episode->episode_number - 1 }}, 0, true])
    @else
        //scroll to latest episode if no specific episode is selected
        $(".episode-items").trigger('to.owl.carousel', [ $(".episode-item").length, 0, true])
    @endif


    /* this also powers post share buttons */
    $(".slider-show .slide-social-btn").each(function(i, el){
	var $id = $(el).attr('id');
	var title = $(el).attr('data-title');
	var desc = $(el).attr('data-desc');
	if(desc.length == 0){
	    /* space required; otherwise fb inherits site desc */
	    desc = " ";
	}
	var image = $(el).attr('data-image');
	var href = $(el).attr('data-href');
	var slide_social_btn = new Share("#" + $id + " .slide-social-btn-inner", {
	    title: title,
	    url: href,
	    {{--description: desc,--}}
	    image: image,
	    ui: {
		flyout: "top right"
	    },
	    networks: {
		facebook: {
		    app_id: window.fbAppId
            /*  ,caption: title  */
		},
                twitter: {
		    description: desc.substring(0, 100) + (desc.length > 100 ? "..." : "")
		},
		google_plus: {
		    title: title,
		    url: href,
		    description: desc,
		    image: image
		},
		pinterest: {
		    enabled: false
		},
		email: {
		    title: title,
		    description: title + " - " + href,
		}
	    }
	});/* slide button share init */
    });/* foreach .slide-social-btn */

    var visibleCarouselItems;
    function updateVisibleCarouselItemCount(){
	visibleCarouselItems = Math.floor($(".episode-items").width() / $(".episode-items .owl-item:first-child").width());
    }
    $(window).resize(function(){ updateVisibleCarouselItemCount(); });
    updateVisibleCarouselItemCount();
    //if you go to episode page, don't change owl-carousel navigation and then
    //|->you resize browser window, this code ensures active item stays visible
    var centerThisEpisode = -1;
    $(".episode-items").on('resized.owl.carousel', function(event) {
	updateVisibleCarouselItemCount();
	if(centerThisEpisode != -1){
	    $(".episode-items").trigger('to.owl.carousel', [ centerThisEpisode - (visibleCarouselItems/2) + 1 , 0, true]);
	}
    });
    /* user changed carousel page, so don't make active item stay in center anymore */
    $(".episode-items").on('translated.owl.carousel', function(event) {
	//this callback also gets fired on programatically caused carousel page changes,
	//|->so for now always center active episode on resize
	//centerThisEpisode = -1;
    });
    //end code for ensuring active episode stays visible after resize

    /* scroll to last scrolled episode */
    @if(isset($season) && $season)
	/* save last scroll position */
	$(".episode-items").on('changed.owl.carousel', function(event) {
	    localStorage.setItem("season_scroll_{{ $season->id }}", event.item.index);
	});
        /* if a specific episode page, then scroll to it's page in the carousel */
        @if(isset($episode) && $episode && $url_target_type == 'episode')
	    centerThisEpisode = {{ $episode->episode_number-1 }};
	    $(".episode-items").trigger('to.owl.carousel', [ centerThisEpisode - (visibleCarouselItems/2) + 1 , 0, true]);
        @else
    	/* restore last scroll position */
	    var lastScrollPos = localStorage.getItem("season_scroll_{{ $season->id }}");
            /* if there's no saved episode, then scroll to the end */
            if(typeof(lastScrollPos) != "undefined" && lastScrollPos != null){
		$(".episode-items").trigger('to.owl.carousel', [lastScrollPos, 0, true]);
	    } else {
		$(".episode-items").trigger('to.owl.carousel', [ $(".episode-item").length, 0, true]);
	    }
        @endif
        /* end restore last scroll position */
    @endif
    /* end scroll to last scrolled episode */




});/* /document.ready */

@endsection

