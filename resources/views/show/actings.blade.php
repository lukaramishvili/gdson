@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>Person</th>
                                <th>პერსონაჟი</th>
                                <th>Character</th>
                                <th>Personaj</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($crews as $crew)
                            <tr>
                                <td>
                                    {{ $crew['person']->getFirstName->value_geo . ' ' . $crew['person']->getLastName->value_geo }}
                                </td>
                                <td>
                                    <a href="javascript:;" id="edit-character-geo" data-type="text" data-pk="{{ $crew['character']->id }}" data-name="geo" data-original-title="პერსონაჟი" class="editable editable-click" style="display: inline;">
                                        {{ $crew['character']->getTitle('geo') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:;" id="edit-character-eng" data-type="text" data-pk="{{ $crew['character']->id }}" data-name="eng" data-original-title="პერსონაჟი" class="editable editable-click" style="display: inline;">
                                        {{ $crew['character']->getTitle('eng') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:;" id="edit-character-rus" data-type="text" data-pk="{{ $crew['character']->id }}" data-name="rus" data-original-title="პერსონაჟი" class="editable editable-click" style="display: inline;">
                                        {{ $crew['character']->getTitle('rus') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="redirect-back">
                        <a href="{{ URL::previous() }}">
                            <button class="btn btn-primary btn-circle">
                                back
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection