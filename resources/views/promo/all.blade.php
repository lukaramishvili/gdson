@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <h2>შოუები</h2>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>შოუ</th>
                            <th>ჰაილაიტი</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($shows as $showKey => $show)
                        <tr>
                            <td>
                                {{ $show['showTitle'] }}
                            </td>
                            <td>
                                <input type="checkbox" class="make-switch promo-switch" data-on-text="Yes" data-off-text="No"
                                       data-whose-type="show" data-whose-id="{{ $showKey }}" @if($show['checkPromo']) checked @endif>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <h2>სეზონები</h2>

                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>შოუ</th>
                        <th>სეზონი</th>
                        <th>ჰაილაიტი</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($seasons as $seasonKey => $season)
                        <tr>
                            <td>
                                {{ $season['showTitle'] }}
                            </td>
                            <td>
                                {{ $season['seasonTitle'] }}
                            </td>
                            <td>
                                <input type="checkbox" class="make-switch promo-switch" data-on-text="Yes" data-off-text="No"
                                       data-whose-type="season" data-whose-id="{{ $seasonKey }}" @if($season['checkPromo']) checked @endif>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <h2>ეპიზოდები</h2>

                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>შოუ</th>
                        <th>სეზონი</th>
                        <th>ეპიზოდი</th>
                        <th>ჰაილაიტი</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($episodes as $episodeKey => $episode)
                        <tr>
                            <td>
                                {{ $episode['showTitle'] }}
                            </td>
                            <td>
                                {{ $episode['seasonTitle'] }}
                            </td>
                            <td>
                                {{ $episode['episodeTitle'] }}
                            </td>
                            <td>
                                <input type="checkbox" class="make-switch promo-switch" data-on-text="Yes" data-off-text="No"
                                       data-whose-type="episode" data-whose-id="{{ $episodeKey }}" @if($episode['checkPromo']) checked @endif>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row">
                <a href="{{ url('/promo/order') }}">
                    <button class="btn btn-primary btn-circle">
                        Order
                    </button>
                </a>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection