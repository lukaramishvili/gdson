@extends('layouts.admin')

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>object type</th>
                            <th>object</th>
                            <th>order</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($promos as $promo)
                            <tr>
                                <td>
                                    {{ $promo->whose_type }}
                                </td>
                                <td>
                                    {{ $promo->whose->getTitle('geo') }}
                                </td>
                                <td>
                                    <select name="order" class="promo-order" data-promo-id="{{ $promo->id }}">
                                        <option value="100">order</option>
                                        @for($i = 1; $i <= sizeof($promos); $i++)
                                            <option value="{{ $i }}" @if($promo->order == $i) selected @endif>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="highlight-go-back">
                        <a href="{{ URL::previous() }}">
                            <button class="btn btn-primary btn-circle">
                                უკან
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection