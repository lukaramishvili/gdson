{{-- if $post_has_video = true, $video_id and $video_cdn_dir should also be set --}}
<div class="post post-{{ $dir }}">
  <div class="container">
    <div class="row">
      <?php //@ if($dir == 'left') ?>
      {{--
     @if(isset($post_id) && isset($post_has_video) && $post_has_video)
       @include("post.image", array("post_id" => $post_id, "post_has_video" => $post_has_video, "video_id" => $video_id, "video_cdn_dir" => $video_cdn_dir, "post_image" => $post_image))
     @else
       @include("post.image", array("post_image" => $post_image))
     @endif
      --}}
      <?php //@ endif ?>
      <div class="post-content-col col-md-12"><!-- col-md-5 -->
	<div class="post-content">
     @if(isset($post_id) && isset($post_has_video) && $post_has_video)
       @include("post.image", array("post_id" => $post_id, "post_has_video" => $post_has_video, "video_id" => $video_id, "video_cdn_dir" => $video_cdn_dir, "post_image" => $post_image, 'inline' => true))
     @else
       @include("post.image", array("post_image" => $post_image, 'inline' => true))
     @endif
	  <h3 class="post-title">{{ $post_title }}</h3>
	  <div class="post-text">
	    <div class="post-text-inner">
	      {!! $post_desc !!}
	    </div>
	    <div class="post-more-btn">
	      <span class="title-expand">ვრცლად</span>
	      <span class="title-collapse">დახურვა</span>
	    </div>
	  </div>
	  <div class="post-meta">
	    @if(isset($post_prof_to_tags) && $post_prof_to_tags)
	    @foreach($post_prof_to_tags as $post_profession_id => $post_profession_tags)
	    <?php
	       /* $profgroup = array of Tag's, groupBy returns this structure:
	       [ profession_id =>  [<Tag>, <Tag>, ..] , ... ] */
		$post_profession = App\Profession::find($post_profession_id);
	       ?>
	    <div class="post-meta-title">
	      @if(count($post_profession_tags) > 1)
	      <?php // {{ strtoupper($post_profession->keyword).'S' }} ?>
	      {{ mb_substr(trim($post_profession->getTitle("geo")), 0, -1, "UTF-8")."ები" }}
	      @else
	      {{ $post_profession->getTitle("geo") }}
	      @endif
	    </div>
	    <div class="post-meta-content">
<?php
$post_profession_tag_comma = "";
foreach($post_profession_tags as $post_profession_tag){
    echo $post_profession_tag_comma;
    echo '<a href="/search/' . $post_profession_tag->person->firstname->getValue("geo") . ' ' . $post_profession_tag->person->lastname->getValue("geo") . '">';
    echo $post_profession_tag->person->firstname->getValue("geo");
    echo ' ';
    echo $post_profession_tag->person->lastname->getValue("geo");
    echo '</a>';
    $post_profession_tag_comma = ", ";
}
?>
	    </div>
	    @endforeach
	    @endif
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>
