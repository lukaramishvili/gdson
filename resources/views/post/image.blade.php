{{-- if $post_has_video = true, $video_id and $video_cdn_dir should also be set --}}

@if(!(isset($inline) && $inline))
<div class="post-image-col col-md-7">
@endif
  <div class="@if(isset($inline) && $inline) post-image-inline @else post-image @endif">
    @if(isset($video_id) && isset($post_has_video) && $post_has_video)
    <!-- -->
    <div class="post-video-container">
      <div class="slider-container slider-container-show slider-container-post">
        <div class="slider slider-show slider-post">
	  <img src="{{ $post_image }}" />
          <div class="slide" style="/*background: black url() center top no-repeat; background-size: auto 100%;*/">
            <div class="slide-video-container" style="width: 100%; margin-bottom: 20px">
              @if(App\Helper::isMobile())
              <div class="slide-video" id="post-slide-video-{{ (isset($inline) && $inline) ? 'inline-' : '' }}{{ $video_id }}">
                <video preload="none" controls="controls">
                  <!-- <source type="video/webm" src="//mydomain.com/video.webm"> -->
                  <source type="video/mp4" src="{{ Config::get('services.cloudfront.web-distribution-url') }}/{{ $video_cdn_dir }}1280x720.mp4">
                </video>
              </div>
              @else
              <a class="slide-video show-slide-video" id="post-slide-video-{{ (isset($inline) && $inline) ? 'inline-' : '' }}{{ $video_id }}" href="mp4:{{ $video_cdn_dir }}1920x1080" data-video-id="{{ $video_id }}" data-video-cdn-dir="{{ $video_cdn_dir }}"></a>
              @endif
            </div>
            <div class="slide-play"></div>
            <div class="slide-buffering-icon"></div>
	    <div class="slide-toolbox-container">
	      <div class="col-md-5 col-sm-8 col-xs-8 post-image-text-container">
		<!-- usually, in main sliders, text goes here -->
	      </div>
	      <div class="col-md-4 col-sm-4 col-xs-4 pull-right slide-buttons-container">
		<div class="slide-buttons">
		  @if(isset($video_id, $video_title, $video_desc, $video_cover, $video_href))
		  <div class="slide-social-btn" id="slide-social-btn-{{ $video_id }}" data-title="{!! htmlspecialchars(strip_tags($video_title)) !!}" data-desc="{!! htmlspecialchars(strip_tags($video_desc)) !!}" data-image="https:{!! htmlspecialchars(strip_tags($video_cover)) !!}" data-href="{!! htmlspecialchars(strip_tags($video_href)) !!}">
		    <div class="slide-social-btn-inner"></div>
                  </div>
		  @endif
		  <div class="slide-playlist-btn"></div>
		  @if(Auth::check())
		  <div class="slide-playlist-btn-list">
		    <div class="slide-playlist-btn-list-inner">
		      @if(Auth::check())
		      @foreach(Auth::user()->playlists as $playlist)
		      <div class="slide-playlist-btn-list-item @if($playlist->videos->contains($video_id)) selected @endif " data-playlist-id="{{ $playlist->id }}" data-video-id="{{ $video_id }}">
			{{ $playlist->getTitle("geo") }}
		      </div>
		      @endforeach
		      @endif
		    </div>
		  </div>
		  @endif
		</div>
	      </div>
	    </div>
          </div>
        </div>
      </div>
    </div>
    <!-- -->
    @else
    <img src="{{ $post_image }}" />
    @endif
  </div>
@if(!(isset($inline) && $inline))
</div>
@endif
