@if($post->hasVideo())
    @include('post.generic-template',
        array(
            "dir" => $dir,
            "post_id" => $post->id,
            "post_image" => asset($post->getImageUrl()),
            "post_has_video" => $post->hasVideo(),
            //"video_id" => $post->id,
            //"video_cdn_dir" => "post/" . $post->id . "/video/",
            "video_id" => $post->video_id,
            "video_cdn_dir" => "videos/" . $post->video_id . "/",
            /*start parameters needed for sharing button*/
            'video_title' => $post->getTitle("geo"),
            'video_desc' => $post->getDescription("geo"),
            'video_cover' => $post->getImageUrl(),
            /*according to GDSON-710, shared post links to the page it was shared from*/
            'video_href' => Request::url(),
            /*end parameters needed for sharing button*/
            //
            "post_title" => $post->getTitle("geo"),
            "post_desc" => $post->getDescription("geo"),
            "post_prof_to_tags" => $post->tags->groupBy('profession_id'),
        )
    )
@else
    @include('post.generic-template',
        array(
            "dir" => $dir,
            "post_id" => $post->id,
            "post_image" => asset($post->getImageUrl()),
            "post_has_video" => false,
            "post_title" => $post->getTitle("geo"),
            "post_desc" => $post->getDescription("geo"),
            "post_prof_to_tags" => $post->tags->groupBy('profession_id'),
        )
    )
@endif

