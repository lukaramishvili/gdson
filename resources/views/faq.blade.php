@extends('layouts.master')



@section('content')



<div class="faq-container">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12 col-lg-offset-1">
	<div class="faq-heading">
	  <h1>კითხვა-პასუხი</h1>
	  <p class="subtitle">ჩვენი მომხმარებლების მიერ ხშირად დასმული შეკითხვები</p>
	</div>
	<div class="faq-content">
	  <div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	      <div class="faq-items">
		@foreach(App\Faq::all() as $faq)
		<div class="faq-item">
		  <div class="faq-item-question">
		    {{ $faq->getTitle("geo") }}
		  </div>
		  <div class="faq-item-answer">
		    {!! $faq->getDescription("geo") !!}
		  </div>
		</div>
		@endforeach
	      </div>
	    </div>
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>












  @endsection








  @section('script-bottom')

  $(document).ready(function(){
    $(".faq-item-question").click(function(){
      $(this).toggleClass('visible');
      $(this).parent().find(".faq-item-answer").toggleClass('visible');
    });
  });

  @endsection
