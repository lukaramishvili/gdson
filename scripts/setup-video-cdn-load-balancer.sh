sudo yum install -y haproxy
echo 'ENABLED=1' > /etc/default/haproxy
mv /etc/haproxy/haproxy.cfg{,.original}
echo 'global
    log 127.0.0.1 local0 notice
    maxconn 50000
    user haproxy
    group haproxy
defaults
    log     global
    mode    http
    option  httplog
    option  dontlognull
    retries 3
    option redispatch
    timeout connect  5000
    timeout client  10000
    timeout server  10000
listen gdsonvideocdn 0.0.0.0:80
    mode http
    stats enable
    stats uri /haproxy?stats
    stats realm Strictly\ Private
    stats auth lramishvili:Gdson2015123
    balance source
    option httpclose
    option forwardfor
    server vcdn2 vcdn2.gdson.net:80 check
    server vcdn3 vcdn3.gdson.net:80 check
    server vcdn4 vcdn4.gdson.net:80 check
    server vcdn5 vcdn5.gdson.net:80 check
    server vcdn6 vcdn6.gdson.net:80 check
    server vcdn7 vcdn7.gdson.net:80 check
    server cldft d8kddvpb911c6.cloudfront.net check backup
' >  /etc/haproxy/haproxy.cfg
chkconfig haproxy on
sudo service haproxy start





