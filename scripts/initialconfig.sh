# copy-paste this script to the server

# TURN OFF SELINUX, NOTHING WORKS WHEN IT'S ON
# TODO: disable SELINUX in /etc and apply settings 

cd /tmp

# WARNING: do this only for centos 6. not necessary for centos 7 (comes with git 1.8)
# TODO: if centos 6 then
# removing git because /usr/bin/git would take priority over /usr/src/git/bin/git
sudo yum remove -y git
sudo yum install -y wget
sudo yum install -y curl-devel expat-devel gettext-devel openssl-devel zlib-devel
sudo yum install -y gcc perl-devel
sudo wget https://git-core.googlecode.com/files/git-1.8.5.3.tar.gz
sudo tar xzf git-1.8.5.3.tar.gz
cd git-1.8.5.3
sudo make prefix=/usr/src/git all
sudo make prefix=/usr/src/git install
sudo sh -c 'echo "export PATH=$PATH:/usr/src/git/bin" >> /etc/bashrc' 
source /etc/bashrc
# clear old git location from cache
hash -r
sudo sh -c 'hash -r'
# after the previous line, $ sudo git still won't work, so make a symbolic link
sudo ln -s /usr/src/git/bin/git /usr/bin/git
sudo git config --global credential.helper 'cache --timeout 86400000'
# TODO: else if centos 7
# for centos 7, which comes with git 1.8, directly install git
sudo yum install -y git
# TODO: endif

mkdir /projects
git clone https://lukaramishvili@bitbucket.org/lukaramishvili/gdson

sudo yum install -y emacs
sudo yum install -y screen
