#!/usr/bin/env bash

# require running as root
if [ `whoami` == "root" ] ; then
	echo "running as root"
else
	echo "This script should be run as root; exiting"
	exit
fi

# /etc/redhat-release should be "CentOS Linux release 7.1.1503 (Core)" or similar
# also there's /etc/system-release, os-release and centos-release
# there's no lsb_release by default on CentOS 7
# should be positive
if [ `cat /etc/*-release | grep "CentOS Linux release 7" | wc -l | awk '{print $1}'` -gt 0 ] ; then
	echo "CentOS 7 requirement satisfied"
else
	echo "This script requires CentOS 7; exiting"
	exit
fi
# should be x86_64
if [ `arch` == "x86_64" ] ; then
	echo "64-bit system requirement satisfied"
else
	echo "This script requires a 64-bit system; exiting"
	exit
fi

# enable EPEL repo
sudo yum install -y epel-release
# download IUS additional package repo (not directly relevant to this script, but contains helpful packages)
wget http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
sudo rpm -Uvh ius-release*.rpm


### TODO: if Centos 6
### install and enable rpmforge and rpmforge-extras
#wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
#sudo rpm --import http://apt.sw.be/RPM-GPG-KEY.dag.txt
#sudo rpm -K rpmforge-release-0.5.3-1.el6.rf.*.rpm
#sudo rpm -i rpmforge-release-0.5.3-1.el6.rf.*.rpm
### TODO: in /etc/yum.repos.d/rpmforge.repo , in the [rpmforge-extras] section, change
###   |-> "enabled = 0" to "enabled = 1"
### TODO: install git 1.8 from source here (working code in initialconfig.sh)
### TODO: end if Centos 6


# we'll be installing java and red5 server here
INSTALLDIR='/opt'

cd $INSTALLDIR

# remove openjdk but leave dependent packages installed (ant, common-xml-apis, etc)
# they'll be automatically using the new oracle java instead (via JAVA_HOME/JRE_HOME and alternatives)
rpm -qa | grep openjdk | awk '{system("rpm -e --nodeps " $0)}'

# download java from oracle
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz"

# unarchive java archive
tar xzvf jdk-7u79-linux-x64.tar.gz 

# automatic version of `alternatives --config java/jar/javac`
alternatives --install /usr/bin/java java $INSTALLDIR/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/jar jar $INSTALLDIR/jdk1.7.0_79/bin/jar 2
alternatives --install /usr/bin/javac javac $INSTALLDIR/jdk1.7.0_79/bin/javac 2
alternatives --set jar $INSTALLDIR/jdk1.7.0_79/bin/jar
alternatives --set javac $INSTALLDIR/jdk1.7.0_79/bin/javac 

echo '# Oracle JDK path config' >> /etc/profile
echo "export PATH=\$PATH:$INSTALLDIR/jdk1.7.0_79/bin:$INSTALLDIR/jdk1.7.0_79/jre/bin" >> /etc/profile
echo "export JAVA_HOME=$INSTALLDIR/jdk1.7.0_79" >> /etc/profile
echo "export JRE_HOME=$INSTALLDIR/jdk1.7.0_79/jre" >> /etc/profile

source /etc/profile

#should be these three lines (because we're using exactly this java version):
# java version "1.7.0_79"
# Java(TM) SE Runtime Environment (build 1.7.0_79-b15)
# Java HotSpot(TM) 64-Bit Server VM (build 24.79-b02, mixed mode)
if [ `java -version | grep "java version \"1.7.0_79\"" | wc -l | awk '{print $1}'` -gt 0 ] ; then
	echo "Java 1.7.0_79 installed"
else
	echo "Java 1.7.0_79 wasn't installed correctly. Check if java is installed and \$PATH variables are correctly configured"
fi

#should be this: "javac 1.7.0_79"
if [ `javac -version | grep "javac 1.7.0_79" | wc -l | awk '{print $1}'` -gt 0 ] ; then
	echo "Java 1.7.0_79 installed"
else
	echo "Java 1.7.0_79 wasn't installed correctly. Check if java is installed and \$PATH variables are correctly configured"
fi


wget https://github.com/Red5/red5-server/releases/download/v1.0.4-RELEASE/red5-server-1.0.4-RELEASE-server.tar.gz
tar xzvf red5-server-1.0.4-RELEASE-server.tar.gz 

mv red5-server-1.0.4-RELEASE red5

# start red5 on boot; red5 should be started in its dir (temporary cd with parens)
echo "(cd $INSTALLDIR/red5 && ./red5.sh &)" >> /etc/rc.d/rc.local


yum install -y sox
