
# run with $ fab -H root@vcdn1.gdson.net,root@vcdn2.gdson.net,root@vcdn3.gdson.net,root@vcdn4.gdson.net,root@vcdn5.gdson.net,root@vcdn6.gdson.net,root@vcdn7.gdson.net,root@vcdn8.gdson.net,root@vcdn9.gdson.net,root@vcdn10.gdson.net,root@vcdn11.gdson.net,root@vcdn12.gdson.net,root@vcdn13.gdson.net,root@vcdn14.gdson.net host_type

from fabric.api import run

def git_pull():
    run('cd /projects/s3video && git pull')

def install_cpulimit():
    run('sudo yum install -y cpulimit')

# provided fabric output is in /tmp/quality.txt, $ grep 480p -A1 -i /tmp/quality.txt | grep -v grep | grep -v echo | grep -v "^--$" | grep -v 480p | sed 's/^.*out: //' | awk 'BEGIN {FS="\t"}; { q480+=$1; q720+=$2; q1920+=$3} END { print q480,FS,q720,FS,q1920 }'

def usage_per_quality():
    # -e enables \t and \n notation; -n disables newline
    run('echo -en "480p\t720p\t1920p\n' +
        '`grep 858x480\.mp4 -i /var/log/nginx/access.log* | wc -l`\t' +
        '`grep 1280x720\.mp4 -i /var/log/nginx/access.log* | wc -l`\t' +
        '`grep 1920x1080\.mp4 -i /var/log/nginx/access.log* | wc -l`\n' +
        '"');

# WARNING: don't modify any git-tracked files on all servers; i was distracted and did this plus a commit, then lost a good two hours reverting everything in git

#def add_dev_server_ip_to_some_file_not_tracked_by_git():
#    run('sed -i.bak \'/s1u7f3hsym509m\.cloudfront\.net/a \ \ <allow-access-from domain="52.29.163.183" />\' /tmp/some_file_not_tracked_by_git.xml && rm /tmp/some_file_not_tracked_by_git.xml')

def download_contactsheets_from_vcdn1():
    run('cd /projects/s3video/public/videos && find . -maxdepth 1 -exec wget http://vcdn1.gdson.net/videos/{}/contactsheet.jpg -O {}/contactsheet.jpg \; && rm /projects/s3video/public/videos/contactsheet.jpg');

def generate_contactsheets():
    run('su - nginx -s /usr/bin/sh -c \'/projects/s3video/artisan video:generatecontactsheets\' ')

def download_458_contactsheet():
    run('wget http://vcdn1.gdson.net/videos/458/contactsheet.jpg -O /projects/s3video/public/videos/458/contactsheet.jpg')

#def cp_test_456_5555():
#    run('rm -R /projects/s3video/public/videos/5555')
