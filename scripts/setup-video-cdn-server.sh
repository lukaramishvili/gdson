# bash warning setlocale no such file or directory fix:
# http://www.cyberciti.biz/faq/os-x-terminal-bash-warning-setlocale-lc_ctype-cannot-change-locale/

# to run these commands on all servers:
# for i in {1..14}; do ssh root@vcdn"$i".gdson.net ' ( echo "connected to " && hostname && echo "hello" && echo "finished working on " && hostname ) '; done


sudo yum -y update
sudo yum -y upgrade


# (not needed currently:) install rpmforge repo:
# rpm -ivh http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el7.rf.x86_64.rpm


# (not needed currently:) install rpmfusion repo:
# su -c 'yum -y localinstall --nogpgcheck http://download1.rpmfusion.org/free/el/updates/6/i386/rpmfusion-free-release-6-1.noarch.rpm http://download1.rpmfusion.org/nonfree/el/updates/6/i386/rpmfusion-nonfree-release-6-1.noarch.rpm'

# install webtatic repo
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

#show full hostname in shell prompt
echo 'PS1="[\u@\H \W]\$ "' >> ~/.bash_profile
echo 'export PS1' >> ~/.bash_profile

sudo yum install -y screen
sudo yum install -y cpulimit

yum install -y php56w
yum list | awk '$1 ~ /^php56w-.*$/ { print $1 }' | grep -v mysqlnd | xargs yum install -y 

sudo mkdir /projects
cd /projects
sudo yum install -y git
git clone https://lukaramishvili:tommarvoloriddle1@github.com/ImmeD/s3video
cd /projects/s3video
echo 'AWS_ACCESS_KEY_ID=AKIAINZOSHNZDLXBDORQ' >> .env
echo 'AWS_SECRET_ACCESS_KEY=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' >> .env
echo 'AWS_REGION=eu-central-1' >> .env
echo 'AWS_CONFIG_FILE=null' >> .env

chown -R apache /projects/s3video
chmod -R 0775 /projects/s3video

crontab -l -u apache > /tmp/vcdn-crontab
echo '* * * * * php /projects/s3video/artisan schedule:run >> /dev/null 2>&1' >> /tmp/vcdn-crontab
crontab -u apache /tmp/vcdn-crontab
unalias rm
rm /tmp/vcdn-crontab

#then php artisan video:download in screen -U

# install nginx
echo '[nginx]' >> /etc/yum.repos.d/nginx.repo
echo 'name=nginx repo' >> /etc/yum.repos.d/nginx.repo
echo 'baseurl=http://nginx.org/packages/centos/7/$basearch/' >> /etc/yum.repos.d/nginx.repo
echo 'gpgcheck=0' >> /etc/yum.repos.d/nginx.repo
echo 'enabled=1' >> /etc/yum.repos.d/nginx.repo
sudo yum install -y nginx
#config nginx
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.original
sed -i.bak 's/worker_processes\s*1\;/worker_processes 4;/g' /etc/nginx/nginx.conf
sed -i.bak '/http\s*{$/ a\
    server {\
        root /projects/s3video/public; \
        location / { \
            mp4; \
            mp4_buffer_size       2m; \
            mp4_max_buffer_size   64m; \
        } \
    }' /etc/nginx/nginx.conf
sed -i.bak '/default_type.*$/ a\
types {\
application/octet-stream mp4; \
}' /etc/nginx/nginx.conf
#
sudo chkconfig nginx on
sudo service nginx start


#install red5
#./serverconfig.sh
# TODO: install oflaDemo by command line then uncomment following lines
#mv /opt/red5/webapps/oflaDemo/streams{,.original}
#ln -s /projects/s3video/public /opt/red5/webapps/oflaDemo/streams

#install ffmpeg
#./setup-video-cdn-server-install-ffmpeg.sh



# to turn off password-based authentication (commented by default in case PAM isn't set up yet):
# sed -i.bak 's/^PasswordAuthentication yes/PasswordAuthentication no/i' /etc/ssh/sshd_config && rm /etc/ssh/sshd_config.bak && service sshd restart

# add generating video contactsheets to root user's cron (nginx doesn't work)
{ crontab -l -u root; echo '*/10 * * * * php /projects/s3video/artisan video:generatecontactsheets >> /dev/null 2>&1'; } | crontab -u root -

