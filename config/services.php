<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

    'facebook' => [
        'client_id' => '1445530722439130',
        'client_secret' => '29e8e42e470836b3813bd066c2174ab8',
        'redirect' => 'http://gdson.net/login/facebook',
    ],

    'ganalytics' => [
        'account_id' => 'UA-65278915-1',
    ],

	'mailgun' => [
		'domain' => 'sandbox89f029ba436644829edc9ffe3746c3e4.mailgun.org',
		'secret' => 'key-abda5c92f732fe607d20c86e44a87584',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

    'cloudfront' => [
		'web-distribution-url'  => '//d8kddvpb911c6.cloudfront.net',
		'bucket-prefix'  => '',/* may be 'uploads/' */
		'rtmp-distribution-url' => 'rtmp://s1u7f3hsym509m.cloudfront.net',
	],

    'video-cdn' => [
		'servers'  => [
            '//vcdn1.gdson.net',
            '//vcdn2.gdson.net',
            '//vcdn3.gdson.net',
            '//vcdn4.gdson.net',
            '//vcdn5.gdson.net',
            '//vcdn6.gdson.net',
            '//vcdn7.gdson.net',
            //'//vcdn8.gdson.net',
            //'//vcdn9.gdson.net',
            //'//vcdn10.gdson.net',
            //'//vcdn11.gdson.net',
            //'//vcdn12.gdson.net',
            //'//vcdn13.gdson.net',
            //'//vcdn14.gdson.net',
        ],
	],

    'red5' => [
		'rtmp-url' => 'rtmp://vcdn2.gdson.net:1935',
		'rtmp-url-path' => 'rtmp://vcdn2.gdson.net:1935/oflaDemo',
        'cdn-servers' => array(
            array('rtmp-url' => 'rtmp://vcdn2.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn2.gdson.net:1935/oflaDemo'),
            array('rtmp-url' => 'rtmp://vcdn3.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn3.gdson.net:1935/oflaDemo'),
            array('rtmp-url' => 'rtmp://vcdn4.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn4.gdson.net:1935/oflaDemo'),
            array('rtmp-url' => 'rtmp://vcdn5.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn5.gdson.net:1935/oflaDemo'),
            array('rtmp-url' => 'rtmp://vcdn6.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn6.gdson.net:1935/oflaDemo'),
            array('rtmp-url' => 'rtmp://vcdn7.gdson.net:1935',
            'rtmp-url-path' => 'rtmp://vcdn7.gdson.net:1935/oflaDemo'),
        )
	],

];
