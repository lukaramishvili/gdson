<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'permissions';

    public $timestamps = false;
    
    
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];


    /**
     * Get permission with specified name
     */
    public static function byName($name)
    {
        return Permission::where('name','=',$name)->first();
    }
 
    /**
     * Get roles that contain this permission
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'permissions_roles')->withTimestamps();
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
