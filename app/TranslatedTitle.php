<?php namespace App;

trait TranslatedTitle {

    //public $title_tr_id;

	/**
	 * @return translated title
	 */
	public function getTitle($lang)
	{
	    $tr = Translation::find($this->title_tr_id)->toArray();
        return $tr["value_$lang"];
	}

    /**
     * Get raw of translated titles
     *
     * @return array
     */
    public function getTitles()
    {
        return Translation::find($this->title_tr_id)->toArray();
    }

}
