<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FbNotification extends Model {

    use TranslatedTitle;
    use TranslatedDescription;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fbnotifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'desc_tr_id', 'whose_type', 'whose_id', 'publish_date', 'already_sent'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get show/season/episode it belongs to
     */
    public function whose()
    {
        return $this->belongsTo('App\\' . (ucwords(strtolower($this->whose_type))), 'whose_id');
    }

}
