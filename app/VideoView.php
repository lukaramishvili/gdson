<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoView extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_views';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['video_id', 'lang', 'viewcount'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }
}
