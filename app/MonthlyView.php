<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyView extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'monthly_views';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
