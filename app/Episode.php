<?php namespace App;

use Aws\Ses\Enum\NotificationType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class Episode extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use TranslatedShortDescription;
    use Directable;
    use Search;
    use Publishable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'episodes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['season_id', 'video_id', 'title_tr_id', 'desc_tr_id', 'episode_number', 'visibility', 'release_date', 'publish_date', 'alias', 'short_desc_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get episode by alias
     */
    public static function byAlias($alias)
    {
        return self::where('alias', $alias)->first();
    }

    /**
     * get a string like "SHOW NAME, SEASON, EPISODE_TITLE
     */
    public function getFullTitle($lang){
        if($this->season->show->hasMoviesGenre()){ return $this->getTitle("geo"); }
        else {
            return $this->season->show->getTitle("geo")
                .", სეზონი " . $this->season->season_number
                . ($this->episode_number > 0 ? ", სერია " . $this->episode_number : "");
        }
    }

    public function getFullTitleNbsp($lang){
        $full_title = str_replace(", ", "&nbsp;&nbsp;&nbsp;", $this->getFullTitle($lang));
        return $full_title;
    }

    public function getTitleForShare($lang){
        $share_title = str_replace(",", " ", $this->getFullTitle($lang));
        return $share_title;
    }

    /**
     * Get episode url
     */
    public function getUrl()
    {
        if($this->alias && strlen($this->alias) > 0){
            return URL::to('/show/view/' . ($this->season->show->alias) . '/sezoni-' . ($this->season->season_number) . '/' . ($this->alias));
        } else {
            return URL::to('/show/view/' . ($this->season->show->alias) . '/sezoni-' . ($this->season->season_number) . '/' . ($this->episode_number));
        }
    }

    /**
     * Get image url
     */
    public function getImageUrl()
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/episode/' . ($this->id) . '/cover.jpg');
    }

    /**
     * Get image url (2x)
     */
    public function getImageUrlSecond()
    {
        return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/episode/' . ($this->id) . '/cover@2x.jpg');
    }

    /**
     * Get video image url
     */
    public function getVideoImageUrl()
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/episode/' . ($this->id) . '/video/' . ($this->video_id) .'/video.jpg');
    }

    /**
     * Get image path
     */
    public function getImagePath()
    {
        return public_path() . '/uploads/episode/' . ($this->id) . '/cover.jpg';
    }

    /**
     * Get the season it belongs to
     */
    public function season()
    {
        return $this->belongsTo('App\Season');
    }

    /**
     * Get episode's posts
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post')->withTimestamps();
    }

    /**
     * Get episode's video
     */
    public function video()
    {
        return $this->belongsTo('App\Video');
    }

    /**
     * Get previous episode
     */
    public function prevEpisode()
    {
        if ($this->episode_number != 1) {
            return Episode::where('season_id', '=', $this->season_id)
                          ->where('episode_number', '=', $this->episode_number - 1);
        }

        $show_id = $this->season->show->id;
        $season  = Season::where('show_id', '=', $show_id)
                         ->where('season_number', '=', $this->season_number - 1)
                         ->get();

        return Episode::where('season_number', '=', $season->season_number)
                      ->where('episode_number', '=', $season->series_count);
    }

    /**
     * Get next episode
     */
    public function nextEpisode()
    {
        $show_id = $this->season->show->id;
        $season  = Season::where('show_id', '=', $show_id)
                         ->where('season_number', '=', $this->season_number)
                         ->get();

        if ($this->episode_number != $season->series_count) {
            return Episode::where('season_id', '=', $this->season_id)
                          ->where('episode_number', '=', $this->episode_number + 1);
        }

        $season  = Season::where('show_id', '=', $show_id)
                         ->where('season_number', '=', $this->season_number + 1)
                         ->get();

        return Episode::where('season_number', '=', $season->season_number)
                      ->where('episode_number', '=', 1);
    }

    /**
     * @return episode trailers
     */
    public function trailers()
    {
        $trailers = Trailer::where('whose_type', 'episode')
                           ->where('whose_id', $this->id)
                           ->get();

        return $trailers;
    }

    /**
     * Check if episode is added in promo
     */
    public function checkPromo()
    {
        $promoCount = Promo::where('whose_id', $this->id)
                           ->where('whose_type', 'episode')
                           ->count();

        if ($promoCount > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    /**
     * Get popular episodes for last month
     */
    public static function allPopular()
    {
        $popularVideos = [];
        $videos        = Video::all();
        foreach ($videos as $video)
        {
            $viewCount = $video->oneMonthWatches();
            if (array_key_exists($viewCount, $popularVideos))
            {
                $viewCount -= 1;
            }

            $popularVideos[$viewCount] = $video;
        }

        ksort($popularVideos);
        $popularVideos = array_reverse($popularVideos, true);

        $count    = 10;
        $episodes = [];
        foreach ($popularVideos as $key => $popularVideo)
        {
            if ($count == 0) break;
            $episode = Episode::where('video_id', $popularVideo->id)->first();
            if (!empty($episode))
            {
                $episodes[] = $episode;
                $count --;
            }
        }

        return $episodes;
    }

    public function mainGenre(){
        return $this->season->show->mainGenre();
    }

    /**
     * Get popular episodes for last month
     */
    public static function getMonthlyPopularEpisodes()
    {
        $ep_keys         = [];
        $popularEpisodes = PopularEpisode::limit(10)->orderBy('viewcount', 'DESC')->get();
        foreach ($popularEpisodes as $popularEpisode)
        {
            $ep_keys[]   = $popularEpisode->episode_id;
        }

        //temporary cache
        $eps             = Episode::findMany($ep_keys);
        $eps_indexed     = array();
        foreach($eps as $ep) { $eps_indexed [$ep->id] = $ep; }
        $eps_ordered     = array();
        foreach($ep_keys as $ep_key) { $eps_ordered []= $eps_indexed[$ep_key]; }
        return $eps_ordered;
        //
        $popularVideos = [];
        $videos        = Video::all();
        foreach ($videos as $video)
        {
            $monthlyVideoView = $video->getMonthlyVideoView();
            if (isset($monthlyVideoView) && !empty($monthlyVideoView))
            {
                if (isset($monthlyVideoView[0]))
                {
                    $viewCount                 = $monthlyVideoView[0]->viewcount;
                    $popularVideos[$viewCount] = $video;
                }
            }
        }

        ksort($popularVideos);
        $popularVideos = array_reverse($popularVideos, true);

        $count    = 10;
        $episodes = [];
        foreach ($popularVideos as $key => $popularVideo)
        {
            if ($count == 0) break;
            $episode = Episode::where('video_id', $popularVideo->id)->first();
            if (!empty($episode))
            {
                $episodes[] = $episode;
                $count --;
            }
        }

        return $episodes;
    }

    /**
     * Get popular episodes for last week
     */
    public static function getWeeklyPopularEpisodes()
    {
        $ep_keys         = [];
        $popularEpisodes = PopularEpisode::limit(10)->orderBy('viewcount', 'DESC')->get();
        foreach ($popularEpisodes as $popularEpisode)
        {
            $ep_keys[]   = $popularEpisode->episode_id;
        }

        //temporary cache
        $eps             = Episode::findMany($ep_keys);
        $eps_indexed     = array();
        foreach($eps as $ep) { $eps_indexed [$ep->id] = $ep; }
        $eps_ordered     = array();
        foreach($ep_keys as $ep_key) { $eps_ordered []= $eps_indexed[$ep_key]; }
        return $eps_ordered;
        //
        $popularVideos = [];
        $videos        = Video::all();
        foreach ($videos as $video)
        {
            $monthlyVideoView = $video->getWeeklyVideoView();
            if (isset($monthlyVideoView) && !empty($monthlyVideoView))
            {
                if (isset($monthlyVideoView[0]))
                {
                    $viewCount                 = $monthlyVideoView[0]->viewcount;
                    $popularVideos[$viewCount] = $video;
                }
            }
        }

        ksort($popularVideos);
        $popularVideos = array_reverse($popularVideos, true);

        $count    = 10;
        $episodes = [];
        foreach ($popularVideos as $key => $popularVideo)
        {
            if ($count == 0) break;
            $episode = Episode::where('video_id', $popularVideo->id)->first();
            if (!empty($episode))
            {
                $episodes[] = $episode;
                $count --;
            }
        }

        return $episodes;
    }

    /**
     * Check if episode has notification
     *
     * @return bool
     */
    public function fbNotification()
    {
        $count = FbNotification::where('whose_id', $this->id)
                               ->where('whose_type', 'episode')
                               ->count();

        return $count > 0 ? true : false;
    }

    /**
     * Get episode notification
     */
    public function getFbNotification()
    {
        $notification = FbNotification::where('whose_id', $this->id)
                                        ->where('whose_type', 'episode')
                                        ->get()
                                        ->first();

        return $notification;
    }

    /**
     * Get last added episodes
     */
    public static function lastAdded($number)
    {
        $count          = 0;
        $episodes_array = [];
        foreach (Episode::orderBy('created_at', 'DESC')->get() as $episode)
        {
            if ($episode->isPublished())
            {
                $episodes_array[] = $episode;
                $count ++;
            }

            if ($count == $number)
            {
                break;
            }
        }

        return $episodes_array;
    }

}
