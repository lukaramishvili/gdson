<?php namespace App\Console;

use App\FbNotification;
use App\Helper;
use App\NotificationRolls;
use App\PopularEpisode;
use App\PopularRolls;
use App\Translation;
use App\User;
use App\Video;
use App\VideoView;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Config;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();

        $schedule->call(function() {
            $serverId = md5(rand());

            while (true)
            {
                PopularRolls::where('server_id', $serverId)->delete();

                $role = round(rand(1, 1000000));
                PopularRolls::create([
                    'server_id' => $serverId,
                    'role'      => $role
                ]);

                //usleep(100000);
                sleep(10);

                $max         = PopularRolls::all()->max('role');
                $winnerRoles = PopularRolls::where('role', $max)->get();
                if ($winnerRoles->count() > 1)
                {
                    //usleep(100000);
                    sleep(10);
                    continue;
                }
                else if ($winnerRoles->count() == 1 && $winnerRoles->first()->server_id == $serverId)
                {
                    $videos     = Video::all();
                    PopularEpisode::whereNotNull('id')->delete();
                    foreach ($videos as $video)
                    {
                        $viewCount  = $video->oneWeekWatches();

                        $videoViews = VideoView::where('video_id', $video->id)
                                               ->get()
                                               ->toArray();

                        if (isset($videoViews) && !empty($videoViews))
                        {
                            VideoView::where('video_id', $video->id)
                                     ->delete();
                        }

                        VideoView::create(['video_id' => $video->id, 'lang' => 'geo', 'viewcount' => $viewCount]);
                        if ($video->episode)
                        {
                            PopularEpisode::create(['episode_id' => $video->episode->id, 'viewcount' => $viewCount]);
                        }
                    }

                    PopularRolls::whereNotNull('id')->delete();
                } /* End else if */

                break;
            } /* End while true */
        })->dailyAt('13:00');

        $schedule->call(function() {
            $serverId = md5(rand());

            while (true)
            {
                NotificationRolls::where('server_id', $serverId)->delete();

                $role = round(rand(1, 1000000));
                NotificationRolls::create([
                    'server_id' => $serverId,
                    'role'      => $role
                ]);

                //usleep(100000);
                sleep(10);

                $max         = NotificationRolls::all()->max('role');
                $winnerRoles = NotificationRolls::where('role', $max)->get();
                if ($winnerRoles->count() > 1)
                {
                    //usleep(100000);
                    sleep(10);
                    continue;
                }
                else if ($winnerRoles->count() == 1 && $winnerRoles->first()->server_id == $serverId)
                {
                    $app_id            = Config::get('services.facebook.client_id');
                    $app_secret        = Config::get('services.facebook.client_secret');
                    $href              = "https://gdson.net/admin/fbcanvas/";
                    $app_access_token  = $app_id . '|' . $app_secret;

                    $notifications     = FbNotification::all();
                    $users             = User::all();
                    foreach ($notifications as $notification)
                    {
                        if ($notification->already_sent != 1)
                        {
                            $publish_time = Helper::strtotime_geo($notification->publish_date);
                            if (time() > $publish_time)
                            {
                                $title    = str_replace(' ', '%20', $notification->getTitle('geo'));
                                $desc     = str_replace(' ', '%20', $notification->getDescription('geo'));
                                $message  = $title . '%20-%20' . $desc;
                                foreach ($users as $user)
                                {
                                    //if ($user->id == 9 || $user->id == 7 || $user->id == 8 || $user->id == 471 || $user->id == 4 || $user->id == 3 || $user->id == 1 || $user->id == 260 || $user->id == 2283 || $user->id == 2276)
                                    //{
                                        $ch   = curl_init();
                                        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/" . $user->provider_id . "/notifications?access_token=" . $app_access_token . "&href=" . $href . '?object=' . $notification->whose_id . '/' . $notification->whose_type . "&template=" . $message . "");
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_exec($ch);
                                        curl_close($ch);
                                    //}
                                }

                                $notification->already_sent = 1;
                                $notification->save();
                            }
                        }
                    }

                    NotificationRolls::whereNotNull('id')->delete();
                } /* End else if */

                break;
            } /* End while true */
        })->everyFiveMinutes(); /* End of scheduled call */
	}

}