<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use App\Episode;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class Video extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use TranslatedSubtitles;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'desc_tr_id', 'subtitles_tr_id', 'type'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get video's view count
     * TODO: gadasaketebelia, exla ramdenime chanaceri aqvs tito lang-istvis
     */
    public function view_count()
    {
        return $this->hasOne('App\VideoView');
    }

    /**
     * Get video's watchs
     */
    public function watchs()
    {
        return $this->hasMany('App\VideoWatch');
    }

    /**
     * Get count of video's views for last month
     */
    public function oneMonthWatches()
    {
        return $this->hasMany('App\VideoWatch')->where('created_at', '>', Carbon::now()->subMonth())->count();
    }

    /**
     * Get count of video's views for last week
     */
    public function oneWeekWatches()
    {
        return $this->hasMany('App\VideoWatch')->where('created_at', '>', Carbon::now()->subWeek())->count();
    }

    /**
     * Get video's views
     */
    public function views()
    {
        return $this->hasOne('App\VideoView');
    }

    /**
     * Get video's view count (aggregated watch count)
     */
    public function getViewcount()
    {
        //TODO: when watch count will be cached in video_views, return that instead
        // (for performance reasons)
        return $this->watchs()->count();
    }

    /**
     * Get monthly views for video
     */
    public function getMonthlyVideoView()
    {
        return VideoView::where('video_id', $this->id)->get();
    }

    /**
     * Get weekly views for video
     */
    public function getWeeklyVideoView()
    {
        return VideoView::where('video_id', $this->id)->get();
    }

    /**
     * Add to video's count
     */
    public function addWatch($lang)
    {
        //TODO: check if lang exists in global lang list
        $vw = VideoWatch::create(array(
            'video_id' => $this->id,
            'lang' => $lang,
        ));
    }

    /**
     * Get video's view count for last month
     */
    public function recent_popular()
    {
        return $this->hasMany('App\MonthlyView');
    }

    /**
     * Get the episode, video belongs to
     */
    public function episode(){
        return $this->hasOne('App\Episode');
    }

    public function trailer()
    {
        return $this->hasOne('App\Trailer');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    /**
     * Get image to crop
     */
    public function getCoverToCrop()
    {
        return '/uploads/video/' . $this->id . '/cover/crop.jpg';
    }

    /**
     * Get image path to crop
     */
    public function getCoverToCropPath()
    {
        return public_path() . '/uploads/video/' . $this->id . '/cover/crop.jpg';
    }

    /**
     * Get cropped image
     */
    public function getCroppedCover()
    {
        return '/uploads/video/' . $this->id . '/cover/cropped.jpg';
    }

    /**
     * Get cropped image path
     */
    public function getCroppedCoverPath()
    {
        return public_path() . '/uploads/video/' . $this->id . '/cover/cropped.jpg';
    }

    /**
     * Get image to crop
     */
    public function getImageToCrop()
    {
        return '/uploads/video/' . $this->id . '/image/crop.jpg';
    }

    /**
     * Get image path to crop
     */
    public function getImageToCropPath()
    {
        return public_path() . '/uploads/video/' . $this->id . '/image/crop.jpg';
    }

    /**
     * Get cropped image
     */
    public function getCroppedImage()
    {
        return '/uploads/video/' . $this->id . '/image/cropped.jpg';
    }

    /**
     * Get cropped image path
     */
    public function getCroppedImagePath()
    {
        return public_path() . '/uploads/video/' . $this->id . '/image/cropped.jpg';
    }

}
