<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Request;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        //when the user denies app access
        if(Request::has('error') && Request::get('error') == "access_denied"){
            return new RedirectResponse(url('/oauth-canceled'));
        }
		if ($this->auth->check())
		{
            //this only works on consequent logins, but on the first login, callback
            //url via fb api doesn't work and we do this same redirect in main.js too
            //change url in both files if we should redirect somewhere else after fb login
			return new RedirectResponse(url('/'));
		}

		return $next($request);
	}

}
