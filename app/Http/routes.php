<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'GdsonController@index');
Route::get('about', 'GdsonController@about');
Route::get('faq', 'GdsonController@faq');
Route::get('contact', 'GdsonController@contact');
Route::post('contact', 'GdsonController@submitcontact');
Route::get('highlights', 'GdsonController@highlights');
Route::get('popular', 'GdsonController@popular');
Route::get('lastAdded', 'GdsonController@lastAdded');
Route::get('search/{name?}', 'GdsonController@search');
Route::post('search', 'GdsonController@searchresults');
Route::get('/fbcanvas/test', 'AdminController@canvastest');

Route::get('/oauth-canceled', 'GdsonController@index');

Route::get('/deploy', function(){
    $output_string = "";
    $o = array();
    exec("cd ".base_path()." && git pull && php artisan migrate", $o);
    foreach($o as $l){
        $output_string .= $l."<br>";
    }
    return $output_string;
});

Route::controllers([
	'auth'      => 'Auth\AuthController',
	'password'  => 'Auth\PasswordController',
    'user'      => 'UserController',
    'admin'     => 'AdminController',
    'show'      => 'ShowController',
    'video'     => 'VideoController',
    'genres'    => 'GenreController',
    'playlist'  => 'PlaylistController',
    'person'    => 'PersonController',
    'faq'       => 'FaqController',
    'page'      => 'PageController',
    'highlight' => 'HighlightController',
    'promo'     => 'PromoController',
    'fb'        => 'FacebookController'
]);

Route::get('login/{provider?}', 'Auth\AuthController@login');


Route::get('loaderio-05b4190c6da598b53ac658f5c7a8b9ad', function(){
    return "loaderio-05b4190c6da598b53ac658f5c7a8b9ad";
});
