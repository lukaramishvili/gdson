<?php namespace App\Http\Controllers;

use App\Faq;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class FaqController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['']]);
    }

    /**
     * Get all faqs
     *
     * @return View
     */
    public function getAll()
    {
        $faqs = Faq::all();

        return view('faq.all')
            ->with('faqs', $faqs)
            ->with('active', 'faq');
    }

    /**
     * Add new faq
     *
     * @return View
     */
    public function getAdd()
    {
        $title_tr = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'title']);
        $desc_tr  = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);

        $faq      = Faq::create(['title_tr_id' => $title_tr->id, 'desc_tr_id' => $desc_tr->id]);

        $faq['title_tr'] = $faq->getTitles();
        $faq['desc_tr']  = $faq->getDescriptions();

        return view('faq.edit')
            ->with('faq', $faq)
            ->with('type', 'add')
            ->with('active', 'faq');
    }

    /**
     * Edit faq
     *
     * @param $faq_id
     * @return View
     */
    public function getEdit($faq_id)
    {
        $faq = Faq::find($faq_id);

        $faq['title_tr'] = $faq->getTitles();
        $faq['desc_tr']  = $faq->getDescriptions();

        return view('faq.edit')
            ->with('faq', $faq)
            ->with('type', 'edit')
            ->with('active', 'faq');
    }

    /**
     * Save editted faq
     */
    public function postEdit()
    {
        $field = Input::get('field');
        $faq   = Faq::find(Input::get('pk'));

        Translation::find($faq->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);
    }

    /**
     * Delete faq
     *
     * @param $faq_id
     * @return Redirect
     * @throws \Exception
     */
    public function getDelete($faq_id)
    {
        Faq::find($faq_id)->delete();

        return redirect('/faq/all');
    }
}
