<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use App\Video;
use App\VideoView;

class VideoController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin', ['except' => ['']]);
    }

    /**
     * 
     */
    public function postAddwatch($video_id, $lang)
    {
        $result = "nothing happened";
        if(is_numeric($video_id) && Video::find($video_id)){
            Video::find($video_id)->addWatch($lang);
            $result = "success";
        } else { $result = "Video not found"; }
        return json_encode(array("result" => $result));
    }
}
