<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class GenreController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getView']]);
    }

    /**
     * Show shows, videos, etc by genre
     *
     * @param $alias
     * @return Response
     */
    public function getView($alias)
    {
        $genre = Genre::byAlias($alias);

        return view('genres.view')
            ->with(array('genre' => $genre, 'activemenu' => 'genre-'.$genre->id));
	}

    /**
     * Get all genres
     *
     * @return View
     */
    public function getAll()
    {
        $genres        = Genre::all();
        $genre_array   = [];
        $parent_genres = ['No parent'];
        foreach ($genres as $genre) {
            $genre_array[]      = array(
                'genre_id'      => $genre['id'],
                'alias'         => $genre['alias'],
                'order'         => $genre['order'],
                'translations'  => $genre->getTitles(),
                'default_genre' => [$genre->parent_id ? $genre->parent_id : 0],
            );

            $geo = $genre->getTitle('geo');
            $eng = $genre->getTitle('eng');
            $rus = $genre->getTitle('rus');
            if (isset($geo)) {
                $parent_genres[$genre['id']] = $geo;
            } else if (isset($eng)) {
                $parent_genres[$genre['id']] = $eng;
            } else if (isset($rus)) {
                $parent_genres[$genre['id']] = $rus;
            }
        }
        return view('genres.all')
            ->with('genres', $genre_array)
            ->with('parent_genres', $parent_genres)
            ->with('active', 'genres');
    }

    /**
     * Edit/Add genre
     */
    public function postEdit()
    {
        if (Input::get('name') == 'alias') {
            if (Input::get('pk') > 0) {
                $genre = Genre::find(Input::get('pk'));
                $genre->alias = Input::get('value');
                $genre->save();
            } else {
                $translation = Translation::create(
                    array(
                        'value_geo' => 'ჟანრი',
                        'value_eng' => 'genre',
                        'value_rus' => 'genre'
                    )
                );

                $genre       = Genre::create(['title_tr_id' => $translation->id, 'alias' => Input::get('value')]);
            }
        } else {
            if (Input::get('pk') > 0) {
                $genre = Genre::find(Input::get('pk'));

                $value = Input::get('value');
                $value = str_replace(explode(',', 'ა,ბ,გ,დ,ე,ვ,ზ,თ,ი,კ,ლ,მ,ნ,ო,პ,ჟ,რ,ს,ტ,უ,ფ,ქ,ღ,ყ,შ,ჩ,ც,ძ,წ,ჭ,ხ,ჯ,ჰ'), explode(',', 'a,b,g,d,e,v,z,th,i,k,l,m,n,o,p,jh,r,s,t,u,f,q,gh,kh,sh,ch,c,dz,ts,tch,kh,j,h'), $value);
                $value = str_replace(explode(',', '?'), explode(',', ''), $value);

                Translation::where('id', '=', $genre->title_tr_id)
                    ->update(['value_' . Input::get('name') => Input::get('value')]);

                $genre->alias = $value;
                $genre->save();
            } else {
                $translation = Translation::create(['value_' . Input::get('name') => Input::get('value')]);
                $genre       = Genre::create(['title_tr_id' => $translation->id, 'alias' => str_random(12)]);
            }

            return json_encode(['id' => $genre->id]);
        }

        return json_encode(['id' => $genre->id]);
    }

    /**
     * Edit genre parent
     */
    public function postParent()
    {
        $genre  = Input::get('genre');
        $parent = Input::get('parent') != 0 ? Input::get('parent') : null;
        Genre::find($genre)
             ->update(['parent_id' => $parent]);
    }

    /**
     * Delete genre
     *
     * @param $genre_id
     * @return View
     */
    public function getDelete($genre_id)
    {
        $genre = Genre::find($genre_id);
        $genre->delete();

        Translation::where('id', '=', $genre->title_tr_id)
                   ->delete();

        return Redirect::back();
    }

    /**
     * Update genre order
     */
    public function postOrder()
    {
        Genre::where('id', '=', Input::get('genre_id'))
            ->update(['order' => Input::get('value')]);
    }
}
