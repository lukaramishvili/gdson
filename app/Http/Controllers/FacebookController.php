<?php namespace App\Http\Controllers;

use App\FbNotification;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FacebookController extends Controller {

    /**
     * Class constructor
     */
    public function __construct()
    {

	}

    /**
     * View for editing fb notifications
     */
    public function getNotifications()
    {
        $notifications = FbNotification::all();

        return view('facebook.fbNotifications')
            ->with('notifications', $notifications)
            ->with('active', 'fbnotifications');
    }

    /**
     * Edit fb notification title/description
     */
    public function postEditfbnotification()
    {
        $field = Input::get('field');

        $target = FbNotification::find(Input::get('pk'));
        Translation::find($target->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);
    }

    /**
     * Edit fb notification publish date
     */
    public function postFbnotificationdatetime()
    {
        $value     = Input::get('value');
        $value     = Carbon::createFromFormat('m/d/Y H:i', $value)->format('Y-m-d H:i');

        $notification = FbNotification::find(Input::get('id'));
        $notification->publish_date = $value;
        $notification->save();
    }

}
