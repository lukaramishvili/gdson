<?php namespace App\Http\Controllers;

use App\Character;
use App\Crewmember;
use App\FbNotification;
use App\Genre;
use App\Highlight;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Person;
use App\PopularEpisode;
use App\Post;
use App\Profession;
use App\Show;
use App\Season;
use App\Episode;
use App\Tag;
use App\Trailer;
use App\Translation;
use App\Video;
use Aws\CloudFront\CloudFrontClient;
use Aws\Ses\Enum\NotificationType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class ShowController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // admin middleware for protecting admin routes
        $this->middlewAre('admin', ['except' => ['getView', 'getTest']]);
    }

    /**
     * Get all shows in admin
     *
     * @return View
     */
    public function getShows()
    {
        $shows = Show::all();

        foreach($shows as &$show) {
            $show['title']       = Translation::find($show->title_tr_id)->toArray();
            $show['description'] = Translation::find($show->desc_tr_id)->toArray();
            $show['cover']       = $show->getImageUrl();
        }

        return view('show.shows')
            ->with('shows', $shows->toArray())
            ->with('active', 'shows');
    }

    /**
     * Show page, getting show with show alias,
     * season number and episode number
     *
     * @param $alias
     * @param int $season_number
     * @param int $episode_number
     * @return View
     */
    public function getView($alias, $season_number = null, $episode_number = null)
    {
        $show = Show::byAlias($alias);
        if(!$show->isPublished()){ return redirect('/'); }

        $url_target_type = "show";
        if(!is_null($season_number)){
            $url_target_type = "season";
        }
        if(!is_null($episode_number)){
            $url_target_type = "episode";
        }

        if(is_null($season_number)){
            $last_visible_season = $show->seasons()->orderBy('season_number')->get()->filter(function($ssn){ return $ssn->isPublished(); })->last();
            if($last_visible_season){
                $season_number = $last_visible_season->season_number;
            } else {
                //no visible seasons found for the show
                $season_number = 1;
            }
        }

        $season_number_matches = array();
        if(is_numeric($season_number)){
            //correct parameter was passed
        } elseif(preg_match("/\d+/", $season_number, $season_number_matches) > 0){
            $season_number = $season_number_matches[0];
        } else {
            //if a parameter was specified but was invalid, use the default number 1
            $season_number = 1;
        }
        $season = $show->getSeason($season_number);
        //check for existence
        if(!$show->getSeason($season_number)){
            $season_number = 1;
            $season = $show->getSeason($season_number);
        }

        if ($season) {
            if(is_null($episode_number)){
                $last_episode = $season->episodes
                    ->filter(function($episode){ return $episode->isPublished(); })
                    ->last();
                if($last_episode){
                    $episode_number = $last_episode->episode_number;
                }
            }
            if(is_numeric($episode_number)){
                $episode = $season->getEpisode($episode_number);
            } else {
                $episode = Episode::byAlias($episode_number);
                if($episode){
                    $episode_number = $episode->episode_number;
                }
            }
        }

        $posts = array();
        if(isset($episode)){
            foreach($episode->posts as $cur_post){
                $posts []= $cur_post;
            }
        }
        if(isset($season)){
            foreach($season->posts as $cur_post){
                $posts []= $cur_post;
            }
        }
        foreach($show->posts as $cur_post){
            $posts []= $cur_post;
        }

        $data = array(
            'activemenu' => 'show-'.$show->id,
            'show' => $show,
            'posts' => $posts,
            'currentpage' => 'show',
            'url_target_type' => $url_target_type,
        );
        if(isset($season)){ $data['season'] = $season; }
        if(isset($episode)){ $data['episode'] = $episode; }

        return view('show.show')
            ->with($data);
    }

    /**
     * Add new show page
     *
     * @return View
     */
    public function getAddshow()
    {
        $title_tr   = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'title']);
        $desc_tr    = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);
        $short_desc = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);
        $show_arr   = array(
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'alias'            => str_random(8),
            'visibility'       => false,
        );

        $show                    = Show::create($show_arr)->toArray();
        $show['title_tr']        = $title_tr->toArray();
        $show['desc_tr']         = $desc_tr->toArray();
        $show['short_desc_tr']   = $short_desc->toArray();
        $show['cover']           = false;
        $show['datetime']        = '';

        $show['publish_datetime'] = '';
        $show['release_datetime'] = '';

        $genres      = Genre::all();
        $genre_array = [];
        foreach ($genres as $genre) {
            $genre_array[$genre['id']] = $genre->getTitle('geo');
        }

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions      = Profession::lists('keyword', 'id');

        return view('show.editShow')
            ->with('show', $show)
            ->with('all_genres', $genre_array)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit show page
     *
     * @param $alias
     * @return View
     */
    public function getEditshow($alias)
    {
        $show = Show::byAlias($alias);

        $show['title_tr']      = $show->getTitles();
        $show['desc_tr']       = $show->getDescriptions();
        $show['short_desc_tr'] = $show->getShortDescriptions();

        $show['cover']       = '/uploads/show/' .  $show->id . '/cover.jpg';
        if (!file_exists(public_path() . $show['cover'])) {
            $show['cover']   = false;
        }

        $show['cover_image']          = $show->getImageUrl();
        $show['cover_image_second']   = $show->getImageUrlSecond();

        $show['publish_datetime']     = '';
        if (isset($show->publish_date) && !empty($show->publish_date)) {
            $show['publish_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $show->publish_date)->format('m/d/Y H:i');
        }

        $show['release_datetime']     = '';
        if (isset($show->release_date) && !empty($show->release_date)) {
            $show['release_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $show->release_date)->format('m/d/Y H:i');
        }

        $genres      = Genre::all();
        $genre_array = [];
        foreach ($genres as $genre) {
            $title = $genre->getTitle('geo');
            if (empty($title)) {
                $title = $genre->getTitle('eng');
            }

            if (empty($title)) {
                $title = $genre->getTitle('rus');
            }
            $genre_array[$genre['id']] = $title;
        }

        $show_genres       = $show->genres;
        $show_genres_array = [];
        foreach ($show_genres as $genre) {
            $show_genres_array[] = $genre['id'];
        }

        $posts = $show->posts;
        foreach ($posts as $post) {
            $post['title_tr'] = $post->getTitles();
            $post['desc_tr']  = $post->getDescriptions();
        }

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $crews = Crewmember::where('where_type', 'show')
                              ->where('where_id', $show->id)
                              ->where('profession_id', $key)
                              ->get();

            foreach ($crews as $crew) {
                $default_persons[$profession][] = $crew->person_id;
            }

        }

        $trailers = $show->trailers();
        foreach ($trailers as &$trailer) {
            $trailer['video'] = $trailer->video()
                                        ->first();

            $trailer['video']['title_tr']     = $trailer['video']->getTitles();
            $trailer['video']['desc_tr']      = $trailer['video']->getDescriptions();
            $trailer['video']['subtitle_tr']  = $trailer['video']->getSubtitles();
        }

        return view('show.editShow')
            ->with('show', $show->toArray())
            ->with('all_genres', $genre_array)
            ->with('posts', $posts)
            ->with('default_genres', $show_genres_array)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('trailers', $trailers)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Store edited show in db
     *
     * @return string
     */
    public function postEditshow()
    {
        $field = Input::get('field');
        $show  = Show::find(Input::get('pk'));

        Translation::find($show->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);

        if (Input::get('name') == 'eng' && $field == 'title_tr_id') {
            $value = Input::get('value');
            $value = str_replace(explode(',', '?, '), explode(',', ',-'), $value);
            if ($this->checkAlias($value)) // if show alias exists, don't change the new value
            {
                $show->update(['alias' => $value]);
            }
        } else if (Input::get('name') == 'geo' && $show->getTitle('eng') == 'title' && $field == 'title_tr_id') {
            $value = Input::get('value');
            $value = str_replace(explode(',', 'ა,ბ,გ,დ,ე,ვ,ზ,თ,ი,კ,ლ,მ,ნ,ო,პ,ჟ,რ,ს,ტ,უ,ფ,ქ,ღ,ყ,შ,ჩ,ც,ძ,წ,ჭ,ხ,ჯ,ჰ'), explode(',', 'a,b,g,d,e,v,z,th,i,k,l,m,n,o,p,jh,r,s,t,u,f,q,gh,kh,sh,ch,c,dz,ts,tch,kh,j,h'), $value);
            $value = str_replace(explode(',', '?, '), explode(',', ',-'), $value);
            if ($this->checkAlias($value)) // if show alias exists, don't change the new value
            {
                $show->update(['alias' => $value]);
            }
        }

        return 'success';
    }

    /**
     * Check alias for episodes and shows,
     * if alias exists return false
     *
     * @param $alias
     * @param string $type
     * @param null $episodeId
     * @return bool
     */
    private function checkAlias($alias, $type = 'show', $episodeId = null)
    {
        if ($type == 'show') // check alias for shows
        {
            foreach (Show::all() as $show)
            {
                if ($show->alias == $alias)
                    return false;
            }
        }
        else if ($type == 'episode' && $episodeId != null) // check alias for episodes
        {
            $episode   = Episode::find($episodeId);
            $seasonId  = $episode->season_id;
            foreach (Episode::where('season_id', $seasonId)->get() as $episode)
            {
                if ($episode->alias == $alias)
                    return false;
            }
        }

        return true;
    }

    /**
     * Delete show
     *
     * @throws \Exception
     */
    public function postCancelshow()
    {
        $show = Show::find(Input::get('show_id'));
        $show->genres()->detach();
        $show->delete();

        Translation::find($show->title_tr_id)->delete();
        Translation::find($show->desc_tr_id)->delete();

        Crewmember::where('where_type', 'show')
                  ->where('where_id', Input::get('show_id'))
                  ->delete();
    }

    /**
     * Edit show alias
     */
    public function postEditalias()
    {
        $value = Input::get('value');
        $value = str_replace(explode(',', '?, '), explode(',', ',-'), $value);
        if ($this->checkAlias($value)) // if show alias exists, don't change the new value
        {
            Show::find(Input::get('pk'))
                ->update(['alias' => $value]);
        }
        else
        {
            return 'exists';
        }
    }

    /**
     * Edit episode alias
     */
    public function postEditepisodealias()
    {
        $epId  = Input::get('pk');
        $value = Input::get('value');
        $value = str_replace(explode(',', '?, '), explode(',', ',-'), $value);
        if ($this->checkAlias($value, 'episode', $epId))// if episode alias exists, don't change the new value
        {
            Episode::find($epId)
                   ->update(['alias' => $value]);
        }
        else
        {
            return 'exists';
        }
    }

    /**
     * Add post page for show
     *
     * @param $alias
     * @return View
     */
    public function getAddshowpost($alias)
    {
        $show       = Show::byAlias($alias);

        $title_tr   = Translation::create(['value_geo' => 'პოსტის სათაური', 'value_eng' => 'Post title', 'value_rus' => 'Post title rus']);
        $desc_tr    = Translation::create(['value_geo' => 'პოსტის აღწერა', 'value_eng' => 'Post description', 'value_rus' => 'Post description rus']);
        $short_desc = Translation::create(['value_geo' => 'პოსტის მოკლე აღწერა', 'value_eng' => 'Post short description', 'value_rus' => 'Post short description rus']);

        $video_title      = Translation::create(['value_geo' => 'ვიდეოს სათაური', 'value_eng' => 'video title', 'value_rus' => 'russian title']);
        $video_desc       = Translation::create(['value_geo' => 'ვიდეოს აღწერა', 'value_eng' => 'video description', 'value_rus' => 'russian description']);
        $video_subtitles  = Translation::create(['value_geo' => 'ვიდეოს სუბტიტრები', 'value_eng' => 'video subtitles', 'value_rus' => 'russian subtitles']);
        $video            = Video::create([
            'title_tr_id'     => $video_title->id,
            'desc_tr_id'      => $video_desc->id,
            'subtitles_tr_id' => $video_subtitles->id
        ]);

        $post       = Post::create([
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'video_id'         => $video->id,
            'visibility'       => false
        ]);

        $post->shows()->attach($show->id);
        $post['title_tr']      = $title_tr->toArray();
        $post['desc_tr']       = $desc_tr->toArray();
        $post['short_desc_tr'] = $short_desc->toArray();
        $post['cover']         = false;

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person)
        {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');
        $sliders           = [];

        return view('show.editPost')
            ->with('show', $show)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('sliders', $sliders)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit post page for show
     *
     * @param $post_id
     * @return View
     */
    public function getEditshowpost($post_id)
    {
        $post                  = Post::find($post_id);
        $post['title_tr']      = $post->getTitles();
        $post['desc_tr']       = $post->getDescriptions();
        $post['short_desc_tr'] = $post->getShortDescriptions();

        $post['cover']      = '/uploads/post/' .  $post->id . '/cover.jpg';
        if (!file_exists(public_path() . $post['cover'])) {
            $post['cover']  = false;
        }

        $post['cover_image']= $post->getImageUrl();

        $sliders            = [];
        try {
            $path           = 'uploads/post/' . $post->id . '/slider';
            $files          = array_diff(scandir($path, 1), array('.', '..'));
            foreach ($files as $file) {
                $sliders[]  = '/uploads/post/' . $post->id . '/slider/' . $file;
            }
        } catch(\exception $e) {
            $sliders        = [];
        }

        $show             = $post->shows->first();

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $tags = Tag::where('post_id', $post_id)
                        ->where('profession_id', $key)
                        ->get();

            foreach ($tags as $tag) {
                $default_persons[$profession][] = $tag->person_id;
            }

        }

        return view('show.editPost')
            ->with('show', $show)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('sliders', $sliders)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Store edited post in db
     *
     * @return success
     */
    public function postEditpost()
    {
        $field = Input::get('field');
        $post  = Post::find(Input::get('pk'));
        Translation::find($post->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);

        return 'success';
    }

    /**
     * Delete post
     */
    public function postCancelpost()
    {
        $target = Input::get('target');
        if ($target == 'show') {
            $show = Show::find(Input::get('target_id'));
            $show->posts()->detach(Input::get('post_id'));
        } else if ($target == 'season') {
            $season = Season::find(Input::get('target_id'));
            $season->posts()->detach(Input::get('post_id'));
        } else if ($target == 'episode') {
            $episode = Episode::find(Input::get('target_id'));
            $episode->posts()->detach(Input::get('post_id'));
        }
    }

    /**
     * Add genres for show
     */
    public function postAddgenres()
    {
        $show = Show::find(Input::get('show_id'));
        $show->genres()->detach();

        foreach (Input::get('genres') as $genre) {
            $show->genres()->attach($genre);
        }
    }

    /**
     * Get all seasons of the show
     *
     * @param $alias
     * @return View
     */
    public function getSeasons($alias)
    {
        $show    = Show::byAlias($alias);
        $seasons = $show->orderedSeasons;

        foreach ($seasons as &$season) {
            $season['title_tr']  = Translation::find($season->title_tr_id);
            $season['desc_tr']   = Translation::find($season->desc_tr_id);
            $season['cover']     = $season->getImageUrl();

            $episodes           = Episode::where('season_id', '=', $season->id)->get();
            foreach ($episodes as &$episode) {
                $episode['title_tr'] = $episode->getTitles();
            }
            $season['episodes'] = $episodes;
        }

        $posts = $show->posts;
        foreach ($posts as $post) {
            $post['title_tr'] = $post->getTitles();
            $post['desc_tr']  = $post->getDescriptions();
        }

        return view('show.season.seasons')
            ->with('show', $show)
            ->with('seasons', $seasons)
            ->with('posts', $posts)
            ->with('active', 'shows');
    }

    /**
     * Page for adding new season to show
     *
     * @param $alias
     * @return View
     */
    public function getAddseason($alias)
    {
        $title_tr   = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $desc_tr    = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);
        $short_desc = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);

        $show       = Show::byAlias($alias);

        $seasons    = $show->seasons;

        $season_arr            = array(
            'show_id'          => $show->id,
            'season_number'    => sizeof($seasons) + 1,
            'series_count'     => 0,
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'visibility'       => false,
        );

        $season                  = Season::create($season_arr);
        $season['title_tr']      = $title_tr->toArray();
        $season['desc_tr']       = $desc_tr->toArray();
        $season['short_desc_tr'] = $short_desc->toArray();
        $season['cover']         = false;
        $season['datetime']      = '';

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions      = Profession::lists('keyword', 'id');

        return view('show.season.editSeason')
            ->with('show', $show)
            ->with('season', $season)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit season page
     *
     * @param $alias
     * @param $season_number
     * @return View
     */
    public function getEditseason($alias, $season_number)
    {
        $show     = Show::byAlias($alias);

        $season   = $show->getSeason($season_number);

        $episodes = $season->episodes;

        foreach ($episodes as &$episode) {
            $episode['title_tr'] = $episode->getTitles();
        }
        $season['episodes'] = $episodes;

        $season['title_tr']      = $season->getTitles();
        $season['desc_tr']       = $season->getDescriptions();
        $season['short_desc_tr'] = $season->getShortDescriptions();

        $season['cover']       = '/uploads/seaseon/' .  $season->id . '/cover.jpg';
        if (!file_exists(public_path() . $season['cover'])) {
            $season['cover']   = false;
        }

        $season['cover_image']        = $season->getImageUrl();
        $season['cover_image_second'] = $season->getImageUrlSecond();

        $season['publish_datetime']     = '';
        if (isset($season->publish_date) && !empty($season->publish_date)) {
            $season['publish_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $season->publish_date)->format('m/d/Y H:i');
        }

        $season['release_datetime']     = '';
        if (isset($season->release_date) && !empty($season->release_date)) {
            $season['release_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $season->release_date)->format('m/d/Y H:i');
        }

        $posts = $season->posts;
        foreach ($posts as &$post) {
            $post['title_tr'] = $post->getTitles();
            $post['desc_tr']  = $post->getDescriptions();
        }

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $crews = Crewmember::where('where_type', 'season')
                ->where('where_id', $season->id)
                ->where('profession_id', $key)
                ->get();

            foreach ($crews as $crew) {
                $default_persons[$profession][] = $crew->person_id;
            }

        }

        $trailers = $season->trailers();
        foreach ($trailers as &$trailer) {
            $trailer['video'] = $trailer->video()
                                        ->first();

            $trailer['video']['title_tr']     = $trailer['video']->getTitles();
            $trailer['video']['desc_tr']      = $trailer['video']->getDescriptions();
            $trailer['video']['subtitle_tr']  = $trailer['video']->getSubtitles();
        }

        return view('show.season.editSeason')
            ->with('show', $show)
            ->with('season', $season)
            ->with('posts', $posts)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('trailers', $trailers)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Store edited season in db
     *
     * @return string
     */
    public function postEditseason()
    {
        $field  = Input::get('field');
        $season = Season::find(Input::get('pk'));

        Translation::find($season->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);

        return 'success';
    }

    /**
     * Delete season
     *
     * @throws \Exception
     */
    public function postCancelseason()
    {
        $seasonId  = Input::get('season_id');

        $showCount = Show::where('priority_id', $seasonId)
                         ->count();

        if ($showCount > 0)
        {
            return 'failed';
        }

        $season = Season::find($seasonId);
        foreach (Season::all() as $value) {
            if ($value->id > $season->id) {
                $value->season_number -= 1;
                $value->save();
            }
        }

        $season->delete();

        Translation::find($season->title_tr_id)->delete();
        Translation::find($season->desc_tr_id)->delete();

        Crewmember::where('where_type', 'season')
                  ->where('where_id', Input::get('season_id'))
                  ->delete();
    }

    /**
     * Add post page for season
     *
     * @param $alias
     * @param $season_number
     * @return View
     */
    public function getAddseasonpost($alias, $season_number)
    {
        $show       = Show::byAlias($alias);

        $season     = $show->getSeason($season_number);

        $title_tr   = Translation::create(['value_geo' => 'პოსტის სათაური', 'value_eng' => 'Post title', 'value_rus' => 'Post title rus']);
        $desc_tr    = Translation::create(['value_geo' => 'პოსტის აღწერა', 'value_eng' => 'Post description', 'value_rus' => 'Post description rus']);
        $short_desc = Translation::create(['value_geo' => 'პოსტის მოკლე აღწერა', 'value_eng' => 'Post short description', 'value_rus' => 'Post short description rus']);

        $video_title      = Translation::create(['value_geo' => 'ვიდეოს სათაური', 'value_eng' => 'video title', 'value_rus' => 'russian title']);
        $video_desc       = Translation::create(['value_geo' => 'ვიდეოს აღწერა', 'value_eng' => 'video description', 'value_rus' => 'russian description']);
        $video_subtitles  = Translation::create(['value_geo' => 'ვიდეოს სუბტიტრები', 'value_eng' => 'video subtitles', 'value_rus' => 'russian subtitles']);
        $video            = Video::create([
            'title_tr_id'     => $video_title->id,
            'desc_tr_id'      => $video_desc->id,
            'subtitles_tr_id' => $video_subtitles->id
        ]);

        $post       = Post::create([
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'video_id'         => $video->id,
            'visibility'       => false
        ]);

        $season->posts()->attach($post->id);
        $post['title_tr']      = $title_tr->toArray();
        $post['desc_tr']       = $desc_tr->toArray();
        $post['short_desc_tr'] = $short_desc->toArray();
        $post['cover']         = false;

        $show = Show::find($season->show_id);

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $sliders           = [];

        return view('show.season.editPost')
            ->with('show', $show)
            ->with('season', $season)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('sliders', $sliders)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit post page for season
     *
     * @param $post_id
     * @return View
     */
    public function getEditseasonpost($post_id)
    {
        $post                  = Post::find($post_id);
        $post['title_tr']      = $post->getTitles();
        $post['desc_tr']       = $post->getDescriptions();
        $post['short_desc_tr'] = $post->getShortDescriptions();

        $post['cover']         = '/uploads/post/' .  $post->id . '/cover.jpg';
        if (!file_exists(public_path() . $post['cover'])) {
            $post['cover']     = false;
        }

        $post['cover_image']   = $post->getImageUrl();

        $season                = $post->seasons->first();
        $show                  = $season->show;

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $tags = Tag::where('post_id', $post_id)
                       ->where('profession_id', $key)
                       ->get();

            foreach ($tags as $tag) {
                $default_persons[$profession][] = $tag->person_id;
            }

        }

        $sliders            = [];
        try {
            $path           = 'uploads/post/' . $post->id . '/slider';
            $files          = array_diff(scandir($path, 1), array('.', '..'));
            foreach ($files as $file) {
                $sliders[]  = '/uploads/post/' . $post->id . '/slider/' . $file;
            }
        } catch(\exception $e) {
            $sliders        = [];
        }

        return view('show.season.editPost')
            ->with('show', $show)
            ->with('season', $season)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('sliders', $sliders)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Add episode page
     *
     * @param $alias
     * @param $season_number
     * @return View
     */
    public function getAddepisode($alias, $season_number)
    {
        $show             = Show::byAlias($alias);
        $season           = $show->getSeason($season_number);
        $episodes         = $season->episodes;

        $title_tr         = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $desc_tr          = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);
        $short_desc       = Translation::create(['value_geo' => 'მოკლე აღწერა', 'value_eng' => 'short description', 'value_rus' => 'value rus']);

        $video_title      = Translation::create(['value_geo' => 'ვიდეოს სათაური', 'value_eng' => 'video title', 'value_rus' => 'russian title']);
        $video_desc       = Translation::create(['value_geo' => 'ვიდეოს აღწერა', 'value_eng' => 'video description', 'value_rus' => 'russian description']);
        $video_subtitles  = Translation::create(['value_geo' => 'ვიდეოს სუბტიტრები', 'value_eng' => 'video subtitles', 'value_rus' => 'russian subtitles']);
        $video            = Video::create([
            'title_tr_id'     => $video_title->id,
            'desc_tr_id'      => $video_desc->id,
            'subtitles_tr_id' => $video_subtitles->id
        ]);

        $episode_number = DB::table('episodes')
                            ->where('season_id', $season->id)
                            ->max('episode_number');

        $episode_arr           = array(
            'season_id'        => $season->id,
            'video_id'         => $video->id,
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'visibility'       => false,
            'episode_number'   => $episode_number + 1,
        );

        $episode                  = Episode::create($episode_arr);
        $episode['title_tr']      = $title_tr->toArray();
        $episode['desc_tr']       = $desc_tr->toArray();
        $episode['short_desc_tr'] = $short_desc->toArray();
        $episode['cover']         = false;
        $episode['video']         = $video;
        $episode['video_img']     = false;
        $episode['datetime']      = '';


        $season->update(['series_count' => $season->series_count + 1]);

        $series_count    = [0];
        for ($i = 1; $i <= $season->series_count; $i++) {
            $series_count[$i] = $i;
        }

        $episode['series_count'] = $series_count;

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions      = Profession::lists('keyword', 'id');

        return view('show.season.episode.editEpisode')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit episode page
     *
     * @param $alias
     * @param $season_number
     * @param $episode_number
     * @return View
     */
    public function getEditepisode($alias, $season_number, $episode_number)
    {
        $show             = Show::byAlias($alias);

        $season           = $show->getSeason($season_number);

        $episode          = $season->getEpisode($episode_number);


        $episode['title_tr']      = $episode->getTitles();
        $episode['desc_tr']       = $episode->getDescriptions();
        $episode['short_desc_tr'] = $episode->getShortDescriptions();

        $series_count    = [0];
        for ($i = 1; $i <= $season->series_count; $i++) {
            $series_count[$i] = $i;
        }

        $episode['series_count'] = $series_count;

        $episode['cover']        = '/uploads/episode/' .  $episode->id . '/cover.jpg';
        if (!file_exists(public_path() . $episode['cover'])) {
            $episode['cover']    = false;
        }

        $episode['cover_image']        = $episode->getImageUrl();
        $episode['cover_image_second'] = $episode->getImageUrlSecond();

        $episode['video']       = $episode->video;

        $episode['video_image'] = $episode->getVideoImageUrl();

        $episode['video_img']= '/uploads/episode/' . $episode->id . '/video/' . $episode->video->id . '/video.jpg';
        if (!file_exists(public_path() . $episode['video_img'])) {
            $episode['video_img']   = false;
        }

        $episode['publish_datetime']     = '';
        if (isset($episode->publish_date) && !empty($episode->publish_date)) {
            $episode['publish_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $episode->publish_date)->format('m/d/Y H:i');
        }

        $episode['release_datetime']     = '';
        if (isset($episode->release_date) && !empty($episode->release_date)) {
            $episode['release_datetime'] = Carbon::createFromFormat('Y-m-d H:i:s', $episode->release_date)->format('m/d/Y H:i');
        }

        $posts = $episode->posts;
        foreach ($posts as &$post) {
            $post['title_tr'] = $post->getTitles();
            $post['desc_tr']  = $post->getDescriptions();
        }

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $crews = Crewmember::where('where_type', 'episode')
                ->where('where_id', $episode->id)
                ->where('profession_id', $key)
                ->get();

            foreach ($crews as $crew) {
                $default_persons[$profession][] = $crew->person_id;
            }
        }

        $trailers = $episode->trailers();
        foreach ($trailers as &$trailer) {
            $trailer['video'] = $trailer->video()
                                        ->first();

            $trailer['video']['title_tr']     = $trailer['video']->getTitles();
            $trailer['video']['desc_tr']      = $trailer['video']->getDescriptions();
            $trailer['video']['subtitle_tr']  = $trailer['video']->getSubtitles();
        }

        return view('show.season.episode.editEpisode')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('posts', $posts)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('trailers', $trailers)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Store edited episode in db
     *
     * @return string
     */
    public function postEditepisode()
    {
        $field   = Input::get('field');
        $episode = Episode::find(Input::get('pk'));
        Translation::find($episode->$field)
            ->update(['value_' . Input::get('name') => Input::get('value')]);

        return 'success';
    }

    /**
     * Add post page for episode
     *
     * @param $alias
     * @param $season_number
     * @param $episode_number
     * @return View
     */
    public function getAddepisodepost($alias, $season_number, $episode_number)
    {
        $show             = Show::byAlias($alias);

        $season           = $show->getSeason($season_number);

        $episode          = $season->getEpisode($episode_number);


        $title_tr   = Translation::create(['value_geo' => 'პოსტის სათაური', 'value_eng' => 'Post title', 'value_rus' => 'Post title rus']);
        $desc_tr    = Translation::create(['value_geo' => 'პოსტის აღწერა', 'value_eng' => 'Post description', 'value_rus' => 'Post description rus']);
        $short_desc = Translation::create(['value_geo' => 'პოსტის მოკლე აღწერა', 'value_eng' => 'Post short description', 'value_rus' => 'Post short description rus']);

        $video_title      = Translation::create(['value_geo' => 'ვიდეოს სათაური', 'value_eng' => 'video title', 'value_rus' => 'russian title']);
        $video_desc       = Translation::create(['value_geo' => 'ვიდეოს აღწერა', 'value_eng' => 'video description', 'value_rus' => 'russian description']);
        $video_subtitles  = Translation::create(['value_geo' => 'ვიდეოს სუბტიტრები', 'value_eng' => 'video subtitles', 'value_rus' => 'russian subtitles']);
        $video            = Video::create([
            'title_tr_id'     => $video_title->id,
            'desc_tr_id'      => $video_desc->id,
            'subtitles_tr_id' => $video_subtitles->id
        ]);

        $post                  = Post::create([
            'title_tr_id'      => $title_tr->id,
            'desc_tr_id'       => $desc_tr->id,
            'short_desc_tr_id' => $short_desc->id,
            'video_id'         => $video->id,
            'visibility'       => false
        ]);

        $episode->posts()->attach($post->id);
        $post['title_tr']      = $title_tr->toArray();
        $post['desc_tr']       = $desc_tr->toArray();
        $post['short_desc_tr'] = $short_desc->toArray();
        $post['cover']         = false;

        $season = Season::find($episode->season_id);
        $show   = Show::find($season->show_id);

        $persons          = Person::all();
        $persons_arr      = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $sliders           = [];

        return view('show.season.episode.editPost')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', null)
            ->with('sliders', $sliders)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit post page for episode
     *
     * @param $post_id
     * @return View
     */
    public function getEditepisodepost($post_id)
    {
        $post                    = Post::find($post_id);
        $post['title_tr']        = $post->getTitles();
        $post['desc_tr']         = $post->getDescriptions();
        $post['short_desc_tr']   = $post->getShortDescriptions();

        $post['cover']           = '/uploads/post/' .  $post->id . '/cover.jpg';
        if (!file_exists(public_path() . $post['cover'])) {
            $post['cover']       = false;
        }

        $post['cover_image']     = $post->getImageUrl();

        $episode                 = $post->episodes->first();

        $season                  = Season::find($episode->season_id);
        $show                    = Show::find($season->show_id);

        $persons                 = Person::all();
        $persons_arr             = [];
        foreach ($persons as $person) {
            $persons_arr[$person->id] = $person->getFirstname->value_geo . ' ' . $person->getLastname->value_geo;
        };

        $professions       = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession) {
            $tags = Tag::where('post_id', $post_id)
                ->where('profession_id', $key)
                ->get();

            foreach ($tags as $tag) {
                $default_persons[$profession][] = $tag->person_id;
            }
        }

        $sliders            = [];
        try {
            $path           = 'uploads/post/' . $post->id . '/slider';
            $files          = array_diff(scandir($path, 1), array('.', '..'));
            foreach ($files as $file) {
                $sliders[]  = '/uploads/post/' . $post->id . '/slider/' . $file;
            }
        } catch(\exception $e) {
            $sliders        = [];
        }

        return view('show.season.episode.editPost')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('post', $post)
            ->with('persons', $persons_arr)
            ->with('professions', $professions)
            ->with('default_persons', $default_persons)
            ->with('sliders', $sliders)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Edit episode video title, description, subtitles
     */
    public function postEditepisodevideo()
    {
        $video_id = Input::get('pk');
        $field    = Input::get('field');
        $lang     = Input::get('name');
        $value    = Input::get('value');

        $video    = Video::find($video_id);

        Translation::find($video->$field)
                   ->update(['value_' . $lang => $value]);
    }

    /**
     * Episodes page
     *
     * @param $alias
     * @param $season_id
     * @param $episode_id
     * @return Response
     */
    public function getEpisode($alias, $season_id, $episode_id)
    {
        return view('show.season.episode.episode')
            ->with('alias', $alias)
            ->with('season', $season_id)
            ->with('episode', $episode_id);
    }

    /**
     * Delete episode
     *
     * @throws \Exception
     */
    public function postCancelepisode()
    {
        $episode = Episode::find(Input::get('episode_id'));
        /*foreach (Episode::all() as $value) {
            if ($value->id > $episode->id) {
                $value->episode_number -= 1;
                $value->save();
            }
        }*/
        $popularEpisode = PopularEpisode::where('episode_id', Input::get('episode_id'))->get();
        if (isset($popularEpisode[0]))
        {
            $popularEpisode[0]->delete();
        }

        $episode->delete();

        $season = Season::find($episode->season_id);
        $season->update(['series_count' => $season->series_count - 1]);


        Translation::find($episode->title_tr_id)->delete();
        Translation::find($episode->desc_tr_id)->delete();


        Crewmember::where('where_type', 'episode')
                  ->where('where_id', Input::get('episode_id'))
                  ->delete();
    }

    /**
     * Delete episode
     *
     * @param $episode_id
     * @return Redirect
     * @throws \Exception
     */
    public function getDeleteepisode($episode_id)
    {
        $episode = Episode::find($episode_id);
        $episode->delete();

        $season = Season::find($episode->season_id);
        $season->update(['series_count' => $season->series_count - 1]);

        Translation::find($episode->title_tr_id)->delete();
        Translation::find($episode->desc_tr_id)->delete();

        Crewmember::where('where_type', 'episode')
                 ->where('where_id', $episode_id)
                 ->delete();

        return redirect('/show/seasons/' . $season->show->alias);
    }

    /**
     * Publish/unpublish show/season/episode/post
     */
    public function postVisibility()
    {
        if (Input::get('target') == 'show') // publish/unpublish show
        {
            Show::find(Input::get('show_id'))
                ->update(['visibility' => filter_var(Input::get('state'), FILTER_VALIDATE_BOOLEAN)]);
        }
        else if (Input::get('target') == 'season') // publish/unpublish season
        {
            Season::find(Input::get('season_id'))
                  ->update(['visibility' => filter_var(Input::get('state'), FILTER_VALIDATE_BOOLEAN)]);
        }
        else if (Input::get('target') == 'episode') // publish/unpublish episode
        {
            Episode::find(Input::get('episode_id'))
                   ->update(['visibility' => filter_var(Input::get('state'), FILTER_VALIDATE_BOOLEAN)]);
        }
        else if (Input::get('target') == 'post') // publish/unpublish post
        {
            Post::find(Input::get('post_id'))
                ->update(['visibility' => filter_var(Input::get('state'), FILTER_VALIDATE_BOOLEAN)]);
        }
    }

    /**
     * Change show priority
     *
     * Defines which season/episode page should be landed, when user click on show
     */
    public function postPriority()
    {
        if (Input::get('state') == 'true')
        {
            Show::find(Input::get('show_id'))
                ->update(['priority_type' => Input::get('priority_type'), 'priority_id' => Input::get('priority_id')]);
        }
        else
        {
            Show::find(Input::get('show_id'))
                ->update(['priority_type' => '', 'priority_id' => '']);
        }
    }

    /**
     * Save object(show, season, episode, post) cover image
     */
    public function postSavecover()
    {
        $file                 = Input::file('cover');
        $target               = Input::get('target');
        $target_id            = Input::get('target_id');
        $destinationPath      = public_path() . '/uploads/' . $target . '/' . $target_id . '/';

        $file->move($destinationPath, 'cover.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/cover.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/cover.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cover.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'cover.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/cover.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Save object(show, season, episode, post) cover @2x image
     */
    public function postSavecoversecond()
    {
        $file                 = Input::file('cover-second');
        $target               = Input::get('target');
        $target_id            = Input::get('target_id');
        $destinationPath      = public_path() . '/uploads/' . $target . '/' . $target_id . '/';

        $file->move($destinationPath, 'cover@2x.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/cover@2x.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/cover@2x.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cover@2x.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'cover@2x.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/cover@2x.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Remove cover image from show/season/episode/post
     *
     * @return string
     */
    public function postRemovecover()
    {
        $target    = Input::get('target');
        $target_id = Input::get('target_id');

        $path      = public_path() . '/uploads/' . $target . '/' . $target_id . '/cover.jpg';

        try {
            unlink($path);
        } catch (\exception $e) {
            return 'failed';
        }

        return 'success';
    }

    /**
     * Save post slider images
     */
    public function postSaveslider()
    {
        $file            = Input::file('slider');
        $post_id         = Input::get('post_id');
        $file_name       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $destinationPath = public_path() . '/uploads/post/' . $post_id . '/slider/';

        $file->move($destinationPath, $file_name . '.jpg');

        $twoYears = Carbon::now()->addYears(2);

        $s3 = App::make('aws')->get('s3');
        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => 'post/' . $post_id . '/' . $file_name . '.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . $file_name . '.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . $file_name . '.jpg');
    }

    /**
     * Remove slider image from post
     *
     * @return string
     */
    public function postRemoveslider()
    {
        $path = public_path() . Input::get('slider_img');

        try {
            unlink($path);
        } catch (\exception $e) {
            return 'failed';
        }

        return 'success';
    }

    /**
     * Remove video image from show/season/episode
     *
     * @return string
     */
    public function postRemovevideocover()
    {
        $target    = Input::get('target');
        $target_id = Input::get('target_id');
        $video_id  = Input::get('video_id');
        $lang      = Input::get('lang');

        $path      = public_path() . '/uploads/' . $target . '/' . $target_id . '/video/' .
                        $video_id . '/' . $lang . '/cover.jpg';

        try {
            unlink($path);
        } catch (\exception $e) {
            return 'failed';
        }

        return 'success';
    }

    /**
     * Save video for post
     */
    public function postSavepostvideo()
    {
        $file            = Input::file('video');
        $post_id         = Input::get('post_id');
        $lang            = Input::get('lang');
        $file_name       = $file->getClientOriginalName();
        $destinationPath = 'uploads/post/' . $post_id . '/video/' . $lang . '/';

        $title_tr        = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $subtitle_tr     = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);

        $video           = Video::create(['title_tr_id' => $title_tr->id, 'subtitles_tr_id' => $subtitle_tr->id]);

        $file->move($destinationPath, $file_name);
    }

    /**
     * Save video for episode
     */
    public function postSaveepisodevideo()
    {
        $file            = Input::file('video');
        $episode_id      = Input::get('episode_id');
        $lang            = Input::get('lang');
        $file_name       = $file->getClientOriginalName();
        $destinationPath = 'uploads/episode/' . $episode_id . '/video/' . $lang . '/';

        $title_tr        = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $subtitle_tr     = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);

        $video           = Video::create(['title_tr_id' => $title_tr->id, 'subtitles_tr_id' => $subtitle_tr->id]);

        $file->move($destinationPath, $file_name);
    }

    /**
     * Add/edit posts' persons' professions
     */
    public function postEditpostprofession()
    {
        Tag::where('post_id', Input::get('post_id'))
           ->where('profession_id', Input::get('profession_id'))
           ->delete();

        foreach (Input::get('persons') as $person_id) {
            Tag::create(['post_id' => Input::get('post_id'), 'person_id' => $person_id, 'profession_id' => Input::get('profession_id')]);
        }
    }

    /**
     * Add/edit show/season/episode people professions
     */
    public function postEditprofession()
    {
        $crewmembers   = Crewmember::where('where_type', Input::get('target'))
                                 ->where('where_id', Input::get('target_id'))
                                 ->where('profession_id', Input::get('profession_id'));

        $old_remaining = [];
        foreach ($crewmembers->get() as $crewmember) {
            if (!in_array($crewmember->person_id, Input::get('persons'))) {
                $crewmember->actings()->detach();
                $crewmember->delete();
            } else {
                $old_remaining[] = $crewmember->person_id;
            }
        }

        foreach (Input::get('persons') as $person) {
            if (!in_array($person, $old_remaining)) {
                $crew_payload       = array(
                    'where_type'    => Input::get('target'),
                    'where_id'      => Input::get('target_id'),
                    'profession_id' => Input::get('profession_id'),
                    'person_id'     => $person,
                );

                $character_title_tr = Translation::create([
                    'value_geo'     => 'პერსონაჟი',
                    'value_eng'     => 'character',
                    'value_rus'     => 'персонаж',
                ]);

                $character_payload  = array(
                    'title_tr_id'   => $character_title_tr->id,
                );

                $profession         = Profession::find(Input::get('profession_id'));

                $crewmember         = Crewmember::create($crew_payload);

                if ($profession->keyword == 'actor') {
                    $character  = Character::create($character_payload);
                    $crewmember->actings()->attach($character->id);
                }
            }
        }
    }

    /**
     * Show trailer view
     *
     * @param $show_id
     * @return View
     */
    public function getAddshowtrailer($show_id)
    {
        $show                 = Show::find($show_id);
        $title_tr             = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $desc_tr              = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);
        $subtitle_tr          = Translation::create(['value_geo' => 'სუბტიტრები', 'value_eng' => 'subtitle', 'value_rus' => 'value rus']);

        $video                = Video::create([
            'title_tr_id'     => $title_tr->id,
            'desc_tr_id'      => $desc_tr->id,
            'subtitles_tr_id' => $subtitle_tr->id
        ]);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = Trailer::create(['whose_type' => 'show', 'whose_id' => $show_id, 'video_id' => $video->id]);

        $image['geo']         = false;
        $image['eng']         = false;
        $image['rus']         = false;

        $highlight            = $trailer->highlight;

        return view('show.trailer')
            ->with('show', $show)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Season trailer view
     *
     * @param $season_id
     * @return View
     */
    public function getAddseasontrailer($season_id)
    {
        $season               = Season::find($season_id);
        $title_tr             = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $desc_tr              = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);
        $subtitle_tr          = Translation::create(['value_geo' => 'სუბტიტრები', 'value_eng' => 'subtitle', 'value_rus' => 'value rus']);

        $video                = Video::create([
            'title_tr_id'     => $title_tr->id,
            'desc_tr_id'      => $desc_tr->id,
            'subtitles_tr_id' => $subtitle_tr->id
        ]);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = Trailer::create(['whose_type' => 'season', 'whose_id' => $season_id, 'video_id' => $video->id]);
        $show                 = $season->show;

        $image['geo']         = false;
        $image['eng']         = false;
        $image['rus']         = false;

        $highlight            = $trailer->highlight;

        return view('show.season.trailer')
            ->with('show', $show)
            ->with('season', $season)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Season trailer view
     *
     * @param $episode_id
     * @return mixed
     */
    public function getAddepisodetrailer($episode_id)
    {
        $episode              = Episode::find($episode_id);
        $title_tr             = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'value rus']);
        $desc_tr              = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'value rus']);
        $subtitle_tr          = Translation::create(['value_geo' => 'სუბტიტრები', 'value_eng' => 'subtitle', 'value_rus' => 'value rus']);

        $video                = Video::create([
            'title_tr_id'     => $title_tr->id,
            'desc_tr_id'      => $desc_tr->id,
            'subtitles_tr_id' => $subtitle_tr->id
        ]);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = Trailer::create(['whose_type' => 'episode', 'whose_id' => $episode_id, 'video_id' => $video->id]);
        $season               = $episode->season;
        $show                 = $season->show;

        $image['geo']         = false;
        $image['eng']         = false;
        $image['rus']         = false;

        $highlight            = $trailer->highlight;

        return view('show.season.episode.trailer')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'add')
            ->with('active', 'shows');
    }

    /**
     * Edit trailer page for show
     *
     * @param $video_id
     * @return View
     */
    public function getEditshowtrailer($video_id)
    {
        $video                = Video::find($video_id);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = $video->trailer()
                                      ->first();

        $video['cover_image'] = $trailer->getImageUrl();
        $video['post_image']  = $trailer->getPostImageUrl();

        $show                 = Show::find($trailer->whose_id);

        $image['geo']         = '/uploads/show/' .  $show->id . '/video/' . $video_id . '/geo/cover.jpg';
        if (!file_exists(public_path() . $image['geo'])) {
            $image['geo']     = false;
        }

        $image['eng']         = '/uploads/show/' .  $show->id . '/video/' . $video_id . '/eng/cover.jpg';
        if (!file_exists(public_path() . $image['eng'])) {
            $image['eng']     = false;
        }

        $image['rus']         = '/uploads/show/' .  $show->id . '/video/' . $video_id . '/rus/cover.jpg';
        if (!file_exists(public_path() . $image['rus'])) {
            $image['rus']     = false;
        }

        $highlight            = $trailer->highlight;

        return view('show.trailer')
            ->with('show', $show)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Edit trailer page for season
     *
     * @param $video_id
     * @return View
     */
    public function getEditseasontrailer($video_id)
    {
        $video                = Video::find($video_id);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = $video->trailer()
                                      ->first();

        $video['cover_image'] = $trailer->getImageUrl();
        $video['post_image']  = $trailer->getPostImageUrl();

        $season               = Season::find($trailer->whose_id);
        $show                 = $season->show;

        $image['geo']         = '/uploads/season/' .  $season->id . '/video/' . $video_id . '/geo/cover.jpg';
        if (!file_exists(public_path() . $image['geo'])) {
            $image['geo']     = false;
        }

        $image['eng']         = '/uploads/season/' .  $season->id . '/video/' . $video_id . '/eng/cover.jpg';
        if (!file_exists(public_path() . $image['eng'])) {
            $image['eng']     = false;
        }

        $image['rus']         = '/uploads/season/' .  $season->id . '/video/' . $video_id . '/rus/cover.jpg';
        if (!file_exists(public_path() . $image['rus'])) {
            $image['rus']     = false;
        }

        $highlight            = $trailer->highlight;

        return view('show.season.trailer')
            ->with('show', $show)
            ->with('season', $season)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Edit trailer page for episode
     *
     * @param $video_id
     * @return View
     */
    public function getEditepisodetrailer($video_id)
    {
        $video                = Video::find($video_id);
        $video['title_tr']    = $video->getTitles();
        $video['desc_tr']     = $video->getDescriptions();
        $video['subtitle_tr'] = $video->getSubtitles();

        $trailer              = $video->trailer()
                                      ->first();

        $video['cover_image'] = $trailer->getImageUrl();
        $video['post_image']  = $trailer->getPostImageUrl();

        $episode              = Episode::find($trailer->whose_id);
        $season               = $episode->season;
        $show                 = $season->show;

        $image['geo']         = '/uploads/episode/' .  $episode->id . '/video/' . $video_id . '/geo/cover.jpg';
        if (!file_exists(public_path() . $image['geo'])) {
            $image['geo']     = false;
        }

        $image['eng']         = '/uploads/episode/' .  $episode->id . '/video/' . $video_id . '/eng/cover.jpg';
        if (!file_exists(public_path() . $image['eng'])) {
            $image['eng']     = false;
        }

        $image['rus']         = '/uploads/episode/' .  $episode->id . '/video/' . $video_id . '/rus/cover.jpg';
        if (!file_exists(public_path() . $image['rus'])) {
            $image['rus']     = false;
        }

        $highlight            = $trailer->highlight;

        return view('show.season.episode.trailer')
            ->with('show', $show)
            ->with('season', $season)
            ->with('episode', $episode)
            ->with('video', $video)
            ->with('image', $image)
            ->with('highlight', $highlight)
            ->with('type', 'edit')
            ->with('active', 'shows');
    }

    /**
     * Delete show video
     *
     * @param $video_id
     * @param $show_alias
     * @return Redirect
     */
    public function getCancelshowvideo($video_id, $show_alias)
    {
        $video   = Video::find($video_id);
        $trailer = $video->trailer()->first();

        if (!empty($trailer)) {
            $trailer->delete();
        }
        $video->delete();

        return redirect('/show/editshow/' . $show_alias);
    }

    /**
     * Delete show video
     *
     * @param $video_id
     * @param $show_alias
     * @param $season_number
     * @return Redirect
     */
    public function getCancelseasonvideo($video_id, $show_alias, $season_number)
    {
        $video   = Video::find($video_id);
        $trailer = $video->trailer()->first();

        if (!empty($trailer)) {
            $trailer->delete();
        }
        $video->delete();

        return redirect('/show/editseason/' . $show_alias . '/' . $season_number);
    }

    /**
     * Delete show video
     *
     * @param $video_id
     * @param $show_alias
     * @param $season_number
     * @param $episode_number
     * @return Redirect
     */
    public function getCancelepisodevideo($video_id, $show_alias, $season_number, $episode_number)
    {
        $video   = Video::find($video_id);
        $trailer = $video->trailer()->first();

        if (!empty($trailer)) {
            $trailer->delete();
        }
        $video->delete();

        return redirect('/show/editepisode/' . $show_alias . '/' . $season_number . '/' . $episode_number);
    }

    /**
     * Edit video title and subtitle
     */
    public function postEditvideo()
    {
        $field = Input::get('field');
        $video = Video::find(Input::get('pk'));
        Translation::find($video->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);
    }

    /**
     * Save object(show, season, episode) trailer cover.jpg
     */
    public function postSavetrailer()
    {
        $file            = Input::file('trailer');
        $target          = Input::get('target');
        $target_id       = Input::get('target_id');
        $video_id        = Input::get('video_id');
        $lang            = Input::get('lang');

        $destinationPath = public_path() . '/uploads/' . $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/';

        $file->move($destinationPath, 'cover.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/cover.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/cover.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cover.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'cover.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/cover.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Save object(show, season, episode) trailer image.jpg
     */
    public function postSavetrailerimage()
    {
        $file            = Input::file('trailer');
        $target          = Input::get('target');
        $target_id       = Input::get('target_id');
        $video_id        = Input::get('video_id');
        $lang            = Input::get('lang');

        $destinationPath = public_path() . '/uploads/' . $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/';

        $file->move($destinationPath, 'image.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/image.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/image.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'image.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'image.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/image.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Save episode video image
     */
    public function postSaveepisodevideoimage()
    {
        $file       = Input::file('video-image');
        $episode_id = Input::get('episode-id');
        $video_id   = Input::get('video-id');

        $destinationPath = public_path() . '/uploads/episode/' . $episode_id . '/video/' . $video_id . '/';

        $file->move($destinationPath, 'video.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'video.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'video.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Remove episode video image
     *
     * @return string
     */
    public function postRemoveepisodevideoimage()
    {
        $episode_id = Input::get('episode_id');
        $video_id   = Input::get('video_id');

        $path       = public_path() . '/uploads/episode/' . $episode_id . '/video/' . $video_id . '/video.jpg';

        try {
            unlink($path);
        } catch (\exception $e) {
            return 'failed';
        }

        return 'success';
    }

    /**
     * Edit acting
     *
     * @param $target
     * @param $target_id
     * @return View
     */
    public function getEditactings($target, $target_id)
    {
        $crewmembers = Crewmember::all();

        $crews       = [];
        foreach ($crewmembers as $crewmember) {
            if ($crewmember->where_type == $target && $crewmember->where_id == $target_id) {
                $profession = Profession::find($crewmember->profession_id);
                if ($profession->keyword == 'actor') {
                    $crews[]         = array(
                        'person'     => Person::find($crewmember->person_id),
                        'profession' => Profession::find($crewmember->profession_id),
                        'character'  => $crewmember->actings->first(),
                        'target'     => $target,
                        'target_id'  => $target_id,
                    );
                }
            }
        }

        return view('show.actings')
            ->with('crews', $crews)
            ->with('active', 'shows');
    }

    /**
     * Save characters
     */
    public function postEditcharacter()
    {
        $character_id = Input::get('pk');
        $lang         = Input::get('name');
        $value        = Input::get('value');

        $character    = Character::find($character_id);

        Translation::find($character->title_tr_id)
                   ->update(['value_' . $lang => $value]);
    }

    /**
     * Save release date in db for shows, seasons, episodes
     */
    public function postDatetime()
    {
        $target    = Input::get('target');
        $target_id = Input::get('target_id');
        $field     = Input::get('field');
        $value     = Input::get('value');
        $value     = Carbon::createFromFormat('m/d/Y H:i', $value)->format('Y-m-d H:i');

        if ($target == 'show') {
            Show::find($target_id)
                ->update([$field => $value]);
        } else if ($target == 'season') {
            Season::find($target_id)
                  ->update([$field => $value]);
        } else if ($target == 'episode') {
            Episode::find($target_id)
                   ->update([$field => $value]);
        }
    }

    /**
     * Change episode number
     */
    public function postEditepisodenumber()
    {
        $episode_id = Input::get('episode_id');
        $season_id  = Input::get('season_id');
        $value      = Input::get('value');

        $episode_number = Episode::where('episode_number', $value)
                                 ->where('season_id', $season_id)
                                 ->get()
                                 ->toArray();

        if (isset($episode_number) && !empty($episode_number)) {
            return 'failed';
        }

        $episode = Episode::find($episode_id);
        $episode->episode_number = $value;
        $episode->save();

        return 'success';
    }

    /**
     * Activate/Deactivate fb notification
     *
     * @return bool
     */
    public function postFbnotification()
    {
        $state     = Input::get('state');
        $whoseId   = Input::get('whose_id');
        $whoseType = Input::get('whose_type');

        if ($whoseType == 'episode')
        {
            $target = Episode::find($whoseId);
        }
        else if ($whoseType == 'show')
        {
            $target = Show::find($whoseId);
        }
        else if ($whoseType == 'season')
        {
            $target = Season::find($whoseId);
        }
        else
        {
            return false;
        }

        if ($state == 'true')
        {
            if ($target->publish_date != null && $target->publish_date != '')
            {
                $title_tr = Translation::create([
                    'value_geo' => 'სათაური',
                    'value_eng' => 'Title',
                    'value_rus' => 'Title'
                ]);
                $desc_tr  = Translation::create([
                    'value_geo' => 'დაემატა ახალი ეპიზოდი',
                    'value_eng' => 'New Episode was added',
                    'value_rus' => 'New Episode was added']);

                $carbonDate   = Carbon::createFromFormat('Y-m-d H:i:s', $target->publish_date);
                $publishDate  = $carbonDate->format('Y-m-d H:i:s');

                FbNotification::create([
                    'title_tr_id'  => $title_tr->id,
                    'desc_tr_id'   => $desc_tr->id,
                    'whose_type'   => $whoseType,
                    'whose_id'     => $whoseId,
                    'publish_date' => $publishDate
                ]);
            }
        }
        else
        {
            FbNotification::where('whose_id', $whoseId)
                ->where('whose_type', $whoseType)
                ->delete();
        }
    }

    /**
     * Edit short description for show/season/episode/post
     */
    public function postEditshortdesc()
    {
        $whose    = Input::get('whose');
        $whose_id = Input::get('whose_id');
        $value    = Input::get('value');
        $lang     = Input::get('lang');

        $who = '';
        if ($whose == 'show')
        {
            $who = Show::find($whose_id);
        }
        else if ($whose == 'season')
        {
            $who = Season::find($whose_id);
        }
        else if ($whose == 'episode')
        {
            $who = Episode::find($whose_id);
        }
        else if ($whose == 'post')
        {
            $who = Post::find($whose_id);
        }

        if ($who != '' && isset($who->short_desc_tr_id) && !empty($who->short_desc_tr_id) && $who->short_desc_tr_id != null)
        {
            $field               = 'value_' . $lang;
            $translation         = Translation::find($who->short_desc_tr_id);
            $translation->$field = $value;
            $translation->save();
        }
        else if ($who != '')
        {
            $translation = Translation::create([
                'value_' . $lang => $value
            ]);

            $who->short_desc_tr_id = $translation->id;
            $who->save();
        }
        else
        {
            return 'failed';
        }

        return 'success';
    }

    /**
     * Save image to crop
     */
    public function postSavecrop()
    {
        $file                 = Input::file('crop-cover');
        $video_id             = Input::get('video_id');
        $type                 = Input::get('type');
        $destinationPath      = public_path() . '/uploads/video/' . $video_id . '/' . $type . '/';

        $file->move($destinationPath, 'crop.jpg');
    }

    /**
     * Crop video image page
     *
     * @param $video_id
     * @param $url
     * @param $type
     */
    public function getCrop($video_id,  $type = 'cover', $url)
    {
        $id    = $video_id;
        $video = Video::find($video_id);

        $post  = Post::where('video_id', $video_id)
                      ->get();

        $post_arr = $post->toArray();
        if (!empty($post_arr))
        {
            $id = $post[0]['id'];
        }

        if ($url == 'editepisode')
        {
            $episode = Episode::where('video_id', $video_id)
                              ->get();

            $episode = $episode[0];
            $season  = $episode->season;
            $show    = $season->show;
            $id      = $show->alias . '/' . $season->season_number . '/' . $episode->episode_number;
        }

        return view('show.crop')
            ->with('video', $video)
            ->with('type', $type)
            ->with('redirect_path', '/show/' . $url . '/' . $id);
    }

    /**
     * Save cropped image
     *
     * @param $video_id
     * @return Redirect
     */
    public function postCropimage($video_id)
    {
        $video = Video::find($video_id);

        $image = Input::get('image');
        $x     = Input::get('x');
        $y     = Input::get('y');
        $w     = Input::get('w');
        $h     = Input::get('h');
        $type  = Input::get('type');

        $img   = Image::make($image);
        $img->crop($w, $h, $x, $y);

        if ($type == 'cover')
        {
            $img->save($video->getCroppedCoverPath());
        }
        else
        {
            $img->save($video->getCroppedImagePath());
        }

        return redirect(Input::get('redirect-path'));
    }

    /**
     * Resize image
     *
     * @param $video_id
     * @return mixed
     */
    public function postResizeimage($video_id)
    {
        $video = Video::find($video_id);

        $image = Input::get('image');
        $width = Input::get('width');

        $img   = Image::make($image);
        $img->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($video->getImageToCropPath());

        return Redirect::back();
    }

    /**
     * Upload cropped image
     */
    public function postUploadcropped()
    {
        $target          = Input::get('target');
        $target_id       = Input::get('target_id');
        $video_id        = Input::get('video_id');
        $lang            = Input::get('lang');
        $type            = Input::get('type');

        $image_name      = 'cover.jpg';
        if ($type == 'image')
        {
            $image_name = 'image.jpg';
        }

        $destinationPath = public_path() . '/uploads/video/'. $video_id . '/' . $type . '/';

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/' . $image_name
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/' . $image_name,
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cropped.jpg',
            'Expires'    => $twoYears,
        ));

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/video/' . $video_id . '/' . $lang . '/' . $image_name]
                ],
                'CallerReference' => time()
            ]);
        }

        return Redirect::back();
    }

    /**
     * Upload cropped post image
     */
    public function postUploadcroppedpost()
    {
        $target               = Input::get('target');
        $target_id            = Input::get('target_id');
        $video_id             = Input::get('video_id');
        $destinationPath      = public_path() . '/uploads/video/'. $video_id . '/image/';

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => $target . '/' . $target_id . '/cover.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => $target . '/' . $target_id . '/cover.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cropped.jpg',
            'Expires'    => $twoYears,
        ));

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . $target . '/' . $target_id . '/cover.jpg']
                ],
                'CallerReference' => time()
            ]);
        }

        return Redirect::back();
    }

    /**
     * Upload cropped episode image
     */
    public function postUploadcroppedepisode()
    {
        $episode_id = Input::get('episode-id');
        $video_id   = Input::get('video_id');

        $destinationPath = public_path() . '/uploads/video/'. $video_id . '/cover/';

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'cropped.jpg',
            'Expires'    => $twoYears,
        ));

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/' . 'episode/' . $episode_id . '/video/' . $video_id . '/video.jpg']
                ],
                'CallerReference' => time()
            ]);
        }

        return Redirect::back();
    }

    /**
     * Change season number
     *
     * @param $showId
     * @param $seasonNumber
     * @param $type
     * @return mixed
     */
    public function getChangeseasonorder($showId, $seasonNumber, $type)
    {
        if ($type == 'up')
        {
            $season = Season::where('show_id', $showId)
                            ->where('season_number', $seasonNumber)
                            ->first();

            $previousSeason = Season::where('show_id', $showId)
                                    ->where('season_number', $seasonNumber - 1)
                                    ->first();

            $season->update(['season_number' => $seasonNumber - 1]);
            $previousSeason->update(['season_number' => $seasonNumber]);

            return Redirect::back();
        }
        else if ($type == 'down')
        {
            $season = Season::where('show_id', $showId)
                            ->where('season_number', $seasonNumber)
                            ->first();

            $nextSeason = Season::where('show_id', $showId)
                                ->where('season_number', $seasonNumber + 1)
                                ->first();

            $season->update(['season_number' => $seasonNumber + 1]);
            $nextSeason->update(['season_number' => $seasonNumber]);

            return Redirect::back();
        }
        else
        {
            return Redirect::back();
        }
    }
}
