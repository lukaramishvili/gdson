<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getView']]);
    }

    /**
     * Show shows, videos, etc by page
     *
     * @param $alias
     * @return Response
     */
    public function getView($alias)
    {
        $page = Page::byAlias($alias);

        return view('pages.view')
            ->with('page', $page);
	}

    public function getAll()
    {
        $pages = Page::all();

        return view('page.all')
            ->with('pages', $pages)
            ->with('active', 'page');
    }

    public function getAdd()
    {
        $title_tr = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'title']);
        $desc_tr  = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);

        $page     = Page::create(['title_tr_id' => $title_tr->id, 'desc_tr_id' => $desc_tr->id, 'alias' => str_random(12)]);

        $page['title_tr'] = $page->getTitles();
        $page['desc_tr']  = $page->getDescriptions();

        return view('page.edit')
            ->with('page', $page)
            ->with('type', 'add')
            ->with('active', 'page');
    }

    public function getEdit($page_id)
    {
        $page = Page::find($page_id);

        $page['title_tr'] = $page->getTitles();
        $page['desc_tr']  = $page->getDescriptions();

        return view('page.edit')
            ->with('page', $page)
            ->with('type', 'edit')
            ->with('active', 'page');
    }

    public function postEdit()
    {
        $field  = Input::get('field');
        $page   = Page::find(Input::get('pk'));

        Translation::find($page->$field)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);

        return 'success';
    }

    public function postAlias()
    {
        Page::find(Input::get('pk'))
            ->update(['alias' => Input::get('value')]);
    }

    /**
     * Delete page
     *
     * @param $page_id
     * @return View
     */
    public function getDelete($page_id)
    {
        Page::find($page_id)->delete();

        return redirect('/page/all');
    }
}
