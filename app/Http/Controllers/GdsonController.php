<?php namespace App\Http\Controllers;

use App\Trailer;
use Auth;
use Greggilbert\Recaptcha\Facades\Recaptcha;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Show;
use App\Season;
use App\Episode;
use App\Post;
use App\Person;
use App\Genre;
use App\Page;
use App\Faq;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input\InputOption;

class GdsonController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        //was causing redirect loops
		//$this->middleware('guest');
	}

	/**
	 * Home page.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('main');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function admin()
	{
		return view('admin.main');
	}

    /**
     * About page
     *
     * @return Response
     */
    public function about()
    {
        //TODO: the "about" page
        $page    = null;
        $trailer = Trailer::find(47);

        return view('about')->with(array('page' => $page, 'activemenu' => 'about', 'trailer' => $trailer));
    }

    /**
     * FAQ page
     *
     * @return Response
     */
    public function faq()
    {
        return view('faq')->with(array('activemenu' => 'faq'));
    }

    /**
     * Contact page
     *
     * @return Response
     */
    public function contact()
    {
        $message = Session::get('message');

        return view('contact')->with(array('activemenu' => 'contact', 'contact_message' => $message));
    }

    /**
     * Submit contact
     *
     * @return Response
     */
    public function submitcontact(Request $request)
    {
        $captcha  = Input::get('g-recaptcha-response');
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret   = Config::get('recaptcha.private_key');

        $response = json_decode(
            file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$captcha."&remoteip=".$remoteip), true
        );

        $validationMessages = [
            'name.required'                 => 'სახელის ველი სავალდებულოა',
            'email.required'                => 'ელ-ფოსტის ველი სავალდებულოა',
            'email.email'                   => 'ელ-ფოსტის მისამართი არ მოიძებნა',
            'message.required'              => 'მესიჯის ველი სავალდებულოა',
            'g-recaptcha-response.required' => 'გთხოვთ დაადასტუროთ'
        ];

        $this->validate($request, [
            'name'                 => 'required',
            'email'                => 'required|email',
            'message'              => 'required',
            'g-recaptcha-response' => 'required',
        ], $validationMessages);

        if ($response['success'] != 1)
        {
            return redirect('/contact');
        }

        $payload    = array(
            'name'  => Input::get('name'),
            'email' => Input::get('email'),
            'msg'   => Input::get('message')
        );

        Mail::send('email.contact', $payload, function($message)
        {
            $message->from('noreply@gdson.net', Input::get('name'));
            $message->to([
                'z.sharashenidze@gds.tv',
                //'b.molashvili@gds.tv',
                'nchimakadze@gmail.com',
                //'luka.ramishvili@gmail.com',
                //'l.devidze@webintelligence.de',
                'g.kopadze@webintelligence.de'
            ], 'GDSON')->subject('Contact');
        });

        return redirect('/contact')->withMessage('თქვენი შეტყობინება წარმატებით გაიგზავნა');
    }

    /**
     * Show highlights
     *
     * @return Response
     */
    public function highlights()
    {
        return view('highlights');
    }

    /**
     * Show popular shows, videos, etc
     *
     * @return Response
     */
    public function popular()
    {
        return view('popular');
    }

    /**
     * Last added episodes
     *
     * @return Response
     */
    public function lastAdded()
    {
        return view('lastAdded');
    }

    /**
     * Search page
     *
     * @return Response
     */
    public function search($name = null)
    {
        return view('search')->with('name', $name);
    }

    /**
     * Search results (ajax call) json
     *
     * @return Response
     */
    public function searchresults()
    {
        $keyword = Input::get('keyword');

        $results = [];

        $shows   = Show::all();
        foreach ($shows as $show) {
            if ($show->search($keyword)) {
                $results []= array(
                    'title' => $show->getTitle('geo'),
                    'desc'  => $show->getDescription('geo'),
                    'image' => Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/show/' . $show->id . '/cover.jpg',
                    'href'  => '/show/view/' . $show->alias
                );
            }
        }

        $seasons  = Season::all();
        foreach ($seasons as $season) {
            if ($season->search($keyword)) {
                $results []= array(
                    'title' => $season->getTitle('geo'),
                    'desc'  => $season->getDescription('geo'),
                    'image' => Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/season/' . $season->id . '/cover.jpg',
                    'href'  => '/show/view/' . $season->show->alias . '/' . $season->season_number
                );
            }
        }

        $episodes  = Episode::all();
        foreach ($episodes as $episode) {
            if ($episode->search($keyword)) {
                $results []= array(
                    'title' => $episode->getTitle('geo'),
                    'desc'  => $episode->getDescription('geo'),
                    'image' => Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/episode/' . $episode->id . '/cover.jpg',
                    'href'  => '/show/view/' . $episode->season->show->alias . '/' .
                                $episode->season->season_number . '/' . $episode->episode_number
                );
            }
        }

        $posts = Post::all();
        foreach ($posts as $post) {
            if ($post->search($keyword)) {
                $results [$post->id]= array(
                    'title' => $post->getTitle('geo'),
                    'desc'  => $post->getDescription('geo'),
                    'image' => Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/post/' . $post->id . '/cover.jpg'
                );

                $target             = $post->shows->first();
                $results[$post->id]['href']    = '#';

                if (!empty($target)) {
                    $results[$post->id]['href'] = '/show/view/' . $target->alias;
                } else if (empty($target)) {
                    $target = $post->seasons->first();
                    if (!empty($target)) {
                        $results[$post->id]['href'] = '/show/view/' . $target->show->alias . '/' . $target->season_number;
                    } else if (empty($target)) {
                        $target = $post->episodes->first();
                        if (!empty($target)) {
                            $results[$post->id]['href'] = '/show/view/' . $target->season->show->alias . '/' .
                                $target->season->season_number . '/' . $target->episode_number;
                        } else {
                            array_pop($results);
                        }
                    }
                }
            }
        }

        $pages  = Page::all();
        foreach ($pages as $page) {
            if ($page->search($keyword)) {
                $results []= array(
                    'title' => $page->getTitle('geo'),
                    'desc'  => $page->getDescription('geo'),
                    'image' => '',
                    'href'  => '#'
                );
            }
        }

        $faqs  = Faq::all();
        foreach ($faqs as $faq) {
            if ($faq->search($keyword)) {
                $results []= array(
                    'title' => $faq->getTitle('geo'),
                    'desc'  => $faq->getDescription('geo'),
                    'image' => '',
                    'href'  => '/faq'
                );
            }
        }

        $persons = Person::all();
        foreach ($persons as $person) {
            if ($person->search($keyword)) {
                $results []= array(
                    'title' => $person->getFirstName()->first()->value_geo,
                    'desc'  => $person->getLastName()->first()->value_geo,
                    'image' => Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/person/' . $person->id . '/person.jpg',
                    'href'  => ''
                );
            }
        }

        return json_encode(array("results" => $results));
    }


}
