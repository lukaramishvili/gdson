<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Person;
use App\Profession;
use App\Translation;
use Aws\CloudFront\CloudFrontClient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PersonController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['']]);
    }

    /**
     * Get actors (DEPRECATED: use getByprofession/actor)
     *
     * @return View
     */
    public function getActors()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'actor') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'actors');
    }

    /**
     * Get producers (DEPRECATED: use getByprofession/producer)
     *
     * @return View
     */
    public function getProducers()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'producer') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'producers');
    }

    /**
     * Get directors (DEPRECATED: use getByprofession/director)
     *
     * @return View
     */
    public function getDirectors()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'director') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'directors');
    }

    /**
     * Get writers  (DEPRECATED: use getByprofession/writer)
     *
     * @return View
     */
    public function getWriters()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'writer') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'writers');
    }

    /**
     * Get anchors (DEPRECATED: use getByprofession/anchor)
     *
     * @return View
     */
    public function getAnchors()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'anchor') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'anchors');
    }

    /**
     * Get journalists (DEPRECATED: use getByprofession/journalist)
     *
     * @return View
     */
    public function getJournalists()
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == 'journalist') {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', 'journalists');
    }


    /**
     * Get person list by profession (generic version of get[Actor]s/etc)
     *
     * @return View
     */
    public function getByprofession($keyword)
    {
        $persons     = Person::all();
        $persons_arr = [];
        foreach ($persons as $person) {
            foreach ($person->professions as $profession) {
                if ($profession->keyword == $keyword) {
                    $persons_arr[] = $person;
                }
            }
        };

        return view('person.all')
            ->with('persons', $persons_arr)
            ->with('active', $keyword.'s');
    }

    /**
     * Get all persons
     *
     * @return View
     */
    public function getAll()
    {
        $persons = Person::all();

        return view('person.all')
            ->with('persons', $persons)
            ->with('active', 'all');
    }

    /**
     * Add person
     *
     * @return View
     */
    public function getAddperson()
    {
        $firstname_tr         = Translation::create(['value_geo' => 'სახელი', 'value_eng' => 'First Name', 'value_rus' => 'First Name']);
        $lastname_tr          = Translation::create(['value_geo' => 'გვარი', 'value_eng' => 'Last Name', 'value_rus' => 'Last Name']);
        $desc_tr              = Translation::create(['value_geo' => 'აღწერა', 'value_eng' => 'description', 'value_rus' => 'description']);

        $person               = Person::create(['firstname_tr_id' => $firstname_tr->id, 'lastname_tr_id' => $lastname_tr->id, 'desc_tr_id' => $desc_tr->id]);
        $person['first_name'] = $firstname_tr->toArray();
        $person['last_name']  = $lastname_tr->toArray();
        $person['desc_tr']    = $desc_tr->toArray();
        $person['image']      = false;

        $professions          = Profession::lists('keyword', 'id');

        return view('person.editPerson')
            ->with('person', $person)
            ->with('person_professions', null)
            ->with('professions', $professions)
            ->with('type', 'add')
            ->with('active', 'all');
    }

    /**
     * Edit person
     *
     * @param $person_id
     * @return View
     */
    public function getEditperson($person_id)
    {
        $person               = Person::find($person_id);
        $person['first_name'] = $person->getFirstname->toArray();
        $person['last_name']  = $person->getLastname->toArray();
        $person['desc_tr']    = $person->getDescriptions();

        $person['image']      = '/uploads/person/' . $person->id . '/person.jpg';
        if (!file_exists(public_path() . $person['image'])) {
            $person['image']      = false;
        }

        $professions              = Profession::lists('keyword', 'id');

        $person_professions       = $person->professions;
        $person_professions_array = [];
        foreach ($person_professions as $profession) {
            $person_professions_array[] = $profession['id'];
        }

        return view('person.editPerson')
            ->with('person', $person)
            ->with('person_professions', $person_professions_array)
            ->with('professions', $professions)
            ->with('type', 'edit')
            ->with('active', 'all');
    }

    /**
     * Edit person's first name, last name in db
     */
    public function postEditperson()
    {
        $person_id = Input::get('pk');
        $value     = Input::get('value');
        $name      = Input::get('name');
        $field     = Input::get('field');

        $person    = Person::find($person_id);
        Translation::find($person[$field])
                    ->update(['value_' . $name => $value]);

    }

    public function getDeleteperson($person_id)
    {
        Person::find($person_id)
              ->delete();

        return redirect('/person/all');
    }

    /**
     * Edit person's profession
     */
    public function postEditprofession()
    {
        $person = Person::find(Input::get('person_id'));
        $person->professions()->detach();

        foreach (Input::get('professions') as $profession) {
            $person->professions()->attach($profession, ['is_main' => filter_var(Input::get('is_main'), FILTER_VALIDATE_BOOLEAN)]);
        }
    }

    /**
     * Get all professions
     *
     * @return View
     */
    public function getProfessions()
    {
        $professions = Profession::all();

        return view('person.professions')
            ->with('professions', $professions)
            ->with('active', 'professions');
    }

    /**
     * Add profession
     *
     * @return Redirect
     */
    public function postAddprofession()
    {
        $geo = Input::get('profession-geo') ? Input::get('profession-geo') : 'პროფესია';
        $eng = Input::get('profession-eng') ? Input::get('profession-eng') : 'profession';
        $rus = Input::get('profession-rus') ? Input::get('profession-rus') : 'profession';
        $key = Input::get('profession-key') ? Input::get('profession-key') : str_random(12);

        $title_tr = Translation::create(['value_geo' => $geo, 'value_eng' => $eng, 'value_rus' => $rus]);

        Profession::create(['title_tr_id' => $title_tr->id, 'keyword' => $key]);

        return redirect('/person/professions');
    }

    /**
     * Edit profession
     */
    public function postEditprof()
    {
        $profession = Profession::find(Input::get('pk'));

        Translation::find($profession->title_tr_id)
                   ->update(['value_' . Input::get('name') => Input::get('value')]);
    }

    /**
     * Edit profession key
     */
    public function postEditprofkey()
    {
        Profession::find(Input::get('pk'))
                  ->update(['keyword' => Input::get('value')]);
    }

    /**
     * Delete profession
     *
     * @param $profession_id
     * @return Redirect
     * @throws \Exception
     */
    public function getDeleteprofession($profession_id)
    {
        $profession = Profession::find($profession_id);
        $profession->delete();

        Translation::find($profession->title_tr_id)
                   ->delete();

        return redirect('/person/professions');
    }

    /**
     * Save person image
     */
    public function postSavepersonimage()
    {
        $file       = Input::file('person-image');
        $person_id  = Input::get('person-id');

        $destinationPath = public_path() . '/uploads/person/' . $person_id . '/';

        $file->move($destinationPath, 'person.jpg');

        $s3 = App::make('aws')->get('s3');

        $object = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'person/' . $person_id . '/person.jpg'
        ))->toArray();

        $twoYears = Carbon::now()->addYears(2);

        $s3->putObject(array(
            'Bucket'     => 'gdsonvideos1',
            'Key'        => 'person/' . $person_id . '/person.jpg',
            'ACL'        => 'public-read',
            'SourceFile' => $destinationPath . 'person.jpg',
            'Expires'    => $twoYears,
        ));

        unlink($destinationPath . 'person.jpg');

        if (isset($object) && !empty($object)) {
            $client = CloudFrontClient::factory();

            $client->createInvalidation([
                'DistributionId'  => 'E1QUT3BR2G48AH',
                'Paths'           => [
                    'Quantity'    => 1,
                    'Items'       => ['/person/' . $person_id . '/person.jpg']
                ],
                'CallerReference' => time()
            ]);
        }
    }

    /**
     * Remove person image
     *
     * @return string
     */
    public function postRemovepersonimage()
    {
        $person_id = Input::get('person_id');

        $path      = public_path() . '/uploads/person/' . $person_id . '/person.jpg';

        try {
            unlink($path);
        } catch (\exception $e) {
            return 'failed';
        }

        return 'success';
    }
}
