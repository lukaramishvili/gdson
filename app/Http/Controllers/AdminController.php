<?php namespace App\Http\Controllers;

use App\Episode;
use App\FbNotification;
use App\Helper;
use App\Highlight;
use App\Http\Controllers\Controller;
use App\Menu;
use App\PopularEpisode;
use App\PopularRolls;
use App\Post;
use App\Season;
use App\Show;
use App\Trailer;
use App\Translation;
use App\User;
use App\VideoView;
use App\VideoWatch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Redirect;
use Input;
use Validator;
use App\Video;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Admin Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated and have proper permissions.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->middleware('admin', ['except' => ['getSavevideoviews', 'getFbcanvas']]);
	}

    /**
     * Main page for admin
     *
     * @return \Illuminate\View\View
     */
    public function getMain()
    {
        $shows = Show::all();

        foreach($shows as &$show) {
            $show['title']       = Translation::find($show->title_tr_id)->toArray();
            $show['description'] = Translation::find($show->desc_tr_id)->toArray();
            $show['cover']       = $show->getImageUrl();
        }

        return view('admin.main')
            ->with('shows', $shows->toArray())
            ->with('active', 'main');
    }

    /**
     * Store video views in db
     */
    public function getSavevideoviews()
    {
        $videos     = Video::all();
        PopularEpisode::whereNotNull('id')->delete();
        foreach ($videos as $video)
        {
            $viewCount = $video->oneWeekWatches();

            $videoViews = VideoView::where('video_id', $video->id)
                                   ->get()
                                   ->toArray();

            if (isset($videoViews) && !empty($videoViews))
            {
                VideoView::where('video_id', $video->id)
                         ->delete();
            }

            VideoView::create(['video_id' => $video->id, 'lang' => 'geo', 'viewcount' => $viewCount]);

            if ($video->episode)
            {
                PopularEpisode::create(['episode_id' => $video->episode->id, 'viewcount' => $viewCount]);
            }
        }

        print 'Saved';
    }

    /**
     * Fb canvas landing url
     *
     * @param Request $request
     * @return View
     */
    public function getFbcanvas(Request $request)
    {
        $data     = $request->all();

        $url          = '';
        $notification = '';
        $exploded     = explode("/", $data['object']);
        if ($exploded[1] == 'episode')
        {
            $episode = Episode::find($exploded[0]);
            $url     = $episode->getUrl();
            $notification = FbNotification::where('whose_type', 'episode')
                                          ->where('whose_id', $exploded[0])
                                          ->get();
        }
        else if ($exploded[1] == 'season')
        {
            $season = Season::find($exploded[0]);
            $url    = $season->getUrl();
            $notification = FbNotification::where('whose_type', 'season')
                                          ->where('whose_id', $exploded[0])
                                          ->get();
        }
        else if ($exploded[1] == 'show')
        {
            $show = Show::find($exploded[0]);
            $url  = $show->getUrl();
            $notification = FbNotification::where('whose_type', 'show')
                                          ->where('whose_id', $exploded[0])
                                          ->get();
        }

        /*return view('fbcanvas')
            ->with('url', $url);*/

        if (!isset($notification[0]))
        {
            return view('main');
        }

        return view('main')
            ->with('notificationId', $notification[0]->id);
    }

    /**
     * Testing
     */
    public function getTest()
    {
        echo '<pre>';
        print_r(Carbon::now());
        print '<br>';
        print_r(PopularRolls::all()->toArray());
        exit;
    }

    /**
     * Update old aliases
     */
    public function getUpdatealias()
    {
        echo '<pre>';
        foreach (Show::all() as $show)
        {
            $showValue = $show -> alias;
            $showValue = str_replace(explode(',', 'ა,ბ,გ,დ,ე,ვ,ზ,თ,ი,კ,ლ,მ,ნ,ო,პ,ჟ,რ,ს,ტ,უ,ფ,ქ,ღ,ყ,შ,ჩ,ც,ძ,წ,ჭ,ხ,ჯ,ჰ'), explode(',', 'a,b,g,d,e,v,z,th,i,k,l,m,n,o,p,jh,r,s,t,u,f,q,gh,kh,sh,ch,c,dz,ts,tch,kh,j,h'), $showValue);
            $showValue = str_replace(explode(',', '?, '), explode(',', ',-'), $showValue);
            var_dump($showValue);

            $show -> update(['alias' => $showValue]);
        }

        foreach (Episode::all() as $episode)
        {
            $episodeValue = $episode -> alias;
            $episodeValue = str_replace(explode(',', 'ა,ბ,გ,დ,ე,ვ,ზ,თ,ი,კ,ლ,მ,ნ,ო,პ,ჟ,რ,ს,ტ,უ,ფ,ქ,ღ,ყ,შ,ჩ,ც,ძ,წ,ჭ,ხ,ჯ,ჰ'), explode(',', 'a,b,g,d,e,v,z,th,i,k,l,m,n,o,p,jh,r,s,t,u,f,q,gh,kh,sh,ch,c,dz,ts,tch,kh,j,h'), $episodeValue);
            $episodeValue = str_replace(explode(',', '?, '), explode(',', ',-'), $episodeValue);
            var_dump($episodeValue);

            $episode -> update(['alias' => $episodeValue]);
        }
    }

    /**
     * Make menu items hierarchy
     *
     * @return array
     */
    private function getOrderedMenu()
    {
        $menu   = Menu::orderBy('order')->get();

        $result = [];
        foreach ($menu as $key => $item)
        {
            $children = Menu::where('parent_id', $item->id)
                            ->orderBy('order')
                            ->get();
            if (!$item->parent_id)
            {
                $result[] = $item;
                if (isset($children[0]))
                {
                    foreach ($children as $child)
                    {
                        $result[] = $child;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Page for viewing all menu items
     *
     * @return mixed
     */
    public function getMenu()
    {
        $menu = $this->getOrderedMenu();

        return view('menu.all')
            ->with('menu', $menu)
            ->with('active', 'menu');
    }

    /**
     * Page for adding new menu item
     *
     * @return mixed
     */
    public function getAddmenuitem()
    {
        $title_tr = Translation::create(['value_geo' => 'სათაური', 'value_eng' => 'title', 'value_rus' => 'title']);
        Menu::create(['title_tr_id' => $title_tr->id]);

        return Redirect::back();
    }

    /**
     * Edit menu item title/link
     */
    public function postEditmenu()
    {
        $menuItemId = Input::get('pk');
        $field      = Input::get('field');
        $value      = Input::get('value');
        $lang       = Input::get('name');

        $menuItem   = Menu::find($menuItemId);
        if ($field == 'title')
        {
            $valueLang   = 'value_' . $lang;
            $translation = Translation::find($menuItem->title_tr_id);
            $translation->$valueLang = $value;
            $translation->save();
        }
        else if ($field == 'link')
        {
            $menuItem->update(['link' => $value]);
        }
    }

    /**
     * Edit menu item parent
     */
    public function postEditmenuparent()
    {
        $menuId   = Input::get('menu_id');
        $parentId = Input::get('parent_id');

        $menuItem = Menu::find($menuId);
        if ($parentId == '')
        {
            $menuItem->parent_id = null;
        }
        else
        {
            $menuItem->parent_id = $parentId;

        }
        $menuItem->save();
    }

    /**
     * Edit menu item order
     */
    public function postEditmenuorder()
    {
        $menuId   = Input::get('menu_id');
        $order    = Input::get('value');

        $menuItem = Menu::find($menuId);
        if ($order == '')
        {
            $menuItem->order = null;
        }
        else
        {
            $menuItem->order = $order;

        }
        $menuItem->save();
    }

    /**
     * Delete menu item
     */
    public function postDeletemenuitem()
    {
        $menuItemId = Input::get('menuItemId');

        Menu::where('parent_id', $menuItemId)
            ->delete();

        Menu::find($menuItemId)
            ->delete();
    }

    /**
     * Page to see video watches statistic
     *
     * @return View
     */
    public function getStatistic()
    {
        $shows = Show::all();

        return view('statistic.statistic')
            ->with('shows', $shows);
    }

    /**
     * Get video watches in the time specified, get all video watches for that video
     *
     * @param $videoId
     * @param $statisticFromTimeFormated
     * @param $statisticToTimeFormated
     * @return array
     */
    protected function getVideoWatches($videoId, $statisticFromTimeFormated, $statisticToTimeFormated, $statisticQuality)
    {
        $watches    = VideoWatch::where('video_id', $videoId)
                                ->where('created_at', '>=', $statisticFromTimeFormated)
                                ->where('created_at', '<=', $statisticToTimeFormated);

        $allWatches = VideoWatch::where('video_id', $videoId);

        if ($statisticQuality == 0)
        {
            $watches    = $watches->count();
            $allWatches = $allWatches->count();
        }
        else
        {
            $watches    = $watches->where('quality', $statisticQuality)
                                  ->count();

            $allWatches = $allWatches->where('quality', $statisticQuality)
                                     ->count();
        }

        return [
            'watches'    => $watches,
            'allWatches' => $allWatches
        ];
    }

    /**
     * Get statistic information in selected time for shows/seasons/episodes
     *
     * @param Request $request
     * @return string
     */
    public function postStatistic(Request $request)
    {
        $statisticShow     = Input::get('statistic-show');
        $statisticSeason   = Input::get('statistic-season');
        $statisticEpisode  = Input::get('statistic-episode');
        $statisticFromTime = Input::get('statistic-from-time');
        $statisticToTime   = Input::get('statistic-to-time');
        $statisticQuality  = Input::get('statistic-quality');

        $rules = [
            'statistic-from-time' => 'required',
            'statistic-to-time'   => 'required'
        ];

        $this->validate($request, $rules);

        $statisticFromTimeFormated = Carbon::createFromFormat('m/d/Y H:i', $statisticFromTime)->format('Y-m-d H:i:s');
        $statisticToTimeFormated   = Carbon::createFromFormat('m/d/Y H:i', $statisticToTime)->format('Y-m-d H:i:s');

        if (isset($statisticEpisode) && $statisticEpisode != 0)
        {
            $episode = Episode::find($statisticEpisode);

            $watches      = 0;
            $allWatches   = 0;
            $video        = $episode->video;
            $videoWatches = $this->getVideoWatches($video->id, $statisticFromTimeFormated, $statisticToTimeFormated, $statisticQuality);
            $watches     += $videoWatches['watches'];
            $allWatches  += $videoWatches['allWatches'];

            $shows = Show::all();

            return view('statistic.statistic')
                ->with('shows', $shows)
                ->with('statisticFromTime', $statisticFromTime)
                ->with('statisticToTime', $statisticToTime)
                ->with('quality', $statisticQuality)
                ->with('allWatches', $allWatches)
                ->with('watches', $watches);
        }
        else if (isset($statisticSeason) && $statisticSeason != 0)
        {
            $episodes = Episode::where('season_id', $statisticSeason)
                               ->get();

            $watches    = 0;
            $allWatches = 0;
            foreach ($episodes as $episode)
            {
                $video        = $episode->video;
                $videoWatches = $this->getVideoWatches($video->id, $statisticFromTimeFormated, $statisticToTimeFormated, $statisticQuality);
                $watches     += $videoWatches['watches'];
                $allWatches  += $videoWatches['allWatches'];
            }

            $shows = Show::all();

            return view('statistic.statistic')
                ->with('shows', $shows)
                ->with('statisticFromTime', $statisticFromTime)
                ->with('statisticToTime', $statisticToTime)
                ->with('quality', $statisticQuality)
                ->with('allWatches', $allWatches)
                ->with('watches', $watches);
        }
        else if (isset($statisticShow))
        {
            if ($statisticShow == 0)
            {
                $seasons = Season::all();

                $watches    = 0;
                $allWatches = 0;
                foreach ($seasons as $season)
                {
                    $episodes = Episode::where('season_id', $season->id)
                        ->get();

                    foreach ($episodes as $episode)
                    {
                        $video        = $episode->video;
                        $videoWatches = $this->getVideoWatches($video->id, $statisticFromTimeFormated, $statisticToTimeFormated, $statisticQuality);
                        $watches     += $videoWatches['watches'];
                        $allWatches  += $videoWatches['allWatches'];
                    }
                }

                $shows = Show::all();

                return view('statistic.statistic')
                    ->with('shows', $shows)
                    ->with('statisticFromTime', $statisticFromTime)
                    ->with('statisticToTime', $statisticToTime)
                    ->with('quality', $statisticQuality)
                    ->with('allWatches', $allWatches)
                    ->with('watches', $watches);
            }
            else
            {
                $seasons = Season::where('show_id', $statisticShow)
                                 ->get();

                $watches    = 0;
                $allWatches = 0;
                foreach ($seasons as $season)
                {
                    $episodes = Episode::where('season_id', $season->id)
                                       ->get();

                    foreach ($episodes as $episode)
                    {
                        $video        = $episode->video;
                        $videoWatches = $this->getVideoWatches($video->id, $statisticFromTimeFormated, $statisticToTimeFormated, $statisticQuality);
                        $watches     += $videoWatches['watches'];
                        $allWatches  += $videoWatches['allWatches'];
                    }
                }

                $shows = Show::all();

                return view('statistic.statistic')
                    ->with('shows', $shows)
                    ->with('statisticFromTime', $statisticFromTime)
                    ->with('statisticToTime', $statisticToTime)
                    ->with('quality', $statisticQuality)
                    ->with('allWatches', $allWatches)
                    ->with('watches', $watches);
            }
        }

        return Redirect::back();
    }

    /**
     * Get seasons/episodes for dynamically updating statistic form select options
     *
     * @return array|string
     */
    public function postStatisticobjects()
    {
        $object   = Input::get('object');
        $objectId = Input::get('objectId');

        if ($objectId == 0)
        {
            return 'failed';
        }

        if ($object == 'show')
        {
            $seasons = Season::where('show_id', $objectId)
                             ->get();

            $seasonsArr = [];
            foreach ($seasons as $season)
            {
                $seasonsArr[] = [
                    'id'    => $season->id,
                    'title' => $season->getTitle('geo')
                ];
            }

            return $seasonsArr;
        }
        else if ($object == 'season')
        {
            $episodes = Episode::where('season_id', $objectId)
                               ->get();

            $episodesArr = [];
            foreach ($episodes as $episode)
            {
                $episodesArr[] = [
                    'id'    => $episode->id,
                    'title' => $episode->getTitle('geo')
                ];
            }

            return $episodesArr;
        }

        return 'failed';
    }

    public function getMigratevideos(){
        $videos = Video::where('record_id','=','')->orWhere('record_id','IS','NULL')->get();
        return view('migratevideos', array('videos' => $videos));
    }

    public function postMigratevideo(){/* $video_id, $record_id */
        $message = "nothing happened";
        $success = false;
        //
        $video_id = Input::get('video_id');
        $record_id = Input::get('record_id');
        if(is_numeric($video_id) && is_numeric($record_id)){
            $video = Video::find($video_id);
            if($video){
                if($video->record_id){
                    $message = "video #$video_id already has record_id: ".$video->record_id;
                } else {
                    $video->record_id = $record_id;
                    $video->save();
                    $success = true;
                    $message = "saved record_id $record_id into video $video_id";
                }
            } else {
                $message = "no video found with that id";
            }
        } else {
            $message = "invalid parameters - must be numeric";
        }
        return json_encode(array("message" => $message, "success" => $success));
    }

}
