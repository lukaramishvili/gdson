<?php namespace App\Http\Controllers;

use App\Episode;
use App\Highlight;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Season;
use App\Show;
use App\Trailer;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class HighlightController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['']]);
    }

    /**
     * Add/remove trailer from highlights
     */
    public function postTrailerhighlight()
    {
        $video_id  = Input::get('video_id');
        $state     = Input::get('state');

        $video     = Video::find($video_id);
        $trailer   = $video->trailer;
        $highlight = $trailer->highlight;

        if($state == 'false' && $highlight) {
            $highlight->delete();
        } else if ($state == 'true' && !$highlight) {
            Highlight::create(['trailer_id' => $trailer->id]);
        }
    }

    /**
     * Get all highlighted tables
     *
     * @return View
     */
    public function getAll()
    {
        $trailers = Trailer::all();

        $shows    = [];
        $seasons  = [];
        $episodes = [];
        foreach ($trailers as $trailer) {
            if ($trailer->whose_type && $trailer->whose_id != 0) {
                if ($trailer->whose_type == 'show') {
                    $shows[]              = array(
                        'show'            => $trailer->whose,
                        'video_title_geo' => $trailer->video->getTitle('geo'),
                        'video_id'        => $trailer->video->id,
                        'highlight'       => $trailer->highlight,
                    );
                } else if ($trailer->whose_type == 'season') {
                    $seasons[]            = array(
                        'season'          => $trailer->whose,
                        'show'            => $trailer->whose->show,
                        'video_title_geo' => $trailer->video->getTitle('geo'),
                        'video_id'        => $trailer->video->id,
                        'highlight'       => $trailer->highlight,
                    );
                } else if ($trailer->whose_type == 'episode') {
                    if (isset($trailer->whose->id) && !empty($trailer->whose->id)) {
                        $episodes[]           = array(
                            'episode'         => $trailer->whose,
                            'season'          => $trailer->whose->season,
                            'show'            => $trailer->whose->season->show,
                            'video_title_geo' => $trailer->video->getTitle('geo'),
                            'video_id'        => $trailer->video->id,
                            'highlight'       => $trailer->highlight,
                        );
                    }
                }
            }
        }

        return view('highlight.all')
            ->with('shows', $shows)
            ->with('seasons', $seasons)
            ->with('episodes', $episodes)
            ->with('active', 'highlight');
    }

    /**
     * Highlight order view
     *
     * @return View
     */
    public function getOrder()
    {
        $highlights   = Highlight::all();

        $trailers     = [];
        foreach ($highlights as $highlight) {
            $video_id = $highlight->trailer->video_id;
            $video    = Video::find($video_id);

            $trailers[] = array(
                'highlight_id' => $highlight->id,
                'order'        => $highlight->order,
                'video'        => $video,
            );
        }

        return view('highlight.order')
            ->with('trailers', $trailers)
            ->with('active', 'highlight');
    }

    /**
     * Update order
     */
    public function postOrder()
    {
        Highlight::where('id', '=', Input::get('highlight_id'))
                 ->update(['order' => Input::get('value')]);
    }
}
