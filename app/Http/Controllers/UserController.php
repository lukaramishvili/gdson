<?php namespace App\Http\Controllers;

use App\Role;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Redirect;
use App\User;
use App\Genre;
use App\Playlist;
use App\Video;
use App\Translation;

class UserController extends Controller {

	/*
      |--------------------------------------------------------------------------
      | User Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        //in VideoController, only => approve works (doesn't require method name)
        //seems like except works differently, except => json doesn't work
		$this->middleware('auth', ['except' => ['getLogin', 'getJson']]);
        $this->middleware('admin', ['only' => ['getUsers', 'postChangerole', 'getBlock', 'getUnblock']]);
	}

	/**
	 * Show the user profile
	 *
	 * @return Response
	 */
	public function getProfile()
	{
		//return view('user.profile');
        return view('user.profile')->with('btn_account_opens_settings', true);
	}

	/**
	 * Show the user settings popup (js in the bottom of layouts.master)
	 *
	 * @return Response
	 */
	public function getSettings()
	{
		//return view('user.profile');
        return view('main')->with('popup', 'settings');
	}

	/**
	 * Show the user login popup (js in the bottom of layouts.master)
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		//return view('user.login');
        return view('main')->with('popup', 'login');
	}

	public function getJson($id)
	{
		return json_encode(User::find($id)->toArray());
	}

    public function getBlock($id){
        if(Auth::check() && Auth::user()->can('EditUsers')){
            $u = User::find($id);
            $u->block();
        }
        return Redirect::back();
    }

    public function getUnblock($id){
        if(Auth::check() && Auth::user()->can('EditUsers')){
            $u = User::find($id);
            $u->unblock();
        }
        return Redirect::back();
    }

    /**
     * Get all users
     *
     * @return View
     */
    public function getUsers()
    {
        $users = User::all();

        return view('user.users')
            ->with('users', $users)
            ->with('active', 'users');
    }

    /**
     * Get user frineds
     *
     * @param $user_id
     */
    public function getUserfriends($user_id)
    {
        $user    = User::find($user_id);
        $likes   = $user->friendLikes();

        echo '<pre>';
        print_r($likes);
        exit;
    }

    /**
     * Change use role
     */
    public function postChangerole()
    {
        $user = User::find(Input::get('user_id'));
        $role = $user->roles->first();

        if (empty($role)) {
            if (Input::get('state') == 'true') {
                $user->roles()->attach(2);
            }
        } else {
            if (Input::get('state') == 'true') {
                $user->roles()->detach();
                $user->roles()->attach(2);
            } else {
                $user->roles()->detach();
                $user->roles()->attach(1);
            }
        }
    }

    /**
     * Subscribe/unsubscribe to a genre
     *
     * @return View
     */
    public function postSubscribe($genre_id, $on)
    {
        $result = "nothing happened";
        if(is_numeric($genre_id) && Genre::find($genre_id)){
            if($on == 1){
                Auth::user()->genres()->attach($genre_id);
            } elseif($on == 0){
                Auth::user()->genres()->detach($genre_id);
            } else { $result = "Invalid parameters"; }
        } else { $result = "Genre not found"; }
        return json_encode(array("result" => $result));
    }

    /**
     * Subscribe/unsubscribe to a genre
     *
     * @return View
     */
    public function postSetvideoinplaylist($playlist_id, $video_id, $on)
    {
        $result = "nothing happened";
        if(is_numeric($playlist_id) && is_numeric($video_id) && Playlist::find($playlist_id) && Video::find($video_id)){
            $playlist = Playlist::find($playlist_id);
            $video = Playlist::find($video_id);
            if($on == 1){
                if(!($playlist->videos->contains($video_id))){
                    $playlist->videos()->attach($video_id);
                }
            } elseif($on == 0){
                $playlist->videos()->detach($video_id);
            } else { $result = "Invalid parameters"; }
        } else { $result = "Playlist or video not found"; }
        return json_encode(array("result" => $result));
    }

    /**
     * add playlist
     *
     * @return View
     */
    public function postAddplaylist()
    {
        $result = "nothing happened";
        //
        $name = Input::get('name');
        //we're guarding this with auth middleware, so can expect the user to be there
        $user = Auth::user();
        if(isset($name) && !is_null($name) && mb_strlen($name) > 0){
            $playlist_title = Translation::create(array(
                "value_geo" => $name,
                "value_eng" => $name,
                "value_ru" => $name
            ));
            $playlist = Playlist::create(array(
                'user_id' => $user->id,
                'is_public' => false,
                'title_tr_id'=>$playlist_title->id
            ));
            $result = "ფლეილისტი დამატებულია";
        } else { $result = "არასწორი პარამეტრები"; }
        return json_encode(array("result" => $result));
    }

    /**
     * Remove user from the website
     *
     * @return success
     */
    public function postRemovemyself()
    {
        $result = "nothing happened";
        $success = false;
        $myself = Auth::user();
        //calling Auth::logout() after deleting user was restoring the user
        //|->apparently Auth::logout() had a copy of the user and saved it back
        //|->with updated last-login information or something similar
        Auth::logout();
        if($myself->deleteData()){
            $success = true;
            $result = "Your account was successfully deleted";
        } else {
            $success = false;
            $result = "Your account could not be deleted";
            //the user couldn't be deleted, so log him/her back in
            Auth::login($myself);
        }
        return json_encode(array("success" => $success, "result" => $result));
    }

}
