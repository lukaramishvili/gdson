<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Playlist;

class PlaylistController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['only' => ['postUserPlaylist', 'postUserPlaylistItem']]);
    }

/**
     * Playlist page
     *
     * @param $alias
     * @param int $season_number
     * @param int $episode_number
     * @return View
     */
    public function getView($id)
    {
        $playlist = Playlist::find($id);
        return view('playlist.playlist')->with('playlist', $playlist);
    }

    /**
     * Get all playlists
     *
     * @return Response
     */
    public function getLists()
    {
        return view('playlist.lists');
    }

    /**
     * Get all playlists that belong to a user
     *
     * @return Response
     */
    public function getUserplaylists($username)
    {
        return view('playlist.userplaylists')
            ->with('username', $username);
    }

    /**
     * Get a playlist that belongs to a user
     *
     * @return Response
     */
    public function getUserplaylist($username, $alias)
    {
        return view('playlist.playlist')
            ->with('username', $username)
            ->with('alias', $alias);
    }

    /**
     * Add use playlist
     */
    public function postUserPlaylist()
    {

    }

    /**
     * Add item in user playlist
     */
    public function postUserPlaylistItem()
    {

    }

}
