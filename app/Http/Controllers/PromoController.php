<?php namespace App\Http\Controllers;

use App\Episode;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Promo;
use App\Season;
use App\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PromoController extends Controller {

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['']]);
	}

    /**
     * Get shows/seasons/episodes
     */
    public function getAll()
    {
        $shows    = [];
        $seasons  = [];
        $episodes = [];
        foreach (Show::all() as $show)
        {
            $showTitle = $show->getTitle('geo');
            $shows[$show->id]['showTitle'] = $showTitle;
            $shows[$show->id]['checkPromo'] = $show->checkPromo();
            foreach ($show->seasons as $season)
            {
                $seasonTitle = $season->getTitle('geo');
                $seasons[$season->id]['showTitle']   = $showTitle;
                $seasons[$season->id]['seasonTitle'] = $seasonTitle;
                $seasons[$season->id]['checkPromo']  = $season->checkPromo();
                foreach ($season->episodes as $episode)
                {
                    $episodeTitle = $episode->getTitle('geo');
                    $episodes[$episode->id]['showTitle']    = $showTitle;
                    $episodes[$episode->id]['seasonTitle']  = $seasonTitle;
                    $episodes[$episode->id]['episodeTitle'] = $episodeTitle;
                    $episodes[$episode->id]['checkPromo']   = $episode->checkPromo();
                }
            }
        }

        return view('promo.all')
            ->with('shows', $shows)
            ->with('seasons', $seasons)
            ->with('episodes', $episodes)
            ->with('active', 'promo');
    }

    /**
     * Add/delete promos
     */
    public function postChangepromo()
    {
        $state      = Input::get('state');
        $whose_id   = Input::get('whose_id');
        $whose_type = Input::get('whose_type');

        if($state == 'false') {
            Promo::where('whose_id', $whose_id)
                 ->where('whose_type', $whose_type)
                 ->delete();
        } else if ($state == 'true') {
            Promo::create([
                'whose_id'   => $whose_id,
                'whose_type' => $whose_type,
            ]);
        }
    }

    /**
     * Promo order view for changing promo order
     */
    public function getOrder()
    {
        $promos = Promo::all();

        return view('promo.order')
            ->with('promos', $promos)
            ->with('active', 'promo');
    }

    /**
     * Update order
     */
    public function postOrder()
    {
        Promo::where('id', '=', Input::get('promo_id'))
            ->update(['order' => Input::get('value')]);
    }

}
