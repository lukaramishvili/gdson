<?php namespace App;

trait TranslatedDescription {

    //public $desc_tr_id;

	/**
	 * @return translated description
	 */
	public function getDescription($lang)
	{
	    $tr = Translation::find($this->desc_tr_id)->toArray();
        return $tr["value_$lang"];
	}

    /**
     * Get raw of translated descriptions
     *
     * @return array
     */
    public function getDescriptions()
    {
        return Translation::find($this->desc_tr_id)->toArray();
    }

}
