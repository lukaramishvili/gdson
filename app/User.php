<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Config;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname', 'lastname', 'username', 'email', 'password',
    'avatar', 'provider', 'provider_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'username', 'email',
    'provider', 'provider_id', 'created_at', 'updated_at'];


    /**
     * Get the permissions a user has
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'roles_users');
    }

    /**
     * Find out if user has a specific permission
     *
     * $return boolean
     */
    public function can($permission)
    {
        //TODO: check in each of users' roles if it contains the permission
        //return in_array($permission, array_fetch($this->permissions->toArray(), 'name'));
    }

    /**
     * Find out if user has a specific role
     *
     * $return boolean
     */
    public function hasRole($role)
    {
        //TODO: switch on $role type; the following works for string type (rolename);
        //TODO also add checking on instances of App\Role
        return in_array($role, array_fetch($this->roles->toArray(), 'name'));
    }

    /**
     * Add role to user; argument can be role name or Role class instance
     */
    public function addRole($role)
    {
        if(is_numeric($role)){
            $role = Role::find($role);
        }
        if(is_string($role)){
            $role = Role::byName($role);
        }
        if(!$role){
            throw new \Exception("Role $role does not exist");
        }
        $this->roles()->attach(array($role->id));
    }

    /**
     * Remove role from user
     */
    public function removeRole($role)
    {
        if(is_numeric($role)){
            $role = Role::find($role);
        }
        if(is_string($role)){
            $role = Role::byName($role);
        }
        if(!$role){
            throw new \Exception("Role $role does not exist");
        }
        $this->roles()->detach(array($role->id));
    }

    public function isBlocked(){
        return !$this->hasRole('user');
    }

    public function block(){
        $this->removeRole('user');
    }

    public function unblock(){
        $this->addRole('user');
    }

    public function fbpages(){
        return $this->belongsToMany('App\FBpage');
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre')->withTimestamps();
    }

    public function playlists()
    {
        return $this->hasMany('App\Playlist');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    /**
     * Delete user data
     */
    public function deleteData()
    {
        $roles = $this->roles;
        if (isset($roles) && !empty($roles))
        {
            foreach ($roles as $role)
            {
                if (strtolower($role->name) == 'admin')
                {
                    return false;
                }
            }
        }

        return $this->delete();
    }

    /**
     * Get user friends
     */
    public function friends()
    {
        $app_id            = Config::get('services.facebook.client_id');
        $app_secret        = Config::get('services.facebook.client_secret');
        $app_access_token  = $app_id . '|' . $app_secret;

        $response          = file_get_contents("https://graph.facebook.com/v2.5/$this->provider_id/friends?access_token=$app_access_token&limit=1000");

        return json_decode($response);
    }

    /**
     * Get user likes
     */
    public function likes()
    {
        $app_id            = Config::get('services.facebook.client_id');
        $app_secret        = Config::get('services.facebook.client_secret');
        $app_access_token  = $app_id . '|' . $app_secret;

        $response          = file_get_contents("https://graph.facebook.com/v2.5/$this->provider_id/likes?access_token=$app_access_token&limit=10000");

        return json_decode($response);
    }

    /**
     * Get users friends with likes
     */
    public function friendLikes()
    {
        $app_id            = Config::get('services.facebook.client_id');
        $app_secret        = Config::get('services.facebook.client_secret');
        $app_access_token  = $app_id . '|' . $app_secret;

        $response          = file_get_contents("https://graph.facebook.com/v2.5/$this->provider_id/friends?access_token=$app_access_token&limit=10000&fields=likes");

        return json_decode($response);
    }

}
