<? namespace App;

use App\User;

class UserRepository {
    public function findByUserNameOrCreate($userData) {
        $user = User::where('provider_id', '=', $userData->id)->first();
        if(!$user) {
            $user = User::create([
                'provider_id' => $userData->id,
                'firstname' => $userData->user['first_name'],
                'lastname' => $userData->user['last_name'],
                'username' => (isset($userData->nickname)
                ? $userData->nickname : $userData->email),
                'email' => $userData->email,
//                'avatar' => $userData->avatar_original
//                    ? $userData->avatar_original : $userData->avatar,
                'avatar' => self::getAvatarUrl($userData->id),
//                'active' => 1,
            ]);
            //create favorites and watch later playlists for the user
            $fav_title = Translation::create(array("value_geo"=>"ფავორიტები","value_eng"=>"Favorites","value_ru"=>"Favoriti"));
            $watchlater_title = Translation::create(array("value_geo"=>"დამახსოვრებული","value_eng"=>"Watch later","value_ru"=>"Smotret pozdnee"));
            $fav = Playlist::create(array('user_id'=>$user->id,'is_public'=>false,'title_tr_id'=>$fav_title->id));
            $watchlater = Playlist::create(array('user_id'=>$user->id,'is_public'=>false,'title_tr_id'=>$watchlater_title->id));
            //
        }

        //https://graph.facebook.com/v2.4/me/taggable_friends?access_token=TOKEN

        $this->checkIfUserNeedsUpdating($userData, $user);
        return $user;
    }

    private static function getAvatarUrl($uid){
        return "https://graph.facebook.com/" . $uid . "/picture?width=512&height=512";
    }

    public function checkIfUserNeedsUpdating($userData, $user) {

        $socialData = [
//            'avatar' => $userData->avatar_original
//                    ? $userData->avatar_original : $userData->avatar,
            'avatar' => self::getAvatarUrl($userData->id),
            'email' => $userData->email,
            'firstname' => $userData->user['first_name'],
            'lastname' => $userData->user['last_name'],
            'username' => (isset($userData->nickname)
            ? $userData->nickname : $userData->email),
        ];
        $dbData = [
            'avatar' => $user->avatar,
            'email' => $user->email,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'username' => $user->username,
        ];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->avatar = $socialData['avatar'];
            $user->email = $socialData['email'];
            $user->firstname = $socialData['firstname'];
            $user->lastname = $socialData['lastname'];
            $user->username = $socialData['username'];
            $user->save();
        }
    }
}