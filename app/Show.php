<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class Show extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use TranslatedShortDescription;
    use Directable;
    use Search;
    use Publishable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'desc_tr_id', 'status', 'release_date', 'end_date', 'alias', 'visibility', 'publish_date', 'priority_type', 'priority_id', 'short_desc_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * get a string like "SHOW NAME"
     */
    public function getFullTitle($lang){
        return $this->getTitle($lang);
    }

    public function getTitleForShare($lang){
        $share_title = str_replace(",", " ", $this->getFullTitle($lang));
        return $share_title;
    }

    /**
     * Defines which season/episode should be landed, when user clicks on show
     *
     * @param int $season_number
     * @param int $episode_number
     * @return mixed
     */
    public function getPriorityUrl($season_number = 1, $episode_number = 1)
    {
        if (isset($this->priority_type) && $this->priority_type == 'season' && isset($this->priority_id) && !empty($this->priority_id))
        {
            $season = Season::find($this->priority_id);

            if ($season)
            {
                return URL::to('/show/view/' . $this->alias . '/sezoni-' . $season->season_number);
            }
        }

        return $this->getUrl($season_number, $episode_number);
    }

    /**
     * Get show url
     */
    public function getUrl($season_number = 1, $episode_number = 1)
    {
        if($season_number == 1 && $episode_number == 1){
            return URL::to('/show/view/' . ($this->alias) );
        } else {
            return URL::to('/show/view/' . ($this->alias) . '/' . $season_number . '/' . $episode_number);
        }
    }

    /**
     * Get image url
     */
    public function getImageUrl()
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/show/' . ($this->id) . '/cover.jpg');
    }

    /**
     * Get image url (2x)
     */
    public function getImageUrlSecond()
    {
        return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/show/' . ($this->id) . '/cover@2x.jpg');
    }

    /**
     * Get image path
     */
    public function getImagePath()
    {
        return public_path() . '/uploads/shows/' . ($this->id) . '/cover.jpg';
    }

    /**
     * Get show by alias
     */
    public static function byAlias($alias)
    {
        return self::where('alias', $alias)->first();
    }

    /**
     * Get popular shows
     */
    public static function allPopular()
    {
        return self::where('id', '!=', 12)->get();
        $newArray = [];
        $shows    = Show::all();
        foreach ($shows as $show)
        {
            if ($show->id == 12)
            {
                continue;
            }

            $view_count     = 0;
            $episodes_count = 0;
            $seasons        = $show->seasons;
            foreach ($seasons as $season)
            {
                $episodes       = $season->episodes;
                $episodes_count += $episodes->count();
                foreach ($episodes as $episode)
                {
                    $video = $episode->video;
                    $view_count += $video->views->viewcount;
                }
            }

            if ($view_count != 0 && $episodes_count != 0)
            {
                $view_count = (int) ($view_count / $episodes_count);
                if (array_key_exists($view_count, $newArray))
                {
                    $view_count -= 1;
                }

                $newArray[$view_count] = $show;
            }
        }

        ksort($newArray);
        $newArray = array_reverse($newArray, true);

        return $newArray;
    }

    /**
     * Get season by season number
     */
    public function getSeason($season_number)
    {
        return $this->seasons()->where('season_number', $season_number)
                             ->first();
    }

    /**
     * Get show's seasons
     */
    public function seasons()
    {
        return $this->hasMany('App\Season');
    }

    /**
     * Get show's seasons ordered by season number
     */
    public function orderedSeasons()
    {
        return $this->hasMany('App\Season')->orderBy('season_number');
    }

    /**
     * get all episodes of all seasons
     */
    public function getAllEpisodesOfAllSeasons()
    {
        return $this->hasManyThrough('App\Episode', 'App\Season');
    }

    /**
     * Get show's characters
     */
    public function characters()
    {
        return $this->belongsToMany('App\Character')->withTimestamps();
    }

    /**
     * Get show's posts
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post')->withTimestamps();
    }

    /**
     * Get show's genres
     */
    public function mainGenre()
    {
        return $this->genres()->first();
    }

    /**
     * Get show's genres
     */
    public function genres()
    {
        return $this->belongsToMany('App\Genre')->withTimestamps();
    }

    /**
     * @return show trailers
     */
    public function trailers()
    {
        $trailers = Trailer::where('whose_type', 'show')
                          ->where('whose_id', $this->id)
                          ->get();

        return $trailers;
    }

    /**
     * Check if show is added in promo
     */
    public function checkPromo()
    {
        $promoCount = Promo::where('whose_id', $this->id)
                       ->where('whose_type', 'show')
                       ->count();

        if ($promoCount > 0)
        {
            return true;
        }

        return false;
    }

    public function hasMoviesGenre(){
        return $this->genres->filter(function($g){ return is_numeric(mb_strpos($g->getTitle("geo"), "მხატვრ")); })->count() > 0;
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
