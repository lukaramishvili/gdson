<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';

    public $timestamps = false;
    
    
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];


    /**
     * Get role with specified name
     */
    public static function byName($name)
    {
        return Role::where('name','=',$name)->first();
    }

    /**
     * Get permissions within this role
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'permissions_roles');
    }
 
    /**
     * Get users who have this role
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'roles_users');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
