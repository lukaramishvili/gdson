<?php namespace App;

trait TranslatedSubtitles {

    //public $subtitles_tr_id;

	/**
	 * @return translated subtitle
	 */
	public function getSubtitle($lang)
	{
	    $tr = Translation::find($this->subtitles_tr_id)->toArray();
        return $tr["value_$lang"];
	}

    /**
     * @return translated subtitles
     */
    public function getSubtitles()
    {
        $tr = Translation::find($this->subtitles_tr_id)->toArray();
        return $tr;
    }

}
