<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

use App\Video;

class Playlist extends Model {

    use TranslatedTitle;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'playlists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'is_public', 'title_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get videos in the playlist
     */
    public function videos()
    {
        return $this->belongsToMany('App\Video')->withTimestamps();
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
