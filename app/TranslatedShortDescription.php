<?php namespace App;

trait TranslatedShortDescription {

    /**
     * @return translated description
     */
    public function getShortDescription($lang)
    {
        if (isset($this->short_desc_tr_id) && !empty($this->short_desc_tr_id) && $this->short_desc_tr_id != null)
        {
            $tr = Translation::find($this->short_desc_tr_id)->toArray();
        }
        else
        {
            $tr = 'აღწერა';
        }

        return $tr["value_$lang"];
    }

    /**
     * Get raw of translated descriptions
     *
     * @return array
     */
    public function getShortDescriptions()
    {
        if (isset($this->short_desc_tr_id) && !empty($this->short_desc_tr_id) && $this->short_desc_tr_id != null)
        {
            $tr = Translation::find($this->short_desc_tr_id)->toArray();
        }
        else
        {
            $tr = array(
                'value_geo' => 'მოკლე აღწერა',
                'value_eng' => 'short description',
                'value_rus' => 'short description'
            );
        }

        return $tr;
    }


}
