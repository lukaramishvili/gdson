<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TranslatedTitle as TranslatedTitle;
use Illuminate\Support\Facades\URL;

class Genre extends Model {

	use TranslatedTitle;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'genres';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title_tr_id', 'parent_id', 'alias'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

    /**
     * Get genre by alias
     */
    public static function byAlias($alias)
    {
        return self::where('alias', $alias)->first();
    }

    /**
     * Get the parent genre (if any)
     */
    public function parent()
    {
        return Genre::find($this->parent_id);
    }

    /**
     * Get shows (if any)
     */
    public function shows()
    {
        return $this->belongsToMany('App\Show');
    }


    /**
     * Users subscribed to this genre
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Get genre page url
     */
    public function getUrl()
    {
        return URL::to('/genres/view/' . ($this->alias) );
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
