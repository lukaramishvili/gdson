<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;

class Post extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use TranslatedShortDescription;
    use Search;
    use Publishable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'desc_tr_id', 'type', 'visibility', 'short_desc_tr_id', 'video_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get image url
     */
    public function getImageUrl()
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/post/' . ($this->id) . '/cover.jpg');
    }

    /**
     * Get image path
     */
    public function getImagePath()
    {
        return public_path() . '/uploads/post/' . ($this->id) . '/cover.jpg';
    }

    /**
     * which object this post belongs to
     */
    public function getTarget(){
        $whose = null;
        if($this->episodes->count()){ $whose = $this->episodes->first(); }
        if($this->seasons->count()){ $whose = $this->seasons->first(); }
        if($this->shows->count()){ $whose = $this->shows->first(); }
        return $whose;
    }

    /**
     * either directly, or via seasons or episodes, find out to which show this post belongs to
     */
    public function getTargetShow(){
        $show = null;
        if($this->episodes->count()){ $show = $this->episodes->first()->season->show; }
        if($this->seasons->count()){ $show = $this->seasons->first()->show; }
        if($this->shows->count()){ $show = $this->shows->first(); }
        return $show;
    }

    /**
     * Checks whether the post has video uploaded or not
     */
    public function hasVideo()
    {
        if($this->video_id > 0){
            $s3 = App::make('aws')->get('s3');
            $object = $s3->getListObjectsIterator(array(
                'Bucket' => 'gdsonvideos1',
                'Prefix' => 'videos/' . $this->video_id . '/'
            ))->toArray();
            //there should be a minimum of 3 videos: 1920x1080, 1280x720 and 858x480
            //note that 854x480 is the correct resolution for 480p
            return count($object) >= 3;
        } else {
            return false;
        }
    }

    /**
     * Get post by video id
     */
    public static function byVideoId($video_id){
        $post = null;
        if(isset($video_id) && is_numeric($video_id)){
            $post = Post::where('video_id', $video_id)->get()->first();
        }
        return $post;
    }

    /**
     * Get the object it belongs to
     */
    public function target()
    {
        return $this->getTarget();
    }

    /**
     * Get shows
     */
    public function shows()
    {
        return $this->belongsToMany('App\Show')->withTimestamps();
    }

    /**
     * Get seasons
     */
    public function seasons()
    {
        return $this->belongsToMany('App\Season')->withTimestamps();
    }

    /**
     * Get episodes
     */
    public function episodes()
    {
        return $this->belongsToMany('App\Episode')->withTimestamps();
    }

    /**
     * Get episodes
     */
    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
