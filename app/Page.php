<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use Search;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'desc_tr_id', 'alias'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
