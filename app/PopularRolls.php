<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularRolls extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'popularrolls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['server_id', 'role'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
