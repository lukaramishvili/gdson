<?php namespace App;

trait Directable {

	/**
	 * @return the directable movie/etc directors
     *
     * assume profession_id = 1
	 */
	public function getDirectors()
	{
		return Crewmember::where('profession_id', '=', 1)
                         ->where('where_type', strtolower(get_class($this)))
                         ->where('where_id', '=', $this->id);
	}

	/**
	 * @return the directable movie/etc producers
     *
     * assume profession_id = 2
	 */
	public function getProducers()
	{
        return Crewmember::where('profession_id', '=', 2)
                         ->where('where_type', strtolower(get_class($this)))
                         ->where('where_id', '=', $this->id);
	}

	/**
	 * @return the directable movie/etc writers
     *
     * assume profession_id = 3
	 */
	public function getWriters()
	{
        return Crewmember::where('profession_id', '=', 3)
                         ->where('where_type', strtolower(get_class($this)))
                         ->where('where_id', '=', $this->id);
	}

	/**
	 * @return the directable movie/etc actors
     *
     * assume profession_id = 4
	 */
	public function getActors()
	{
        return Crewmember::where('profession_id', '=', 4)
                         ->where('where_type', strtolower(get_class($this)))
                         ->where('where_id', '=', $this->id);
	}

}
