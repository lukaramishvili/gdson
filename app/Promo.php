<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model {

    protected $table = 'promos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['whose_type', 'whose_id', 'order'];

    /**
     * Get the object {show, season, episode} it belogns to
     */
    public function whose()
    {
        return $this->belongsTo('App\\' . (ucwords(strtolower($this->whose_type))), 'whose_id');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
