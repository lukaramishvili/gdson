<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class Season extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use TranslatedShortDescription;
    use Directable;
    use Search;
    use Publishable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seasons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['show_id', 'season_number', 'series_count', 'title_tr_id', 'desc_tr_id', 'visibility', 'release_date', 'publish_date', 'short_desc_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get episode by number
     */
    public function getEpisode($episode_number)
    {
        return $this->episodes()->where('episode_number', $episode_number)
                             ->first();
    }

    /**
     * get a string like "SHOW NAME, SEASON
     */
    public function getFullTitle($lang){
        return $this->show->getTitle("geo").", სეზონი ".$this->season_number;
    }

    public function getTitleForShare($lang){
        $share_title = str_replace(",", " ", $this->getFullTitle($lang));
        return $share_title;
    }

    /**
     * Get season url
     */
    public function getUrl($episode_number = "")
    {
        return URL::to('/show/view/' . ($this->show->alias) . '/sezoni-' . ($this->season_number) . '/' . $episode_number );
    }

    /**
     * Get image url
     */
    public function getImageUrl()
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/season/' . ($this->id) . '/cover.jpg');
    }

    /**
     * Get image url
     */
    public function getImageUrlSecond()
    {
        return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/season/' . ($this->id) . '/cover@2x.jpg');
    }

    /**
     * Get image path
     */
    public function getImagePath()
    {
        return public_path() . '/uploads/season/' . ($this->id) . '/cover.jpg';
    }

    /**
     * Get the show it belongs to
     */
    public function show()
    {
        return $this->belongsTo('App\Show');
    }

    /**
     * Get season episodes
     */
    public function episodes()
    {
        return $this->hasMany('App\Episode');
    }

    /**
     * Get season posts
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post')->withTimestamps();
    }

    /**
     * Get previous season
     */
    public function prevSeason()
    {
        return Season::where('show_id', '=', $this->show_id)
                     ->where('season_number', '=', $this->season_number - 1);
    }

    /**
     * Get next season
     */
    public function nextSeason()
    {
        return Season::where('show_id', '=', $this->show_id)
                     ->where('season_number', '=', $this->season_number + 1);
    }

    /**
     * @return season trailers
     */
    public function trailers()
    {
        $trailers = Trailer::where('whose_type', 'season')
                           ->where('whose_id', $this->id)
                           ->get();

        return $trailers;
    }


    public function mainGenre(){
        return $this->show->mainGenre();
    }

    /**
     * Check if season is added in promo
     */
    public function checkPromo()
    {
        $promoCount = Promo::where('whose_id', $this->id)
                           ->where('whose_type', 'season')
                           ->count();

        if ($promoCount > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    public function getCrewmemberTags()
    {
        $professions     = Profession::lists('keyword', 'id');

        $default_persons = [];
        foreach ($professions as $key => $profession)
        {
            if ($profession == 'writer' || $profession == 'director')
            {
                $crews = Crewmember::where('where_type', 'season')
                                   ->where('where_id', $this->id)
                                   ->where('profession_id', $key)
                                   ->get();

                foreach ($crews as $crew)
                {
                    $default_persons[$key][] = $crew;
                }
            }
        }

        return $default_persons;
    }

}
