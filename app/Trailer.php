<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class Trailer extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trailers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['whose_type', 'whose_id', 'video_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get video
     */
    public function video()
    {
        return $this->belongsTo('App\Video');
    }

    /**
     * Get the object {show, season, episode} it belogns to
     */
    public function whose()
    {
        return $this->belongsTo('App\\' . (ucwords(strtolower($this->whose_type))), 'whose_id');
    }

    public function highlight()
    {
        return $this->hasOne('App\Highlight');
    }

    /**
     * Get image url
     */
    public function getImageUrl($lang = "geo")
    {
      return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/' . $this->whose_type . '/' . $this->whose_id . '/video/' . ($this->video_id) . '/' . $lang. '/cover.jpg');
    }

    /**
     * Get image url
     */
    public function getPostImageUrl($lang = "geo")
    {
        return URL::to(Config::get('services.cloudfront.web-distribution-url').Config::get('services.cloudfront.bucket-prefix').'/' . $this->whose_type . '/' . $this->whose_id . '/video/' . ($this->video_id) . '/' . $lang. '/image.jpg');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }
}
