<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Fbpage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fbpages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    /**
     * Get page data
     */
    public function getCategory()
    {
        $app_id            = Config::get('services.facebook.client_id');
        $app_secret        = Config::get('services.facebook.client_secret');
        $app_access_token  = $app_id . '|' . $app_secret;

        $response          = file_get_contents("https://graph.facebook.com/v2.5/$this->fbid/?fields=category&access_token=$app_access_token&limit=1000");

        return json_decode($response);
    }

}
