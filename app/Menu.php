<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    use TranslatedTitle;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id', 'parent_id', 'link', 'order'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function rootLevelItems(){
        return self::where('parent_id', null)->orderBy('order')->get();
    }

    public function children(){
        return self::where('parent_id', $this->id)->orderBy('order')->get();
    }

    public function getUrl(){
        return str_replace(array("\"", "'"), "", $this->link);
    }

}
