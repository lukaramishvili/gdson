<?php namespace App;

trait Search {

    public function search($keyword)
    {
        foreach (explode(' ', $keyword) as $value)
        {
            $title_tr = $desc_tr = $subt_tr = $first_tr = $last_tr = ['value_geo' => '', 'value_eng' => '', 'value_rus' => ''];

            if (isset($this->title_tr_id) && !empty($this->title_tr_id)) {
                $title_tr = $this->getTitles();
            }

            if (isset($this->desc_tr_id) && !empty($this->desc_tr_id)) {
                $desc_tr = $this->getDescriptions();
            }

            if (isset($this->subtitles_tr_id) && !empty($this->subtitles_tr_id)) {
                $subt_tr = $this->getSubtitles();
            }

            if (isset($this->firstname_tr_id) && !empty($this->firstname_tr_id)) {
                $first_tr = $this->getFirstname()->first()->toArray();
            }

            if (isset($this->lastname_tr_id) && !empty($this->lastname_tr_id)) {
                $last_tr = $this->getLastname()->first()->toArray();
            }

            return $this->checkKeyword($value, $title_tr, $desc_tr, $subt_tr, $first_tr, $last_tr);
        }
    }

    private function checkKeyword($keyword, $title_tr, $desc_tr, $subt_tr, $first_tr, $last_tr)
    {
        return preg_match('/' . $keyword . '/', $title_tr['value_geo']) ||
                preg_match('/' . $keyword . '/', $title_tr['value_eng']) ||
                preg_match('/' . $keyword . '/', $title_tr['value_rus']) ||
                preg_match('/' . $keyword . '/', $desc_tr['value_geo']) ||
                preg_match('/' . $keyword . '/', $desc_tr['value_eng']) ||
                preg_match('/' . $keyword . '/', $desc_tr['value_rus']) ||
                preg_match('/' . $keyword . '/', $subt_tr['value_geo']) ||
                preg_match('/' . $keyword . '/', $subt_tr['value_eng']) ||
                preg_match('/' . $keyword . '/', $subt_tr['value_rus']) ||
                preg_match('/' . $keyword . '/', $first_tr['value_geo']) ||
                preg_match('/' . $keyword . '/', $first_tr['value_eng']) ||
                preg_match('/' . $keyword . '/', $first_tr['value_rus']) ||
                preg_match('/' . $keyword . '/', $last_tr['value_geo']) ||
                preg_match('/' . $keyword . '/', $last_tr['value_eng']) ||
                preg_match('/' . $keyword . '/', $last_tr['value_rus']);
    }

}
