<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularEpisode extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'popularepisodes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['episode_id', 'viewcount'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
