<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

    use TranslatedTitle;
    use TranslatedDescription;
    use Search;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'persons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname_tr_id', 'lastname_tr_id', 'desc_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get person first name
     */
    public function getFirstname()
    {
        return $this->belongsTo('App\Translation', 'firstname_tr_id', 'id');
    }

    /**
     * Get person last name
     */
    public function getLastname()
    {
        return $this->belongsTo('App\Translation', 'lastname_tr_id', 'id');
    }

    /**
     * Get person first name
     */
    public function firstname()
    {
        return $this->belongsTo('App\Translation', 'firstname_tr_id', 'id');
    }

    /**
     * Get person last name
     */
    public function lastname()
    {
        return $this->belongsTo('App\Translation', 'lastname_tr_id', 'id');
    }

    /**
     * Get person's professions
     */
    public function professions()
    {
        return $this->belongsToMany('App\Profession')->withTimestamps();
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
