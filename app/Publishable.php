<?php namespace App;

use Auth;

trait Publishable {

    /**
     * Get published status
     */
    public function isPublished()
    {
        /*if(Auth::check() && Auth::user()->hasRole('Admin')){
            return true;
        }*/
        $publish_time = (is_null($this->publish_date) || "" == $this->publish_date)
        ? 0
        : Helper::strtotime_geo($this->publish_date);
        return $this->visibility > 0
            && time() > $publish_time;
    }

}
