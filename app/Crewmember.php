<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Crewmember extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'crewmembers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['person_id', 'where_type', 'where_id', 'profession_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the person
     */
    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    /**
     * Get the profession
     */
    public function profession()
    {
        return $this->belongsTo('App\Profession');
    }

    /**
     * Get the crewmember actings
     */
    public function actings()
    {
        return $this->belongsToMany('App\Character', 'actings', 'crewmember_id', 'character_id');
    }

    /**
     * Get the object(show, season, episode) where he/she takes part
     */
    public function inWhat()
    {
        return $this->belongsTo('App\\' . (ucwords(strtolower($this->where_type))), 'where_id');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
