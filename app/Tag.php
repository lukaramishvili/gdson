<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'person_id', 'profession_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get tag's post
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    /**
     * Get tag's person
     */
    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    /**
     * Get tag's profession
     */
    public function profession()
    {
        return $this->belongsTo('App\Profession');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
