<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model {

    use TranslatedTitle;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['keyword', 'title_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get persons with the profession
     */
    public function persons()
    {
        return $this->belongsToMany('App\Persons');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

    /**
     * Quickly create a profession object
     * example: Profession::quickNew(array('keyword'=>'journalist','name_geo'=>"ჟურნალისტი"))
     * also: Profession::quickNew(array('keyword'=>'anchor','name_geo'=>"წამყვანი"))
     */
    public static function quickNew($values){
        if(!isset($values['keyword'], $values['name_geo'])){
            throw new Exception('incomplete parameters to Profession::quickNew');
        }
        $keyword = $values['keyword'];
        $name_geo = $values['name_geo'];
        $name_eng = isset($values['name_eng']) ? $values['name_eng'] : $name_geo;
        $name_ru = isset($values['name_ru']) ? $values['name_ru'] : $name_geo;
        $profession_title = Translation::create(array("value_geo"=>$name_geo,"value_eng"=>$name_eng,"value_ru"=>$name_ru));
        $new = Profession::create(array('keyword'=>$keyword,'title_tr_id'=>$profession_title->id));
        return $new;
    }

}
