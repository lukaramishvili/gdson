<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'translations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['value_geo', 'value_eng', 'value_rus'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

    /**
     * Get the translation for passed language
     */
    public function getValue($lang)
    {
        $tr = $this->toArray();
        return $tr["value_$lang"];
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
