<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoWatch extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_watchs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['video_id', 'lang', 'quality'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Override
     */
    public function video(){
        return $this->hasOne('App\Video');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
