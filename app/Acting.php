<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Acting extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the crewmember
     */
    public function crewmember()
    {
        return $this->belongsToMany('App\Crewmember', 'actings', 'character_id', 'crewmember_id');
    }

    /**
     * Get the character
     */
    public function character()
    {
        return $this->belongsTo('App\Character');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
