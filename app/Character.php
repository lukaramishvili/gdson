<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model {

    use TranslatedTitle;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'characters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title_tr_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get character shows
     */
    public function shows()
    {
        return $this->belongsToMany('App\Show')->withTimestamps();
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
