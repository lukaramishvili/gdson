<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Highlight extends Model {

    protected $table = 'highlights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['trailer_id'];

    /**
     * Override
     */
    public function trailer(){
        return $this->belongsTo('App\Trailer');
    }

    /**
     * Override
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }

}
