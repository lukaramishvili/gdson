<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHighlightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('highlights', function($table)
        {
            $table->dropForeign(['video_id']);
            $table->dropColumn('video_id');
            $table->integer('trailer_id')->unsigned();
            $table->foreign('trailer_id')->references('id')->on('trailers')
                  ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('highlights', function ($table) {
            $table->dropForeign(['trailer_id']);
            $table->dropColumn('trailer_id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onDelete('cascade');
        });
	}

}
