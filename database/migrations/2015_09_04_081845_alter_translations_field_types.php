<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTranslationsFieldTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_geo TEXT');
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_eng TEXT');
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_rus TEXT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_geo VARCHAR');
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_eng VARCHAR');
        DB::statement('ALTER TABLE translations MODIFY COLUMN value_rus VARCHAR');
	}

}
