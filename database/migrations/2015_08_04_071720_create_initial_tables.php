<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('translations', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('value_geo')->nullable();
            $table->string('value_eng')->nullable();
            $table->string('value_rus')->nullable();
			$table->timestamps();
		});
		Schema::create('genres', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('genres');
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
		Schema::create('shows', function(Blueprint $table)
		{
			$table->increments('id');
            $table->datetime('release_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->string('status')->nullable();
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
        Schema::create('genre_show', function(Blueprint $table)
        {
            $table->integer('genre_id')->unsigned()->nullable();
            $table->foreign('genre_id')->references('id')->on('genres');
            $table->integer('show_id')->unsigned()->nullable();
            $table->foreign('show_id')->references('id')->on('shows');
            $table->timestamps();
        });
		Schema::create('seasons', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows')
                  ->onDelete('cascade');
            $table->integer('season_number')->unsigned();
            $table->integer('series_count')->unsigned();
            $table->datetime('release_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
        Schema::create('videos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('subtitles_tr_id')->unsigned();
            $table->foreign('subtitles_tr_id')->references('id')->on('translations');
            $table->integer('view_count')->unsigned()->default(0);
            $table->timestamps();
        });
        Schema::create('video_views', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->integer('view_count_geo')->unsigned()->default(0);
            $table->integer('view_count_eng')->unsigned()->default(0);
            $table->integer('view_count_rus')->unsigned()->default(0);
            $table->timestamps();
        });
		Schema::create('episodes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons')
                  ->onDelete('cascade');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->integer('episode_number')->unsigned();
            $table->datetime('release_date')->nullable();
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('type')->nullable();
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
        Schema::create('post_show', function(Blueprint $table)
        {
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')
                  ->onDelete('cascade');
            $table->integer('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows')
                  ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('post_season', function(Blueprint $table)
        {
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade');
            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons')
                ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('episode_post', function(Blueprint $table)
        {
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade');
            $table->integer('episode_id')->unsigned();
            $table->foreign('episode_id')->references('id')->on('episodes')
                ->onDelete('cascade');
            $table->timestamps();
        });
		Schema::create('professions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('keyword');
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
		Schema::create('persons', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('firstname_tr_id')->unsigned();
            $table->foreign('firstname_tr_id')->references('id')->on('translations');
            $table->integer('lastname_tr_id')->unsigned();
            $table->foreign('lastname_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
		Schema::create('characters', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
			$table->timestamps();
		});
		Schema::create('character_show', function(Blueprint $table)
		{
            $table->integer('character_id')->unsigned();
            $table->foreign('character_id')->references('id')->on('characters')
                  ->onDelete('cascade');
            $table->integer('show_id')->unsigned();
            $table->foreign('show_id')->references('id')->on('shows')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('person_profession', function(Blueprint $table)
		{
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons')
                  ->onDelete('cascade');
            $table->integer('profession_id')->unsigned();
            $table->foreign('profession_id')->references('id')->on('professions')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('fbpages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fbid');
			$table->timestamps();
		});
		Schema::create('fbpage_user', function(Blueprint $table)
		{
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
            $table->integer('fbpage_id')->unsigned();
            $table->foreign('fbpage_id')->references('id')->on('fbpages')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('fbfriend_user', function(Blueprint $table)
		{
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
			$table->string('friend_fb_id');
			$table->timestamps();
		});
		Schema::create('crewmembers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons')
                  ->onDelete('cascade');
            $table->string('where_type')->nullable();
            $table->integer('where_id')->nullable();
            $table->integer('profession_id')->unsigned();
            $table->foreign('profession_id')->references('id')->on('professions')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('actings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('crewmember_id')->unsigned();
            $table->foreign('crewmember_id')->references('id')->on('crewmembers')
                  ->onDelete('cascade');
            $table->integer('character_id')->unsigned();
            $table->foreign('character_id')->references('id')->on('characters')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('tags', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')
                  ->onDelete('cascade');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons')
                  ->onDelete('cascade');
            $table->integer('profession_id')->unsigned();
            $table->foreign('profession_id')->references('id')->on('professions')
                  ->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('trailers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('whose_type')->nullable();
            $table->integer('whose_id')->nullable();
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onDelete('cascade');
			$table->timestamps();
		});
        Schema::create('highlights', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('monthly_views', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('playlists', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
            $table->string('is_public');
            $table->timestamps();
        });
        Schema::create('playlist_video', function(Blueprint $table)
        {
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onDelete('cascade');
            $table->integer('playlist_id')->unsigned();
            $table->foreign('playlist_id')->references('id')->on('playlists')
                  ->onDelete('cascade');
            $table->string('is_public');
            $table->timestamps();
        });
        Schema::create('faqs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('translations');
		Schema::drop('genres');
		Schema::drop('shows');
		Schema::drop('genre_show');
		Schema::drop('seasons');
		Schema::drop('episodes');
		Schema::drop('posts');
		Schema::drop('post_show');
		Schema::drop('post_season');
		Schema::drop('episode_post');
		Schema::drop('professions');
		Schema::drop('persons');
		Schema::drop('characters');
		Schema::drop('character_show');
		Schema::drop('person_profession');
		Schema::drop('fbpages');
		Schema::drop('fbpage_user');
		Schema::drop('fbfriend_user');
		Schema::drop('crewmembers');
		Schema::drop('actings');
		Schema::drop('tags');
		Schema::drop('videos');
		Schema::drop('video_views');
		Schema::drop('monthly_views');
		Schema::drop('trailers');
		Schema::drop('highlights');
		Schema::drop('playlists');
		Schema::drop('playlist_video');
		Schema::drop('faqs');
	}

}
