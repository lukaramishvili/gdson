<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublishDate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('shows', function($table)
        {
            $table->datetime('publish_date')->nullable();
        });
        Schema::table('seasons', function($table)
        {
            $table->datetime('publish_date')->nullable();
        });
        Schema::table('episodes', function($table)
        {
            $table->datetime('publish_date')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('shows', function($table)
        {
            $table->dropColumn('publish_date');
        });
        Schema::table('seasons', function($table)
        {
            $table->dropColumn('publish_date');
        });
        Schema::table('episodes', function($table)
        {
            $table->dropColumn('publish_date');
        });
	}

}
