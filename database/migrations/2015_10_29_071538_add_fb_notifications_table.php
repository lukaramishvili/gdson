<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFbNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fbnotifications', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('title_tr_id')->unsigned();
            $table->foreign('title_tr_id')->references('id')->on('translations');
            $table->integer('desc_tr_id')->unsigned();
            $table->foreign('desc_tr_id')->references('id')->on('translations');
            $table->string('whose_type');
            $table->integer('whose_id');
            $table->datetime('publish_date');
            $table->integer('already_sent')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fbnotifications');
	}

}
