<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VideoViews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::drop('monthly_views');
        Schema::drop('video_views');
        Schema::create('video_watchs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->string('lang');
            $table->timestamps();
        });
        Schema::create('video_views', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->string('lang');
            $table->integer('viewcount');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('video_views');
        Schema::drop('video_watchs');
        Schema::create('video_views', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->integer('view_count_geo')->unsigned()->default(0);
            $table->integer('view_count_eng')->unsigned()->default(0);
            $table->integer('view_count_rus')->unsigned()->default(0);
            $table->timestamps();
        });
        Schema::create('monthly_views', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos')
                   ->onDelete('cascade');
            $table->timestamps();
        });
	}

}
