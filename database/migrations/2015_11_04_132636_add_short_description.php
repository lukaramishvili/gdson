<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShortDescription extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('shows', function($table)
        {
            $table->integer('short_desc_tr_id')->nullable()->unsigned();
            $table->foreign('short_desc_tr_id')->references('id')->on('translations');

        });
        Schema::table('seasons', function($table)
        {
            $table->integer('short_desc_tr_id')->nullable()->unsigned();
            $table->foreign('short_desc_tr_id')->references('id')->on('translations');

        });
        Schema::table('episodes', function($table)
        {
            $table->integer('short_desc_tr_id')->nullable()->unsigned();
            $table->foreign('short_desc_tr_id')->references('id')->on('translations');

        });
        Schema::table('posts', function($table)
        {
            $table->integer('short_desc_tr_id')->nullable()->unsigned();
            $table->foreign('short_desc_tr_id')->references('id')->on('translations');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('shows', function ($table)
        {
            $table->dropForeign(['short_desc_tr_id']);
            $table->dropColumn('short_desc_tr_id');
        });
        Schema::table('seasons', function ($table)
        {
            $table->dropForeign(['short_desc_tr_id']);
            $table->dropColumn('short_desc_tr_id');
        });
        Schema::table('episodes', function ($table)
        {
            $table->dropForeign(['short_desc_tr_id']);
            $table->dropColumn('short_desc_tr_id');
        });
        Schema::table('posts', function ($table)
        {
            $table->dropForeign(['short_desc_tr_id']);
            $table->dropColumn('short_desc_tr_id');
        });
	}

}
