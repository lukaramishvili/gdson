<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVisibility extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('shows', function ($table) {
            $table->boolean('visibility');
        });
        Schema::table('seasons', function ($table) {
            $table->boolean('visibility');
        });
        Schema::table('episodes', function ($table) {
            $table->boolean('visibility');
        });
        Schema::table('posts', function ($table) {
            $table->boolean('visibility');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('shows', function ($table) {
            $table->dropColumn('visibility');
        });
        Schema::table('seasons', function ($table) {
            $table->dropColumn('visibility');
        });
        Schema::table('episodes', function ($table) {
            $table->dropColumn('visibility');
        });
        Schema::table('posts', function ($table) {
            $table->dropColumn('visibility');
        });
	}

}
