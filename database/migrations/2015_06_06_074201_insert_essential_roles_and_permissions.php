<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Role;
use App\Permission;

class InsertEssentialRolesAndPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Role::create(array('name'=>'User'));
        Role::create(array('name'=>'Admin'));
		Permission::create(array('name'=>'Login'));
		Permission::create(array('name'=>'EditUsers'));
        Permission::create(array('name'=>'EditContent'));
        Role::byName('User')->permissions()->attach(array(
            Permission::byName('Login')->id
        ));
        Role::byName('Admin')->permissions()->attach(array(
            Permission::byName('Login')->id,
            Permission::byName('EditUsers')->id,
            Permission::byName('EditContent')->id
        ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Role::where('name','=','User')->delete();
		Role::where('name','=','Admin')->delete();
		Permission::where('name','=','Login')->delete();
		Permission::where('name','=','EditUsers')->delete();
        Permission::where('name','=','EditContent')->delete();
	}

}
