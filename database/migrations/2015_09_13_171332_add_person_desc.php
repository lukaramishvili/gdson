<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonDesc extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('persons', function($table)
        {
            $table->integer('desc_tr_id')->unsigned()->default(1);
            $table->foreign('desc_tr_id')->references('id')->on('translations');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('persons', function ($table) {
            $table->dropColumn('desc_tr_id');
        });
	}

}
