<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePlaylistVideoIsPublic extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('playlist_video', function($table)
        {
            $table->dropColumn('is_public');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('playlist_video', function($table)
        {
            $table->string('is_public');
        });
	}

}
