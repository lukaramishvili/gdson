function drawChart() {
    var allWatches   = parseInt($('.all-watches').val()),
        watches      = parseInt($('.watches').val()),
        otherWatches = allWatches - watches;

    // Create our data table.
    data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['დარჩენილი - ' + otherWatches, otherWatches],
        ['არჩეულ დროში - ' + watches, watches]
    ]);

    // Set chart options
    var options = {'title':'სულ ' + allWatches + ' ნახვა',
        'width':600,
        'height':500};

    // Instantiate and draw our chart, passing in some options.
    chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}

$(function() {
    $('[name="genres[]"], [name="professions[]"], [name="post-persons[]"], [name="crew-persons[]"], [name="parent-genre"], [name="order"], [name="episode-number"], [name="menu-parent"], [name="menu-order"]').select2();

    $.fn.editable.defaults.mode = 'inline';

    $('#show-title-geo, #show-title-eng, #show-title-rus').editable({
        url: '/show/editshow',
        params: function(params) {
            var data = {};
            data['pk']    = params.pk;
            data['name']  = params.name;
            data['value'] = params.value;
            data['field'] = 'title_tr_id';

            return data;
        }
    });

    $('#post-title-geo, #post-title-eng, #post-title-rus').editable({
        url: '/show/editpost',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#post-description-geo, #post-description-eng, #post-description-rus').editable({
        url: '/show/editpost',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('.post-description-save').click(function() {
        var description    = $(this)
                             .parents('.post-description')
                             .find('.cke_wysiwyg_frame')
                             .contents()
                             .find('body')
                             .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/show/editpost",
            method: "POST",
            data: {
                pk:    textarea.attr('data-pk'),
                name:  textarea.attr('data-name'),
                value: description,
                field: 'desc_tr_id'
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('.page-description-save').click(function() {
        var description    = $(this)
                            .parents('.page-description')
                            .find('.cke_wysiwyg_frame')
                            .contents()
                            .find('body')
                            .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/page/edit",
            method: "POST",
            data: {
                pk:    textarea.attr('data-pk'),
                name:  textarea.attr('data-name'),
                value: description,
                field: 'desc_tr_id'
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('#show-description-geo, #show-description-eng, #show-description-rus').editable({
        url: '/show/editshow',
        params: function(params) {
            var data = {};
            data['pk']    = params.pk;
            data['name']  = params.name;
            data['value'] = params.value;
            data['field'] = 'desc_tr_id';

            return data;
        }
    });

    $('.show-description-save').click(function() {
        var description    = $(this)
                             .parents('.show-description')
                             .find('.cke_wysiwyg_frame')
                             .contents()
                             .find('body')
                             .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/show/editshow",
            method: "POST",
            data: {
                pk:    textarea.attr('data-pk'),
                name:  textarea.attr('data-name'),
                value: description,
                field: 'desc_tr_id'
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('.short-description-save').click(function() {
        var description    = $(this)
                            .parents('.short-description')
                            .find('.cke_wysiwyg_frame')
                            .contents()
                            .find('body')
                            .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/show/editshortdesc",
            method: "POST",
            data: {
                whose:    textarea.attr('data-whose'),
                whose_id: textarea.attr('data-whose-id'),
                value:    description,
                lang:     textarea.attr('data-lang')
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('#show-alias').editable({
        url: '/show/editalias',
        success: function(response) {
            if (response == 'exists') {
                alert('ასეთი ალიასი უკვე არსებობს');
            }
        }
    });

    $('#episode-alias').editable({
        url: '/show/editepisodealias',
        success: function(response) {
            if (response == 'exists') {
                alert('ასეთი ალიასი უკვე არსებობს');
            }
        }
    });

    $('.show-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelshow",
            method: "POST",
            data: {show_id: $('.show-id').val()}
        }).done(function(data) {
            window.location = '/show/shows/';
        });
    });

    $('.video-cancel').click(function(e) {
        if(!confirm('Delete ?')) {
            e.preventDefault();
        }
    });

    $('.season-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelseason",
            method: "POST",
            data: {season_id: $('.season-id').val()}
        }).done(function(data) {
            window.location = '/show/seasons/' + $('.show-alias').val();
        });
    });

    $('.episode-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelepisode",
            method: "POST",
            data: {episode_id: $('.episode-id').val()}
        }).done(function(data) {
            window.location = '/show/seasons/' + $('.show-alias').val();
        });
    });

    $('.show-post-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelpost",
            method: "POST",
            data: {post_id: $('.post-id').val(), target: 'show', target_id: $('.show-id').val()}
        }).done(function(data) {
            window.location = '/show/seasons/' + $('.show-alias').val();
        });
    });

    $('.season-post-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelpost",
            method: "POST",
            data: {post_id: $('.post-id').val(), target: 'season', target_id: $('.season-id').val()}
        }).done(function(data) {
            window.location = '/show/seasons/' + $('.show-alias').val();
        });
    });

    $('.episode-post-cancel').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/show/cancelpost",
            method: "POST",
            data: {post_id: $('.post-id').val(), target: 'episode', target_id: $('.episode-id').val()}
        }).done(function(data) {
            window.location = '/show/editepisode/' + $('.show-alias').val() + '/' + $('.season-number').val() + '/' + $('.episode-number').val();
        });
    });

    $('#season-title-geo, #season-title-eng, #season-title-rus').editable({
        url: '/show/editseason',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#season-description-geo, #season-description-eng, #season-description-rus').editable({
        url: '/show/editseason',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('.season-description-save').click(function() {
        var description    = $(this)
                            .parents('.season-description')
                            .find('.cke_wysiwyg_frame')
                            .contents()
                            .find('body')
                            .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/show/editseason",
            method: "POST",
            data: {
                pk:    textarea.attr('data-pk'),
                name:  textarea.attr('data-name'),
                value: description,
                field: 'desc_tr_id'
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('#episode-title-geo, #episode-title-eng, #episode-title-rus').editable({
        url: '/show/editepisode',
        params: function(params) {
            var data = {};
            data['pk']    = params.pk;
            data['name']  = params.name;
            data['value'] = params.value;
            data['field'] = 'title_tr_id';

            return data;
        }
    });

    $('#episode-description-geo, #episode-description-eng, #episode-description-rus').editable({
        url: '/show/editepisode',
        params: function(params) {
            var data = {};
            data['pk']    = params.pk;
            data['name']  = params.name;
            data['value'] = params.value;
            data['field'] = 'desc_tr_id';

            return data;
        }
    });

    $('.episode-description-save').click(function() {
        var description    = $(this)
                .parents('.episode-description')
                .find('.cke_wysiwyg_frame')
                .contents()
                .find('body')
                .html()
            , textareaName = $(this).attr('data-textarea')
            , textarea     = $('[name="' + textareaName + '"]');

        $.ajax({
            url: "/show/editepisode",
            method: "POST",
            data: {
                pk:    textarea.attr('data-pk'),
                name:  textarea.attr('data-name'),
                value: description,
                field: 'desc_tr_id'
            }
        }).done(function(data) {
            if (data == 'success') {
                alert('saved');
            } else {
                alert('failed');
            }
        });
    });

    $('.genre-geo, .genre-eng, .genre-rus, .genre-alias').editable({
        url: '/genres/edit'
    });

    var deleteGenre  = function(selector) {
        $(selector).click(function(e) {
           if (!confirm('Delete ?')) {
               e.preventDefault();
           }
        });
    };

    var makeEditable = function(selector) {
        $(selector).each(function(i, el) {
            $(el).editable({
                url: '/genres/edit',
                success: function(response) {
                    var id = JSON.parse(response)['id'];
                    if ($(el).hasClass('add-genre-geo') || $(el).hasClass('add-genre-eng') || $(el).hasClass('add-genre-rus') || $(el).hasClass('add-alias')) {
                        var table  = $('.genres-table')
                            , html = '<tr>' +
                                        '<td class="col-md-2 seasons-episode-title">' +
                                            '<a href="javascript:;" data-type="text" data-pk=0 data-name="geo" class="editable editable-click add-genre-geo" style="display: inline;">' +
                                            '</a>' +
                                        '</td>' +
                                        '<td class="col-md-2 seasons-episode-title">' +
                                            '<a href="javascript:;" data-type="text" data-pk=0 data-name="eng" class="editable editable-click add-genre-eng" style="display: inline;">' +
                                            '</a>' +
                                        '</td>' +
                                        '<td class="col-md-2 seasons-episode-title">' +
                                            '<a href="javascript:;" data-type="text" data-pk=0 data-name="rus" class="editable editable-click add-genre-rus" style="display: inline;">' +
                                            '</a>' +
                                        '</td>' +
                                        '<td class="col-md-2 seasons-episode-title">' +
                                            '<a href="javascript:;" data-type="text" data-pk=0 data-name="alias" class="editable editable-click add-alias" style="display: inline;">' +
                                            '</a>' +
                                        '</td>' +
                                        '<td class="col-md-2 seasons-episode-title">' +
                                            '&nbsp;' +
                                        '</td>' +
                                        '<td class="col-md-2">' +
                                            '&nbsp;' +
                                        '</td>' +
                                    '</tr>';

                        var editables       = $(el).parents('tr').find('a')
                            , deleteGEnreTd = $(el).parents('tr').find('td').eq(5);

                        deleteGEnreTd.html(
                            '<a href="/genres/delete/' + id + '" class="btn btn-circle btn-default delete-genre">' +
                                '<i class="fa fa-remove"></i> Delete </a>'
                        );

                        deleteGenre('.delete-genre');

                        $(editables).each(function(index, element) {
                            if ($(element).hasClass('add-genre-geo')) {
                                $(element).removeClass('add-genre-geo').addClass('genre-geo').attr('data-pk', id);
                            } else if ($(element).hasClass('add-genre-eng')) {
                                $(element).removeClass('add-genre-eng').addClass('genre-eng').attr('data-pk', id);
                            } else if ($(element).hasClass('add-genre-rus')) {
                                $(element).removeClass('add-genre-rus').addClass('genre-rus').attr('data-pk', id);
                            } else if ($(element).hasClass('add-alias')) {
                                $(element).removeClass('add-alias').addClass('genre-alias').attr('data-pk', id);
                            }
                        });

                        table.find('tbody').append(html);
                        makeEditable('.add-genre-geo');
                        makeEditable('.add-genre-eng');
                        makeEditable('.add-genre-rus');
                        makeEditable('.add-alias');
                    }
                },
                params: function(params) {
                    params.pk = $(el).attr('data-pk');

                    return params;
                }
            });
        })
    };

    makeEditable('.add-genre-geo');
    makeEditable('.add-genre-eng');
    makeEditable('.add-genre-rus');
    makeEditable('.add-alias');

    deleteGenre('.delete-genre');

    $('.show-genres').change(function() {
        $.ajax({
            url: "/show/addgenres",
            method: "POST",
            data: {show_id: $('.show-id').val(), genres: $(this).val()}
        }).done(function(data) {
            console.log(data);
        });
    });

    $('.person-professions').change(function() {
        $.ajax({
            url: "/person/editprofession",
            method: "POST",
            data: {person_id: $('.person-id').val(), professions: $(this).val(), is_main: 1}
        });
    });

    $('.post-persons').change(function() {
        $.ajax({
            url: "/show/editpostprofession",
            method: "POST",
            data: {post_id: $('.post-id').val(), persons: $(this).val(), profession_id: $(this).attr('data-profession-id')}
        });
    });

    $('.crew-persons').change(function() {
        var data = {
            target: $(this).attr('data-target'),
            target_id: $(this).attr('data-target-id'),
            persons: $(this).val(),
            profession_id: $(this).attr('data-profession-id')
        };

        $.ajax({
            url: "/show/editprofession",
            method: "POST",
            data: data
        });
    });

    $('.show-visibility').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/visibility",
            method: "POST",
            data: {show_id: $('.show-id').val(), target: 'show', state: state}
        });
    });

    $('.season-visibility').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/visibility",
            method: "POST",
            data: {season_id: $('.season-id').val(), target: 'season', state: state}
        });
    });

    $('.season-priority').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/priority",
            method: "POST",
            data: {show_id: $('.show-id').val(), priority_type: 'season', priority_id: $('.season-id').val(), state: state}
        });
    });

    $('.episode-visibility').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/visibility",
            method: "POST",
            data: {episode_id: $('.episode-id').val(), target: 'episode', state: state}
        });
    });

    $('.post-visibility').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/visibility",
            method: "POST",
            data: {post_id: $('.post-id').val(), target: 'post', state: state}
        });
    });

    var img      = $('#cover-image')
        , src    = '/uploads/' + $('#cover-target').val() + '/' + $('#cover-target-id').val() + '/cover.jpg'
        , d      = new Date();

    img.attr('src', src + '?' + d.getTime());

    $('#cover').change(function() {
        $('#cover-form').submit();

        var img      = $('#cover-image')
            , src    = '/uploads/' + $('#cover-target').val() + '/' + $('#cover-target-id').val() + '/cover.jpg'
            , d      = new Date()
            , fbUrl  = $('.fb-url').val();

        setTimeout(function() {
            img.attr('src', src + '?' + d.getTime());

            /*if (fbUrl) {
                $.post(
                    'https://graph.facebook.com',
                    {
                        id: fbUrl,
                        scrape: true
                    },
                    function(response) {
                        console.log(response);
                    }
                );
            }*/
        }, 1000);

    });

    $('#cover-second').change(function() {
        $('#cover-form-second').submit();
    });

    $('.remove-cover-img').click(function() {
        if (confirm('Delete ?')) {
            $.ajax({
                url: "/show/removecover",
                method: "POST",
                data: {target: $(this).attr('data-target'), target_id: $(this).attr('data-target-id')}
            }).done(function(data) {
                if (data == 'success') {
                    setTimeout(function() {
                        $('#cover-image').attr('src', '/?' + d.getTime());
                    }, 500);
                }
            });
        }
    });

    $('#slider').change(function() {
        $('#slider-form').submit();

        var img            = $(this).val().split(/(\\|\/)/g).pop()
            , sliderImages = $('.slider-images')
            , src          = '/uploads/post/' + $('.post-id').val() +'/slider/' + img;

        setTimeout(function() {
            sliderImages.append(
                '<div class="row slider-row">' +
                    '<img class="slider-image col-md-4" src="' + src + '">' +
                    '<span class="col-md-1 no-padding glyphicon glyphicon-remove remove-slider-img"' +
                        'data-slider-img="' + src + '"></span>' +
                '</div>'
            );

            removeSliderImg('.remove-slider-img');
        }, 500);
    });

    var removeSliderImg = function(selector) {
        $(selector).click(function() {
            var image   = $(this);

            if (confirm('Delete ?')) {
                $.ajax({
                    url: "/show/removeslider",
                    method: "POST",
                    data: {slider_img: $(this).attr('data-slider-img')}
                }).done(function(data) {
                    if (data == 'success') {
                        setTimeout(function() {
                            image.parents('.slider-row').html('');
                        }, 500);
                    }
                });
            }
        });
    };

    removeSliderImg('.remove-slider-img');

    $('#trailer-geo').change(function() {
        $('#trailer-form-geo').submit();
    });

    $('#trailer-image-geo').change(function() {
        $('#trailer-image-form-geo').submit();
    });

    $('#crop-cover').change(function() {
        $('#trailer-video-crop-cover-form').submit();
    });

    $('#crop-image').change(function() {
        $('#trailer-video-crop-image-form').submit();
    });

    $('#trailer-eng').change(function() {
        $('#trailer-form-eng').submit();

        var img      = $('#trailer-image-eng')
            , src    = '/uploads/' + $('#video-target').val() + '/' + $('#video-target-id').val() +
                '/video/' + $('#video-image-id').val() + '/eng/cover.jpg'
            , d      = new Date();

        setTimeout(function() {
            img.attr('src', src + '?' + d.getTime());
        }, 500);
    });

    $('#trailer-rus').change(function() {
        $('#trailer-form-rus').submit();

        var img      = $('#trailer-image-rus')
            , src    = '/uploads/' + $('#video-target').val() + '/' + $('#video-target-id').val() +
                '/video/' + $('#video-image-id').val() + '/rus/cover.jpg'
            , d      = new Date();

        setTimeout(function() {
            img.attr('src', src + '?' + d.getTime());
        }, 500);
    });

    $('.remove-video-img').click(function() {
        var image = $(this)
            , d   = new Date();

        if (confirm('Delete ?')) {
            $.ajax({
                url: "/show/removevideocover",
                method: "POST",
                data: {
                    target:    $('#video-target').val(),
                    target_id: $('#video-target-id').val(),
                    video_id:  $('#video-image-id').val(),
                    lang:      $(this).attr('data-lang')
                }
            }).done(function(data) {
                if (data == 'success') {
                    setTimeout(function() {
                        image.parents('.show-trailer-image').find('img').attr('src', '/?' + d.getTime());
                    }, 500);
                }
            });
        }
    });

    $('#episode-video-image').change(function() {
        $('#episode-video-image-form').submit();

        var img      = $('#episode-video-img')
            , src    = '/uploads/episode/' + $('.episode-id').val() +
                '/video/' + $('#episode-video-id').val() + '/video.jpg'
            , d      = new Date();

        setTimeout(function() {
            img.attr('src', src + '?' + d.getTime());
        }, 500);
    });

    $('.remove-episode-video-img').click(function() {
        var image = $(this)
            , d   = new Date();

        if (confirm('Delete ?')) {
            $.ajax({
                url: "/show/removeepisodevideoimage",
                method: "POST",
                data: {
                    episode_id: $('.episode-id').val(),
                    video_id:   $('#episode-video-id').val()
                }
            }).done(function(data) {
                if (data == 'success') {
                    setTimeout(function() {
                        image.parents('.episode-video-image').find('img').attr('src', '/?' + d.getTime());
                    }, 500);
                }
            });
        }
    });

    $('#person-image').change(function() {
        $('#person-image-form').submit();

        var img      = $('#person-img')
            , src    = '/uploads/person/' + $('.person-id').val() + '/person.jpg'
            , d      = new Date();

        setTimeout(function() {
            img.attr('src', src + '?' + d.getTime());
        }, 500);
    });

    $('.remove-person-img').click(function() {
        var image = $(this)
            , d   = new Date();

        if (confirm('Delete ?')) {
            $.ajax({
                url: "/person/removepersonimage",
                method: "POST",
                data: {
                    person_id: $('.person-id').val()
                }
            }).done(function(data) {
                if (data == 'success') {
                    setTimeout(function() {
                        image.parents('.person-image-div').find('img').attr('src', '/?' + d.getTime());
                    }, 500);
                }
            });
        }
    });

    $('#video-geo').change(function() {
        $('#video-form-geo').submit();
    });

    $('#video-eng').change(function() {
        $('#video-form-eng').submit();
    });

    $('#video-rus').change(function() {
        $('#video-form-rus').submit();
    });

    $('#person-first-name-geo, #person-first-name-eng, #person-first-name-rus').editable({
        url: '/person/editperson',
        params: function(params) {
            params.field = 'firstname_tr_id';

            return params;
        }
    });

    $('#person-last-name-geo, #person-last-name-eng, #person-last-name-rus').editable({
        url: '/person/editperson',
        params: function(params) {
            params.field = 'lastname_tr_id';

            return params;
        }
    });

    $('#person-description-geo, #person-description-eng, #person-description-rus').editable({
        url: '/person/editperson',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('#faq-title-geo, #faq-title-eng, #faq-title-rus').editable({
        url: '/faq/edit',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#faq-desc-geo, #faq-desc-eng, #faq-desc-rus').editable({
        url: '/faq/edit',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('#trailer-title-geo, #trailer-title-eng, #trailer-title-rus').editable({
        url: '/show/editvideo',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#trailer-desc-geo, #trailer-desc-eng, #trailer-desc-rus').editable({
        url: '/show/editvideo',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('#trailer-subt-geo, #trailer-subt-eng, #trailer-subt-rus').editable({
        url: '/show/editvideo',
        params: function(params) {
            params.field = 'subtitles_tr_id';

            return params;
        }
    });

    $('[name="parent-genre"]').change(function() {
        $.ajax({
            url: "/genres/parent",
            method: "POST",
            data: {genre: $(this).attr('data-genre'), parent: $(this).val()}
        });
    });

    $('.trailer-highlight').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/highlight/trailerhighlight",
            method: "POST",
            data: {video_id: $(this).attr('data-video-id'), state: state}
        });
    });

    $('.profession-geo, .profession-eng, .profession-rus').editable({
        url: '/person/editprof'
    });

    $('.profession-key').editable({
        url: '/person/editprofkey'
    });

    $('.delete-profession').click(function(e) {
        if (!confirm('Delete ?')) {
            e.preventDefault();
        }
    });

    $('#episode-video-title-geo, #episode-video-title-eng, #episode-video-title-rus').editable({
        url: '/show/editepisodevideo',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#episode-video-desc-geo, #episode-video-desc-eng, #episode-video-desc-rus').editable({
        url: '/show/editepisodevideo',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('#episode-video-subt-geo, #episode-video-subt-eng, #episode-video-subt-rus').editable({
        url: '/show/editepisodevideo',
        params: function(params) {
            params.field = 'subtitles_tr_id';

            return params;
        }
    });

    $('#edit-character-geo, #edit-character-eng, #edit-character-rus').editable({
        url: '/show/editcharacter'
    });

    $('.delete-episode, .person-cancel').click(function(e) {
        if (!confirm('Delete ?')) {
            e.preventDefault();
        }
    });

    $('.order').change(function() {
        $.ajax({
            url: "/highlight/order",
            method: "POST",
            data: {highlight_id: $(this).attr('data-highlight-id'), value: $(this).val()}
        });
    });

    $('.promo-order').change(function() {
        $.ajax({
            url: "/promo/order",
            method: "POST",
            data: {promo_id: $(this).attr('data-promo-id'), value: $(this).val()}
        });
    });

    $('.genre-order').change(function() {
        $.ajax({
            url: "/genres/order",
            method: "POST",
            data: {genre_id: $(this).attr('data-genre-id'), value: $(this).val()}
        });
    });

    $('#page-title-geo, #page-title-eng, #page-title-rus').editable({
        url: '/page/edit',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#page-alias').editable({
        url: '/page/alias'
    });

    $('.user-admin').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/user/changerole",
            method: "POST",
            data: {user_id: $(this).attr('data-user-id'), state: state}
        });
    });

    $('#fb-notification').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/show/fbnotification",
            method: "POST",
            data: {whose_id: $(this).attr('data-whose-id'), whose_type: $(this).attr('data-whose-type'), state: state}
        });
    });

    var $datetime       = $('.datetime')
        , $publish_date = $('#publish-date')
        , $release_date = $('#release-date');

    $publish_date.val($publish_date.attr('data-datetime'));
    $release_date.val($release_date.attr('data-datetime'));

    $datetime.on('hide', function(e) {
        $.ajax({
            url: "/show/datetime",
            method: "POST",
            data: {
                target:    $(this).attr('data-target'),
                target_id: $(this).attr('data-target-id'),
                field:     $(this).attr('data-field'),
                value:     $(this).val()
            }
        });
    }).datetimepicker({
        format: "mm/dd/yyyy hh:ii"
    });

    var $fb_notification_publish_date = $('#fb-notification-publish-date');

    $fb_notification_publish_date.val($fb_notification_publish_date.attr('data-datetime'));
    $fb_notification_publish_date.on('hide', function(e) {
        $.ajax({
            url: "/fb/fbnotificationdatetime",
            method: "POST",
            data: {
                id:    $(this).attr('data-notification-id'),
                value: $(this).val()
            }
        });
    }).datetimepicker({
        format: "mm/dd/yyyy hh:ii"
    });

    $('[name="episode-number"]').change(function() {
        $.ajax({
            url: "/show/editepisodenumber",
            method: "POST",
            data: {
                episode_id: $('.episode-id').val(),
                season_id:  $('.season-id').val(),
                value:      $(this).val()
            }
        }).done(function(data) {
            if (data == 'failed') {
                alert('Episode already exists');
            }
        });
    });

    $('.promo-switch').on('switchChange.bootstrapSwitch', function(e, state) {
        $.ajax({
            url: "/promo/changepromo",
            method: "POST",
            data: {whose_type: $(this).attr('data-whose-type'), whose_id: $(this).attr('data-whose-id'), state: state}
        });
    });

    $('#fb-notification-title-geo').editable({
        url: '/fb/editfbnotification',
        params: function(params) {
            params.field = 'title_tr_id';

            return params;
        }
    });

    $('#fb-notification-desc-geo').editable({
        url: '/fb/editfbnotification',
        params: function(params) {
            params.field = 'desc_tr_id';

            return params;
        }
    });

    $('#menu-item-title-geo, #menu-item-link-geo').editable({
        url: '/admin/editmenu',
        params: function(params) {
            params.field = $(this).attr('data-field');

            return params;
        }
    });

    $('.menu-parent').change(function() {
        $.ajax({
            url: "/admin/editmenuparent",
            method: "POST",
            data: {
                menu_id: $('option:selected', this).attr('menu-id'),
                parent_id: $('option:selected', this).attr('parent-id')
            }
        }).done(function() {
            location.reload();
        });;
    });

    $('.menu-order').change(function() {
        $.ajax({
            url: "/admin/editmenuorder",
            method: "POST",
            data: {
                menu_id: $('option:selected', this).attr('menu-id'),
                value: $(this).val()
            }
        });
    });

    $('.delete-menu-item').click(function() {
        if(!confirm('Delete ?')) {
            return;
        }

        $.ajax({
            url: "/admin/deletemenuitem",
            method: "POST",
            data: {menuItemId: $(this).attr('menu-item-id')}
        }).done(function(data) {
            window.location = '/admin/menu';
        });
    });

    $('#statistic-show, #statistic-season, #statistic-episode').select2();

    $('#statistic-show').change(function () {
        var value = $(this).val();

        $.ajax({
            url: "/admin/statisticobjects",
            method: "POST",
            data: { object: 'show', objectId: value }
        }).done(function(data) {
            var statisticSeason = $('#statistic-season');

            statisticSeason.find('option').remove();
            $('#statistic-episode').find('option').remove();
            statisticSeason.append(new Option('არ არის არჩეული', 0));
            $(data).each(function() {
                var id     = $(this)[0].id,
                    title  = $(this)[0].title,
                    option = new Option(title, id);

                statisticSeason.append(option);
            });
        });
    });

    $('#statistic-season').change(function () {
        var value = $(this).val();

        $.ajax({
            url: "/admin/statisticobjects",
            method: "POST",
            data: { object: 'season', objectId: value }
        }).done(function(data) {
            var statisticEpisode = $('#statistic-episode');

            statisticEpisode.find('option').remove();
            statisticEpisode.append(new Option('არ არის არჩეული', 0));
            $(data).each(function() {
                var id     = $(this)[0].id,
                    title  = $(this)[0].title,
                    option = new Option(title, id);

                statisticEpisode.append(option);
            });
        });
    });

    $('.statistic-from-time').datetimepicker({
        format: "mm/dd/yyyy hh:ii"
    }).val($('.statisticFromTime').val());

    $('.statistic-to-time').datetimepicker({
        format: "mm/dd/yyyy hh:ii"
    }).val($('.statisticToTime').val());

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);
});