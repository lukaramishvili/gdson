
function isBrowser(browser){
    return navigator.userAgent.toLowerCase().indexOf(browser) > -1;
}

function isFirefox(){
    return isBrowser('firefox');
}