var elixir = require('laravel-elixir');
var gulp = require("gulp");
var shell = require("gulp-shell");

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	//mix.scss('app.less');
    mix.sass('app.scss');
    mix.copy('resources/assets/img', 'public/img');
    mix.copy('resources/assets/swf', 'public/swf');
    mix.copy('resources/assets/fonts', 'public/fonts');
    mix.copy('resources/assets/cloudfront', 'public/cloudfront');
    mix.copy('resources/assets/favicons/favicon.ico', 'public');
    mix.copy('resources/assets/publish', 'public');
    mix.copy('resources/assets/admin/publish', 'public/admin');
    mix.styles([
	//'../../assets/bower/jScrollPane/style/jquery.jscrollpane.css',
	'../../assets/bower/owl-carousel2/dist/assets/owl.carousel.min.css'
    ], 'public/css/vendor.css');
    mix.scripts([
	'main.js'
    ], 'public/js/main.js');
    mix.scripts([
	'../../assets/bower/jquery/dist/jquery.min.js',
	'../../assets/bower/jquery-mousewheel/jquery.mousewheel.min.js',
	//'../../assets/bower/jScrollPane/script/jquery.jscrollpane.min.js',
	'../../assets/bower/bootstrap/dist/js/bootstrap.js',
	'../../assets/bower/modernizr/modernizr.js',
	'../../assets/bower/swfobject/swfobject/src/swfobject.js',
	'../../assets/bower/share-button/build/share.min.js',
	'../../assets/bower/owl-carousel2/dist/owl.carousel.min.js',
	//'../../assets/js/retina.js',
	//downloaded from http://releases.flowplayer.org/6.0.3/flowplayer.min.js
	'../../assets/js/flowplayer.min.js',
	'../../assets/cloudfront/flowplayer-3.2.13.min.js',
	// d/l from http://flash.flowplayer.org/plugins/javascript/brselect.html
	'../../assets/js/flowplayer.bitrateselect-3.2.11.js'
    ], 'public/js/vendor.js');
});
