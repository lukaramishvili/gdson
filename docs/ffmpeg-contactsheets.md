					
ffmpeg -i 858x480.mp4 -vf fps=1/10 -s 115x65 /www/test/thumb/thumb%03d.jpg

from https://www.binpress.com/tutorial/how-to-generate-video-previews-with-ffmpeg/138

// capture every 250th frame because we encode videos at 25fps, so every ten seconds become 250 frames
// this method was way more resource-intensive than the current way - ffmpeg generates frame .jpg files,
// then ImageMagick's montage generates the final tiled contactsheet .jpg file
ffmpeg -loglevel panic -y -i "858x480.mp4" -frames 1 -q:v 12 -vf "select=not(mod(n\,250)),scale=115:-1,tile=64x64" contactsheet.jpg












