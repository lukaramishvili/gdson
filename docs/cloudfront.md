diagnosing rtmp streams: http://d1k5ny0m6d4zlj.cloudfront.net/diag/CFStreamingDiag.html
enter fields as follows:
* Streaming distribution: s*****.cloudfront.net/cfx/st/ (without rtmp:// , trailing slash required)
* Video filename: mp4:VIDEOFILEPATH_NO_EXTENSION (or VIDEOFILEPATH.flv, depending on the file format. in case of cloudfront, it also worked without "mp4:" prefix)

for diagnostics, http://www.wowza.com/testplayers can also be used, but "rtmp://" prefix before the distribution url and "mp4:" prefix before the video filename required here.


A tutorial that more or less describes how to do what I did to configure cloudfront streaming (I didn't follow this tutorial so steps may vary): http://www.streamingmedia.com/Articles/Editorial/Featured-Articles/How-To-Get-Started-With-Amazon-Cloudfront-Streaming-65769.aspx

JWPlayer configuration for adaptive bitrate streaming: http://www.frankie.bz/blog/developers/adaptive-rtmp-streaming-on-cloudfront-and-jw-player/
also see http://www.tuxkiddos.com/2014/04/dynamic-bit-rate-rtmp-streaming-via.html , which explains how to create a configuration that works generally for any host and supporting player.
See documentation for flowplayer's SMIL support (and plugin download links) here: http://flash.flowplayer.org/plugins/streaming/smil.html
Flowplayer bandwidth detection plugin documentation (and download links): http://flash.flowplayer.org/plugins/streaming/bwcheck.html

HLS (Apple HTTP Live Streaming) CloudFront configuration documentation: http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/on-demand-streaming-hls.html

Flowplayer MPEG-DASH configuration documentationL http://demos.flowplayer.org/api/dash.html




HTML5 streaming error can occur on Chrome: https://flowplayer.org/i/flowplayer/setup:aws-cloudfront-html5-video