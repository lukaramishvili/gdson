
# there are undocumented parameters that you can look up in Config.as (probably all plugins have their own, for controls, see FLOWPLAYER_SRC_DIR/flash/plugins/controls/src/actionscript/org/flowplayer/controls/config/Config.as ).



# unintuitional quirks:
# 1. git clone github/flowplayer/flash-build and */tld in the same dir as "flash" repo
# 2. official flowplayer build cannot recognize your custom build plugins; you should build everything you use yourself. cd into "flash-build" repo's root and `ant build-biz` to build commercial flowplayer
# 3. you should remove plugins/controls/src/flash/SKINNAME/buttons_default.swc and .swf every time you want to recompile the .fla file in the same dir. on a happy note, even the newest Flash IDE can be used.
# 4. ant build-biz only recompiles swf if it detects changes in source text files; otherwise it will only generate js files, so if you change something in .fla, you should trigger a recompile by e.g. adding a space to some source files



# download flex 4.6 sdk and place in /opt, in this example make it /opt/flex_sdk_4.6
# make a new directory ALL_FLOWPLAYERS_DIR and git clone github/flowplayer/flash&flash-build&tld
# cd into ALL_FLOWPLAYERS_DIR/flash
# add "flexdir=/opt/flex_sdk_4.6/" to plugins/controls/build.properties
# create file core/build.properties and paste this inside:
flexdir=flexdir=/opt/flex_sdk_4.6/
controls-dir=../plugins/controls
plugin-classes=${plugins.dir}controls/src/actionscript ${plugins.dir}common/src/actionscript
plugin.buildfiles=../plugins/controls/build.xml
allplugins.buildfiles=../plugins/controls/build.xml
# end of core/build.properties
create file lib/flowplayer.devkit/plugin-build.properties and write into it: "flexdir=/opt/flex_sdk_4.6/"
# cd into ALL_FLOWPLAYERS_DIR/flash-build
# open build.properties and replace flexdir=... line with "flexdir=/opt/flex_sdk_4.6/"
# * also there replace "site.dir=.." with "site.dir=ALL_FLOWPLAYERS_DIR/build-output"
# open plugin-build.properties and replace flexdir=... line with "flexdir=/opt/flex_sdk_4.6/"


# to create your own named custom skin, copy xml tags related to e.g. air in plugins/controls/build.xml and paste in the same way but replace "air" with new name, e.g. "gdson"
# * also copy plugins/controls/build-air.xml to samedir/build-gdson.xml and inside it replace "air" with "gdson"
# * also copy plugins/controls/src/flash/air into plugins/controls/src/flash/gdson
# to make changes to new skin file, cd to ALL_FLOWPLAYERS_DIR/flash/plugins/controls/, rm src/flash/SKINNAME/buttons_default.sw* , then make changes in .fla and publish (ctrl+enter), then ant build-SKINNAME (e.g. build-gdson) in the terminal
# for GDSON: cd /www/test/flowplayer/flash/plugins/controls/ && rm src/flash/gdson/buttons_default.sw*
# NOTE: you will still have to $ ant-build-gdson after this for changes to appear in the swf

# to build the controls and copy to website working directory (then test in incognito):
# cd /www/test/flowplayer/flash/plugins/controls && ant build-gdson && cp /www/test/flowplayer/flash/plugins/controls/build/flowplayer.controls-gdson-3.2.16.swf /projects/on/resources/assets/cloudfront/ && cp /Applications/XAMPP/xamppfiles/htdocs/test/flowplayer/flash/plugins/controls/build/flowplayer.controls-gdson-3.2.16.swf /projects/on/public/cloudfront/

# to build the menu plugin, cd /www/test/flowplayer/flash/plugins/menu && ant && cp /www/test/flowplayer/flash-build/dist/flowplayer.menu-3.2.13.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.menu-3.2.13.swf /projects/gdson/public/cloudfront/


# quickly copy custom built flowplayer files:
# cp /www/test/flowplayer/build-output/content/swf/flowplayer.commercial-3.2.18.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.cluster-3.2.10.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.pseudostreaming-3.2.13.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.bwcheck-3.2.13.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.bitrateselect-3.2.14.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.menu-3.2.13.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.rtmp-3.2.13.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash-build/dist/flowplayer.analytics-3.2.9.swf /projects/gdson/resources/assets/cloudfront/ && cp /www/test/flowplayer/flash/plugins/controls/build/flowplayer.controls-gdson-3.2.16.swf /projects/gdson/resources/assets/cloudfront/

# quickly rebuild core commercial flowplayer:
# cd /www/test/flowplayer/flash-build && ant build-biz && cp /www/test/flowplayer/flash-build/build/flowplayer.commercial-3.2.18.swf /projects/on/resources/assets/cloudfront/flowplayer.commercial-3.2.18.swf && cp /www/test/flowplayer/flash-build/build/flowplayer.commercial-3.2.18.swf /projects/on/public/cloudfront/flowplayer.commercial-3.2.18.swf
